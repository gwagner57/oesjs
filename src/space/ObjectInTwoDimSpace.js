/*******************************************************************************
 * The Object class for 2D space simulations.
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
oes.ObjectInTwoDimSpace = new cLASS({
  Name: "ObjectInTwoDimSpace",
  supertypeName: "oBJECT",
  properties: {
    "pos": {range: Array, initialValue: [0,0], label:"Position"},
    "width": {range: "Decimal", initialValue: 0, label:"Width"},
    "height": {range: "Decimal", initialValue: 0, label:"Height"},
  },
  methods: {}
});