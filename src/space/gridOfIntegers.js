/**
 * @fileOverview Integer Grid library.
 * In an integer grid, each grid cell contains an integer that is used for
 * representing its state. How this state is defined in terms of the available
 * bits is model-specific. An integer grid is implemented as a typed array of
 * 8-bit integers. The grid cell at (x,y) is represented by the array index
 * (y-1)*xMax + x-1.
 *
 * Example expression: x & 15 yields the lower 4 bits of x
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 */
/**
 * Initialize the grid.
 */
oes.space.grid.i.initialize = function (xMax, yMax) {
  sim.space.grid = new Uint8Array( xMax * yMax);  // typed array
}
/**
 * Grid Cell Loop
 */
oes.space.grid.i.forAllCells = function (f) {
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax,
      i=0, j=0;
  for (i=1; i <= yMax; i++) {
    for (j=1; j <= xMax; j++) {
      // call the method with coordinates and cell value as parameters
      f( j, i, sim.space.grid[(i-1)*xMax + j-1]);
    }
  }
};
/**
 * Get the value/content of af grid cell
 */
oes.space.grid.i.getCellValue = function (x,y) {
  var xMax = sim.model.space.xMax;
  return sim.space.grid[(y-1)*xMax + x-1];
};
/**
 * Set the value/content of af grid cell
 */
oes.space.grid.i.setCellValue = function (x,y,z) {
  var xMax = sim.model.space.xMax;
  sim.space.grid[(y-1)*xMax + x-1] = z;
};
/**
 * Test if grid cell is free
 */
oes.space.grid.i.isFreeCell = function (x,y) {
  var xMax = sim.model.space.xMax;
  return sim.space.grid[(y-1)*xMax + x-1] === 0;
};
/**
 * Move a cell's value to a new position
 */
oes.space.grid.i.move = function (pos, newPos) {
  var xMax = sim.model.space.xMax,
      x = pos[0], y = pos[1],
      val = sim.space.grid[(y-1)*xMax + x-1];
  // mark old position in grid as free
  sim.space.grid[(y-1)*xMax + x-1] = 0;
  // assign new position in grid
  x = newPos[0]; y = newPos[1];
  sim.space.grid[(y-1)*xMax + x-1] = val;
};
/**
 * Find a free grid cell either close to given position
 * or close to a random position
 */
oes.space.grid.i.findFreeCell = function (pos) {
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax,
      x = pos ? pos[0] : rand.uniformInt(1,xMax),
      y = pos ? pos[1] : rand.uniformInt(1,yMax),
      c=0, r=0;  // col,row
  // make sure that grid cell is free
  while (sim.space.grid[(y-1)*xMax + x-1] > 0) {
    x = x < xMax ? x+1 : 1;  // choose next cell to the right
    c = c+1;  // count column shifts
    if (c >= xMax-1) {
      // when entire row populated, go one up
      y = y < yMax ? y+1 : 1;
      c = 0;
      r = r+1;  // count row shifts
    }
    if (r >= yMax-1) throw("Attempt to over-populate grid!");
  }
  return [x,y];
};

/**
 * Test if grid cell's bit 0 is set
 */
oes.space.grid.i.isSetBit0 = function (x,y) {
  var xMax = sim.model.space.xMax;
  return sim.space.grid[(y-1)*xMax + x-1] & 1;
};
/**
 * Set grid cell's bit 0
 */
oes.space.grid.i.setBit0 = function (x,y) {
  var xMax = sim.model.space.xMax;
  sim.space.grid[(y-1)*xMax + x-1] |= 1;  // ORing with 00000001
};
/**
 * Unset grid cell's bit 0
 */
oes.space.grid.i.unsetBit0 = function (x,y) {
  var xMax = sim.model.space.xMax;
  sim.space.grid[(y-1)*xMax + x-1] &= ~1;  // ANDing with the inverse of 00000001
};
/**
 * Get the number of neighbor cells with bit 0 set
 */
oes.space.grid.i.getNmrOfNeighborCellsWithBit0 = function (x,y) {
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax,
      N=0, xE, xW, yN, yS;
  // compute N,E,S,W coordinates
  yN = y < yMax ? y+1 : 1;
  xE = x < xMax ? x+1 : 1;
  yS = y > 1 ? y-1 : yMax;
  xW = x > 0 ? x-1 : xMax;
  // get value of neighbor cell North
  N += sim.space.grid[(yN-1)*xMax + x - 1] & 1;
  // get value of neighbor cell North-East
  N += sim.space.grid[(yN-1)*xMax + xE - 1] & 1;
  // get value of neighbor cell East
  N += sim.space.grid[(y-1)*xMax + xE - 1] & 1;
  // get value of neighbor cell South-East
  N += sim.space.grid[(yS-1)*xMax + xE - 1] & 1;
  // get (type) value of neighbor cell South
  N += sim.space.grid[(yS-1)*xMax + x - 1] & 1;
  // get value of neighbor cell South-West
  N += sim.space.grid[(yS-1)*xMax + xW - 1] & 1;
  // get value of neighbor cell West
  N += sim.space.grid[(y-1)*xMax + xW - 1] & 1;
  // get value of neighbor cell North-West
  N += sim.space.grid[(yN-1)*xMax + xW - 1] & 1;
  return N;
};
