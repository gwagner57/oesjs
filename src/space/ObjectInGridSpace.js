/*******************************************************************************
 * The ObjectInGridSpace class for grid space simulations
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 ******************************************************************************/
oes.ObjectInGridSpace = new cLASS({
  Name: "ObjectInGridSpace",
  supertypeName: "oBJECT",
  properties: {
    "pos": {range: Array, initialValue: [0,0], label:"Position"}
  },
  methods: {
    "moveToCell": function (newPos) {
      var xMax = sim.model.space.xMax,
          yMax = sim.model.space.yMax,
          newX = newPos[0], newY = newPos[1],
          oldX = this.pos[0], oldY = this.pos[1];
      if (sim.model.space.geometry === "EUCLIDEAN") {
        if (newX < 1) newX = 1;
        else if (newX > xMax) newX = xMax;
        if (newY < 1) newY = 1;
        else if (newY > yMax) newY = yMax;
      } else {  // TORROIDAL geometry
        if (newX < 1) newX = xMax + newX;
        else if (newX > xMax) newX = newX % xMax;
        if (newY < 1) newY = yMax + newY;
        else if (newY > yMax) newY = newY % yMax;
      }
      // move object from its old to the new position in the grid
      if (sim.model.space.type === "IntegerGrid") {
        sim.space.grid[(newY-1)*xMax + newX-1] = sim.space.grid[(oldY-1)*xMax + oldX-1];
        sim.space.grid[(oldY-1)*xMax + oldX-1] = 0;
      } else {  // ObjectGrid
        sim.space.grid[newX][newY].objects[this.id] = this;
        delete sim.space.grid[oldX][oldY].objects[this.id];
      }
      // update position of object
      this.pos[0] = newX; this.pos[1] = newY;
    },
    "moveToCellIfFree": function (newPos) {
      var xMax = sim.model.space.xMax,
          newX = newPos[0], newY = newPos[1];
      if (sim.model.space.type === "IntegerGrid" &&
          sim.space.grid[(newY-1)*xMax + newX-1] === 0 ||
          sim.model.space.type === "ObjectGrid" &&
          Object.keys( sim.space.grid[newX][newY].objects).length === 0) {
        this.moveToCell( newPos);
        return true;
      } else return false;
    },
    "moveInDirection": function (dir) {
      var newPos = oes.space.grid.getTranslationPosition( this.pos, dir);
      if (!newPos.isEqualTo( this.pos)) this.moveToCellIfFree( newPos);
    },
    "moveInRandomDirection": function () {
      var dir = oes.space.grid.directions[rand.uniformInt(0,7)];
      this.moveInDirection( dir);
    }
  }
});