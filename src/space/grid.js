/**
 * @fileOverview Grid Space (discrete 2-dimensional space)
 * The coordinate origin (1,1) is the bottom-left corner of the grid.
 * A grid space can be implemented as an "integer grid" (an array of integers/bytes)
 * or as an "object grid" (a 2-dimensional array of lists of objects).
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 */
/**
 * Compute the new position after a translation of (x,y) towards dir
 */
oes.space.grid.getTranslationPosition = function (pos, dir) {
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax,
      x = pos[0], y = pos[1];
  if (sim.model.space.geometry === "EUCLIDEAN") {
    if (dir.includes("N")) y = Math.min( y+1, yMax);
    if (dir.includes("E")) x = Math.min( x+1, xMax);
    if (dir.includes("S")) y = Math.max( y-1, 1);
    if (dir.includes("W")) x = Math.max( x-1, 1);
  } else {
    if (dir.includes("N")) y = y < yMax ? y+1 : 1;
    if (dir.includes("E")) x = x < xMax ? x+1 : 1;
    if (dir.includes("S")) y = y > 1 ? y-1 : yMax;
    if (dir.includes("W")) x = x > 1 ? x-1 : xMax;
  }
  return [x,y];
};
