/**
 * @fileOverview Object Grid library
 * In an object grid, grid cells may have properties, including the pre-defined
 * property "objects" containing zero or more object references
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 */

/**
 * Initialize the object grid.
 */
oes.space.grid.o.initialize = function (xMax, yMax) {
  var grid = null, i = 0, j = 0;
  var GridCell = null;
  oes.space.grid.getCell = oes.space.grid.o.getCell;
  oes.space.grid.move = oes.space.grid.o.move;
  sim.space.grid = grid = new Array(xMax);  // 2D array of GridCell objects
  if (typeof sim.model.space.gridCellProperties === 'object') {
    // declare pre-defined grid cell properties "objects" and "pos"
    sim.model.space.gridCellProperties.objects = {
      range: Object, initialValue: {}
    };
    sim.model.space.gridCellProperties.pos = {
      range: Array, label: "Position"
    };
    GridCell = new cLASS({
      Name: "GridCell",
      properties: sim.model.space.gridCellProperties,
      methods: {
        "addObject": function (o) {
          o.pos = this.pos;
          this.objects[String(o.id)] = o;
        },
        "removeObject": function (o) {
          delete this.objects[String(o.id)];
        }
      }
    });
    for (i=0; i < xMax; i++) {
      grid[i] = new Array(yMax);
      for (j=0; j < yMax; j++)
        grid[i][j] = new GridCell({pos: [i+1, yMax-j]});
    }
  }
};
/**
 * Grid Cell Loop
 */
oes.space.grid.o.forAllCells = function (fun) {
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax,
      i=0, j=0, c=null;
  for (i=1; i <= xMax; i++) {
    for (j=1; j <= yMax; j++) {
      c = oes.space.grid.o.getCell(i,j);
      // call the passed function with coordinates and cell as arguments
      fun( i, j, c);
    }
  }
};


/**
 * Move a cell's object to a new position
 */
oes.space.grid.o.move = function (pos, newPos) {
  // TODO: implement management of the objects in the cells lists.
};

/**
 * Get the value/content of af grid cell
 */
oes.space.grid.o.getCell = function (x,y) {
  return sim.space.grid[x-1][sim.model.space.yMax - y];
};
