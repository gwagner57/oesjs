var ObjectInOneDimSpace = new cLASS({
  Name:"ObjectInOneDimSpace",
  supertypeName: "oBJECT",
  properties: {
    "pos": {range: cLASS.Array("Decimal", 2)},
    "vel": {range: cLASS.Array("Decimal", 2)},
    "acc": {range: cLASS.Array("Decimal", 2)}
  },
  methods: {
    "computeNextVelocity": function () {
      return this.vel[0] + this.acc[0] * sim.timeIncrement;
    },
    "computeNextPosition": function () {
      return this.pos[0] + this.vel[0] * sim.timeIncrement;
    }
  }
});
