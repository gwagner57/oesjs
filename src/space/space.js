/**
 * @fileOverview Space library.
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 */
oes.space = {
  grid: {
    directions: ["N","NE","E","SE","S","SW","W","NW"],
    getRandomDirection: function () {return this.directions[rand.uniformInt(0,7)];},
    i: {},  // namespace object for integer grid features
    o: {}   // namespace object for object grid features
  },
  oneDim: {},
  twoDim: {},
  threeDim: {},
  dimensions: {"1D":1, "1D-Grid":1, "IntegerGrid":2, "ObjectGrid":2, "2D":2, "3D":3, "3D-Grid":3}
};

/**
 * Initialize the space object
 */
oes.space.initialize = function () {
  var space = sim.model.space;
  // set TOROIDAL as default geometry
  if (!space.geometry) space.geometry = "TOROIDAL";
  switch (space.type) {
  case "1D":
    break;  // nothing to do
  case "IntegerGrid":
    oes.space.grid.forAllCells = oes.space.grid.i.forAllCells;
    oes.space.grid.i.initialize( space.xMax, space.yMax);
    break;
  case "ObjectGrid":
    oes.space.grid.forAllCells = oes.space.grid.o.forAllCells;
    oes.space.grid.o.initialize( space.xMax, space.yMax);
    break;
  case "2D":
    if (space.overlayGridCellSize) {
      sim.space.overlayGrid.cellSize = space.overlayGridCellSize;
      oes.space.overlayGrid.initialize( space.xMax, space.yMax, sim.space.overlayGrid.cellSize);
    }
    break;
  }
};
