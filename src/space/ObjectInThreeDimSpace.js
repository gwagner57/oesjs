/*******************************************************************************
 * The Object class for pure 3D space simulations.
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
oes.ObjectInThreeDimSpace = new cLASS({
  Name: "ObjectInThreeDimSpace",
  supertypeName: "oBJECT",
  properties: {
    // pos has the format [x, y, z, nextX, nextY, nextZ], allowing for continuous change computations
    "pos": {range: cLASS.Array("Decimal", 2), label:"Position"},
    // width has the format [width, nextWidth], allowing for continuous change computations
    "width": {range: cLASS.Array("Decimal", 2), label:"Width"},
    // width has the format [height, nextHeight], allowing for continuous change computations
    "height": {range: cLASS.Array("Decimal", 2), label:"Height"},
    // velocity has the format [vel, nextVel], allowing for continuous change computations
    // NOTE: this is a composed resulting value of the 3D velocity vector
    "vel": {range: cLASS.Array("Decimal", 2), label:"Velocity"},
    // acceleration has the format [vel, nextVel], allowing for continuous change computations
    // NOTE: this is a composed resulting value of the 3D acceleration vector
    "acc": {range: cLASS.Array("Decimal", 2), label:"Acceleration"}
  },
  methods: {}
});