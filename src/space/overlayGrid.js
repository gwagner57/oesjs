/**
 * @fileOverview Overlay Grid library.
 *  An overlay grid is a logical concept which allows to combine/overlay a continuous space
 *  with a grid space, by dividing it into cells of specified size.
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
 */

oes.space.overlayGrid = {};

oes.space.overlayGrid.initialize = function (xMax, yMax, cellSize) {
  var cellsOnX = 0, cellsOnY = 0;
  cellSize = cellSize || 1;
  // overlay logical grid space
  cellsOnX = parseInt( xMax / cellSize);
  cellsOnY = parseInt( yMax / cellSize);
  oes.space.grid.o.initialize(cellsOnX, cellsOnY);
};

/**
 * @param f
 *   the method that invoked for each cell
 * Grid Cell Loop in case of overlay space
 */
oes.space.overlayGrid.forAllCells = function (f) {
  var overlayGridCellSize = sim.model.space.overlayGridCellSize;
  var xMax = parseInt(sim.model.space.xMax / overlayGridCellSize),
    yMax = parseInt(sim.model.space.yMax / overlayGridCellSize);
  var i=0, j=0;
  for (i=0; i < xMax; i++) {
    for (j=0; j < yMax; j++) {
      // call the method with coordinates and cell as parameters
      f(i+1,j+1,sim.space.grid[i][j]);
    }
  }
};

/**
 * @param x
 *   the x coordinate of the cell
 * @param y
 *   the y coordinate of the cell
 * Get the value/content of af grid cell, in the case of overlay cells
 */
oes.space.overlayGrid.getCell = function (x,y) {
  var gridCellSize = sim.space.overlayGrid.cellSize || 1;
  var yMax = parseInt(sim.model.space.yMax / gridCellSize);
  x = parseInt(x/gridCellSize);
  y = parseInt(y/gridCellSize);
  return sim.space.grid[x][yMax - y - 1];
};