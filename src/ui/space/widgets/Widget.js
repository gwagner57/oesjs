/**
 * Superclass of any Widget.
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
 */
if (typeof wizz === 'undefined') var wizz = {};
wizz.widget = {};
wizz.widget.path = "../../../ui/space/widgets/";
wizz.widget.Widget = new cLASS({
  Name: "Widget",
  properties: {
    "context": {range: Object, label: "Context"},
    // observed object reference
    "objectRef": {range: Object, label: "Object reference"},
    // observed object property
    "objectProperty": {range: Object, label: "Property"},
    // observed object references
    "objectsRef": {range: Array, initialValue: [], label: "Object references"},
    // observed objects property
    "objectsProperty": {range: Array, initialValue: [], label: "Objects property"},
    "pos": {range: Array, initialValue: [/** don't add values here! **/], label:"Position"},
    "width": {range: "Decimal", initialValue: 256, label:"Width"},
    "height": {range: "Decimal", initialValue: 256, label:"Height"},
    // each widget will instantiate this list, corresponding to
    // the images / sprites that needs to be pre-loaded.
    "images": {range: Array, initialValue: [], label: "Images"},
    // each widget will instantiate this list, corresponding to
    // the bitmap fonts that needs to be pre-loaded.
    "bitmapFonts": {range: Array, initialValue: [], label: "Bitmap fonts"},
    // the background color - a visual widget has in most of the cases a background color (default: black)
    "backgroundColor": {range: "NonEmptyString", initialValue: "#000000", label: "Background color"},
    // the text color - used to display all (or some) of the texts shown by the widget (default: white)
    "textColor": {range: "NonEmptyString", initialValue: "#ffffff", label: "Text color"}
  },
  methods: {
    // Initialize the widget.
    // Note: this method should be overridden by each specific widget.
    "init": function () {},
    // Render the widget (graphics, state, etc)
    // Note: this method should be overridden by each specific widget.
    "render": function () {},
    // This method executes after pre-loading all the widget assets.
    // Note: this method should be overridden by each specific widget.
    "doAfterPreload": function () {}
  }
});

// store all image resources for widgets
// NOTE: use unique keys!
wizz.widget.Widget.images = {};
// store all bitmap fonts for widgets
// NOTE: use unique keys!
wizz.widget.Widget.bitmapFonts = {};




/*******  NOTE: the following code is experimental and will change in the future! *****/

wizz.widget.Widget.create = function (typeName, slots) {
  return new wizz.widget[typeName](slots);
};

// Preload all resources: images, bitmap fonts, etc
wizz.widget.Widget.preload = function (context) {
  var images = wizz.widget.Widget.images;
  var bitmapFonts = wizz.widget.Widget.bitmapFonts;
  // create list of images to preload
  Object.keys(images).forEach(function (imgId) {
    console.log("Widget: loading widget image " + "'" + imgId + "' ===> " + images[imgId]);
    context.load.image(imgId, images[imgId]);
  });
  /// create list of bitmap font images to preload
  Object.keys(bitmapFonts).forEach(function (bfKey) {
    console.log("Widget: loading widget bitmap font " + "'" + bfKey +"' ===> " + bitmapFonts[bfKey] + "(.fnt,.png)");
    context.load.bitmapFont(bfKey, bitmapFonts[bfKey] + ".png", bitmapFonts[bfKey] + ".fnt");
  });
};