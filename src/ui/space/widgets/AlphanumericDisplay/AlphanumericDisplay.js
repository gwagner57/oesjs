/**
 * Widget of type Alphanumeric display (1, 2, 4 rows, 8, 16 or 20 columns).
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
 */
wizz.widget.AlphanumericDisplay = new cLASS({
  Name: "AlphanumericDisplay",
  supertypeName: "Widget",
  properties: {
    "images": {range: Object,
      initialValue: {
        "alphanumeric-display-widget-bg_8x1": wizz.widget.path + "AlphanumericDisplay/media/background_8x1.png",
        "alphanumeric-display-widget-bg_8x2": wizz.widget.path + "AlphanumericDisplay/media/background_8x2.png",
        "alphanumeric-display-widget-bg_8x4": wizz.widget.path + "AlphanumericDisplay/media/background_8x4.png",
        "alphanumeric-display-widget-bg_16x1": wizz.widget.path + "AlphanumericDisplay/media/background_16x1.png",
        "alphanumeric-display-widget-bg_16x2": wizz.widget.path + "AlphanumericDisplay/media/background_16x2.png",
        "alphanumeric-display-widget-bg_16x4": wizz.widget.path + "AlphanumericDisplay/media/background_16x4.png",
        "alphanumeric-display-widget-bg_20x1": wizz.widget.path + "AlphanumericDisplay/media/background_20x1.png",
        "alphanumeric-display-widget-bg_20x2": wizz.widget.path + "AlphanumericDisplay/media/background_20x2.png",
        "alphanumeric-display-widget-bg_20x4": wizz.widget.path + "AlphanumericDisplay/media/background_20x4.png"
      }, label: "Images"},
    "spriteGroup": {range: Object, label: "Sprite group"},
    "spriteWidth": {range: "PositiveInteger", initialValue: 512},
    "height": {range: "Decimal", initialValue: 192, label:"Height"},
    "rows": {range: "PositiveInteger", min: 1, max: 4, initialValue: 2, label: "Rows number"},
    "cols": {range: "PositiveInteger", min: 1, max: 20, initialValue: 16, label: "Columns number"},
    // optional label - defaults to object property label if provided
    "label": {range: "String", initialValue: "", label: "Label"},
    // suppress label - do not show the widget label
    "suppressLabel": {range: "Boolean", initialValue: false, label:"Suppress label"},
    // the array of chars (16 chars per line, 2 lines)
    "chars": {range: Array, label: "Chars"},
    "textPosition": {range: Array, initialValue: [], label: "Text positions"},
    // override the default background color, with the standard blue color used by real LCD displays
    "backgroundColor": {range: "NonEmptyString", initialValue: "#111a69", label: "Background color"},
    // use white as the standard color for displaying chars on the display (it looks nice on the blue background)
    "textColor": {range: "NonEmptyString", initialValue: "#ffffff", label: "Text color"}
  },
  methods: {
    "init": function() {
      var bgResIdSuffix = this.cols + "x" + this.rows;
      // only 8x1, 8x2, 8x4, 16x1, 16x2, 16x4, 20x1, 20x2 and 20x4 displays are allowed
      if (typeof this.cols !== "number" || this.cols % 1 !== 0 || [8, 16, 20].indexOf(this.cols) === -1)
        throw "AlphanumericDisplay: the number of columns must be an integer from the set {8, 16, 20}."
          + " Found: " + this.cols;
      if (typeof this.rows !== "number" || this.rows % 1 !== 0 || [1, 2, 4].indexOf(this.rows) === -1)
        throw "AlphanumericDisplay: the number of rows must be an integer from the set {1, 2, 4}."
        + " Found: " + this.rows;
      // load the background image for the correct display type (one of the supported types)
      wizz.widget.Widget.images["alphanumeric-display-widget-bg_" + bgResIdSuffix] =
        wizz.widget.path + "AlphanumericDisplay/media/background_" + bgResIdSuffix + ".png";
    },
    "doAfterPreload": function () {
      var sprite = null, text = null;
      var scale = this.width / this.spriteWidth;
      var bgColorBitmap = this.context.add.bitmapData(this.width, this.height * scale);
      var x = this.pos[0] || this.context.canvas.width / 2,
        y = this.pos[1] || this.context.canvas.height / 2;
      var i = 0, j = 0;

      // create the group of sprites
      this.spriteGroup = this.context.add.group();
      // adjust scale on height
      this.height *= scale;
      // render background color
      bgColorBitmap.context.fillStyle = this.backgroundColor;
      bgColorBitmap.context.fillRect(0, 0, this.width * 1 / scale, this.height * 1 / scale);
      sprite = this.context.add.sprite(x, y, bgColorBitmap);
      sprite.anchor.setTo(0.5, 0.5);
      this.spriteGroup.add(sprite);
      // create widget background
      sprite = this.context.add.sprite(x, y, "alphanumeric-display-widget-bg_"
        + this.cols + "x" + this.rows);
      this.spriteGroup.add(sprite);
      // set anchor to middle of the sprite
      sprite.anchor.setTo(0.5, 0.5);
      // scale to current width
      sprite.scale.setTo(scale, scale);
      // add to group
      this.spriteGroup.add(sprite);
      // add sprites for alphanumeric chars
      this.chars = [];
      for (j = 0; j < 2; j++) {
        this.chars[j] = [];
        for (i = 0; i < 16; i++) {
          sprite = this.context.add.sprite(x - (204 - 25.75 * i) * scale, y + (3 - 42 * j) * scale);
          sprite.width = 22 * scale;
          sprite.height = 32 * scale;
          this.spriteGroup.add(sprite);
          this.chars[j][i] = sprite;
          text = this.context.add.text(0, 0, "", this.createFont(40, 40, 1, this.textColor));
          text.setTextBounds(-4, 1, 40, 40);
          sprite.addChild(text);
        }
      }
      // display label text, if available and not suppressed
      if (this.label && !this.suppressLabel) {
        text = this.context.add.text(x, y + 82 * scale, this.label, this.createFont(12, 10, scale, "#ffffff"));
        text.setTextBounds(-this.width * scale / 2, -8, this.width * scale, 16);
        this.spriteGroup.add(text);
      }
      // render current state
      this.render();

    },
    "createFont": function (size, minSize, scale, textColor) {
      return {
        font: "bold " + Math.max(parseInt(size * scale), minSize) + "px Arial",
        fill: textColor,
        boundsAlignH: "center",
        boundsAlignV: "middle"
      };
    },
    "setText": function (row, col, text) {
      var phaserText = null;
      var n = text && text.length ? text.length : 0, i = 0;
      var textPosX = 0, textPosY = 2 - row;
      if (row > 2 || row < 1) throw "AlphanumericDisplay: row must be 1 or 2!";
      if (col > 16 || col < 1) throw "AlphanumericDisplay: column must be in the range [1, 16]!";
      for ( i = 0; i < n; i++) {
        textPosX = col + i - 1;
        // end of line, can't continue writing text
        // TODO: check if it makes sense to continue on the next line, if next line is available.
        if (textPosX > 15) break;
        phaserText = this.chars[textPosY][textPosX].getChildAt(0);
        phaserText.setText(text.charAt(i));
      }
    },
    "getValue": function (index) {
      var value = null, propDef = null, oRef = null;
      var decimalPlaces = 0;
      // single object, single property
      if (index === undefined){
        propDef = this.objectProperty;
        oRef = this.objectRef;
      } else {
        propDef = this.objectsProperty[index];
        oRef = this.objectsRef[index];
      }
      if (!oRef) throw "AlphaDisplay16x2Widget.getValue: undefined object reference!";
      if (!propDef) throw "AlphaDisplay16x2Widget.getValue: undefined property definition!";
      decimalPlaces = propDef.decimalPlaces;
      value = oRef[propDef.name];
      if (typeof decimalPlaces === "function") decimalPlaces = decimalPlaces(oRef);
      if (typeof value === "number") {
        if (decimalPlaces > 0) value = value.toFixed(decimalPlaces);
        else if (value % 1 !== 0) value = value.toFixed(2);
      }
      if (propDef.label) {
        if (typeof propDef.label === "function") value = propDef.label(oRef);
        else value = propDef.label + ": " + ((value || value === 0) ? value : "N/A");
      }
      if (propDef.unit) {
        if (typeof propDef.unit === "function") value = propDef.unit(oRef);
        else value += propDef.unit;
      }
      return value;
    },
    "render": function () {
      // single object, single porperty
      if (this.objectRef && this.objectProperty)
        this.setText(this.textPosition[0] || 1, this.textPosition[1] || 1,
          String(this.getValue()));
      // multiple objects, each with its own property
      else if (this.objectsRef && Array.isArray(this.objectsRef)
        && this.objectsProperty && Array.isArray(this.objectsProperty)) {
        this.objectsProperty.forEach(function (propDef, index) {
          this.setText(this.textPosition[index][0] || 1, this.textPosition[index][1],
            String(this.getValue(index)));
        }, this);
      }
    }
  }
});