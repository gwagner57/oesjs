/**
 * Widget of type Meter.
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
 */
wizz.widget.Meter = new cLASS({
  Name: "Meter",
  supertypeName: "Widget",
  properties: {
    "value": {range: "Decimal", label: "Value"},
    "divisions": {range: Object, label: "Divisions"},
    "images": {range: Object,
      initialValue: {
        "meter-widget-bg": wizz.widget.path + "Meter/media/background.png",
        "meter-widget-bg-full": wizz.widget.path + "Meter/media/background_no_divisions.png",
        "meter-widget-display": wizz.widget.path + "Meter/media/display.png",
        "meter-widget-needle": wizz.widget.path + "Meter/media/needle.png"
      }, label: "Images"},
    "bitmapFonts": {range: Object,
      initialValue: {
        "meter-widget-digits": wizz.widget.path + "Meter/media/digits"
      }, label: "Bitmap fonts"},
    "spriteGroup": {range: Object, label: "Sprite group"},
    "needleSprite": {range: Object, label: "Needle"},
    "bitmapText": {range: Object, label: "Digital text"},
    "spriteWidth": {range: "PositiveInteger", initialValue: 256},
    "suppressDigitalDisplay": {range: "Boolean", initialValue: false, label: "Hide digital display"},
    // measurement unit - used as label
    "unit": {range: "NonEmptyString", label: "Unit"},
    // optional label - defaults to object property label if provided
    "label": {range: "String", initialValue: "", label: "Label"},
    // minimum value - extracted from the observed property or specified directly
    "minValue": {range: "Decimal", initialValue: 0, label: "Min. value"},
    // maximum value - extracted from the observed property or specified directly
    "maxValue": {range: "Decimal", initialValue: 100, label: "Max. value"},
    // minimum difference between two values (the needle does not move for smaller differences)
    "minDivision": {range: "Decimal"},
    "angle0": {range: "PositiveInteger", initialValue: 135, label: "Needle start angle"},
    "degPerValueUnit": {range: "Decimal", initialValue: 270 / 100, label: "Degrees per value unit"},
    "colors": {range: Object, initialValue: {
        "green": 0x00FF00, //rgb(0, 255, 0)
        "yellow": 0xFFFF00, //rgb(255, 255, 0),
        "red": 0xFF0000, //rgb(204, 0, 0),
        "blue": 0x80FF, //rgb(0, 128, 255),
        "orange": 0xF39C12, //rgb(243, 156, 18)
        "white": 0xECF0F1 // rgb(236, 240, 241)
      }, label: "Division colors"}
  },
  methods: {
    "init": function() {
      // create list of images to preload
      Object.keys(this.images).forEach(function (imgId) {
        wizz.widget.Widget.images[imgId] = this.images[imgId];
      }, this);
      /// create list of bitmpa font images to preload
      Object.keys(this.bitmapFonts).forEach(function (bfKey) {
        wizz.widget.Widget.bitmapFonts[bfKey] =  this.bitmapFonts[bfKey];
      }, this);
    },
    "doAfterPreload": function () {
      var text = null, needleSprite = null, sprite = null;
      var objRef = this.objectRef, objProp = this.objectProperty.name;
      var x = this.pos[0] || this.context.canvas.width / 2,
        y = this.pos[1] || this.context.canvas.height / 2;
      var scale = this.width / this.spriteWidth;

      // create the group of sprites
      this.spriteGroup = this.context.add.group();
      // detect if digital display must be suppressed
      this.suppressDigitalDisplay = this.suppressDigitalDisplay === true;

      // extract some values from object property definition, if available
      if (objRef && objRef.constructor && objRef.constructor.properties
        && objRef.constructor.properties[objProp]) {
        // try to get the label from object property definition
        this.label = this.label || objRef.constructor.properties[objProp].label;
        // try to get min value from object property definition
        this.minValue = objRef.constructor.properties[objProp].min || this.minValue;
        // try to get max value from object property definition
        this.maxValue = objRef.constructor.properties[objProp].max || this.minValue;
        this.minDivision = this.minDivision || (this.maxValue - this.minValue) / 270;
        // recompute the unit per degree based on the new max/min values
        this.degPerValueUnit = 270.0 / Math.abs(this.maxValue - this.minValue);
      } else {
        this.minDivision = this.minDivision || (this.maxValue - this.minValue) / 270;
      }

      // create the divisions color effect
      if (this.divisions) {
        sprite = this.context.add.sprite(x, y);
        Object.keys(this.divisions).forEach(function (colorName) {
          var color = this.colors[colorName];
          var range = this.divisions[colorName];
          // a set of ranges for this color
          if (Array.isArray(range[0]))
            range.forEach(function (r) {
              sprite.addChild(this.createDivision(r, color));
            }, this);
          // only one range for this color
          else  sprite.addChild(this.createDivision(range, color));
        }, this);
        // scale to fit the widget size
        sprite.scale.set(scale, scale);
        // division is added to group
        this.spriteGroup.add(sprite);
      }

      // create widget sprites
      Object.keys(this.images).forEach(function (aKey) {
        var sprite = null;
        if (aKey === "meter-widget-display" && this.suppressDigitalDisplay) return;
        if (aKey === "meter-widget-bg" && !this.divisions) return;
        if (aKey === "meter-widget-bg-full" && this.divisions) return;
        sprite = this.context.add.sprite(x, y, aKey);
        if (aKey === "meter-widget-needle")  needleSprite = sprite;
        else this.spriteGroup.add(sprite);
        // set anchor to middle of the sprite
        sprite.anchor.setTo(0.5, 0.5);
        // scale to current width
        sprite.scale.setTo(scale, scale);
      }, this);
      this.needleSprite = needleSprite;

      // digital display text, if digital display is active.
      if (!this.suppressDigitalDisplay) {
        this.bitmapText = this.context.add.bitmapText(x, y + 67 * scale,
          'meter-widget-digits', String(this.getValue()), 32);
        this.bitmapText.anchor.setTo(0.5, 0.5);
        this.bitmapText.scale.set(scale, scale);
      }
      // display unit text - if one is provided
      if (this.unit) {
        text = this.context.add.text(x, y - 36 * scale, this.unit,
          this.createFont(28, 16, scale)).setTextBounds(-16, -16, 32, 32);
        this.spriteGroup.add(text);
      }
      // display label text, if provided
      if (this.label) {
        text = this.context.add.text(x, y + 32 * scale, this.label,
          this.createFont(16, 10, scale)).setTextBounds(-16, -16, 32, 32);
        this.spriteGroup.add(text);
      }
      // add needle to group
      this.spriteGroup.add(needleSprite);
      // render current widget state (initial state)
      this.render();
    },
    "createFont": function (size, minSize, scale) {
      return {
        font: "bold " + Math.max(parseInt(size * scale), minSize) + "px Arial",
          fill: "#ffffff",
        boundsAlignH: "center",
        boundsAlignV: "middle"
      };
    },
    "createDivision": function (range, color) {
      var graphics = this.context.add.graphics(0, 0);
      var startAngle = 0, endAngle = 0;
      if (typeof range === "function") range = range(this.objectRef);
      startAngle = Phaser.Math.degToRad(this.angle0 + Math.abs(range[0] - this.minValue) * this.degPerValueUnit);
      endAngle = startAngle + Phaser.Math.degToRad(Math.abs(range[1] - range[0]) * this.degPerValueUnit);
      //  define the arc to be drawn as a line only
      graphics.lineStyle(14, color);
      // draw the arc with the corresponding color
      graphics.arc(0, 0, 103, startAngle, endAngle, false);
      return graphics;
    },
    "getValue": function () {
      var oRef = this.objectRef;
      var value = oRef[this.objectProperty.name] || 0;
      var decimalPlaces = this.objectProperty.decimalPlaces;
      if (typeof decimalPlaces === "function") decimalPlaces = decimalPlaces(oRef);
      if (typeof value === "number") {
        if (decimalPlaces > 0) value = value.toFixed(decimalPlaces);
        else if (value % 1 !== 0) value = value.toFixed(2);
      }
      return value;
    },
    "render": function () {
      var value = this.getValue(), tValue = '';
      var newAngle = Math.abs(value - this.minValue) * this.degPerValueUnit;
      // update only if the new value is over the value of the minimum division
      if (Math.abs(this.value - value) < this.minDivision) return;
      // maximum 6 digits are available for digital display
      if (value === undefined || value === null) {
        value = this.minValue;
        tValue = '------';
      } else tValue = String(value);
      this.value = value;
      if(tValue.length > 6) tValue = tValue.substring(0, 6);
      if (!this.suppressDigitalDisplay) this.bitmapText.text = tValue;
      if (newAngle > 270) newAngle = 270;
      if (newAngle > 180) {
        if (180 - this.needleSprite.angle >= 0) {
          this.context.add.tween(this.needleSprite).to({ angle: 180},
            50, Phaser.Easing.Quadratic.InOut, true);
        }
        this.context.add.tween(this.needleSprite).to({ angle: -359.9 + newAngle},
          50, Phaser.Easing.Quadratic.InOut, true);
      } else {
        this.context.add.tween(this.needleSprite).to({ angle: newAngle},
          100, Phaser.Easing.Quadratic.InOut, true);
      }
    }
  }
});