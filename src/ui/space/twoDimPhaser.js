/**
 * 2D visualization using Phaser API and the Phaser Isometric Plugin.
 *
 * Phaser: http://phaser.io/
 * Phaser Isometric Plugin: http://rotates.org/phaser/iso/
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
 */
var oes = oes || {};
oes.ui = oes.ui || {};
oes.ui.space = oes.ui.space || {};
oes.ui.space.twoDim = {};
oes.ui.space.twoDim.Phaser = {
  // the default space view (scenario defined values override the default values)
  spaceView: {
    canvasColor: "#B6DCF6", // default canvas color (use array of values to create gradients)
    canvasImage: null, // image to be rendered as canvas background
    backgroundColor: "#BFBFBF", // default background color: gray, rgb(191, 191, 191)
    tileImage: null, // no background tile
    width: 0, // 0 means: use full available width of the canvas (same if no value is provided)
    height: 400, // by default, the canvas height is 400px
    // The size of one background tile.
    // Note: 1 tile unit = 1 space unit.
    // e.g., xMax = 100, yMax = 50 and tileSize = 10 => 10 x 5 space tiles)
    // by default, the overlayGridCellSize is used, and when missing, the xMax is divided in 10 tiles
    tileSize: sim.model.space.overlayGridCellSize || sim.model.space.xMax / 10
  },
  // the map of object views
  objectViews: {
    // TODO: define default object view.
  },
  // the map of monitor definition
  monitor: {},

  /***** the following properties are Phaser specific views or components ***/
  // the engine is a Phaser instance, which does all the magic:
  // visualization, sonification, user interactions and when needed, physics.
  phaserEngine: null,
  // the map of all used Phaser assets
  phaserAssets: {},
  // the map of the Phaser compiled widgets
  phaserWidgets: {},
  // the map of Phaser views
  phaserViews: {},
  // default path to images (relative to project folder)
  imagesPath: "media/img/"

};

/**
 * Prepare the visualization engine.
 * @param containerEl
 *    the parent element, for the canvas where the visualization is rendered.
 *    If not provided, the body element is used as parent.
 */
oes.ui.space.twoDim.Phaser.setup = function (containerEl) {
  var spaceView = oes.ui.space.twoDim.Phaser.spaceView;
  // adjust resources path for the backend simulations case.
  if (typeof backend === "object" && parseInt(backend.sid) > 0
    && oes.ui.space.twoDim.Phaser.imagesPath.indexOf("?resource") === -1) {
    oes.ui.space.twoDim.Phaser.imagesPath =
      backend.sid + "?resource=" + oes.ui.space.twoDim.Phaser.imagesPath;
  }
  if (sim.config.observationUI) {
    // space view definition was provided in the scenario file
    if (sim.config.observationUI.spaceView) {
      spaceView = util.mergeObjects(spaceView,
        util.cloneObject(sim.config.observationUI.spaceView));
      oes.ui.space.twoDim.Phaser.spaceView = spaceView;
    }
    // clone object views
    if (sim.config.observationUI.objectViews) {
      oes.ui.space.twoDim.Phaser.objectViews =
        util.cloneObject(sim.config.observationUI.objectViews);
    }
    // clone monitors
    if (sim.config.observationUI.monitor) {
      oes.ui.space.twoDim.Phaser.monitor =
        util.cloneObject(sim.config.observationUI.monitor);
    }
  }
  // width property is a function
  if (typeof oes.ui.space.twoDim.Phaser.spaceView.width === "function")
    oes.ui.space.twoDim.Phaser.spaceView.width = oes.ui.space.twoDim.Phaser.spaceView.width();
  // no width was provided, use the full available width of the parent
  if (oes.ui.space.twoDim.Phaser.spaceView.width === 0) {
    oes.ui.space.twoDim.Phaser.spaceView.width =
      parseInt(parseInt(getComputedStyle(containerEl || document.body, null)
        .getPropertyValue("width")));
  }
  // height property is a function
  if (typeof sim.config.observationUI.spaceView.height === "function")
    oes.ui.space.twoDim.Phaser.spaceView.height = oes.ui.space.twoDim.Phaser.spaceView.height();
  // create the Phaser engine
  oes.ui.space.twoDim.Phaser.phaserEngine = new Phaser.Game(
    oes.ui.space.twoDim.Phaser.spaceView.width, // canvas width in pixels
    oes.ui.space.twoDim.Phaser.spaceView.height, // canvas height in pixels
    Phaser.AUTO, // detect rendering mode: Canvas of WebGL
    containerEl || document.body, // the parent element (id or ref) where the canvas element is created
    { preload: oes.ui.space.twoDim.Phaser.phaserPreload, // preload method
      create: oes.ui.space.twoDim.Phaser.phaserCreate, // create method
      update: oes.ui.space.twoDim.Phaser.phaserUpdate // update/redraw method
    },
    false, // background is opaque (true means transparent)
    true, // anti-aliasing is enabled
    false // physics engine is disabled
  );
};

/**
 * Phaser pre-load method. It is called when the HTML document finished loading
 * and it is used to preload resources (image, sound, etc).
 * It is also used to do initializations that are ONLY possible after
 * the document finished loading.
 */
oes.ui.space.twoDim.Phaser.phaserPreload = function () {
  var monitors = oes.ui.space.twoDim.Phaser.monitor;
  // Initialize the Phaser engine
  // NOTE: it must be done here, because only then we have all the initialization data.
  oes.ui.space.twoDim.Phaser.initializeEngine();
  // create/process the space view (inclusive the assets loading)
  oes.ui.space.twoDim.Phaser.createSpaceView();
  // create/process the object views (inclusive the assets loading)
  oes.ui.space.twoDim.Phaser.createObjectViews();
  // initialize monitors/widgets - create their initial state
  Object.keys(monitors).forEach(function (key) {
    var widget = oes.ui.space.twoDim.Phaser.createWidget(monitors[key], key);
    widget.init();
    oes.ui.space.twoDim.Phaser.phaserWidgets[key] = widget;
  });
  // preload all widgets assets
  wizz.widget.Widget.preload(oes.ui.space.twoDim.Phaser.phaserEngine);
};

/**
 * Phaser create method. It is called after pre-loading is done.
 * THis is used to create the initial world and to perform
 * various configurations before starting the rendering.
 * NOTE: it is also used to render the "initial state".
 */
oes.ui.space.twoDim.Phaser.phaserCreate = function () {
  var bgImageId = oes.ui.space.twoDim.Phaser.spaceView.tileImage;
  var engine = oes.ui.space.twoDim.Phaser.phaserEngine;
  var monitors = oes.ui.space.twoDim.Phaser.monitor;
  var canvasColor = oes.ui.space.twoDim.Phaser.spaceView.canvasColor;
  var worldGroup = engine.add.group(), objectsGroup = engine.add.group();
  var xMax = sim.model.space.xMax,
    yMax = sim.model.space.yMax;
  var img = null, sprite = null, gradient = null;
  var scaleX = 1, scaleY = 1;
  var x = 0, y = 0, i = 0, n = 0;
  var tileSize = oes.ui.space.twoDim.Phaser.spaceView.tileSize;
  var tileSize2 = tileSize / 2;
  // canvas background image
  if (engine.cache.checkImageKey("canvasImage")) {
    sprite = engine.add.sprite(0, 0, "canvasImage");
    img = engine.cache.getImage("canvasImage");
    sprite.scale.setTo(engine.canvas.width / img.width,
      engine.canvas.height / img.height);
    worldGroup.add(sprite);
  }
  // gradient canvas color
  if(Array.isArray(canvasColor)) {
    n = canvasColor.length;
    if (n < 2) throw "SpaceView: gradient background requires minimum two colors!";
    img = engine.add.bitmapData(engine.canvas.width, engine.canvas.height);
    for (i = 0; i < n - 1; i++) {
      gradient = img.context.createLinearGradient(
        engine.canvas.width / 2,
        engine.canvas.height / (n-1)*i,
        engine.canvas.width / 2,
        engine.canvas.height / (n-1)*(i+1));
      gradient.addColorStop(0, canvasColor[i]);
      gradient.addColorStop(1, canvasColor[i+1]);
      img.context.fillStyle = gradient;
      img.context.fillRect(0, engine.canvas.height / (n-1)*i,
        engine.canvas.width, engine.canvas.height / (n-1));
    }
    sprite = engine.add.sprite(0, 0, img);
    worldGroup.add(sprite);
  } else {
    // default background color
    engine.stage.backgroundColor = canvasColor;
  }
  // create the background, if an image exists for it.
  // The image is multiplied to fill the whole space.
  if (engine.cache.checkImageKey(bgImageId)) {
    img = engine.cache.getImage(bgImageId);
    console.log("Creating space view visualization: render background tiles.");
    for (x = 0; x < xMax; x += tileSize) {
      for (y = 0; y < yMax; y += tileSize) {
        sprite = new Phaser.Plugin.Isometric.IsoSprite(engine,
          x + tileSize2,  yMax - y - tileSize2,  0, bgImageId);
        // "scale" sprite to fit tile size (sprite image is scaled to fit tile size)
        sprite.width = 2 * tileSize;
        sprite.height = 2 * tileSize;
        // anchor is centered on x and set to bottom on y
        sprite.anchor.x = 0.5;
        sprite.anchor.y = 1;
        // add sprite to world group
        worldGroup.add(sprite);
      }
    }
  }
  // create the Phaser iso sprites for object views
  Object.keys(sim.objects).forEach(function (idStr) {
    var o = sim.objects[idStr];
    var typeName = o.constructor.Name, spriteId = "";
    var oViews = oes.ui.space.twoDim.Phaser.objectViews;
    var oView = oViews[idStr] ? oViews[idStr] : oViews[typeName];
    // no view found...nothing to do
    //if (o.constructor.Name === "TomatoPlant") return;
    if(!oView) return;
      spriteId = oView.computeImage(o);
      // no view definition for this object, so nothing to do
      if (!spriteId) return;
      // create sprite from view definition
      img = engine.cache.getImage(spriteId);
      scaleX = o.width / xMax * img.width;
      scaleY = o.height / yMax * img.height;
      sprite = new Phaser.Plugin.Isometric.IsoSprite(
        engine, o.pos[0], yMax - o.pos[1], 0, spriteId);
      sprite.width = 2 * o.width;
      sprite.height = 2 * o.height;
      // anchor is centered on x and set to bottom on y
      sprite.anchor.x = 0.5;
      sprite.anchor.y = 1;
      // add sprite to objects group
      objectsGroup.add(sprite);
      // store object view with the sprite for later usage
      sprite.oesView = util.cloneObject(oView);
      oes.ui.space.twoDim.Phaser.phaserViews[idStr] = sprite;
  });
  // sort the objects topologically
  engine.iso.topologicalSort(objectsGroup);
  // initialize monitors/widgets - create their initial state
  Object.keys(monitors).forEach(function (key) {
    oes.ui.space.twoDim.Phaser.phaserWidgets[key].doAfterPreload();
  });
};

oes.ui.space.twoDim.Phaser.phaserUpdate = function () {
  var engine = oes.ui.space.twoDim.Phaser.phaserEngine;
  var pos = engine.input.activePointer.position;
  engine.debug.text("cursor:[" + pos.x.toFixed(2) +", " + pos.y.toFixed(2) + "]", 125, 14);
  // show FPS - only for debug reasons!
  engine.debug.text("FPS: ", 2, 14, "white");
  engine.debug.text(engine.time.fps || "--", 45, 14, "white");
};

/**
 * Visualizes the space and all the objects.
 */
oes.ui.space.twoDim.Phaser.render = function () {
  var widgets = oes.ui.space.twoDim.Phaser.phaserWidgets;
  var objectViews =  oes.ui.space.twoDim.Phaser.phaserViews;
  Object.keys(widgets).forEach(function (key) {
    widgets[key].render();
  });
  // render object views
  Object.keys(objectViews).forEach(function (oId) {
    var o = sim.objects[oId];
    var view = objectViews[oId];
    view.loadTexture(view.oesView.computeImage(o));
  });
};

/**
 * Reset the visualization: the space and all the objects.
 */
oes.ui.space.twoDim.Phaser.reset = function () {
  // TODO: find out how is possible to reset all sprites at once in Phaser.

  // NOTE: do we even need this method ?!
};

/**
 * Initialize the Phaser engine and set the configuration parameters.
 */
oes.ui.space.twoDim.Phaser.initializeEngine = function () {
  var xMax = sim.model.space.xMax,
    yMax = sim.model.space.yMax;
  var engine = oes.ui.space.twoDim.Phaser.phaserEngine;
  // provide an ID to the Canvas element to access it later
  // NOTE: used by the simulatorUI.js to show/hide visualization.
  if (engine.canvas) engine.canvas.id = "visCanvas";
  // enable cross origin resource loading (the target server must support CORS!)
  engine.load.crossOrigin = true;
  // add the isometric plugin to Phaser
  engine.plugins.add(new Phaser.Plugin.Isometric(engine,
    null, Phaser.Plugin.Isometric.ISOMETRIC));
  // set the world size
  engine.world.setBounds(0, 0, xMax, yMax);
  // set the middle of the world in the middle of the screen
  engine.iso.anchor.setTo(0.5, (engine.canvas.height - yMax) / yMax * 0.5 );
  // scale the world by keeping proportions
  engine.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
  engine.scale.setUserScale(1, 1, 0, 0);
  // enable Phaser timing
  // TODO: need to figure out how to control the time!
  engine.time.advancedTiming = true;
};

/**
 * Create the space view by processing the definition provided in the scenario.
 */
oes.ui.space.twoDim.Phaser.createSpaceView = function () {
  var tileImg = oes.ui.space.twoDim.Phaser.spaceView.tileImage;
  if (typeof tileImg === "function") tileImg = tileImg();
  // load background tile image, if one is provided
  if (tileImg) {
    oes.ui.space.twoDim.Phaser.phaserEngine.load.image("space-tileImage",
      oes.ui.space.twoDim.Phaser.imagesPath + tileImg);
    oes.ui.space.twoDim.Phaser.phaserAssets["space-tileImage"] = tileImg;
    oes.ui.space.twoDim.Phaser.spaceView.tileImage = "space-tileImage";
  }
  // load canvas background image if one is defined
  if(oes.ui.space.twoDim.Phaser.spaceView.canvasImage) {
    console.log("Loading canvas image: ", "canvasImage",
      oes.ui.space.twoDim.Phaser.spaceView.canvasImage);
    oes.ui.space.twoDim.Phaser.phaserEngine.load.image("canvasImage",
      oes.ui.space.twoDim.Phaser.imagesPath + oes.ui.space.twoDim.Phaser.spaceView.canvasImage);
  }
};

/**
 * Create the object views by processing the definition(s) provided in the scenario.
 */
oes.ui.space.twoDim.Phaser.createObjectViews = function () {
  var objViewDefs = null;
  var objViews = oes.ui.space.twoDim.Phaser.objectViews;
  var processObjectView = oes.ui.space.twoDim.Phaser.processObjectView;
  // initialize object views
  if (sim.config.observationUI && sim.config.observationUI.objectViews) {
    objViewDefs = sim.config.observationUI.objectViews;
    Object.keys(objViewDefs).forEach(function (viewId) {
      objViews[viewId] = processObjectView(objViewDefs[viewId], viewId);
    });
  }
};

/**
 * Process one particular object view and create a structure that is
 * then used to render the object, whenever needed.
 * @param objViewDef
 *    the object view definition, as provided in the scenario file.
 * @param viewId
 *    the id of the view.
 */
oes.ui.space.twoDim.Phaser.processObjectView = function (objViewDef, viewId) {
  var engine = oes.ui.space.twoDim.Phaser.phaserEngine;
  // multiple images, with specified unique ID
  if (objViewDef.images){
    var assets = oes.ui.space.twoDim.Phaser.phaserAssets;
    var images = objViewDef.images;
    Object.keys(images).forEach(function (imgId) {
      // asset already loaded, nothing to do here
      if (assets[imgId]) return;
      // load asset
      assets[imgId] = images[imgId];
      console.log("Loading object asset: ", imgId, images[imgId]);
      engine.load.image(imgId, oes.ui.space.twoDim.Phaser.imagesPath + images[imgId]);
    });
  }
  // single image, the ID is the type name
  else if (objViewDef.image) {
    console.log("Loading object asset: ", viewId, objViewDef.image);
    engine.load.image(viewId, oes.ui.space.twoDim.Phaser.imagesPath + objViewDef.image);
  }
  return {
    computeImage: oes.ui.space.twoDim.Phaser.processImage(objViewDef, viewId),
    computeBackgroundColor: oes.ui.space.twoDim.Phaser.processColor(objViewDef.backgroundColor),
    computeColor: oes.ui.space.twoDim.Phaser.processColor(objViewDef.color),
    viewDef: objViewDef
  };
};

/**
 * Create a widget for a given object.
 * @param wgtDef
 *  the widget definition (as provided in the scenario)
 * @param wgtId
 *  the id of the widget
 */
oes.ui.space.twoDim.Phaser.createWidget = function (wgtDef, wgtId) {
  var wgtType = wgtDef.type, wgtProps = {}, wgtPropDef = null;
  var propName = "";
  var srcVar = null, oRef = null;
  // no type provided for the widget or no widget exists with this type
  if (!wgtType || typeof wizz.widget[wgtType] !== "function") return;
  console.log("Creating widget of type '" + wgtType + "', with ID=" + wgtId);
  // prepare widget parameters
  Object.keys(wgtDef).forEach(function (p) {
    // copy relevant properties
    if (p === "type" || p === "sourceVariable" || p === "sourceVariables") return;
    wgtProps[p] = wgtDef[p];
  });
  if (wgtDef.sourceVariables) {
    wgtProps.objectsRef = [];
    wgtProps.objectsProperty = [];
    wgtDef.sourceVariables.forEach(function (srcVar) {
      var wgtPropDef = {};
      oRef = oes.ui.space.twoDim.Phaser.getWidgetSourceObject(srcVar);
      if (!oRef) throw "oes.ui.space.twoDim.Phaser.createWidget: "
        + "cannot find object reference for monitor with ID=" + wgtId;
      wgtProps.objectsRef.push(oRef);
      propName = srcVar.name || wgtId;
      wgtPropDef = oes.ui.space.twoDim.Phaser.createWidgetPropDef(propName, oRef, srcVar);
      wgtProps.objectsProperty.push(wgtPropDef);
    });
  } else if ( wgtDef.sourceVariable) {
    srcVar = wgtDef.sourceVariable;
    propName = srcVar.name || wgtId;
    oRef = oes.ui.space.twoDim.Phaser.getWidgetSourceObject(srcVar);
    if (!oRef) throw "oes.ui.space.twoDim.Phaser.createWidget: "
      + "cannot find object reference for monitor with ID=" + wgtId;
    wgtPropDef = oes.ui.space.twoDim.Phaser.createWidgetPropDef(propName, oRef, srcVar);
    wgtProps.objectRef = oRef;
    wgtProps.objectProperty = wgtPropDef;
  }
  wgtProps.context = oes.ui.space.twoDim.Phaser.phaserEngine;
  return wizz.widget.Widget.create(wgtType, wgtProps);
};

/**
 * Create the object source property definition as expected by the widget.
 * @param propName
 *    the name of the observed property
 * @param oRef
 *    the object that is is observed by the widget
 * @param srcVarDef
 *    the definition of the source variable as appear on the scenario
 */
oes.ui.space.twoDim.Phaser.createWidgetPropDef = function (propName, oRef, srcVarDef) {
  var wgtPropDef = {}, propDefSource = null;
  if (oRef.constructor && oRef.constructor.properties && oRef.constructor.properties[propName])
    propDefSource = oRef.constructor.properties[propName];
  else if (oRef === sim.stat)
    propDefSource = sim.model.statistics[propName];
  else propDefSource = {};
  wgtPropDef.name = propName;
  wgtPropDef.label = srcVarDef.label || propDefSource.label || propName;
  wgtPropDef.decimalPlaces = srcVarDef.decimalPlaces || propDefSource.decimalPlaces;
  wgtPropDef.unit = srcVarDef.unit || propDefSource.unit;
  return wgtPropDef;
};

/**
 * Helper method to extract object references used with an widget.
 * @param srcDef
 *    the definition of the object source
 */
oes.ui.space.twoDim.Phaser.getWidgetSourceObject = function (srcDef) {
  var oRef = null;
  switch (srcDef.type) {
    // bound to simulator property
    case "simulator":
      oRef = sim;
      break;
    // bound to statistics variable
    case "statistics":
      oRef = sim.stat;
      break;
    // bound to global variable
    case "global":
      oRef = sim.model.v;
      break;
    // bound to object property
    case "property":
      if (typeof srcDef.object === "object")
        oRef = srcDef.object;
      else if (typeof srcDef.object === "function")
        oRef = srcDef.object();
      else if (typeof srcDef.object === "number")
        oRef = sim.objects[String(srcDef.object)];
      else throw "oes.ui.space.twoDim.Phaser.getWidgetSourceObject: failed to create widget."
        + " The sourceVariable.object property must be of type: number, object or function.";
      break;
  }
  return oRef;
};

/**
 * Process an image definition. It can be a string (representing the asset id)
 * or a function which returns a string that represents the asset id.
 * @param oView
 *    the object view definition
 * @param viewId
 *    the view id (used when single images are provided, and usually is the type name)
 * @returns {Function}
 */
oes.ui.space.twoDim.Phaser.processImage = function (oView, viewId) {
  var imgDef = oView.imageId || oView.tileImageId;
  if (!imgDef) return function () {return viewId;};
  if (typeof imgDef === "string")
    return function () { return imgDef};
  if (typeof imgDef === "function")
    return function (o) { return imgDef(o);}
};

/**
 * Given a color description {H:..., S:..., L:...} or
 * {R:..., G:..., B:..., A:...}, construct the HSL or RGB CSS style value.
 *
 * @param cDef
 *    the color description (e.g., a color definition)
 * @return hsl(h, s, l) or rgb(r, g, b) or rgba(r, g, b, a) CSS style value
 */
oes.ui.space.twoDim.Phaser.processColor = function (cDef) {
  var n=0, resColor=[], tColor =[], i=0;
  // no color definition provided - default is opaque gray
  if(!cDef) return function () {return "rgb(191, 191, 191)";};
  // CSS Color String (color name or any other CSS accepted color format)
  if (typeof cDef === "string") return function () {return cDef};
  // custom function (it should return a CSS accepted color format)
  if (typeof cDef === "function") return function (o) {return cDef(o);};
  // CSS color in HSL or RGB(A) format
  tColor[0] = cDef.H || cDef.R || 0;  // R or H channel
  tColor[1] = cDef.S || cDef.G || 0;  // G or S channel
  tColor[2] = cDef.L || cDef.B || 0;  // B or L channel
  tColor[3] = resColor[3] = cDef.A || 1; // alpha channel for RGBA
  n = cDef.A || cDef.A === 0 ? 4 : 3;
  for (i = 0; i < n; i++) {
    // user defined custom function f(o)
    if (typeof tColor[i] === "function") {
      resColor[i] = function (idx) {
        return function (o) {
          return idx === 3 ? tColor[idx](o) : parseInt(tColor[idx](o));
        }
      }(i);
    }
    // property name (use its value)
    else if (typeof tColor[i] === "string")
      resColor[i] = function (idx) {
        return function (o) {
          return idx === 3 ? o[tColor[idx]] : parseInt(o[tColor[idx]]);
        }
      }(i);
    // [prop-name, function(x, o) {...}]
    else if (Array.isArray(tColor[i])) {
      resColor[i] = function (idx) {
        var pName = tColor[idx][0],
          func = tColor[idx][1];
        return function (o) {
          if (o[pName] !== undefined && typeof func === "function")
            return idx === 3 ? func(o[pName], o) : parseInt(func(o[pName], o));
          else return 0;
        }
      }(i);
    }
    // a numeric value is given
    else if (typeof tColor[i] === "number") {
      resColor[i] = function (idx) {
        return function () {
          return idx === 3 ? tColor[idx] : parseInt(tColor[idx]);
        }
      }(i);
    }
    else console.log("oes.ui.space.gridDom.processColor: "
        + "Unsuported color computation method.", cDef, tColor[i]);
  }
  if (cDef.H !== undefined && cDef.S !== undefined && cDef.L !== undefined) {
    return function (o) {
      return "hsl(" + resColor[0](o) + "," + resColor[1] (o)
        + "%," + resColor[2](o) + "%)";
    }
  } else if ((cDef.R !== undefined && cDef.G !== undefined && cDef.B !== undefined)) {
    if (typeof resColor[3] !== "function") resColor[3] = function () {return 1;};
    return function (o) {
      return "rgba(" + resColor[0](o) + "," + resColor[1](o) + ","
        + resColor[2](o) + "," + resColor[3](o) + ")";
    }
  } else return function () {return "rgb(191, 191, 191)";};  // default: opaque gray
};
