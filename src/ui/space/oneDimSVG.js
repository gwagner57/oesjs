/**
 * 2D visualization using Phaser API and the Phaser Isometric Plugin.
 *
 * Phaser: http://phaser.io/
 * Phaser Isometric Plugin: http://rotates.org/phaser/iso/
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
 */
var oes = oes || {};
oes.ui = oes.ui || {};
oes.ui.space = oes.ui.space || {};
oes.ui.space.oneDim = {
  SVG: {
    objectViewDefaultSize: 10,
    objectViewDefaultColors:["blue","green","yellow","red"]
  }
};

/**
 * Convert linear position to SVG coordinates
 * Convert linear position on the space view circle to radians, then convert
 * the polar coordinates to Cartesian coordinates with the circle's center as the
 * origin. Finally transform these coordinates to the view's SVG coordinates
 * (r, theta) [polar] = (r * cos(\theta), r * sin( theta)) [cartesian]
 * @method
 * @param pos  the object's position in 1D space
 */
oes.ui.space.oneDim.SVG.convertPos2SvgCoordinates = function (pos) {
  // convert linear position to corresponding angle in radians
  var theta = (pos % sim.spaceView.circumference) / sim.spaceView.r;
  // convert the polar coordinates to Cartesian coordinates
  var x = sim.spaceView.r * Math.cos( theta);
  var y = sim.spaceView.r * Math.sin( theta);
  // transform these coordinates to the view's SVG coordinates
  var svgX = x + sim.spaceView.cx;
  var svgY = sim.spaceView.cy - y;
  return [svgX,svgY];
};
/**
 * Render an initial object view.
 * @method
 * @param o  the object the view of which is to be rendered
 */
oes.ui.space.oneDim.SVG.renderInitialObjectView = function (o) {
  var svgCoord = this.convertPos2SvgCoordinates( o.pos[0]),
      fillColor = this.objectViewDefaultColors[o.id-1],
      radius = this.objectViewDefaultSize;
  // create object view element
  sim.objectViews[String(o.id)] = svg.createCircle( svgCoord[0], svgCoord[1],
      radius, {fill: fillColor});
  // render the newly created element
  sim.visualEl.appendChild( sim.objectViews[String(o.id)]);
};
/**
 * Update an object view element.
 * @method
 * @param o  the object the view element of which is to be updated
 */
oes.ui.space.oneDim.SVG.updateObjectView = function (o) {
  var svgCoord = this.convertPos2SvgCoordinates( o.pos[0]);
  sim.objectViews[String(o.id)].setAttribute("cx", svgCoord[0]);
  sim.objectViews[String(o.id)].setAttribute("cy", svgCoord[1]);
};
/**
 * Set up the initial SVG space view.
 * @method
 * @param containerEl  where the space view is created, if not provided,
 *                     use the document body
 */
oes.ui.space.oneDim.SVG.setup = function (containerEl) {
  var parentEl = null;
  var trackDiameter=0, computedStyle=null, radius=0;
  if (containerEl) parentEl = containerEl;
  else parentEl = document.body;
  // assign space view parameters
  if (sim.config.observationUI && sim.config.observationUI.spaceView) {
    if (sim.config.observationUI.spaceView.trackDiameter) {
      trackDiameter = sim.config.observationUI.spaceView.trackDiameter;
    } else {
      computedStyle = getComputedStyle( parentEl, null);
      trackDiameter = Math.floor( parseInt( computedStyle.getPropertyValue("width"))/2);
    }
  }
  radius = Math.floor( trackDiameter/2);
  sim.spaceView = {cx:radius+50, cy:radius+20, r:radius};
  sim.spaceView.circumference = 2 * Math.PI * sim.spaceView.r;
  // render initial space view
  sim.visualEl = svg.createSVG({width:trackDiameter+100, height:trackDiameter+50});
  sim.visualEl.appendChild(
      svg.createCircle( sim.spaceView.cx, sim.spaceView.cy, sim.spaceView.r,
          {stroke:"lightgrey", strokeWidth:"20"})
  );
  parentEl.appendChild( sim.visualEl);
  // render all initial object views
  Object.keys( ObjectInOneDimSpace.instances).forEach( function (objIdStr) {
    var obj = ObjectInOneDimSpace.instances[objIdStr];
    oes.ui.space.oneDim.SVG.renderInitialObjectView( obj);
  })
};

//TODO: needed?
oes.ui.space.oneDim.SVG.reset = function () {};

/**
 * Visualize the current simulation state
 */
oes.ui.space.oneDim.SVG.renderSimState = function () {
  var keys = Object.keys( ObjectInOneDimSpace.instances),
      i= 0, o=null, objIdStr="";
  for (i=0; i < keys.length; i++) {
    objIdStr = keys[i];
    o = ObjectInOneDimSpace.instances[objIdStr];
    oes.ui.space.oneDim.SVG.updateObjectView( o);
  }
};
