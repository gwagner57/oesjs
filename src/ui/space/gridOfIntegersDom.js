/**
 * @fileOverview Create a DOM view of a (2-dimensional) integer grid.
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 */
/**
 * Visualizes an integer grid in the form of an HTML table DOM object.
 * The row/column table cell position (r,c) corresponds to the grid coordinates
 * (yMax-r, c+1) or to the integer grid array index (yMax-r-1)*xMax + c.
 */
oes.ui.space.grid.i.dom = {};
oes.ui.space.grid.i.dom.renderIntegerGrid = function () {
  var r=0, c=0;  // row/column
  var v=0;
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax;
  var cells=null, rows = sim.visualEl.rows;
  var colors = sim.config.observationUI.gridCellValueColors ||
      oes.ui.space.grid.gridCellValueColors;
  for (r=0; r < yMax ; r++) {
    cells = rows[r].cells;
    for (c=0; c < xMax; c++) {
      v = sim.space.grid[(yMax-r-1)*xMax + c] & 15;  // lower 4 bits
      cells[c].style.backgroundColor = colors[v];
      //??? if v===0 then cells[c].removeAttribute("style");
      // display cell value
      v = (sim.space.grid[(yMax-r-1)*xMax + c] & 240)/16;  // upper 4 bits
      if (v>0) cells[c].textContent = String(v);
    }
  }
};

