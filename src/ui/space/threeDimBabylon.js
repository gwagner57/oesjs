/**
 * 3D visualization using BabylonJS API and the Phaser Isometric Plugin.
 *
 * Phaser: http://www.babylonjs.com
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
 */
var oes = oes || {}, backend = backend || null;
oes.ui = oes.ui || {};
oes.ui.space = oes.ui.space || {};
oes.ui.space.threeDim = {};
oes.ui.space.threeDim.Babylon = {
  // the default space view (scenario defined values override the default values)
  spaceView: {
    canvasColor: "#B6DCF6", // default canvas color (use array of values to create gradients)
    canvasImage: null, // image to be rendered as canvas background
    // in 1D and 2D spaces, the ground image is used to cover the ground plane
    // if not specified, then the groundColor value is used
    groundImage: null,
    // in 1D and 2D spaces, the ground color is used to fill the ground plane
    groundColor: "#606060",
    // default track color
    trackColor: "#bfbfbf",
    // 0 means: use full available width of the canvas (same if no value is provided)
    width: 0,
    // by default, the canvas height is 400px
    height: 400,
    // default the width of the track
    trackWidth: 10,
    // the maximum value of the cell in case of an IntegerGrid
    // -256 means unset, and is the default value
    maxCellValue: -256
  },
  // the map of object views
  objectViews: {
    shape: "sphere"
  },
  // the map of monitor definition
  monitor: {},
  // default path to images (relative to project folder)
  imagesPath: "media/img/",

  /***** the following properties are Babylon specific views or components ***/
  // auto width computation
  autoWidth: true,
  // the container element (parent of the canvas element)
  containerEL: null,
  // the canvas element (where the scene is drawn)
  canvasEL: null,
  // the engine is a Babylon render engine instance
  engine: null,
  // the scene is a Babylon scene instance
  scene: null,
  // the view camera, which can be adjusted to various angles
  camera: null,
  // the ground plane (used with 1D and 2D spaces)
  ground: null,
  // the map of Babylon views
  babylonViews: {},
  // the internal babylon scene params
  sceneParams: {
    width: 15,
    height: 10,
    depth: 6
  },
  // space adjustment factors
  spaceAdjFactor: [
    41.5, // width factor
    16.5, // height factor
    1 // depth factor
  ],
  // internal parameters and objects used for 1D visualization
  oneDimVis: {
    params: {
      trackWidth: 0,
      trackRadius: 0,
      trackWidthRatio: 0.07
    }
  },
  // internal parameters and objects used for grid space visualization
  twoDimGridVis: {
    cellWidth: 0,
    gridCellValueColors: ["white","blue","red","green","brown","grey"],
    gridCellViews: [],
    maxCellHeight: 10
  },
  // master meshes are used for being cloned or instanced,
  // thus improving the rendering performance a few orders
  // of magnitude, specially on the case of a grid space.
  masterMeshes: {},
  // handler for window browser resize event
  resizeHandler: function(){
    var canvasEL = oes.ui.space.threeDim.Babylon.canvasEL;
    // width property is a function
    if (typeof oes.ui.space.threeDim.Babylon.spaceView.width === "function")
      oes.ui.space.threeDim.Babylon.spaceView.width = oes.ui.space.threeDim.Phaser.spaceView.width();
    // no width was provided, use the full available width of the parent
    if (oes.ui.space.threeDim.Babylon.autoWidth) {
      oes.ui.space.threeDim.Babylon.spaceView.width =
        parseInt(parseInt(getComputedStyle(oes.ui.space.threeDim.Babylon.containerEL || document.body, null)
          .getPropertyValue("width")));
    }
    // height property is a function
    if (typeof sim.config.observationUI.spaceView.height === "function")
      oes.ui.space.threeDim.Babylon.spaceView.height = oes.ui.space.threeDim.Babylon.spaceView.height();
    canvasEL.width = oes.ui.space.threeDim.Babylon.spaceView.width;
    canvasEL.style.width = oes.ui.space.threeDim.Babylon.spaceView.width + "px";
    canvasEL.height = oes.ui.space.threeDim.Babylon.spaceView.height;
    canvasEL.style.height = oes.ui.space.threeDim.Babylon.spaceView.height + "px";
    if (oes.ui.space.threeDim.Babylon.engine)
      oes.ui.space.threeDim.Babylon.engine.resize();
  },
  // a set of default colors used whenever a view does not have a color defined
  colors: ["blue","green","yellow","red"],
  colorIndex : 1,
  // handler that is invoked just before rendering the scene
  // NOTE: this is a BabylonJS feature (not used yet in our visualization module).
  beforeRenderHandler: function() {}
};

/**
 * Transform a CSS color name or HEX format color (#ABCDEF) into
 * a color accepted by BabylonJS
 * @param color
 *  the color as CSS color name or HEX format
 */
oes.ui.space.threeDim.Babylon.createColor = function (color) {
  color = color || "black"; // default color is black
  // color in HEX format
  if (typeof color === "string" && color.startsWith("#"))
    return new BABYLON.Color3.FromHexString(color);
  // color as CSS color name
  return new BABYLON.Color3.FromHexString(oes.ui.cssColorNames[color.toLowerCase()]);
};

/**
 * Prepare the visualization engine.
 * @param containerEl
 *    the parent element, for the canvas where the visualization is rendered.
 *    If not provided, the body element is used as parent.
 */
oes.ui.space.threeDim.Babylon.setup = function (containerEl) {
  var spaceView = oes.ui.space.threeDim.Babylon.spaceView;
  var canvasEL = oes.ui.space.threeDim.Babylon.canvasEL = document.createElement("canvas");
  var scene = null, engine = null, camera = null, light = null;
  if (containerEl) {
    oes.ui.space.threeDim.Babylon.containerEL = containerEl;
    containerEl.appendChild(canvasEL);
  }
  else document.body.appendChild(canvasEL);
  // adjust resources path for the backend simulations case.
  if (backend && typeof backend === "object" && parseInt(backend.sid) > 0
    && oes.ui.space.threeDim.Babylon.imagesPath.indexOf("?resource") === -1) {
    oes.ui.space.threeDim.Babylon.imagesPath =
      backend.sid + "?resource=" + oes.ui.space.threeDim.Babylon.imagesPath;
  }
  if (sim.config.observationUI) {
    // space view definition was provided in the scenario file
    if (sim.config.observationUI.spaceView) {
      spaceView = util.mergeObjects(spaceView,
        util.cloneObject(sim.config.observationUI.spaceView));
      oes.ui.space.threeDim.Babylon.spaceView = spaceView;
    }
    // clone object views
    if (sim.config.observationUI.objectViews) {
      oes.ui.space.threeDim.Babylon.objectViews =
        util.cloneObject(sim.config.observationUI.objectViews);
    }
  }
  // detect if canvas width must be computed automatically (not provided by the scenario author)
  oes.ui.space.threeDim.Babylon.autoWidth = !oes.ui.space.threeDim.Babylon.spaceView.width;
  // adjust sizes based on the available space and scenario properties
  oes.ui.space.threeDim.Babylon.resizeHandler();
  // create the Babylon engine
  engine = oes.ui.space.threeDim.Babylon.engine = new BABYLON.Engine(canvasEL, true);
  // create the Babylon scene
  scene = oes.ui.space.threeDim.Babylon.scene = new BABYLON.Scene(engine);
  // create the camera, that is oriented towards the origin of the scene
  camera = oes.ui.space.threeDim.Babylon.camera = new BABYLON.ArcRotateCamera("camera1",
    0, 0, 0, new BABYLON.Vector3(0, 0, 0), scene);
  // set the position (location) of the camera
  camera.setPosition(new BABYLON.Vector3(0, 7.5, -5));
  // attach camera to canvas
  camera.attachControl(canvasEL, true);
  // set background (canvas) color
  scene.clearColor =  oes.ui.space.threeDim.Babylon.createColor(
    oes.ui.space.threeDim.Babylon.spaceView.canvasColor);
  // create and adjust light intensity and specular color
  // the light aims 0,1,0 - meaning, to the sky
  light = oes.ui.space.threeDim.Babylon.light =
    new BABYLON.HemisphericLight('sceneLight', new BABYLON.Vector3(0, 1, 0), scene);
  light.intensity = 1;
  light.specular = new BABYLON.Color3(0.1, 0.1, 0.1);

  // detect which physical space type is rendered: 1D, 2D or 3D
  switch (sim.model.space.type) {
    case "IntegerGrid":
      oes.ui.space.threeDim.Babylon.setupIntegerGrid();
      oes.ui.space.threeDim.Babylon.render =
        oes.ui.space.threeDim.Babylon.renderIntegerGrid;
      break;
    case "ObjectGrid":
      oes.ui.space.threeDim.Babylon.setupObjectGrid();
      oes.ui.space.threeDim.Babylon.render =
        oes.ui.space.threeDim.Babylon.renderObjectGrid;
      break;
    case "1D":
      oes.ui.space.threeDim.Babylon.setupOneDim();
      oes.ui.space.threeDim.Babylon.createGroundPlane();
      oes.ui.space.threeDim.Babylon.render =
        oes.ui.space.threeDim.Babylon.renderOneDim;
      break;
    case "2D":
      oes.ui.space.threeDim.Babylon.setupTwoDim();
      oes.ui.space.threeDim.Babylon.render =
        oes.ui.space.threeDim.Babylon.renderTwoDim;
      break;
    case "3D":
      oes.ui.space.threeDim.Babylon.setupThreeDim();
      oes.ui.space.threeDim.Babylon.render =
        oes.ui.space.threeDim.Babylon.renderThreeDim;
      break;
  }

  // run the render loop
  oes.ui.space.threeDim.Babylon.engine.runRenderLoop( function (){
    oes.ui.space.threeDim.Babylon.render();
    oes.ui.space.threeDim.Babylon.scene.render();
  });
  // assign handler that is called before rendering the scene
  scene.beforeRender = oes.ui.space.threeDim.Babylon.beforeRenderHandler;
  // the canvas/window resize event handler
  window.addEventListener('resize', oes.ui.space.threeDim.Babylon.resizeHandler);
  // render the initial state
  oes.ui.space.threeDim.Babylon.render();
  oes.ui.space.threeDim.Babylon.scene.render();
  scene.debugLayer.show();
};

/**
 * Update all object views, so that the visualisation shows the current states.
 * NOTE: this method is not directly used, but instead it becomes reference to
 *       one of following update/render methods:
 *       - oes.ui.space.threeDim.Babylon.renderIntegerGrid, for an IntegerGrid space
 *       - oes.ui.space.threeDim.Babylon.renderObjectGrid, for an ObjectGrid space
 *       - oes.ui.space.threeDim.Babylon.renderOneDim, for a 1D space type
 *       - oes.ui.space.threeDim.Babylon.renderTwoDim, for a 2D space type
 *       - oes.ui.space.threeDim.Babylon.renderThreeDim, for a 3D space type
 */
oes.ui.space.threeDim.Babylon.render = function () {};

/**
 * On special cases, a ground plane is shown as part of the 3D visualization.
 * For example, on 1D spaces, this acts like the tracks ground.
 */
oes.ui.space.threeDim.Babylon.createGroundPlane = function() {
  var groundMaterial =  null, ground = null;
  var  w = oes.ui.space.threeDim.Babylon.spaceView.width,
    h = oes.ui.space.threeDim.Babylon.spaceView.height;
  var bgWidth = 1, bgHeight = 1;
  var adjFactor = oes.ui.space.threeDim.Babylon.spaceAdjFactor;
  var scene = oes.ui.space.threeDim.Babylon.scene;
  // create the ground plane and its corresponding material
  groundMaterial =  new BABYLON.StandardMaterial("groundMaterial", scene);
  bgWidth = adjFactor[0] * h / w;
  bgHeight = adjFactor[1] * h / w;
  ground = oes.ui.space.threeDim.Babylon.ground =
    BABYLON.Mesh.CreateGround('ground', bgWidth, bgHeight, 2,
      scene, false, BABYLON.Mesh.DOUBLESIDE);
  // ground image detected...
  if (oes.ui.space.threeDim.Babylon.spaceView.groundImage) {
    groundMaterial.diffuseTexture  = new BABYLON.Texture(
      oes.ui.space.threeDim.Babylon.imagesPath
      + oes.ui.space.threeDim.Babylon.spaceView.groundImage, scene);
  }
  // no ground texture/image then use ground color
  else {
    groundMaterial.diffuseColor = oes.ui.space.threeDim.Babylon.createColor(
      oes.ui.space.threeDim.Babylon.spaceView.groundColor);
  }
  // assign material to ground plane
  ground.material = groundMaterial;

  /** adjust camera so that the ground fits the canvas window as much as possible **/
  // NOTE: this works only to fit larger space in the screen, but not other way around.
  while(!ground.isCompletelyInFrustum(scene.activeCamera) && scene.activeCamera.fov < 500) {
    scene.activeCamera.fov += 0.005;
    scene.activeCamera.setTarget(ground.position);
  }
  // NOTE: this works only to fit larger screen when space is smaller, but not other way around.
  while(ground.isCompletelyInFrustum(scene.activeCamera) && scene.activeCamera.fov > 0) {
    scene.activeCamera.fov -= 0.005;
    scene.activeCamera.setTarget(ground.position);
  }
  /********************* END: adjust space to fit window/canvas *********************/
};

/**
 * Setup the 3D engine to display integer grid space visualisation
 */
oes.ui.space.threeDim.Babylon.setupIntegerGrid = function() {
  var sceneParams = oes.ui.space.threeDim.Babylon.sceneParams;
  var cellsOnX = sim.model.space.xMax,
    cellsOnY = sim.model.space.yMax;
  var borderWidth= 0.012, x = 0, y = 0, cellValue = 0;
  var xC = sceneParams.width / 2, zC = sceneParams.depth / 2;
  var cellWidth = oes.ui.space.threeDim.Babylon.twoDimGridVis.cellWidth
    = sceneParams.width / cellsOnX;
  var cellWidth2 = cellWidth / 2;
  var scene = oes.ui.space.threeDim.Babylon.scene;
  var masterMesh = null, gridCellView = null, material = null;
  var masterMeshes = oes.ui.space.threeDim.Babylon.masterMeshes;
  var createColor = oes.ui.space.threeDim.Babylon.createColor;
  var colors = sim.config.observationUI.gridCellValueColors ||
    oes.ui.space.threeDim.Babylon.twoDimGridVis.gridCellValueColors;
  var color = null;
  var gridCellViews = oes.ui.space.threeDim.Babylon.twoDimGridVis.gridCellViews;
  var maxCellHeight = oes.ui.space.threeDim.Babylon.twoDimGridVis.maxCellHeight,
    cellHeightRatio = 1;

  // max cell height
  if (oes.ui.space.threeDim.Babylon.spaceView.maxCellValue <= maxCellHeight) {
    cellHeightRatio =
      oes.ui.space.threeDim.Babylon.twoDimGridVis.cellHeightRatio = 1;
  } else {
    cellHeightRatio =
      oes.ui.space.threeDim.Babylon.twoDimGridVis.cellHeightRatio = maxCellHeight / 256;
  }
  // create the master meshes
  colors.forEach(function (color) {
    masterMesh = BABYLON.Mesh.CreateBox("box", cellWidth - borderWidth, scene);
    // master meshes are not shown (not visible on the scene)
    masterMesh.setEnabled(false);
    // create the material for the master mesh
    material =  new BABYLON.StandardMaterial("gridCellMaterial", scene);
    // set color
    material.diffuseColor = createColor(color);
    // don't render hidden faces (improves performance)
    material.backFaceCulling = true;
    // assign material to master mesh
    masterMesh.material = material;
    // store the master mesh for later usage
    masterMeshes[color] = masterMesh;
  });
  // create grid cell meshes by using instances, thus
  // the render performance is about 20 times better in most cases
  // than when using a Mesh for each cell, and about 10 times as
  // in the case of using cloned meshes.
  for (x = 0; x < cellsOnX; x++){
    gridCellViews[x] = [];
    for (y = 0; y < cellsOnY; y++) {
      cellValue = oes.space.grid.i.getCellValue(x, y) || 0;
      color = colors[cellValue];
      gridCellView = masterMeshes[color]
        .createInstance("cell_" + x + "_" + y);
      // set cell X position (width in the screen)
      gridCellView.position.x = cellWidth * x - xC;
      // set cell Y position (height in the screen)
      gridCellView.position.y = cellWidth / 2;
      // set cell Z pozition (depth in the screen)
      gridCellView.position.z = cellWidth * y - zC;
      // store the current represented integer value for the cell
      gridCellView.representedValue = cellValue;
      // scale according with the cell value
      gridCellView.scaling.y = cellValue + cellHeightRatio;
      gridCellView.position.y = cellWidth2 * (cellValue + cellHeightRatio);
      // store grid cell view
      gridCellViews[x][y] = gridCellView;
    }
  }
};

/**
 * Update (synchronize with simulator objects) IntegerGrid views.
 */
oes.ui.space.threeDim.Babylon.renderIntegerGrid = function () {
  var cellsOnX = sim.model.space.xMax,
    cellsOnY = sim.model.space.yMax;
  var cellWidth2 = oes.ui.space.threeDim.Babylon.twoDimGridVis.cellWidth / 2;
  var cellHeightRatio = oes.ui.space.threeDim.Babylon.twoDimGridVis.cellHeightRatio;
  var gridCellViews = oes.ui.space.threeDim.Babylon.twoDimGridVis.gridCellViews;
  var colors = sim.config.observationUI.gridCellValueColors ||
    oes.ui.space.threeDim.Babylon.twoDimGridVis.gridCellValueColors;
  var masterMeshes = oes.ui.space.threeDim.Babylon.masterMeshes;
  var cellValue = 0, hScale = 1;
  var cellMesh = null, newCellMesh = null, color = null;
  // update grid cell meshes
  for (x = 0; x < cellsOnX; x++){
    for (y = 0; y < cellsOnY; y++) {
      cellValue = oes.space.grid.i.getCellValue(x, y) || 0;
      cellMesh = gridCellViews[x][y];
      // only update if color change is detected
      if (cellMesh.representedValue !== cellValue) {
        hScale = cellValue + cellHeightRatio;
        color = colors[cellValue];
        newCellMesh = masterMeshes[color]
          .createInstance("cell_" + x + "_" + y);
        // set cell X position (width in the screen)
        newCellMesh.position.x = cellMesh.position.x;
        // set cell Y position (height in the screen)
        newCellMesh.position.y = cellMesh.position.y;
        // set cell Z pozition (depth in the screen)
        newCellMesh.position.z = cellMesh.position.z;
        // store the current represented integer value for the cell
        newCellMesh.representedValue = cellValue;
        // store the new grid cell view
        gridCellViews[x][y] = newCellMesh;
        // adjust scale and position
        newCellMesh.scaling.y = hScale;
        newCellMesh.position.y = cellWidth2 * hScale;
        // dispose the old mesh
        cellMesh.dispose();
      }
    }
  }
};

/**
 * Setup the 3D engine to display 1D space visualisation
 */
oes.ui.space.threeDim.Babylon.setupOneDim = function () {
  var w = oes.ui.space.threeDim.Babylon.spaceView.width,
      h = oes.ui.space.threeDim.Babylon.spaceView.height;
  var path = [], i = 0, pathStep = Math.PI / 360;
  var params1D = oes.ui.space.threeDim.Babylon.oneDimVis.params;
  var trackWidth = oes.ui.space.threeDim.Babylon.oneDimVis.params.trackWidth =
    params1D.trackWidthRatio * h / w * oes.ui.space.threeDim.Babylon.spaceView.trackWidth;
  var adjFactor = oes.ui.space.threeDim.Babylon.spaceAdjFactor;
  var bgWidth = adjFactor[0] * h / w, bgHeight = adjFactor[1] * h / w;
  var trackRadius = params1D.trackRadius =
        Math.min(bgWidth, bgHeight) / 2 - trackWidth - 0.15;
  var scene = oes.ui.space.threeDim.Babylon.scene;
  var extrudedTack = null;
  var trackMaterial = new BABYLON.StandardMaterial("trackMaterial", scene);
  var trackTexture = null;
  // create the 2D shape, that is then extruded and used to display the 1D track in 3D view
  // this is a thin rectangle in the XY plane
  var shape = [
    new BABYLON.Vector3(-trackWidth, 0.01, 0),
    new BABYLON.Vector3(trackWidth, 0.01, 0),
    new BABYLON.Vector3(trackWidth, 0.1, 0),
    new BABYLON.Vector3(-trackWidth, 0.1, 0),
    new BABYLON.Vector3(-trackWidth, 0.01, 0)
  ];
  // set track material properties
  trackMaterial.alpha = 1.0;
  // use texture for the track
  if (oes.ui.space.threeDim.Babylon.spaceView.trackImage) {
    trackTexture = new BABYLON.Texture(
      oes.ui.space.threeDim.Babylon.imagesPath
      + oes.ui.space.threeDim.Babylon.spaceView.trackImage, scene);
    trackTexture.uScale = 2.1;
    trackTexture.vScale = 10;
    trackMaterial.diffuseTexture = trackTexture;
  } else { // use color for the track
    trackMaterial.diffuseColor = oes.ui.space.threeDim.Babylon.createColor(
      oes.ui.space.threeDim.Babylon.spaceView.trackColor);
  }
  trackMaterial.backFaceCulling = false;
  // create the extrusion path for the 1D track in a 3D view
  for(i = 0; i <= 2 * Math.PI + 3*pathStep; i += pathStep) {
    path.push(new BABYLON.Vector3(trackRadius * Math.cos(i), 0.0, trackRadius * Math.sin(i)));
  }
  // create the track extrusion
  extrudedTack = BABYLON.Mesh.ExtrudeShape("extruded", shape, path, 1, 0, BABYLON.Mesh.NO_CAP, scene);
  // assign material to the track
  extrudedTack.material = trackMaterial;
  // create space unit (1 space unit represents the arc for 1 unit in the 1D space)
  // which means that for the 2*PI (complete circle) we have sim.model.space.xMax space units
  params1D.spaceUnitRatio = Math.PI / sim.model.space.xMax;
  // create object views
  Object.keys(sim.objects).forEach(function (objId) {
    oes.ui.space.threeDim.Babylon.createOneDimView(sim.objects[objId]);
  });
};

/**
 * Create the view for a 1D object.
 * This method is called only when initial views are created
 * or when a new object is created, so a view must be assigned to it.
 *
 * TODO: optimizations to create views for dynamically created objects
 *
 * @param obj
 *  the object for which the view is created
 */
oes.ui.space.threeDim.Babylon.createOneDimView = function (obj) {
  var params1D = oes.ui.space.threeDim.Babylon.oneDimVis.params;
  var sUR = params1D.spaceUnitRatio;
  var scene = oes.ui.space.threeDim.Babylon.scene;
  var trackRadius = params1D.trackRadius;
  var views = oes.ui.space.threeDim.Babylon.objectViews;
  var objViewRadius =  params1D.trackWidth * 1.25;
  var pos = obj.pos;
  var objViewShape = null, objView = null, shape = null;
  var material = new BABYLON.StandardMaterial("objViewMaterial", scene);
  var materialColor = null;
  // material transparency (alpha factor) - opaque (non-transparent)
  material.alpha = 1.0;
  // display also the back face of the material
  // NOTE: this means slightly lower performance but better visualisation effect
  material.backFaceCulling = false;
  // try to get a view by object
  objView = views[obj.id];
  // try to get a view by object type
  if (!objView) objView = views[obj.constructor.Name];
  // particular shape for one particular object of the specified type
  if(objView[obj.id] && objView[obj.id].shape) shape = objView[obj.id].shape;
  else shape = objView.shape;
  // create the shape for the object view
  switch (shape) {
    // TODO: add more shapes, such as equilateral-triangle/tetrahedron, cylinder, etc
    // circles are rendered as sphere
    case "circle":
      objViewShape = BABYLON.Mesh.CreateSphere("sphere", 16, objViewRadius, scene);
      break;
    // spheres are rendered as sphere
    case "sphere":
      objViewShape = BABYLON.Mesh.CreateSphere("sphere", 16, objViewRadius, scene);
      break;
    case "square":
      objViewShape = BABYLON.Mesh.CreateBox("box", objViewRadius, scene);
      break;
    case "cube":
      objViewShape = BABYLON.Mesh.CreateBox("box", objViewRadius, scene);
      break;
    case "box":
      objViewShape = BABYLON.Mesh.CreateBox("box", objViewRadius, scene);
      break;
    // default render a sphere
    default:
      objViewShape = BABYLON.Mesh.CreateSphere("sphere", 16, objViewRadius, scene);
      break;
  }
  if (objView.fillColor) {
    materialColor = oes.ui.space.threeDim.Babylon.createColor(objView.fillColor);
  } else if (objView[obj.id] && objView[obj.id].fillColor) {
    materialColor = oes.ui.space.threeDim.Babylon.createColor(objView[obj.id].fillColor);
  } else { // use next available color
    materialColor = oes.ui.space.threeDim.Babylon.createColor(
      oes.ui.space.threeDim.Babylon.colors[parseInt(
        oes.ui.space.threeDim.Babylon.colorIndex)]);
    // move to the next color from the default colors list
    oes.ui.space.threeDim.Babylon.colorIndex++;
    oes.ui.space.threeDim.Babylon.colorIndex %=
      oes.ui.space.threeDim.Babylon.colors.length;
  }
  material.diffuseColor = materialColor;
  // add material
  objViewShape.material = material;
  // set shape position in 3D space
  objViewShape.position.y = objViewRadius / 2 + 0.1; // 0.1 is the track height
  objViewShape.position.x = trackRadius * Math.cos(pos[0] * sUR);
  objViewShape.position.z = trackRadius * Math.sin(pos[0] * sUR);
  objViewShape.rotation.y = -pos[0] * sUR;
  // store the view
  oes.ui.space.threeDim.Babylon.babylonViews[obj.id] = objViewShape;
};

/**
 * Update (synchronize with simulator objects) 1D object views.
 */
oes.ui.space.threeDim.Babylon.renderOneDim = function () {
  var objects = sim.objects;
  var views = oes.ui.space.threeDim.Babylon.babylonViews;
  var params1D = oes.ui.space.threeDim.Babylon.oneDimVis.params;
  var sUR = params1D.spaceUnitRatio, trackRadius = params1D.trackRadius;
  // update/sync physical properties used by the object views
  Object.keys(views).forEach(function (objId) {
    var pos = objects[objId].pos, objView = views[objId];
    objView.position.x = trackRadius * Math.cos(pos[0] * sUR);
    objView.position.z = trackRadius * Math.sin(pos[0] * sUR);
    objView.rotation.y = -pos[0] * sUR;
    // TODO: color changes, etc, based on property values when needed.
  });
};

/**
 * Setup the 3D engine to display 2D space visualisation
 */
oes.ui.space.threeDim.Babylon.setupTwoDim = function () {
  alert("The 3D rendering of a 2D space is not implemented yet!");
};

/**
 * Create the view for a 2D object
 * @param obj
 *  the object for which the view is created
 */
oes.ui.space.threeDim.Babylon.createTwoDimView = function (obj) {
  alert("The 3D rendering of a 2D space is not implemented yet!");
};

/**
 * Update (synchronize with simulator objects) 2D object views.
 */
oes.ui.space.threeDim.Babylon.renderTwoDim = function () {
  alert("The 3D rendering of a 2D space is not implemented yet!");
};

/**
 * Setup the 3D engine to display 3D space visualisation
 */
oes.ui.space.threeDim.Babylon.setupThreeDim = function () {
  alert("The 3D rendering of a 3D space is not implemented yet!");
};

/**
 * Create the view for a 3D object
 * @param obj
 *  the object for which the view is created
 */
oes.ui.space.threeDim.Babylon.createTwoDimView = function (obj) {
  alert("The 3D rendering of a 3D space is not implemented yet!");
};

/**
 * Update (synchronize with simulator objects) 3D object views.
 */
oes.ui.space.threeDim.Babylon.renderThreeDim = function () {
  alert("The 3D rendering of a 3D space is not implemented yet!");
};