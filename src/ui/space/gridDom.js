/**
 * @fileOverview Create a DOM view of a (2-dimensional) object grid.
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 */
var oes = oes || {};
oes.ui = oes.ui || {};
oes.ui.space = oes.ui.space || {};
oes.ui.space.grid = {
  i: {},  // namespace object for integer grid rendering features
  o: {objectViews: {}} ,  // namespace object for object grid rendering features
  // style defaults
  gridCellSize: 3,
  gridCellValueColors: ["white","blue","red","green","brown","grey","yellow"],
  // see https://graphemica.com/characters/tags/animal
  unicodeCharacters: {"sheep":"&#128017;", "wolf":"&#128058;"}
};
/**
 * Creates a DOM table structure for visualizing a simple grid
 */
oes.ui.space.grid.setup = function (containerEl) {
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax;
  var rowEl=null, i=0, j=0, size=0;
  var gridTableEl = dom.createElement("table", {id:"visCanvas"});
  var styleEl = document.createElement("style");
  if (sim.config.observationUI && sim.config.observationUI.spaceView)
    size = sim.config.observationUI.spaceView.gridCellSize;
  size = size || oes.ui.space.grid.gridCellSize;
  if (typeof size === "function") size = parseInt( size());
  // assign the correct PositiveInteger value for the grid cell size
  oes.ui.space.grid.gridCellSize = parseInt( size);
  styleEl.innerHTML = "table#visCanvas td {width:" + size +
      "px; height:" + size + "px; " + "font-size:" + (size-4) +
      "px; line-height:" + size + "px;}";
  document.head.appendChild( styleEl);
  if (containerEl) containerEl.appendChild( gridTableEl);
  else document.body.appendChild( gridTableEl);
  // set sim.visualEl
  sim.visualEl = gridTableEl;
  for (i=0; i < yMax; i++) {
    rowEl = gridTableEl.insertRow();
    for (j=0; j < xMax; j++) {
      rowEl.insertCell();
    }
  }
};
/**
 * Reset the grid DOM element for visualizing a grid space
 * by clearing the contents of the grid table
 */
oes.ui.space.grid.reset = function () {
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax;
  var rowEl=null, i=0, j=0;
  sim.visualEl.innerHTML = "";
  for (i=0; i < yMax; i++) {
    rowEl = sim.visualEl.insertRow();
    for (j=0; j < xMax; j++) {
      rowEl.insertCell();
    }
  }
};

