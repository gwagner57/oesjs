/**
 * @fileOverview User interface variables and procedures
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 *
 * To be loaded after loading frontMatter.js
 */

/*
Improvements/extensions
- format percentages with new Intl.NumberFormat('de-DE',{style:"percent",maximumFractionDigits:1}).format( number)

*/

var oes = oes || {};
oes.ui = oes.ui || {};
oes.ui.vis = {SVG:{}};  // name space for generic UI procedures/functions
sim.ui = sim.ui || {};  // name space for scenario/model-specific UI settings

// flag used to create UI variations for Client or Server version of the simulation
oes.ui.fullUI = true;

/*******************************************************
 * Create the Model Menu with a "Narrative", "Description" and "Code" button.
 *******************************************************/
oes.ui.createModelMenu = function() {
  var downloadLink=null, closeBtn=null,
      menuEl = document.getElementById("model-menu"),
      el=null,
      simPageUrl = window.location.href,
      pos=0, tail="";
  pos = simPageUrl.indexOf("simulation.html");
  if (pos === -1) {
    pos = simPageUrl.indexOf("index.html");
  }
  if (pos > -1) {
    sim.ui.simFolderPath = simPageUrl.substring( 0, pos);
  } else {
    pos = simPageUrl.lastIndexOf("/");
    if (pos === simPageUrl.length-1) {  // last character is a slash
      sim.ui.simFolderPath = simPageUrl;
    }
    else {
      sim.ui.simFolderPath = simPageUrl.substring( 0, pos+1);
      // accept paths that end with a simulation number
      tail = simPageUrl.substring( pos+1);
      if (/\d*/.test( tail)) sim.ui.simFolderPath += tail + "/";
    }
  }
  sim.ui.showCodeElems = {};  // map of UI elements for showing code
  // create showCode form element
  el = document.createElement("form");
  el.id = "showCodeForm";
  el.style.display = "none";
  el.innerHTML = "<span class='closeButton'>&times;</span><p>" +
      i18n.t("Show code of...") + "</p><select><option>"+ i18n.t("-- choose file --") +
      "</option></select><p id='download'></p></form>";
  menuEl.appendChild( el);
  document.getElementById("download").innerHTML = i18n.t("Or <a>download all</a>");
  downloadLink = document.querySelector("#showCodeForm a");
  downloadLink.href = sim.ui.simFolderPath + "Download.zip";
  downloadLink.title = i18n.t("Download simulation code");
  closeBtn = document.querySelector("#showCodeForm span.closeButton");
  closeBtn.addEventListener("click", function () {
    document.getElementById("showCodeBtn").style.display = "inline";
    document.getElementById("showCodeForm").style.display = "none";
  });
  // create space size def. activation button
  if (sim.model.space.type) {
    if (!document.getElementById("spaceSizeDefBtn")) {
      el = document.createElement("button");
      el.id = "spaceSizeDefBtn";
      el.textContent = i18n.t("Space");
      el.onclick = oes.ui.showSpaceSizeDefUI;
      menuEl.appendChild( el);
    }
  }
};
oes.ui.showNarrative = function() {
  var narrativeEl=null;
  if (!sim.ui.narrativeEl) {
    narrativeEl = document.createElement("div");
    narrativeEl.innerHTML = sim.model.systemNarrative;
    sim.ui.narrativeEl = dom.createModal({
      fromElem: narrativeEl,
      title: i18n.t("System Narrative")
    });
  }
  document.getElementById("overlay").style.display = "block";
  sim.ui.narrativeEl.style.display = "block";
};
oes.ui.showDescription = function() {
  var descrEl=null;
  if (!sim.ui.descriptionEl) {
    descrEl = document.createElement("div");
    descrEl.innerHTML = sim.scenario.shortDescription || sim.model.shortDescription;
    sim.ui.descriptionEl = dom.createModal({
      fromElem: descrEl,
      title: i18n.t("Model Description")
    });
  }
  document.getElementById("overlay").style.display = "block";
  sim.ui.descriptionEl.style.display = "block";
};
oes.ui.showCode = function() {
  var showCodeBtn = document.getElementById("showCodeBtn"),
      showCodeForm = document.getElementById("showCodeForm"),
      codeFileSelEl = document.querySelector("#showCodeForm > select");
  if (!sim.ui.simFolderPath) {
    console.log("Cannot show code since folder path is not available!");
    return;
  }
  showCodeBtn.style.display = "none";
  showCodeForm.style.display = "inline-block";
  if (codeFileSelEl.length === 1) {  // have options not yet been created?
    codeFileSelEl.appendChild(
        dom.createOption({text:"simulation.js", value:"simulation"}));
    if (sim.model.objectTypes) {
      sim.model.objectTypes.forEach( function (objT) {
        codeFileSelEl.appendChild( dom.createOption({text: objT + ".js", value: objT}));
      });
    }
    if (sim.model.eventTypes) {
      sim.model.eventTypes.forEach( function (evtT) {
        codeFileSelEl.appendChild( dom.createOption({text: evtT + ".js", value: evtT}));
      });
    }
    if (sim.model.activityTypes) {
      sim.model.activityTypes.forEach( function (actT) {
        codeFileSelEl.appendChild( dom.createOption({text: actT + ".js", value: actT}));
      });
    }
    codeFileSelEl.addEventListener("change", function () {
      var fileName = codeFileSelEl.value;

      function showCode( txt) {
        var showCodeEl=null;
        if (!sim.ui.showCodeElems[fileName]) {
          showCodeEl = document.createElement("div");
          showCodeEl.innerHTML = "<pre><code class='language-javascript'>"+ txt +"</code></pre>";
          sim.ui.showCodeElems[fileName] = dom.createModal({
            fromElem: showCodeEl,
            title: i18n.t("JavaScript code of ") + fileName +".js",
            width: "50em"});
        }
        if (typeof Prism === "object") Prism.highlightAll();  // run the Prism syntax highligter
        document.getElementById("overlay").style.display = "block";
        sim.ui.showCodeElems[fileName].style.display = "block";
      }

      if (fileName) {  // a choice has been made
        fetch( sim.ui.simFolderPath + fileName + ".js")
            .then( function (response) {
              return response.text().then( showCode);
            })
      }
    });
  }
};
/*******************************************************
 UI for Space Definition
 *******************************************************/
oes.ui.showSpaceSizeDefUI = function () {
  var menuEl = document.getElementById("model-menu"),
      spaceSizeDefBtn = document.getElementById("spaceSizeDefBtn"),
      spaceSizeDefForm = document.getElementById("spaceSizeDefForm"),
      closeBtn=null;
  if (!spaceSizeDefForm) {
    // create space size def. form panel element
    spaceSizeDefForm = document.createElement("form");
    spaceSizeDefForm.id = "spaceSizeDefForm";
    spaceSizeDefForm.innerHTML = "<span class='closeButton'>&times;</span>";
    spaceSizeDefForm.style.display = "none";
    menuEl.appendChild( spaceSizeDefForm);
    closeBtn = document.querySelector("#spaceSizeDefForm span.closeButton");
    closeBtn.addEventListener("click", function () {
      spaceSizeDefBtn.style.display = "inline";
      spaceSizeDefForm.style.display = "none";
    });
    sim.ui["space"] = new oBJECTvIEW({
      modelObject: sim.model.space,
      fields: [["xMax", "yMax", "zMax"].slice(0,
          oes.space.dimensions[sim.model.space.type])],
      suppressNoValueFields: false,
      heading: i18n.t("Space Size"),
      userActions: {
        "applyChanges": function () {
          sim.updateInitialStateObjects();
          oes.ui.resetCanvas();
          // visualize initial state (at start of step 0)
          if (sim.config.visualize) oes.ui.visualizeStep();
        }
      }
    });
    sim.ui["space"].userActions["applyChanges"].label = i18n.t("Apply changes");
    // render view in modalBodyEl and store its data binding
    sim.ui["space"].dataBinding = sim.ui["space"].render( spaceSizeDefForm);
  }
  spaceSizeDefBtn.style.display = "none";
  spaceSizeDefForm.style.display = "inline-block";
};
/*******************************************************
 * Create the UI page footer.
 *******************************************************/
oes.ui.createPageFooter = function() {
  var created = new Date( sim.model.created),
      modified = new Date( sim.model.modified),
      mainEl = document.querySelector("body > main"),
      license = sim.model.license || oes.defaults.license,
      licenseLinks=[], el=null,
      contributions = !sim.model.contributors ? "" :
          ", "+ i18n.t("with contributions by") +" "+ sim.model.contributors,
      sourceAuthors = !sim.model.sourceAuthors ? "" :
          ", "+ i18n.t("based on work by") +" "+ sim.model.sourceAuthors,
      artworkCredits = !sim.config.artworkCredits ? "" :
          " | <a href='#' title='"+ sim.config.artworkCredits + "'>" +
          i18n.t("Artwork Credits") + "</a>";
  var dateFmt = new Intl.DateTimeFormat( i18n.accessLang || "en-US");
  licenseLinks["CC BY"] = "https://creativecommons.org/licenses/by/4.0/";
  licenseLinks["CC BY-SA"] = "https://creativecommons.org/licenses/by-sa/4.0/";
  licenseLinks["CC BY-NC"] = "https://creativecommons.org/licenses/by-nc/4.0/";
  if (licenseLinks[license]) {
    license = "<a href='"+ licenseLinks[license] +"'>"+ license +"</a>";
  }
  if (!document.querySelector("body > footer")) {
    el = document.createElement("footer");
    el.innerHTML = "<hr/><p>&copy; "+ sim.model.creator +" ("+ license +"), " +
        i18n.t("created on") +" "+ dateFmt.format( created) +", "+
        i18n.t("last modified on") +" "+ dateFmt.format( modified) + contributions + sourceAuthors +
        artworkCredits + " | <a href='https://sim4edu.com/credits.html'>" + i18n.t("OESjs Credits") +"</a>";
    // insert footer after body > main
    dom.insertAfter( el, mainEl);
  }
};

/*******************************************************
 Set up the simulation UI
 *******************************************************
 * @method
 * @author Gerd Wagner
 */
oes.ui.setupUI = function () {
  var el=null, el2=null,
      mainEl = document.querySelector("body > main"),
      statistics = sim.model.statistics,
      createTimeSeriesChart = false;
  oes.ui.timeUnitLabels = {"ms":"milliseconds", "s":"seconds", "m":"minutes", "h":"hours",
      "D":"Days", "W":"Weeks", "M":"Months", "Y":"Years"};
  function setupUiForStepIncrem( formName) {
    var runStepBtn = document.forms[formName].elements["run step"];
    var incrEl = dom.createButton({classValues:"stepIncrement", content: String(sim.stepIncrement)});
    incrEl.contentEditable = true;
    incrEl.title = i18n.t("Click for changing the increment value");
    dom.insertAfter( incrEl, runStepBtn);
    dom.insertAfter( dom.createElement("span",{content:"+"}), runStepBtn);
    incrEl.addEventListener("blur", function () {
      var newIncrVal = parseInt( incrEl.textContent),
          simStepIncrEl = null, hint="";
      var scenarioRunStepBtn = document.forms["scenario"].elements["run step"],
          simRunStepBtn = document.forms["sim"].elements["run step"];
      if (isNaN( newIncrVal)) incrEl.textContent = sim.stepIncrement;
      else if (newIncrVal !== sim.stepIncrement) {
        sim.stepIncrement = newIncrVal;
        simStepIncrEl = document.querySelector("form#sim .stepIncrement");
        if (simStepIncrEl !== incrEl) simStepIncrEl.textContent = newIncrVal;
        hint = i18n.t(
            sim.stepIncrement===1 ? "Execute a single simulation step" : `Execute ${sim.stepIncrement} steps`
        );
        simRunStepBtn.title = scenarioRunStepBtn.title = hint;
      }
    })
  }
  function setupUiForStopCondition() {
    var stopCondSelLabelEl = dom.createLabeledSelect({name:"selectStopCond", labelText:"Stop condition"}),
        stopCondSelectEl = stopCondSelLabelEl.lastElementChild,
        stopCondNames = Object.keys( sim.model.stopConditions);
    document.querySelector("form#scenario div.field-group").appendChild( stopCondSelLabelEl);
    dom.fillSelectWithOptionsFromStringList( stopCondSelectEl, stopCondNames);
    if (sim.config.stopConditionName) stopCondSelectEl.value = sim.config.stopConditionName;
    stopCondSelectEl.addEventListener("change", function () {
      sim.config.stopConditionName = stopCondSelectEl.value;
    })
  }
  /*********************************************************************
   Create title/headings
   **********************************************************************/
  // Set HTML title
  if (!document.title){
    document.title = "OES - " + (sim.scenario.name || sim.model.name);
  }
  //oes.ui.createFrontMatter();
  oes.ui.createModelMenu();  // rendered underneath the model title
  /*********************************************************************
   Set up a start UI based on the objects sim.scenario, sim.model and sim.config
   *********************************************************************/
  try {
    sim.ui["scenario"] = new oBJECTvIEW({
      heading: sim.scenario.title ? sim.scenario.title : i18n.t("Default scenario"),
      modelObjects: {"scenario":sim.scenario, "model":sim.model, "config":sim.config},
      fields: [["scenario.simulationEndTime", "scenario.warmUpTime", "config.stepDuration", "config.createLog",
          "config.visualize"]],
      userActions: {
        "run": function () {
          var statFormEl = document.forms["expost-statistics"],
              expFormEl = document.forms["experiment"],
              locale = i18n.accessLang ? i18n.accessLang : "en-US",
              numFmt = new Intl.NumberFormat( locale),
              statGraphWidth=0, worker=null, msg={}, changedModelVarValues={};
          var progressContainer = dom.createProgressBar(i18n.t("Executing the simulation scenario..."));
          document.body.appendChild( progressContainer);
          // log simulation start time (in the main thread)
          sim.startTime = (new Date()).getTime();
          // drop experiment form
          if (expFormEl) expFormEl.remove();
          // hide simulation parameters and show simulator controls
          document.forms["scenario"].style.display = "none";
          document.forms["sim"].style.display = "block";
          // show simulation log table
          if (sim.config.createLog && oes.ui.fullUI) {
            oes.ui.setupSimLog();
          }
          // disable continue and reset buttons
          document.forms["sim"].elements["continue"].disabled = true;
          document.forms["sim"].elements["restart"].disabled = true;
          // compute statistics compression factor
          if (statistics) {
            // since the width is not reported when display=none, we need to reset display
            statFormEl.style.display = "block";
            statGraphWidth = parseInt( getComputedStyle( statFormEl).width);
            statFormEl.style.display = "none";
            oes.stat.prepareTimeSeriesCompression( statGraphWidth);
          }
          if (!sim.config.runInMainThread && !sim.config.visualize &&
              !sim.config.stopConditionName && window.Worker) {
            // start the scenario simulation worker
            worker = new Worker("simulation-worker.js");
            msg = {runExperiment: false, endTime: sim.scenario.simulationEndTime,
              createLog: sim.config.createLog};
            Object.keys( sim.model.v).forEach( function (varName) {
              if (sim.model.v[varName].value !== undefined) {
                changedModelVarValues[varName] = sim.model.v[varName].value;
              }
            });
            if (Object.keys( changedModelVarValues).length > 0) {
              msg.changedModelVarValues = changedModelVarValues;
            }
            worker.postMessage( msg);
            // on incoming messages from worker
            worker.onmessage = function (e) {
              // update progress bar
              if (e.data.progressIncrement !== undefined) {
                document.querySelector("#progress-container > progress").value += e.data.progressIncrement;
                // update step/time info only when updating the progress bar
                sim.ui["sim"].dataBinding["step"].value = numFmt.format( e.data.simStep);
                sim.ui["sim"].dataBinding["time"].value = numFmt.format( e.data.simTime);
              } else if (e.data.simTime !== undefined) {
                // receive step log message
                sim.logStep( e.data);
              } else if (e.data.simStat !== undefined) {
                // receive ex post statistics at simulation end
                sim.stat = e.data.simStat;
                oes.ui.updateUiOnSimulationEnd();
              }
            };
          } else {  // start main thread simulator
            sim.runScenario();
          }
        },
        "run step": function () {
          var i=0;
          sim.mode = "SingleStepSim";
          // hide simulation parameters and show simulator controls
          document.forms["scenario"].style.display = "none";
          document.forms["sim"].style.display = "block";
          // hide stop button
          document.forms["sim"].elements["stop"].style.display = "none";
          // show simulation log table
          if (sim.config.createLog && oes.ui.fullUI) {
            oes.ui.setupSimLog();
          }
          if (!sim.step) sim.initializeSimulationRun();
          for (i=1; i <= sim.stepIncrement; i++) {
            sim.runScenarioStep();
          }
        },
        "defineExperiment": function () {
          var experimentsUiPanelEl = document.getElementById("experimentsUI"),
              mainContentEl = null;
          if (experimentsUiPanelEl) {
            mainContentEl = experimentsUiPanelEl.lastElementChild;
          } else return;
          experimentsUiPanelEl.style.display = "block";
          // display the panel's main content element
          mainContentEl.style.display = "block";
          // hide +Experiment button
          document.forms["scenario"].defineExperiment.style.display = "none";
          try {
            sim.experiment.experimentNo = 1;
            sim.ui["experiments"] = new oBJECTvIEW({
              modelObject: sim.experiment,
              //fields: [["experimentNo", "title", "replications", "parameters"]],
              fields: [[
                {name: "experimentNo",
                  label: sim.experiment.properties["experimentNo"].label,
                  range: sim.experiment.properties["experimentNo"].range,
                  inputOutputMode: "O"},
                "experimentTitle", "replications", "parameterDefs"]],
              suppressNoValueFields: false,
              userActions: {
                "save": function () {
                  alert("Sorry! Defining new experiments is not yet implemented!");
                }
              }
            });
            /* sim.ui["experiments"].userActions["save"].label = "Save"; */
            // render view and store its data binding
            sim.ui["experiments"].dataBinding = sim.ui["experiments"].render( mainContentEl);
          } catch (e) {
            console.log( e.constructor.name +": "+ e.message);
          }
        }
      }
    });
  } catch (e) {
    console.log( e.constructor.name +": "+ e.message);
  }
  sim.ui["scenario"].userActions["run"].label = "►";
  sim.ui["scenario"].userActions["run"].hint = i18n.t("Run simulation scenario");
  sim.ui["scenario"].userActions["run step"].label = "⏯";  // = \u23EF = "&#9199;"
  sim.ui["scenario"].userActions["run step"].hint = i18n.t("Execute a single simulation step");
  sim.ui["scenario"].userActions["defineExperiment"].label = i18n.t("+Experiment");
  sim.ui["scenario"].userActions["defineExperiment"].hint = i18n.t("Define an experiment");
  sim.ui["scenario"].userActions["defineExperiment"].showCondition = function () {
    return !sim.experiment.replications;
  };
  // render the view and store its databinding
  sim.ui["scenario"].dataBinding = sim.ui["scenario"].render( mainEl);
  // show simulation time unit
  el = document.querySelector("form#scenario input[name*='simulationEndTime']");
  if (sim.model.timeUnit) {
    el2 = document.createElement("span");
    el2.title = oes.ui.timeUnitLabels[sim.model.timeUnit];
    el2.textContent = sim.model.timeUnit;
    dom.insertAfter( el2, el);
  }
  // show step duration time unit (ms)
  el = document.querySelector("form#scenario input[name*='stepDuration']");
  if (el) dom.insertAfter( document.createTextNode(" ms"), el);
  setupUiForStepIncrem("scenario");
  if (typeof sim.model.stopConditions === "object" && Object.keys( sim.model.stopConditions).length > 0) {
      setupUiForStopCondition();
  }
  /*********************************************************************
   Set up UI for showing/modifying model variables within scenario UI
   **********************************************************************/
  if (Object.keys( sim.model.v).length > 0) {
    oes.ui.setupModelVariablesUI( document.forms["scenario"]);
  }
  /*TODO: Is this needed? (outcommented 9.7.2020)
  // possibly re-create initial objects/events
  if (!(sim.scenario.initialState.objects && sim.scenario.initialState.events) &&
      typeof sim.scenario.setupInitialState === "function") {
    sim.scenario.setupInitialState();
  }
  */
  /*********************************************************************
   Set up UI for modifying the initial objects within scenario UI
   **********************************************************************/
  if (Object.keys( sim.objects).length > 0 && !sim.config.suppressInitialStateUI) {
    oes.ui.setupInitialObjectsUI( document.forms["scenario"]);
  }
  /*********************************************************************
   Set up UI for modifying the initial events within scenario UI
  **********************************************************************/
  if (!sim.FEL.isEmpty() && !sim.config.suppressInitialStateUI) {
    oes.ui.setupInitialEventsUI( document.forms["scenario"]);
  }
  /*********************************************************************
   Set up UI for defining/modifying experiments within scenario UI
  **********************************************************************/
  if (sim.experiment.replications && !sim.config.suppressExperimentsUI) {
    oes.ui.setupExperimentsUI( document.forms["scenario"]);
    oes.ui.setupExportStatisticsDefUI( document.forms["scenario"]);
  }

  /*********************************************************************
   Simulator Control UI
   *********************************************************************/
  try {
    sim.ui["sim"] = new oBJECTvIEW({
      modelObject: sim,
      fields: [[
        { name:"step", inputOutputMode:"O"},  // property-based
        { name:"time", inputOutputMode:"O"}   // property-based
      ]],
      suppressNoValueFields: false,  // always render fields (even without a value)
      userActions: {
        "stop": function () {
          sim.stopRequested = true;
        },
        "continue": function () {
          if (sim.mode === "SingleStepSim") sim.mode = "MainThreadSim";
          document.forms["sim"].elements["stop"].disabled = false;
          sim.runScenarioStep();
        },
        "run step": function () {
          var i=0;
          sim.mode = "SingleStepSim";
          for (i=1; i <= sim.stepIncrement; i++) {
            sim.runScenarioStep();
          }
        },
        "restart": function () {
          var scenarioRunStepBtn = null, simRunStepBtn = null;
          document.getElementById("sim").remove();
          ["scenario","experiment","visCanvas","expost-statistics","simLogTbl"].forEach( function (elemID) {
            if (document.getElementById( elemID)) document.getElementById( elemID).remove();
          });
          //oes.ui.reset();
          document.getElementsByTagName("footer")[0].remove();
          oes.setupFrontEndSimEnv();
          sim.step = 0;
          sim.stepIncrement = 1;  // for single-step(s) mode
          scenarioRunStepBtn = document.forms["scenario"].elements["run step"];
          simRunStepBtn = document.forms["sim"].elements["run step"];
          simRunStepBtn.title = scenarioRunStepBtn.title = i18n.t("Execute a single simulation step");
        }
      }
    });
    sim.ui["sim"].userActions["stop"].label = "⏹";  // = "&#9209;"
    sim.ui["sim"].userActions["stop"].hint = i18n.t("Stop simulation");
    sim.ui["sim"].userActions["continue"].label = "►";  // = "&#9654;"
    sim.ui["sim"].userActions["continue"].hint = i18n.t("Continue simulation");
    sim.ui["sim"].userActions["run step"].label = sim.ui["scenario"].userActions["run step"].label;
    sim.ui["sim"].userActions["run step"].hint = sim.ui["scenario"].userActions["run step"].hint;
    sim.ui["sim"].userActions["restart"].label = "⏮";  // = "&#9198;"
    sim.ui["sim"].userActions["restart"].hint = i18n.t("Reset simulation");
    // render view and store its data binding
    sim.ui["sim"].dataBinding = sim.ui["sim"].render( mainEl);
    el = document.querySelector("form#sim output[name*='time']");
    if (sim.model.timeUnit) {
      dom.insertAfter( document.createTextNode(" "+ sim.model.timeUnit), el);
    }
    setupUiForStepIncrem("sim");
    document.forms["sim"].style.display = "none";
  } catch (e) {
    console.log( e.constructor.name +": "+ e.message);
  }
  /*********************************************************************
   Set up UIs for Visualization, Event Appearances and User Interaction
   **********************************************************************/
  if (sim.config.visualize) {
    oes.ui.setupVisualization();
    if (sim.config.observationUI.eventAppearances &&
        Object.keys( sim.config.observationUI.eventAppearances).length > 0) {
      oes.ui.setupEventAppearances();
    }
    if (sim.scenario.userInteractions &&
        Object.keys( sim.scenario.userInteractions).length > 0 &&
        sim.config.userInteractive) {
      oes.ui.setupUserInteraction();
    }
  } else sim.config.userInteractive = false;  // no visual. implies no usr interaction
  //TODO: set up UI for ex-post statistics using oBJECTvIEW
  /*
   try {
   sim.ui["expoststat"] = new oBJECTvIEW({
   modelObject: sim.model.statistics,
   suppressNoValueFields: false,
   userActions: {
   }
   });
   // render view and store its data binding
   sim.ui["expoststat"].dataBinding = sim.ui["expoststat"].render();
   document.forms["expoststat"].style.display = "none";
   } catch (e) {
   console.log( e.constructor.name +": "+ e.message);
   }
   */
  /*********************************************************************
   Set up UI for ex-post statistics
   **********************************************************************/
  if (statistics) {
    el = dom.createElement("form", {id:"expost-statistics"});
    el.style.overflowX = "auto";  // horizontal scrolling
    Object.keys( statistics).forEach( function (statVar) {
      var lbl = statistics[statVar].label, contEl=null;
      if (lbl) {
        // turn it on when there is at least one showTimeSeries variable
        createTimeSeriesChart |= statistics[statVar].showTimeSeries;
        if (!statistics[statVar].showTimeSeries) {
          contEl = dom.createElement("div", {classValues:"I-O-field"});
          contEl.appendChild( dom.createLabeledOutputField({
              name: statVar, labelText: i18n.t( lbl)}));
          el.appendChild( contEl);
        }
      }
    });
    dom.insertAfter( el, document.forms["sim"]);
    document.forms["expost-statistics"].style.display = "none";
    if (createTimeSeriesChart) {
      el = dom.createElement("div", {id:"time-series-chart", classValues:"ct-chart"});
      document.forms["expost-statistics"].appendChild( el);
    }
  }
  // hide UI components that are not relevant for backend simulations
  if (!oes.ui.fullUI) {
    el.style.display = "none";
    document.forms["sim"].elements["stop"].style.display = "none";
    document.forms["sim"].elements["continue"].style.display = "none";
  }
  oes.ui.createPageFooter();
};
/*********************************************************************
 Set up the simulation/experiment log (create initially empty log table)
 **********************************************************************/
oes.ui.setupSimLog = function (isExperiment) {
  var el = document.getElementById("simLogTbl"),
      mainEl = document.querySelector("body > main"),
      N=0, statVarHeadings="", colHeadingsRow="", M=0;
  if (!el) {
    el = document.createElement("table");
    el.id = "simLogTbl";
    mainEl.appendChild( el);
  }
  if (isExperiment) {
    el.classList.add("expStatistics");
    Object.keys( sim.model.statistics).forEach( function (v) {
      var unit="", label = sim.model.statistics[v].label;
      if (sim.model.statistics[v].isSimpleOutputStatistics) {
        unit = sim.model.statistics[v].unit;
        if (unit) unit = " ["+ unit +"]";
        else unit = "";
        N = N+1;
        statVarHeadings += "<th>"+ label+unit +"</th>";
      }
    })
    if (sim.experiment.parameterDefs.length > 0) {
      colHeadingsRow = "<tr><th rowspan='2'>"+ i18n.t("Experiment scenario") +
          "</th><th rowspan='2'>"+ i18n.t("Parameter values") +"</th>" +
          "<th colspan='"+ N +"'>"+ i18n.t("Statistics") +"</th></tr>";
      M = 2;
    } else {
      colHeadingsRow = "<tr><th rowspan='2'>"+ i18n.t("Replication") +"</th>" +
          "<th colspan='"+ N +"'>"+ i18n.t("Statistics") +"</th></tr>";
      M = 1;
    }
    el.innerHTML = "<thead><tr><th colspan='"+ (M+N) + "'>" +
        i18n.t("Experiment Log") +"</th></tr>" +
        colHeadingsRow + "<tr>"+ statVarHeadings +"</tr></thead>";
  } else {
    el.innerHTML = "<thead><tr><th colspan='4'>"+ i18n.t("Simulation Log") +"</th></tr>" +
        "<tr><th colspan='2'>"+ i18n.t("Time") +"</th><th>"+ i18n.t("System State") +
        "</th><th>"+ i18n.t("Future Events") +"</th></tr></thead>";
  }
  sim.ui.logEl = dom.createElement("tbody",{id:"simLog"});
  el.appendChild( sim.ui.logEl);
  el.style.overflowX = "auto";  // horizontal scrolling
}

/*******************************************************
 Reset front-end simulation environment
 *******************************************************
 * @method
 * @author Gerd Wagner
 */
oes.ui.reset = function () {
  // display/hide UI panels
  document.forms["scenario"].style.display = "block";
  document.forms["sim"].style.display = "none";
  if (document.forms["expost-statistics"]) {
    document.forms["expost-statistics"].style.display = "none";
  }
  // enable/disable user action buttons
  document.forms["sim"].elements["stop"].disabled = false;
  document.forms["sim"].elements["continue"].disabled = false;
  // reset simulation log table
  sim.ui.logEl.parentNode.style.display = "none";
  sim.ui.logEl.innerHTML = "";
};
/*******************************************************
 Update Simulation UI on Stop
 *******************************************************
 * @method
 * @author Gerd Wagner
 */
oes.ui.updateUiOnStop = function () {
  // enable/disable user action buttons
  document.forms["sim"].elements["stop"].disabled = true;
  document.forms["sim"].elements["continue"].disabled = false;
  document.forms["sim"].elements["run step"].disabled = false;
  document.forms["sim"].elements["restart"].disabled = false;
};
/*******************************************************
 Update Simulation UI on Simulation End
 *******************************************************
 * @method
 * @author Gerd Wagner
 */
oes.ui.updateUiOnSimulationEnd = function ( isExperiment) {
  if (document.getElementById("progress-container")) {
    document.getElementById("progress-container").remove();
  }
  // log simulation time
  console.log("Execution time: ", ((new Date()).getTime() - sim.startTime) + " ms");
  // enable/disable user action buttons
  document.forms["sim"].elements["stop"].disabled = true;
  document.forms["sim"].elements["continue"].disabled = true;
  document.forms["sim"].elements["run step"].disabled = true;
  document.forms["sim"].elements["restart"].disabled = false;
  if (!isExperiment) {
    if (sim.model.statistics) {
      document.forms["expost-statistics"].style.display = "block";
      oes.ui.showExPostStatistics();
    }
  }
};
/*******************************************************
 Show Ex-Post Statistics
 *******************************************************
 * @method
 * @author Gerd Wagner
 */
oes.ui.showExPostStatistics = function () {
  var statistics = sim.model.statistics,
      chart=null, displayStr="",
      locale = i18n.accessLang ? i18n.accessLang : "en-US",
      numFmt = new Intl.NumberFormat( locale, {maximumFractionDigits:2}),
      showTimeSeries=false,
      legendLabels = [];
  var chartSeries = [], dataT = [], i=0, varName="",
      statVarNames = Object.keys( statistics);
  // determine if there is a time series and set dataT
  for (i=0; i < statVarNames.length; i++) {
    varName = statVarNames[i];
    if (statistics[varName].showTimeSeries) {
      showTimeSeries = true;
      if (sim.timeIncrement) {  // fixed-increment time progression
        for (i=0; i < sim.stat.timeSeries[varName].length; i++) {
          dataT.push(i * sim.timeIncrement);
        }
      } else {  // next-event time progression
        dataT = sim.stat.timeSeries[varName][0];
      }
      break;
    }
  }
  Object.keys( statistics).forEach( function (varName) {
    var statVarDecl = statistics[varName],
        val = sim.stat[varName],
        lbl = statVarDecl.label,
        unit = statVarDecl.unit || "",
        tsScaleFactor = statVarDecl.timeSeriesScalingFactor,
        tsScalFactorLbl = "",
        decPl = 2,  // default
        legendLabel = "";
    var dataY=[];
    if (lbl) {
      if (statVarDecl.showTimeSeries) {
        if (tsScaleFactor) {
          tsScalFactorLbl = String( 1/tsScaleFactor);
        }
        legendLabel = i18n.t( lbl) || varName;
        if (unit || tsScalFactorLbl) legendLabel += " (" + tsScalFactorLbl +" "+ unit + ")";
        legendLabels.push( legendLabel);
        if (sim.timeIncrement) {  // fixed-increment time progression
          dataY = sim.stat.timeSeries[varName];
        } else {  // next-event time progression
          dataY = sim.stat.timeSeries[varName][1];
        }
        chartSeries.push({name: lbl, data: dataY});
      } else {
        if (Array.isArray( val)) {
          decPl = statVarDecl.decimalPlaces || 0;
          // format each number and concatenate them to a display string
          displayStr = val.map( function (v) {
            return (new Intl.NumberFormat( locale, {maximumFractionDigits: decPl})).format(v);
          }).reduce( function (outputStr, v) {return outputStr +" | "+ v});
        } else if (statVarDecl.decimalPlaces) {
          decPl = statVarDecl.decimalPlaces;
          displayStr = new Intl.NumberFormat( locale, {maximumFractionDigits: decPl}).
              format( val);
        } else displayStr = numFmt.format( val);
        if (statVarDecl.unit) displayStr += " " + statVarDecl.unit;
        document.forms["expost-statistics"].elements[varName].value = displayStr;
      }
    }
  });
  // show resource utilization statistics
  if (sim.stat.resUtil && Object.keys( sim.stat.resUtil).length > 0) {
    document.forms["expost-statistics"].appendChild(
        dom.createElement("h2", {content: i18n.t("Resource Utilization")})
    );
    Object.keys( sim.stat.resUtil).forEach( function (actT) {
      var activityTypeLabel = cLASS[actT].label || actT,
          columnLabel = oes.isProcNetModel() ? " (% busy | % blocked)" : " (% busy)";
      document.forms["expost-statistics"].appendChild(
          dom.createElement("h3", {content: i18n.t(activityTypeLabel) + columnLabel})
      );
      Object.keys( sim.stat.resUtil[actT]).forEach( function (objIdStr) {
        //console.log(objIdStr +": "+ sim.stat.resUtil[actT][objIdStr]/sim.time);
        var objName = sim.objects[objIdStr].name || objIdStr,
            contEl = dom.createElement("div", {classValues:"I-O-field"}),
            resUtilInfo = sim.stat.resUtil[actT][objIdStr],
            cumResUtil = typeof resUtilInfo === "object" ? resUtilInfo.busy : resUtilInfo,
            resUtil = Math.round( cumResUtil / sim.scenario.simulationEndTime * 10000) / 100,
            resInfoText = numFmt.format( resUtil) +" %", resBlockTime=0;
        if (typeof resUtilInfo === "object" && resUtilInfo.blocked !== undefined) {
          resBlockTime = Math.round( resUtilInfo.blocked / sim.scenario.simulationEndTime * 10000) / 100;
          resInfoText += " | "+ numFmt.format( resBlockTime) +" %";
        }
        contEl.appendChild( dom.createLabeledOutputField({ name: objIdStr,
            labelText: objName, value: resInfoText}));
        document.forms["expost-statistics"].appendChild( contEl);
      });
    });
  }
  if (showTimeSeries) {
    chart = new Chartist.Line('#time-series-chart', {
        labels: dataT,
        series: chartSeries
      }, {
        showPoint: false,
        lineSmooth: true,
        width: "90%", height: "400px",
        axisX: {
          labelInterpolationFnc: function ( value, index ) {
            var interval = parseInt( dataT.length / 10 );
            return index % interval === 0 ? value : null;
          }
        },
        axisY: {
          //offset: 60,
          /*
          labelInterpolationFnc: function ( value ) {
            return value.toFixed( 2 );
          }
          */
        },
        plugins: [
          // display chart legend
          Chartist.plugins.legend({
            legendNames: legendLabels
          })
        ]}
    );
  }
};

/*******************************************************
 UI for defining the initial state
 *******************************************************
 * Set up UI for model varables
 *
 * @method
 * @author Gerd Wagner
 */
oes.ui.setupModelVariablesUI = function (parentEl) {
  var uiPanelEl = dom.createExpandablePanel({id: "ModelVariablesUI",
      heading: i18n.t("Model Variables"), borderColor:"aqua",
      hint: i18n.accessLang ? i18n.t("ModelVariablesUI hint") :
          "Define/set variables that can be used, for instance, in the initial state " +
          "or as parameters in functions or in an experiment."});
  var mainContentEl = uiPanelEl.lastElementChild;
  var labeledVarDefs={}, vm={};
  sim.config.modelVariablesUI = sim.config.modelVariablesUI || {};
  Object.keys( sim.model.v).forEach( function (varName) {
    if (sim.model.v[varName].label) {
      labeledVarDefs[varName] = sim.model.v[varName];
      labeledVarDefs[varName].label = i18n.t(sim.model.v[varName].label);
    }
  });
  if (Object.keys( labeledVarDefs).length === 0) return;  // nothing to show
  vm = {inputFields: labeledVarDefs};
  // create form element
  mainContentEl.appendChild( oBJECTvIEW.createUiFromViewModel( vm));
  sim.config.modelVariablesUI.userActions = {
    "applyChanges": function () {
      Object.keys( vm.fieldValues).forEach( function (fld) {
        sim.model.v[fld].value = vm.fieldValues[fld];  // this will be used by worker
        sim.v[fld] = vm.fieldValues[fld];
      });
      sim.createInitialObjEvt();
      // redraw visualization of initial state (step 0)
      if (sim.config.visualize) {
        oes.ui.resetCanvas();
        oes.ui.visualizeStep();
      }
    }
  };
  sim.config.modelVariablesUI.userActions["applyChanges"].label = i18n.t("Apply changes");
  // create buttons for userActions
  mainContentEl.appendChild( oBJECTvIEW.createUiElemsForUserActions(
      sim.config.modelVariablesUI.userActions
  ));
  parentEl.appendChild( uiPanelEl);
};
/*******************************************************
 UI for defining the initial state objects
 *******************************************************
 *
 * @method
 * @author Gerd Wagner
 */
oes.ui.setupInitialObjectsUI = function (parentEl) {
  var objTypes = sim.model.objectTypes.concat( oes.predefinedObjectTypes);  // an array
  var uiPanelEl = dom.createExpandablePanel({id:"InitialStateObjectsUI",
      heading: i18n.t("Initial Objects"), borderColor:"aqua",
    // "Delete, create or edit initial objects."
      hint: i18n.accessLang ? i18n.t("InitialStateObjectsUI hint") :
          "Change initial attribute values of objects - as a part of the initial state"
  });
  var contentEl = uiPanelEl.lastElementChild,
      mainEl = dom.createElement("div", {classValues:"xpanel-main"});
  contentEl.appendChild( mainEl);
  sim.scenario.initialStateUI = sim.scenario.initialStateUI || {};
  // create a ClassPopulationWidget for each object type
  objTypes.forEach( function (className) {
    var editProps=[], classPopWidget=null,
        slots = {type: className},
        Class = cLASS[className];
    if (!Class.instances || Object.keys( Class.instances).length === 0) return;
    if (sim.scenario.initialStateUI &&
        sim.scenario.initialStateUI.editableProperties &&
        sim.scenario.initialStateUI.editableProperties[className]) {
      editProps = sim.scenario.initialStateUI.editableProperties[className];
      slots.editProps = editProps;
    }
    classPopWidget = oBJECTvIEW.createRecordTableWidget( slots);
    mainEl.appendChild( classPopWidget);
  });
  sim.scenario.initialStateUI.userActions = {
    "applyChanges": function () {
      alert("Changing the initial state is not yet implemented!");
      /*
      sim.updateInitialStateObjects();
      sim.createInitialObjEvt();
      if (sim.config.visualize) {
        oes.ui.resetCanvas();
        oes.ui.visualizeStep();  // visualize initial state
      }
      */
    }
  };
  sim.scenario.initialStateUI.userActions["applyChanges"].label = i18n.t("Apply changes");
  // create buttons for userActions
  contentEl.appendChild( oBJECTvIEW.createUiElemsForUserActions(
      sim.scenario.initialStateUI.userActions
  ));
  parentEl.appendChild( uiPanelEl);
};
/*******************************************************
 UI for defining the initial state events
 *******************************************************
 *
 * @method
 * @author Gerd Wagner
 */
oes.ui.setupInitialEventsUI = function (parentEl) {
  var evtTypes = sim.model.eventTypes.concat( oes.predefinedEventTypes);  // an array
  var uiPanelEl = dom.createExpandablePanel({id:"InitialStateEventsUI",
      heading: i18n.t("Initial Events"), borderColor:"aqua",
      // "Delete, create or edit initial events."
    hint: i18n.accessLang ? i18n.t("InitialStateEventsUI hint") :
        "Change initial attribute values of events - as a part of the initial state"
  });
  var contentEl = uiPanelEl.lastElementChild,
      mainEl = dom.createElement("div", {classValues:"xpanel-main"});
  var initEvts = sim.FEL.getAllEvents();
  contentEl.appendChild( mainEl);
  sim.scenario.initialEventsUI = sim.scenario.initialEventsUI || {};
  // create a ClassPopulationWidget for each event type
  evtTypes.forEach( function (className) {
    var editProps=[], classPopWidget=null,
        Class = cLASS[className],
        slots = {type: className};
    Class.instances = initEvts.filter(
        function (evt) {return evt.constructor.Name === className;});
    if (Object.keys( Class.instances).length === 0) return;
    if (sim.scenario.initialEventsUI.editableProperties &&
        sim.scenario.initialEventsUI.editableProperties[className]) {
      editProps = sim.scenario.initialEventsUI.editableProperties[className];
      slots.editProps = editProps;
    }
    // add inherited property
    Class.properties["occTime"] = {range:"NonNegativeNumber", label:"Occ. time"};
    classPopWidget = oBJECTvIEW.createRecordTableWidget( slots);
    mainEl.appendChild( classPopWidget);
  });
  sim.scenario.initialEventsUI.userActions = {
    "applyChanges": function () {
      sim.updateInitialStateObjects();
      sim.createInitialObjEvt();
      if (sim.config.visualize) {
        oes.ui.resetCanvas();
        oes.ui.visualizeStep();  // visualize initial state
      }
    }
  };
  sim.scenario.initialEventsUI.userActions["applyChanges"].label = i18n.t("Apply changes");
  // create buttons for userActions
  contentEl.appendChild( oBJECTvIEW.createUiElemsForUserActions(
      sim.scenario.initialEventsUI.userActions
  ));
  parentEl.appendChild( uiPanelEl);
};
/*******************************************************
 UI for Experiments
 *******************************************************
 *
 * @method
 * @author Gerd Wagner
 */
oes.ui.setupExperimentsUI = function (parentEl) {
  var uiPanelEl = dom.createExpandablePanel({id:"experimentsUI",
      heading: i18n.t("Experiments"), borderColor:"chartreuse",
      hint: i18n.accessLang ? i18n.t("experimentsUI hint") :
        "An experiment is defined on top of a scenario by defining (1) the number of replications, " +
        "(2) zero or more experiment parameters (bound to model variables), and " +
        "(3) possibly a list of seed values, one for each replication."
  });
  var mainContentEl = uiPanelEl.lastElementChild;
  parentEl.appendChild( uiPanelEl);
  if (sim.experiment.replications) {  // an experiment has been defined
    try {
      sim.ui["experiments"] = new oBJECTvIEW({
        modelObject: sim.experiment,
        heading: i18n.t("Experiment") +" "+ sim.experiment.experimentNo +
            (sim.experiment.title ? ": "+ i18n.t( sim.experiment.title) : ""),
        fields: [["replications", "parameterDefs"]],
        suppressNoValueFields: true,
        userActions: {
          "run": function () {
            var tbodyEl=null, worker=null, msg={}, changedModelVarValues={};
            var progressContainer = dom.createProgressBar(i18n.t("Executing simulation experiment..."));
            function logExpScenarioRun( data) {
              var rowEl = tbodyEl.insertRow();  // create new table row
              var locale = i18n.accessLang ? i18n.accessLang : "en-US",
                  numFmt = new Intl.NumberFormat( locale,
                      {maximumFractionDigits: oes.defaults.expostStatDecimalPlaces});
              rowEl.insertCell().textContent = data.expScenNo;
              rowEl.insertCell().textContent = data.expScenParamValues.toString();
              Object.keys( data.expScenStat).forEach( function (v) {
                var statVal = data.expScenStat[v], displayStr="",
                    decPl = sim.model.statistics[v].decimalPlaces;
                if (decPl) {
                  displayStr = new Intl.NumberFormat( locale,
                      {maximumFractionDigits:decPl}).format( statVal);
                } else displayStr = numFmt.format( statVal);
                rowEl.insertCell().textContent = displayStr;
              });
            }
            function logSimpleExpRun( data) {
              var nmrOfReplications=0, rowEl=null, i=0;
              var locale = i18n.accessLang ? i18n.accessLang : "en-US";
              if (Object.keys( data.expReplicStat).length === 0) return;
              else nmrOfReplications = data.expReplicStat[Object.keys( data.expReplicStat)[0]].length;
              for (i=0; i < nmrOfReplications; i++) {
                rowEl = tbodyEl.insertRow();  // create new table row
                rowEl.insertCell().textContent = i+1;  // replication No
                Object.keys( data.expReplicStat).forEach( function (varName    ) {
                  var range = sim.model.statistics[varName].range,
                      val = data.expReplicStat[varName][i],
                      decPl = sim.model.statistics[varName].decimalPlaces || oes.defaults.expostStatDecimalPlaces;
                  if (typeof val === "number") {
                    if (cLASS.isIntegerType(range)) val = parseInt(val);
                    else val = (new Intl.NumberFormat( locale, {maximumFractionDigits: decPl})).format(val);
                    rowEl.insertCell().textContent = val;
                  }
                });
              }
              // create footer with summary statistics
              Object.keys( oes.stat.summary).forEach( function (aggr) {
                rowEl = tbodyEl.insertRow();  // create new table row
                rowEl.insertCell().textContent = oes.stat.summary[aggr].label;
                Object.keys( data.expScenStat).forEach( function (varName) {
                  var statVar = sim.model.statistics[varName],
                      range = statVar.range,
                      decPl = statVar.decimalPlaces || oes.defaults.expostStatDecimalPlaces,
                      val = data.expScenStat[varName][aggr];
                  if (statVar.isSimpleOutputStatistics) {
                    if (cLASS.isIntegerType( range)) val = parseInt(val);
                    else val = (new Intl.NumberFormat( locale, {maximumFractionDigits: decPl})).format(val);
                    rowEl.insertCell().textContent = val;
                  }
                });
              });
              /*
              rowEl = tbodyEl.insertRow();  // create new table row
              rowEl.insertCell().textContent = "Average";
              Object.keys( data.expScenStat).forEach( function (varName) {
                var range = sim.model.statistics[varName].range,
                    val = data.expScenStat[varName].average;
                if (cLASS.isIntegerType(range)) val = parseInt( val);
                else val = val.toFixed( oes.defaults.expostStatDecimalPlaces);
                rowEl.insertCell().textContent = val;
              });
              */
            }
            document.body.appendChild( progressContainer);
            // configure experiment log
            oes.ui.setupSimLog( true);
            tbodyEl = sim.ui.logEl;
            // drop scenario form
            document.forms["scenario"].remove();
            // show simulator controls
            document.forms["sim"].style.display = "block";
            // log simulation start time (in the main thread)
            sim.startTime = (new Date()).getTime();
            // start the simulation worker
            if (window.Worker) {
              worker = new Worker("simulation-worker.js");
              // send "experiment mode" message to worker
              msg = {runExperiment: true,
                     endTime: sim.scenario.simulationEndTime,
                     expReplications: sim.experiment.replications,
                     dbName: sim.model.name};
              Object.keys( sim.model.v).forEach( function (varName) {
                if (sim.model.v[varName].value !== undefined) {
                  changedModelVarValues[varName] = sim.model.v[varName].value;
                }
              });
              if (Object.keys( changedModelVarValues).length > 0) {
                msg.changedModelVarValues = changedModelVarValues;
              }
              worker.postMessage( msg);
              // on incoming messages from worker
              worker.onmessage = function (e) {
                if (e.data.expScenNo !== undefined) logExpScenarioRun( e.data);
                if (e.data.expReplicStat !== undefined) logSimpleExpRun( e.data);
                if (e.data.progressIncrement !== undefined) {
                  document.querySelector("#progress-container > progress").value +=
                      e.data.progressIncrement;
                }
                if (e.data.endOfExp) oes.ui.updateUiOnSimulationEnd( true);
              };
            } else {
              alert("Experiment cannot be executed since browser does not support web workers!");
            }
          }
        }
      });
      sim.ui["experiments"].userActions["run"].label = "►";  // i18n.t("Run Experiment")
      sim.ui["experiments"].userActions["run"].hint = i18n.t("Run experiment");
      // render view and store its data binding
      sim.ui["experiments"].dataBinding = sim.ui["experiments"].render( mainContentEl);
    } catch (e) {
      console.log( e.constructor.name +": "+ e.message);
    }
  } else {
    //mainContentEl.innerHTML = "<p>No experiment defined.</p>";
    uiPanelEl.style.display = "none";
  }
};
/*******************************************************
 TODO: UI for Export Statistics
 *******************************************************
 *
 * @method
 * @author Luis Gustavo Nardin
 */
sim.export.header = true;
sim.export.sep = ";";
sim.export.timeSeries = true;
sim.export.defFilename = "definitions.csv";
sim.export.sumFilename = "summary.csv";
sim.export.tsFilename = "timeseries.csv";

oes.ui.setupExportStatisticsDefUI = function (parentEl) {
  var uiPanelEl = dom.createExpandablePanel( {
    id: "exportStatisticsUI",
    heading: "Export exper. results", borderColor: "chartreuse",
    hint: "Export simulation statistics stored in the IndexedDB."
  } );
  var mainContentEl = uiPanelEl.lastElementChild;
  parentEl.appendChild( uiPanelEl);
  sim.ui["export"] = new oBJECTvIEW({
    modelObject: sim.export,
    //heading: "Export Statistics",
    fields: [["header", "sep", "timeSeries"],
             ["defFilename", "sumFilename", "tsFilename"]],
    suppressNoValueFields: false,
    userActions: {
      "Exper. definitions": function () {
        if (!sim.storeMan) {
          oes.setupStorageManagement(sim.model.name);
        }

        sim.storeMan.retrieveAll( oes.ExperimentRun).then( function (runs) {
          if (runs.length > 0) {
            sim.storeMan.retrieveAll( oes.ExperimentScenarioRun).then(
              function (records) {
                var param, output, expScenRun;
                var defHeader, defText, defLine;  // Definitions
                var i, j;

                defText = "";

                // Create output records
                for (i = 0; i < records.length; i++) {
                  defLine = [];
                  expScenRun = new oes.ExperimentScenarioRun(records[i]);
                  param = expScenRun.parameterValueCombination;
                  output = expScenRun.outputStatistics;

                  // Definition Header
                  if (typeof defHeader === "undefined") {
                    defHeader = [];
                    if (sim.export.header) {
                      for (j = 0; j < sim.experiment.parameterDefs.length;
                        j += 1) {
                        defHeader.push(
                          sim.experiment.parameterDefs[j].name);
                      }
                      defText = ["id",
                        "experimentRun",
                        "experimentScenarioNo"].concat( defHeader).join(
                          sim.export.sep) + "\n";
                    }
                  }
                  // Definition Line
                  defLine.push( expScenRun["id"]);
                  defLine.push( expScenRun["experimentRun"]);
                  defLine.push( expScenRun["experimentScenarioNo"]);
                  param.forEach( function (prop) {
                    defLine.push( prop);
                  });

                  defText += defLine.join( sim.export.sep) + "\n";
                }

                // Export data
                util.generateTextFile( sim.export.defFilename, defText);
              }).catch( function (err) {
                console.log( err.name + ": " + err.message);
              });
          }
        });
      },
      "summary": function () {
        if (!sim.storeMan) {
          oes.setupStorageManagement(sim.model.name);
        }

        sim.storeMan.retrieveAll( oes.ExperimentRun).then( function (runs) {
          if (runs.length > 0) {
            sim.storeMan.retrieveAll( oes.ExperimentScenarioRun).then(
              function (records) {
                var param, output, expScenRun;
                var defHeader, defText, defLine;  // Definitions
                var sumHeader, sumText, sumLine;  // Summary
                var statVarName, i, j;

                sumText = "";
                defText = "";

                // Create output records
                for (i = 0; i < records.length; i += 1) {
                  defLine = [];
                  sumLine = [];
                  expScenRun = new oes.ExperimentScenarioRun( records[i]);
                  param = expScenRun.parameterValueCombination;
                  output = expScenRun.outputStatistics;

                  //  Definition Header
                  if (typeof defHeader === "undefined") {
                    defHeader = [];
                    if (sim.export.header) {
                      for (j = 0; j < sim.experiment.parameterDefs.length;
                        j += 1) {
                        defHeader.push(
                          sim.experiment.parameterDefs[j].name);
                      }
                      defText = ["id",
                        "experimentRun",
                        "experimentScenarioNo"].concat( defHeader).join(
                          sim.export.sep) + "\n";
                    }
                  }
                  // Definition Line
                  defLine.push( expScenRun["id"]);
                  defLine.push( expScenRun["experimentRun"]);
                  defLine.push( expScenRun["experimentScenarioNo"]);
                  param.forEach( function (prop) {
                    defLine.push( prop);
                  } );

                  // Summary Header
                  if (typeof sumHeader === "undefined") {
                    sumHeader = [];
                    for (statVarName of Object.keys(output)) {
                      if (typeof output[statVarName] !== "object") {
                        sumHeader.push( statVarName);
                      }
                    }
                    if ( sim.export.header) {
                      sumText = ["id"].concat( sumHeader)
                        .join( sim.export.sep) + "\n";
                    }
                  }
                  // Summary Line
                  sumLine.push( expScenRun["id"] );
                  sumHeader.forEach( function (prop) {
                    sumLine.push( output[prop]);
                  });

                  sumText += sumLine.join( sim.export.sep) + "\n";
                }

                // Export data
                util.generateTextFile( sim.export.sumFilename, sumText);
              }).catch( function (err) {
                console.log( err.name + ": " + err.message);
              });
          }
        });
      },
      "timeseries": function () {
        if (!sim.storeMan) {
          oes.setupStorageManagement( sim.model.name);
        }

        sim.storeMan.retrieveAll( oes.ExperimentRun).then( function (runs) {
          if (runs.length > 0) {
            sim.storeMan.retrieveAll( oes.ExperimentScenarioRun).then(
              function (records) {
                var param, output, expScenRun;
                var tsHeader, tsText="", tsLine, tsVarName, ts; // Time Series
                var i, r, t, v;

                // Create output records
                for (i = 0; i < records.length; i += 1) {
                  expScenRun = new oes.ExperimentScenarioRun( records[i]);
                  param = expScenRun.parameterValueCombination;
                  output = expScenRun.outputStatistics;

                  // Time Series
                  if ( sim.export.timeSeries ) {
                    // Time Series Header
                    if (typeof tsHeader === "undefined") {
                      tsHeader = [];
                      for (tsVarName of Object.keys( output.timeSeries)) {
                        tsHeader.push( tsVarName);
                      }
                      if (sim.export.header) {
                        if (typeof sim.model.timeIncrement !== "undefined") {
                          tsText = ["id", "time"].concat( tsHeader)
                            .join( sim.export.sep) + "\n";
                        } else {
                          tsText = ["id", "time", "variable", "value"]
                            .join( sim.export.sep ) +
                            "\n";
                        }
                      }
                    }

                    // Time Series Line
                    if (tsHeader.length > 0) {

                      ts = [];
                      if (typeof sim.model.timeIncrement !== "undefined") {
                        for(v = 0; v < tsHeader.length; v += 1) {
                          if (output.timeSeries[ tsHeader[v]]) {
                            ts[v] = output.timeSeries[tsHeader[v]];
                          }
                        }

                        for(r = 0, t = 0; r < ts[0].length;
                          r += 1, t += sim.model.timeIncrement) {
                          tsLine = [];
                          tsLine.push( expScenRun["id"]);
                          tsLine.push( t);
                          for(v = 0; v < tsHeader.length; v += 1) {
                            tsLine.push( ts[v][r]);
                          }

                          tsText += tsLine.join( sim.export.sep) + "\n";
                        }
                      } else {
                        for(v = 0; v < tsHeader.length; v += 1) {
                          ts = output.timeSeries[tsHeader[v]];

                          for(r = 0; r < ts[0].length; r += 1) {
                            tsLine = [];
                            tsLine.push( expScenRun["id"]);
                            tsLine.push( ts[0][r]);
                            tsLine.push( tsHeader[v]);
                            tsLine.push( ts[1][r]);
                            tsText += tsLine.join( sim.export.sep) + "\n";
                          }
                        }
                      }
                    }
                  }
                }

                if (sim.export.timeSeries) {
                  util.generateTextFile( sim.export.tsFilename, tsText);
                }
              } ).catch( function (err) {
                console.log( err.name + ": " + err.message);
              } );
          }
        } );
      }
    }
  });
  // render view and store its data binding
  sim.ui["export"].dataBinding = sim.ui["export"].render( mainContentEl);
};
/*******************************************************
 TODO: UI for Expost Statistics
 *******************************************************
 *
 * @method
 * @author Gerd Wagner
 */
oes.ui.setupExpostStatisticsDefUI = function (parentEl) {
  var uiPanelEl = dom.createExpandablePanel({id:"spaceUI", heading:"Space"});
  var mainContentEl = uiPanelEl.lastElementChild;
  parentEl.appendChild( uiPanelEl);
  sim.ui["space"] = new oBJECTvIEW({
    modelObject: sim.model.space,
    fields: [["xMax", "yMax", "zMax"].slice(0,
        oes.space.dimensions[sim.model.space.type])],
    suppressNoValueFields: false,
    userActions: {
      "applyChanges": function () {
        sim.updateInitialStateObjects();
        oes.ui.resetCanvas();
        // visualize initial state (at start of step 0)
        if (sim.config.visualize) oes.ui.visualizeStep();
      }
    }
  });
  sim.ui["space"].userActions["applyChanges"].label = "Apply changes";
  // render view and store its data binding
  sim.ui["space"].dataBinding = sim.ui["space"].render( mainContentEl);
};
/*******************************************************
 Set up the Visualization
 *******************************************************/
oes.ui.setupVisualization = function () {
  var mainEl = document.querySelector("body > main");
  if (sim.model.space.type) {
    oes.ui.setupSpaceView();
  } else if (sim.config.observationUI.type) {  // visualizing a non-spatial model
    switch (sim.config.observationUI.type) {
      case "SVG":
        oes.ui.setupCanvas = oes.ui.vis.SVG.setup;
        oes.ui.resetCanvas = oes.ui.vis.SVG.reset;
        oes.ui.visualizeStep = oes.ui.vis.SVG.visualizeStep;
        break;
      case "Zdog":
        oes.ui.setupCanvas = zdogVis.setup;
        oes.ui.resetCanvas = zdogVis.reset;
        oes.ui.visualizeStep = zdogVis.visualizeStep;
        break;
      default:
        console.log("Invalid visualization type: "+ sim.config.observationUI.visualType);
        sim.config.visualize = false;
    }
  } else sim.config.visualize = false;
  if (sim.config.visualize) oes.ui.setupCanvas( mainEl);

};
/*******************************************************
 Set up the Space Visualization
 *******************************************************/
oes.ui.setupSpaceView = function () {
  if (sim.model.space.type === undefined) throw "No space type defined in *setupSpaceView*";
  switch (sim.model.space.type) {
    // TODO: use (detect?) correct references methods, when other than the DOM
    // visualization "modules" are implemented for IntegerGrid case.
  case "IntegerGrid":
    switch (sim.config.observationUI.spaceView.type) {
    case "threeDim":
      oes.ui.setupCanvas = oes.ui.space.threeDim.Babylon.setup;
      oes.ui.resetCanvas = oes.ui.space.threeDim.Babylon.reset;
      oes.ui.visualizeStep = oes.ui.space.threeDim.Babylon.render;
      break;
    default:
      oes.ui.setupCanvas = oes.ui.space.grid.setup;
      oes.ui.resetCanvas = oes.ui.space.grid.reset;
      oes.ui.visualizeStep = oes.ui.space.grid.i.dom.renderIntegerGrid;
    }
   break;
  // TODO: use (detect?) correct references methods, when other than the DOM
  // visualization "modules" are implemented for ObjectGrid case.
  case "ObjectGrid":
    oes.ui.setupCanvas = oes.ui.space.grid.o.dom.setupObjectGrid;
    oes.ui.resetCanvas = oes.ui.space.grid.reset;
    oes.ui.visualizeStep = oes.ui.space.grid.o.dom.renderObjectGrid;
    break;
  case "1D":
    switch (sim.config.observationUI.spaceView.type) {
    case "oneDimSVG":
      oes.ui.setupCanvas = oes.ui.space.oneDim.SVG.setup;
      oes.ui.resetCanvas = oes.ui.space.oneDim.SVG.reset;
      oes.ui.visualizeStep = oes.ui.space.oneDim.SVG.renderSimState;
      break;
    case "threeDim":
      oes.ui.setupCanvas = oes.ui.space.threeDim.Babylon.setup;
      oes.ui.resetCanvas = oes.ui.space.threeDim.Babylon.reset;
      oes.ui.visualizeStep = oes.ui.space.threeDim.Babylon.render;
      break;
    // defaults to oneDimSVG visualization
    default:
      oes.ui.setupCanvas = oes.ui.space.oneDim.SVG.setup;
      oes.ui.resetCanvas = oes.ui.space.oneDim.SVG.reset;
      oes.ui.visualizeStep = oes.ui.space.oneDim.SVG.renderSimState;
    }
    break;
  case "2D":
    oes.ui.setupCanvas = oes.ui.space.twoDim.Phaser.setup;
    oes.ui.resetCanvas = oes.ui.space.twoDim.Phaser.reset;
    oes.ui.visualizeStep = oes.ui.space.twoDim.Phaser.render;
    break;
  case "3D":
    // TODO: complete when a 3D space is supported.
    break;
  }
};
/*====================================================================================
    S V G
 ==================================================================================== */
oes.ui.vis.SVG.setup = function () {
  var obsUI = sim.config.observationUI,
      fixedElems = obsUI.fixedElements,
      objViews = obsUI.objectViews,
      canvasWidth = obsUI.canvas.width || 600,
      canvasHeight = obsUI.canvas.height || 400,
      canvasSvgEl = svg.createSVG({id:"canvasSVG",
          width: canvasWidth, height: canvasHeight});
  var defsEl = svg.createDefs(),
      mainEl = document.querySelector("body > main");

  function renderInitialObjView( objViewId, objIdStr) {
    var el=null, shapeGroupEl=null, fp=null,
        obj = objIdStr ? sim.objects[objIdStr] : sim.namedObjects[objViewId],
        objView = objViews[objViewId],   // objViews[obj.constructor.Name]
        objViewItems = Array.isArray( objView) ? objView : [objView];
    shapeGroupEl = svg.createGroup();
    objViewItems.forEach( function (itemDefRec) {
      var txt="";
      if (itemDefRec.shapeName) {
        if (itemDefRec.fillPatternImage) {
          fp = itemDefRec.fillPatternImage;
          if (!fp.file.includes("/")) {
            fp.file = oes.defaults.imgFolder + fp.file;
          }
          el = svg.createImageFillPattern( fp.id, fp.file);
          defsEl.appendChild( el);
          itemDefRec.style = "fill: url(#" + fp.id + ");" + itemDefRec.style;
        }
        el = svg.createShapeFromDefRec( itemDefRec, obj);
        itemDefRec.elements[obj.id] = el;
        canvasSvgEl.appendChild( el);
      } else if (itemDefRec.text) {  // objViewItem defines a text element
        if (typeof itemDefRec.text === "function") txt = itemDefRec.text( obj);
        else txt = itemDefRec.text;
        el = svg.createText( itemDefRec.x, itemDefRec.y, txt, itemDefRec.style)
      } else {  // itemDefRec maps enum attribs to lists of visualization items
        Object.keys( itemDefRec).forEach( function (key) {
          var enumIndex = 0, currentEnumViewDefRec = [];
          // ommit special fields
          if (key !== "object" && key !== "element" && key !== "elements") {
            enumIndex = obj[key];  // key is enum attr name
            currentEnumViewDefRec = itemDefRec[key][enumIndex-1];
            itemDefRec[key].forEach( function (shDefRec) {
              var el = oes.ui.vis.SVG.createShapeFromDefRec( shDefRec, obj);
              el.style.display = "none";
              shDefRec.element = el;
              canvasSvgEl.appendChild( el);
              if (shDefRec.canvasBackgroundColor) {
                sim.visualEl.style.backgroundColor = shDefRec.canvasBackgroundColor;
              }
            });
            itemDefRec[key].element = currentEnumViewDefRec.element;
            currentEnumViewDefRec.element.style.display = "block";
          }
        });
      }
      if (el) shapeGroupEl.appendChild( el);
    });
    canvasSvgEl.appendChild( shapeGroupEl);
  }

  // define SVG canvas
  sim.visualEl = dom.createElement("div",{id:"visCanvas", classValues:"uiBlock"});
  if (obsUI.canvas.style) canvasSvgEl.style = obsUI.canvas.style;
  sim.visualEl.appendChild( canvasSvgEl);
  canvasSvgEl.appendChild( defsEl);
  mainEl.appendChild( sim.visualEl);
  if (fixedElems) {  // render fixed elements
    Object.keys( fixedElems).forEach( function (id) {
      var el = oes.ui.vis.SVG.createShapeFromDefRec( fixedElems[id]);
      canvasSvgEl.appendChild( el);
    });
  }
  if (objViews) {  // render initial object views
    Object.keys( objViews).forEach( function (objViewId) {
      var objView = objViews[objViewId];
      if (Array.isArray( objView)) {
        objView.forEach( function (itemDefRec) {
          itemDefRec.elements = {};
        })
      } else {
        objView.elements = {};
      }
      if (sim.model.objectTypes.includes( objViewId)) {
        // an object view for all instances of an object type
        Object.keys( cLASS[objViewId].instances).forEach( function (objIdStr) {
          renderInitialObjView( objViewId, objIdStr);
        })
      } else if (sim.namedObjects[objViewId]) {
        renderInitialObjView( objViewId);
      } else {
        console.error("Object view ID "+ objViewId +
            " does neither correspond to an object type nor an object name.")
      }
    });
  }
};
oes.ui.vis.SVG.reset = function () {
  oes.ui.vis.SVG.visualizeStep();  //TODO: replace with real reset code
};
oes.ui.vis.SVG.visualizeStep = function () {
  var obsUI = sim.config.observationUI,
      objViews = obsUI.objectViews;
  /*************************************************************************/
  function updateObjView( viewId, objIdStr) {
    var itemDefRec={}, shAttribs=[], el=null, i=0, val,
        obj = objIdStr ? sim.objects[objIdStr] : sim.namedObjects[viewId],
        objView = objViews[viewId],
        objViewItems = Array.isArray( objView) ? objView : [objView];
    // objViewItems is a list of view item definition records
    for (i=0; i < objViewItems.length; i++) {
      itemDefRec = objViewItems[i];
      el = itemDefRec.elements[obj.id];
      if (itemDefRec.shapeName) {
        shAttribs = itemDefRec.shapeAttributes;
        Object.keys( shAttribs).forEach( function (attrName) {
          if (typeof shAttribs[attrName] === "function") {
            val = shAttribs[attrName]( obj);
            switch (attrName) {
              case "textContent":
                el.textContent = val;
                break;
              case "file":
                if (!val.includes("/")) {
                  val = oes.defaults.imgFolder + val;
                }
                el.setAttributeNS( svg.XLINK_NS, "href", val);
                break;
              default:
                el.setAttribute( attrName, val);
                break;
            }
          }
        });
      } else {  // objView maps enum attribs to lists of vis item def rec
        Object.keys( itemDefRec).forEach( function (key) {
          var enumIndex=0, currentEnumViewDefRec = {};
          // exclude properties that itemDefRec may also contain
          if (key !== "object" && key !== "element") {
            enumIndex = obj[key];
            if (Number.isInteger( enumIndex) && Array.isArray( itemDefRec[key]) &&
                enumIndex >= 1 && enumIndex <= itemDefRec[key].length) {
              currentEnumViewDefRec = itemDefRec[key][enumIndex-1];
              // hide previous enum view
              itemDefRec[key].element.style.display = "none";
              // display current enum view
              currentEnumViewDefRec.element.style.display = "block";
              // store current enum view element
              itemDefRec[key].element = currentEnumViewDefRec.element;
              if (currentEnumViewDefRec.canvasBackgroundColor) {
                sim.visualEl.style.backgroundColor = currentEnumViewDefRec.canvasBackgroundColor;
              }
            }
          }
        });
      }
    }
  }
  /*************************************************************************/
  Object.keys( objViews).forEach( function (objViewId) {
    if (sim.model.objectTypes.includes( objViewId)) {
      // an object view for all instances of an object type
      Object.keys( cLASS[objViewId].instances).forEach( function (objIdStr) {
        updateObjView( objViewId, objIdStr);
      })
    } else if (sim.namedObjects[objViewId]) {
      updateObjView( objViewId);
    }
  });
};
oes.ui.vis.SVG.createShapeFromDefRec = function (shDefRec) {
  var fn = shDefRec.shapeAttributes.file;
  if (fn && !fn.includes("/")) {
    shDefRec.shapeAttributes.file = oes.defaults.imgFolder + fn;
  }
  return svg.createShapeFromDefRec( shDefRec);
};

/***********************************************************************
 Set up the User Interaction (UIA) Elements
 **********************************************************************/
oes.ui.setupUserInteraction = function () {
  sim.ui.userInteractions = sim.ui.userInteractions || {};
  sim.currentEvents = {};  // map of current events by type
  Object.keys( sim.scenario.userInteractions).forEach( function (trigEvtTypeName) {
    var uiDefRec = sim.scenario.userInteractions[trigEvtTypeName],
        uiContainerEl=null, followupEvents=[], title="";
    //TODO: check if this reset can be dropped: uiDefRec.fieldValues = {};  // reset
    uiDefRec.userActions = {
      "continue": function () {
        var inpFldValues={};  // initialize onEvent parameter record
        Object.keys( uiDefRec.inputFields).forEach( function (inpFldName) {
          // extract input field values from oBJECTvIEW's fieldValues map
          inpFldValues[inpFldName] = uiDefRec.fieldValues[inpFldName];
        });
        uiDefRec.domElem.style.display = "none";
        followupEvents = sim.currentEvents[trigEvtTypeName].onEvent( inpFldValues);
        sim.runScenarioStep( followupEvents);  // restart simulator
      }
    };
    uiDefRec.userActions["continue"].label = "Continue";
    title = uiDefRec.title;

    delete uiDefRec.title;
    uiContainerEl = oBJECTvIEW.createUiFromViewModel( uiDefRec);  // create form element
    uiContainerEl.querySelectorAll("input")[0].setAttribute("autofocus","true");
    uiDefRec.domElem = dom.createDraggableModal({fromElem: uiContainerEl,
        title:title, classValues:"action-required"});
    uiDefRec.domElem.style.display = "none";
  })
};
/*******************************************************
 Set up the Event Appearances (Sound + Animations)
 TODO: support audio/sound
 *******************************************************/
oes.ui.setupEventAppearances = function () {
  var eventAppearances = sim.config.observationUI.eventAppearances;
  sim.ui.animations = sim.ui.animations || {};
  Object.keys( eventAppearances).forEach( function (trigEvtTypeName) {
    var evtAppearDefRec = eventAppearances[trigEvtTypeName],
        evtView = evtAppearDefRec.view,
        domElem=null, animation=null, timingDefRec={};
    if (evtView.imageFile) {
      domElem = document.createElement("img");
      if (!evtView.imageFile.includes("/")) {
        domElem.src = oes.defaults.imgFolder + evtView.imageFile;
      } else {
        domElem.src = evtView.imageFile;
      }
      if (evtView.style) domElem.style = evtView.style;
      sim.visualEl.appendChild( domElem);
	  /*
    } else if (evtView.shapeName === "text") {
      domElem = svg.createText( );  
      canvasSvgEl.appendChild( el);
	  */
    } else {
      domElem = evtView.domElem();
    }
    timingDefRec.duration = evtView.duration || 1000;
    if (evtView.iterations) timingDefRec.iterations = evtView.iterations;
    if (evtView.fill) timingDefRec.fill = evtView.fill;
    animation = domElem.animate( evtView.keyframes, timingDefRec);
    animation.pause();  // do not yet start the animation
    sim.ui.animations[trigEvtTypeName] = animation;  // store the animation handle
  });
};

/*******************************************************
 Templates map
 *******************************************************/
oes.ui.i18n.translations = oes.ui.i18n.translations || {};
oes.ui.i18n.translations["templExec$N$Steps"] = `Execute ${sim.stepIncrement} steps`;
