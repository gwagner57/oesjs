/*******************************************************************************
 * This library file contains several OES foundation elements
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 ******************************************************************************/
var oes = oes || {};
var sim = sim || {};

oes.defaults = {
  license: "CC BY-SA",
  imgFolder: "img/",
  validateOnInput: false,
  expostStatDecimalPlaces: 2,
  timeRoundingDecimalPlaces: 2,
  displayDecimalPlaces: 2
};
oes.predfinedProperties = ["shortLabel", "history"];

oes.Object = new cLASS({
  Name: "oBJECT",
  isAbstract: true,
  properties: {
    "id": {range: "Integer"},
    "name": {range: "NonEmptyString", optional:true}
  },
  methods: {
    "toLogString": function () {
      var str1="", str2="", i=0;
      if (!this.constructor.shortLabel && !this.name) return "";
      else {  // show class name + object ID
        str1 = this.name || this.constructor.shortLabel +"-"+ this.id;
      }
      str2 = "{ ";
      Object.keys( this).forEach( function (key) {
        var propDecl = cLASS[this.constructor.Name].properties[key],
            val = this[key], propLabel="", valStr="", listOfActTypeNames=[];
        if (key==="activityState") {
          listOfActTypeNames = Object.keys( val);
          valStr = JSON.stringify( listOfActTypeNames.map( function (atn) {
            var shortLabel = cLASS[atn].shortLabel;
            return shortLabel || atn;
          }));
          propLabel = "actState";
        } else if (propDecl && propDecl.shortLabel) {
          propLabel = propDecl.shortLabel;
          if (cLASS[propDecl.range]) {  // a reference property
            // is the property multi-valued?
            if (propDecl.maxCard && propDecl.maxCard > 1) {
              if (Array.isArray( val)) {
                valStr = JSON.stringify( val.map( function (o) {return o.id;}));
              } else valStr = JSON.stringify( Object.keys( val));
            } else {  // if the property is single-valued
              valStr = val.id;
            }
          } else {  // if the property is not a reference property
            valStr = this.getValueAsString( key);          }
        }
        if (this[key] !== undefined && propLabel) {
          str2 += (i>0 ? ", " : "") + propLabel +": "+ valStr;
          i = i+1;
        }
      }, this);
      str2 += "}";
      if (str2 === "{ }") str2 = "";
      return str1 + str2;
    }
  }
});
/***
 * Events subsume activities. While instantaneous events have an occTime,
 * activities may not have an occTime on creation, but only a startTime.
 * For events with duration it holds that occTime = startTime + duration.
 */
oes.Event = new cLASS({
  Name: "eVENT",
  isAbstract: true,
  properties: {
    "occTime": {range: "NonNegativeNumber", optional:true},
    "priority": {range: "NonNegativeNumber", optional:true},
    // only meaningful for events with duration
    "startTime": {range: "NonNegativeNumber", optional:true},
    "duration": {range: "NonNegativeNumber", optional:true}
  },
  methods: {
    "toLogString": function () {
      var occT = sim.model.time === "continuous" && sim.timeRoundingFactor ?
          Math.round( this.occTime * sim.timeRoundingFactor) / sim.timeRoundingFactor :
          this.occTime;
      var str1="", str2="", evtStr="", i=0,
          eventTypeName = this.constructor.Name, AT=null,
          propDs={}, slots={};
      switch (eventTypeName) {
      case "aCTIVITYsTART":
        AT = cLASS[this.activityType];
        if (!AT.shortLabel) return "";
        str1 = AT.shortLabel + "Start";
        propDs = AT.properties;
        slots = this.resourceRoles;
        break;
      case "pROCESSINGaCTIVITYsTART":
        break;
      case "aCTIVITYeND":
        AT = cLASS[this.activityType];
        if (!AT.shortLabel) return "";
        str1 = AT.shortLabel + "End";
        propDs = AT.properties;
        slots = {"activityIdRef": this.activityIdRef};
        break;
      default:
        if (!this.constructor.shortLabel) return "";
        str1 = this.constructor.shortLabel;
        propDs = cLASS[eventTypeName].properties;
        slots = this;
      }
      str2 = "{";
      Object.keys( slots).forEach( function (p) {
        var propDecl = propDs[p], val = slots[p], propLabel="", valStr="";
        if (propDecl && propDecl.shortLabel) {
          propLabel = propDecl.shortLabel;
          if (cLASS[propDecl.range]) {  // a reference property
            valStr = val.id;
          } else {  // if the property is not a reference property
            if (typeof val === "number" && !Number.isInteger(val) && sim.timeRoundingFactor) {
              valStr = JSON.stringify( Math.round(
                      val * sim.timeRoundingFactor) / sim.timeRoundingFactor);
            } else valStr = JSON.stringify( val);
          }
        }
        if (val !== undefined && propLabel) {
          str2 += (i>0 ? ", " : "") + propLabel +":"+ valStr;
          i = i+1;
        }
      });
/*
      Object.keys( this).forEach( function (key) {
        var propDecl = cLASS[eventTypeName].properties[key],
            val = this[key], propLabel="", valStr="";
        if (propDecl && propDecl.shortLabel) {
          propLabel = propDecl.shortLabel;
          if (cLASS[propDecl.range]) {  // a reference property
            valStr = val.id;
          } else {  // if the property is not a reference property
            if (typeof val === "number" && !Number.isInteger(val) && sim.timeRoundingFactor) {
              valStr = JSON.stringify( Math.round(
                      val * sim.timeRoundingFactor) / sim.timeRoundingFactor);
            } else valStr = JSON.stringify( val);
          }
        }
        if (this[key] !== undefined && propLabel) {
          str2 += (i>0 ? ", " : "") + propLabel +":"+ valStr;
          i = i+1;
        }
      }, this);
*/
      str2 += "}";
      if (str2 === "{}") str2 = "";
      evtStr = str1 + str2 + "@" + occT;
      return evtStr;
    }
  }
});
// compare function for Array.sort
oes.Event.rank = function (e1, e2) {
  var p1=0, p2=0;
  if (e1.constructor.priority) p1 = e1.constructor.priority;
  if (e2.constructor.priority) p2 = e2.constructor.priority;
  return p2 - p1;
}
/******************************************************************************
 *** Activities Package *******************************************************
 ******************************************************************************/
/**
 *  Activities are events having some duration and using resources. Their duration
 *  may be either pre-set to a fixed value or to a random value (in which case they
 *  have a scheduled end), or it may be determined by the occurrence of an activity
 *  end event that is caused by another simulation event (in which case they have an
 *  open end). The duration of a pre-set duration activity can be defined in 3 ways:
 *  either for all activities of some type AT by a) a class-level attribute
 *  AT.duration or b) a class-level function AT.duration(), or
 *  c) by setting the attribute "duration" of its aCTIVITYsTART event.
 *
 *  Activities may consume, and also produce, resources. The actor(s)
 *  that (jointly) perform(s) an activity can be viewed as (a) special resource(s).
 *  At any simulation step there is a (possibly empty) set of ongoing activities.
 *  The objects that participate in an ongoing activity as resources are in a
 *  certain activity state (e.g., "printing", "service-performing"), in which they
 *  are no more available for other activities that try to allocate them as
 *  resources, if their resource role is exclusive/non-shareable.
 *
 *  For any resource of an activity, its utilization by that activity during
 *  a certain time period is measured by the simulator and can be included
 *  in the ex-post statistics.
 *
 *  An activity type is defined as a subtype of the OES class "aCTIVITY" with a
 *  mandatory class-level method "generateId" and a mandatory class-level attribute
 *  "resourceTypes", and an optional class-level method "duration".
 *
 *  A pre-defined event type oes.ActivityStart is used for creating activity start
 *  events with a constructor parameter "resourceRoles" defining a resource roles map
 *  assigning resource object references to resource role names. When an activity
 *  start event occurs, a JS object representing the activity is created, the
 *  resource roles map is copied to corresponding property slots of the activity,
 *  and the value of the activityState property of all resource objects is updated
 *  by adding the activity type name (the activityState is a set/map of the names
 *  of those types of activities, in which the object is participating).
 */
oes.Activity = new cLASS({
  Name: "aCTIVITY",
  supertypeName: "eVENT",
  isAbstract: true,
  properties: {
    "id": {range: "Integer"},
    // on activity creation resource roles are copied to corresp. property slots
    "resourceRoles": {range: cLASS.Map("oBJECT"), optional:true}
  },
  methods: {}
});
oes.ActivityStart = new cLASS({
  Name: "aCTIVITYsTART",
  supertypeName: "eVENT",
  properties: {
    "activityType": {range: "NonEmptyString"},  //TODO: should allow type names (like IdRefs)
    "resourceRoles": {range: cLASS.Map("oBJECT"), optional:true}
  },
  methods: {
    "toLogString": function () {
      var occT = sim.model.time === "continuous" && sim.timeRoundingFactor ?
          Math.round( this.occTime * sim.timeRoundingFactor) / sim.timeRoundingFactor :
          this.occTime;
      var str1 = cLASS[this.activityType].shortLabel, str2 = "";
      if (!str1) return "";
      str1 += "Start";
      Object.keys( this.resourceRoles).forEach( function (resRole) {
        var resObj = this.resourceRoles[resRole];
        str2 += (resObj.name || String(resObj.id)) +", ";
      }, this);
      return str1 +"("+ str2.slice(0, -2) +")" + "@" + occT;
    },
    "onEvent": function () {
      var slots={}, acty=null, followupEvents=[];
      var AT = cLASS[this.activityType];
      if (this.duration > 0) slots.duration = this.duration;
      else if (AT.duration) {
        if (typeof AT.duration === "function") slots.duration = AT.duration();
        else slots.duration = AT.duration;
      }
      Object.keys( this.resourceRoles).forEach( function (resRole) {
        var resObj = this.resourceRoles[resRole];
        // copy resource def. slots as ref. prop. slots
        if (!slots[resRole]) slots[resRole] = resObj;
        // set activity state for resource object
        if (!resObj.activityState) resObj.activityState = {};
        resObj.activityState[this.activityType] = true;
      }, this);
      slots.id = sim.idCounter++;  // activities need an ID
      slots.startTime = this.occTime;
      // create new activity
      acty = new AT( slots);
      // assign resourceRoles map to new activity
      acty.resourceRoles = this.resourceRoles;
      // register new activity as an ongoing activity
      sim.ongoingActivities[acty.id] = acty;
      // if there is an onActivityStart procedure, execute it
      if (typeof acty.onActivityStart === "function") {
        followupEvents = acty.onActivityStart();
        //Better: followupEvents.push(...pN.onActivityStart());
      }
      if (acty.duration) {
        // schedule activity end event
        slots = {
          occTime: this.occTime + acty.duration,
          activityType: AT.Name,
          activityIdRef: acty.id
        };
        if (this.performer) slots.performer = this.performer;
        followupEvents.push( new oes.ActivityEnd( slots));
      }
      return followupEvents;
    }
  }
});
oes.ActivityEnd = new cLASS({
  Name: "aCTIVITYeND",
  supertypeName: "eVENT",
  properties: {
    "activityType": {range: "NonEmptyString"},
    "activityIdRef": {range: "Integer"}
  },
  methods: {
    "toLogString": function () {
      var occT = sim.model.time === "continuous" && sim.timeRoundingFactor ?
          Math.round( this.occTime * sim.timeRoundingFactor) / sim.timeRoundingFactor :
          this.occTime;
      var str1 = cLASS[this.activityType].shortLabel, str2 = "",
          resourceRoles = sim.ongoingActivities[this.activityIdRef].resourceRoles;
      if (!str1) return "";
      str1 += "End";
      Object.keys( resourceRoles).forEach( function (resRole) {
        var resObj = resourceRoles[resRole];
        str2 += (resObj.name || String(resObj.id)) +", ";
      }, this);
      return str1 +"("+ str2.slice(0, -2) +")" + "@" + occT;
    },
    "onEvent": function () {
      var followupEvents=[];
      var acty = sim.ongoingActivities[this.activityIdRef];  // retrieve activity
      // if there is an onActivityEnd procedure, execute it
      if (acty.onActivityEnd) followupEvents = acty.onActivityEnd();
      // set occTime and duration if there was no pre-set duration
      if (!acty.duration) {
        acty.occTime = this.occTime;
        acty.duration = acty.occTime - acty.startTime;
      }
      // compute resource utilization per resource role
      Object.keys( acty.resourceRoles).forEach( function (resRole) {
        var objIdStr = String(acty[resRole].id),
            resUtilMap = sim.stat.resUtil[this.activityType];
        if (resUtilMap[objIdStr] === undefined) resUtilMap[objIdStr] = 0;
        resUtilMap[objIdStr] += acty.duration;
        // update the activity state of resource objects
        delete acty[resRole].activityState[this.activityType];
      }, this);
      // drop activity from list of ongoing activities
      delete sim.ongoingActivities[String( this.activityIdRef)];
      return followupEvents;
    }
  }
});
/******************************************************************************
 *** Processing Network Package ***********************************************
 ******************************************************************************/
/**
 * Processing nodes are objects that play an resource role in processing
 * activities. The definition of a processing node combines defining both an
 * object (as resource) and an implicit activity type, possibly with
 * duration, resource types and onActivityStart/onActivityEnd event rule methods.
 *
 * A simple processing node has an input queue for processing objects and a
 * successor processing node. Processing objects may be either of a generic
 * type "pROCESSINGoBJECT" or of a model-specific subtype of "pROCESSINGoBJECT"
 * (such as "Customer").
 *
 * A processing node object may be defined with a value for its "duration"
 * attribute or with a "duration" function, applying to its processing activities.
 * If neither a "duration" attribute nor a "duration" function are defined,
 * the exponential distribution with an event rate of 1 is used as a default function
 * for sampling processing durations. By default, a processing node processes one
 * processing object at a time, but it may also have its "capacity" attribute set to
 * a value greater than 1.
 *
 * In the general case, a processing node may have several input object types,
 * and an input queue for each of them, and either a successor processing node or
 * else an (automatically generated) output queue for each type of output object.
 * By default, when no explicit transformation of inputs to outputs is modeled by
 * specifying an outputTypes map, there is no transformation and it holds that
 * outputs = inputs.
 *
 * TODO: Add resourceTypes
 */
oes.ResourceStatusEL = new eNUMERATION( "ResourceStatusEL",
    ["available", "busy", "down", "blocked"] );
oes.ProcessingNode = new cLASS({
  Name: "pROCESSINGnODE", label: "Processing node", shortLabel: "PN",
  supertypeName: "oBJECT",
  properties: {
    "inputQueue": {range:"oBJECT", minCard: 0, maxCard: Infinity, isOrdered:true,
        label:"Input Queue", shortLabel:"inpQ"},
    "inputType": {range:"oBJECTtYPE", optional:true},  // default: "pROCESSINGoBJECT"
    "status": {range: "ResourceStatusEL", shortLabel:"stat",
        initialValue: oes.ResourceStatusEL.AVAILABLE},
    "successorNode": {range: "pROCESSINGnODE|eXITnODE", optional:true},
    "predecessorNode": {range: "pROCESSINGnODE", optional:true},
    "fixedDuration": {range: "PositiveInteger", optional:true},
    "inputBufferCapacity": {range: "PositiveInteger", optional:true},
    // Ex: {"lemons": {type:"Lemon", quantity:2}, "ice": {type:"IceCubes", quantity:[0.2,"kg"]},...
    "inputTypes": {range: cLASS.Map( Object), optional:true},
    // Ex: {"lemonade": {type:"Lemonade", quantity:[1,"l"]}, ...
    "outputTypes": {range: cLASS.Map( Object), optional:true},
    // a map with PN object names as keys and conditions as values for (X)OR/AND splitting
    "successorNodes": {range: cLASS.Map( Function), optional:true}
  },
  methods: {}
});
/**
 * Processing Objects are generic objects that arrive at an entry node of a PN
 * and are processed at one or more processing nodes before they leave the
 * PN at an exit node.
 */
oes.ProcessingObject = new cLASS({
  Name: "pROCESSINGoBJECT",
  supertypeName: "oBJECT",
  properties: {
    "arrivalTime": { range: "Number", label: "Arrival time", shortLabel: "arrT"}
  }
});
/**
 * Processing Activities are activities that have inputs and outputs and are
 * performed by a processing node (as their performer). The input types/roles,
 * output types/roles and duration of a processing activity are defined in its
 * underlying processing node, which is associated via its "procNode" property.
 *
 * A processing node object definition may have a slot for defining a "duration"
 * attribute value or a "duration" function (for sampling a PDF).
 */
oes.ProcessingActivity = new cLASS({
  Name: "pROCESSINGaCTIVITY",
  label: "Processing Activity",
  shortLabel: "Proc",  // for the log
  supertypeName: "aCTIVITY",
  properties: {
    "procNode": {range: "pROCESSINGnODE"}
  },
  methods: {}
});
// define the exponential distribution as the default inter-arrival time
oes.ProcessingActivity.defaultEventRate = 1;
oes.ProcessingActivity.defaultDuration = function () {
  return rand.exponential( oes.ProcessingActivity.defaultEventRate)
};

oes.ProcessingActivityStart = new cLASS({
  Name: "pROCESSINGaCTIVITYsTART",
  supertypeName: "aCTIVITYsTART",
  properties: {
    "procNode": {range: "pROCESSINGnODE"}
  },
  methods: {
    "onConstructionBeforeAssigningProperties": function () {
      // assign fixed (implied) activity type
      this.activityType = "pROCESSINGaCTIVITY";
    },
    "onConstructionAfterAssigningProperties": function () {
      this.resourceRoles = this.resourceRoles || {};
      // make sure that processing node is a resource
      this.resourceRoles["procNode"] = this.procNode;
    },
    "onEvent": function () {
      var pN = this.procNode, slots = {procNode: pN},
          acty=null, followupEvents=[];
      if (!pN.inputQueue[0]) {
        console.log("ProcessingActivityStart with empty inputQueue at "+ pN.name +
            " at step "+ sim.step);
      }
      // create slots for constructing new ProcessingActivity
      if (this.duration) slots.duration = this.duration;
      else if (pN.duration) {
        if (typeof pN.duration === "function") slots.duration = pN.duration();
        else slots.duration = pN.duration;
      } else slots.duration = oes.ProcessingActivity.defaultDuration();
      pN.status = oes.ResourceStatusEL.BUSY;
      Object.keys( this.resourceRoles).forEach( function (resRole) {
        var resObj = this.resourceRoles[resRole];
        // copy resource def. slots as ref. prop. slots
        if (!slots[resRole]) slots[resRole] = resObj;
        // set activity state for resource object
        if (!resObj.activityState) resObj.activityState = {};
        resObj.activityState[this.activityType] = true;
      }, this);
      slots.id = sim.idCounter++;  // activities need an ID
      slots.startTime = this.occTime;
      // create new activity
      acty = new oes.ProcessingActivity( slots);
      acty.resourceRoles = this.resourceRoles;  // assign resourceRoles map
      sim.ongoingActivities[acty.id] = acty;
      // create slots for constructing a ProcessingActivityEnd event
      slots = {
        occTime: this.occTime + acty.duration,
        activityIdRef: acty.id,
        procNode: pN
      };
      // if there is an onActivityStart procedure, execute it
      if (typeof pN.onActivityStart === "function") {
        followupEvents = pN.onActivityStart();
        //Better: followupEvents.push(...pN.onActivityStart());
      }
      // schedule activity end event
      followupEvents.push( new oes.ProcessingActivityEnd( slots));
      return followupEvents;
    }
  }
});
oes.ProcessingActivityEnd = new cLASS({
  Name: "pROCESSINGaCTIVITYeND",
  supertypeName: "aCTIVITYeND",
  properties: {
    "procNode": {range: "pROCESSINGnODE"}
  },
  methods: {
    "onConstructionBeforeAssigningProperties": function () {
      // assign fixed (implied) activity type
      this.activityType = "pROCESSINGaCTIVITY";
    },
    "onEvent": function () {
      var nextNode=null, followupEvt1=null, followupEvt2=null,
          unloaded=false, followupEvents=[], pN = this.procNode;
      // retrieve activity
      var acty = sim.ongoingActivities[this.activityIdRef];
      // if there is an onActivityEnd procedure, execute it
      if (pN.onActivityEnd) followupEvents = pN.onActivityEnd();
      // set occTime and duration if there was no pre-set duration
      if (!acty.duration) {
        acty.occTime = this.occTime;
        acty.duration = acty.occTime - acty.startTime;
      }
      // compute resource utilization per resource role
      Object.keys( acty.resourceRoles).forEach( function (resRole) {
        var objIdStr = String(acty[resRole].id),
            resUtilMap = sim.stat.resUtil[this.activityType];
        if (resUtilMap[objIdStr] === undefined) resUtilMap[objIdStr] = {busy:0, blocked:0};
        resUtilMap[objIdStr].busy += acty.duration;
        // update the activity state of resource objects
        delete acty[resRole].activityState[this.activityType];
      }, this);
      // drop activity from list of ongoing activities
      delete sim.ongoingActivities[String( this.activityIdRef)];
      // the successor node may be dynamically assigned by a.onActivityEnd()
      nextNode = pN.successorNode || acty.successorNode;
      // is the next node a processing node?
      if (nextNode.constructor.Name === "pROCESSINGnODE") {
        // is the next processing node available?
        if (nextNode.inputQueue.length === 1 &&
            nextNode.status === oes.ResourceStatusEL.AVAILABLE) {
          // then allocate next node and start its ProcessingActivity
          nextNode.status = oes.ResourceStatusEL.BUSY;
          followupEvt1 = new oes.ProcessingActivityStart({
            occTime: this.occTime + sim.nextMomentDeltaT,
            procNode: nextNode,
            resourceRoles: acty.resourceRoles || {}
          });
          followupEvents.push( followupEvt1);
        } else if (nextNode.inputBufferCapacity &&
                   nextNode.inputQueue.length < nextNode.inputBufferCapacity) {
          // pop processing object and push it to the input queue of the next node
          nextNode.inputQueue.push( pN.inputQueue.shift());
          unloaded = true;
        } else if (nextNode.inputBufferCapacity &&
            nextNode.inputQueue.length === nextNode.inputBufferCapacity) {
          pN.status = oes.ResourceStatusEL.BLOCKED;
          pN.blockedStartTime = sim.time;
        }
      } else {  // the next node is an exit node
        // pop processing object and push it to the input queue of the next node
        nextNode.inputQueue.push( pN.inputQueue.shift());
        followupEvents.push( new oes.Departure({
          occTime: this.occTime + sim.nextMomentDeltaT,
          exitNode: nextNode
        }));
      }
      if (pN.status === oes.ResourceStatusEL.BUSY) {
        // are there more items in the input queue?
        if (pN.inputQueue.length > 0) {
          followupEvt2 = new oes.ProcessingActivityStart({
            occTime: this.occTime + sim.nextMomentDeltaT,
            procNode: pN,
            resourceRoles: {}
          });
          followupEvents.push( followupEvt2);
          if (unloaded && pN.inputQueue.length === pN.inputBufferCapacity-1 &&
              pN.predecessorNode.status === oes.ResourceStatusEL.BLOCKED) {
            // then unload predecessor node
            pN.inputQueue.push( pN.predecessorNode.inputQueue.shift());
            pN.predecessorNode.status = oes.ResourceStatusEL.AVAILABLE;
            // collect processing node blocked time statistics
            if (sim.stat.resUtil["pROCESSINGaCTIVITY"][pN.predecessorNode.id].blocked === undefined) {
              sim.stat.resUtil["pROCESSINGaCTIVITY"][pN.predecessorNode.id].blocked =
                  sim.time - pN.predecessorNode.blockedStartTime;
            } else {
              sim.stat.resUtil["pROCESSINGaCTIVITY"][pN.predecessorNode.id].blocked +=
                  sim.time - pN.predecessorNode.blockedStartTime;
            }
            pN.predecessorNode.blockedStartTime = 0;  // reset
          }
        } else pN.status = oes.ResourceStatusEL.AVAILABLE;
      }
      return followupEvents;
    }
  }
});
/**
 * Entry nodes are objects that participate in exogenous arrival events
 * leading to the creation of processing objects, which are either routed to a
 * successor node or pushed to an output queue. The definition of an entry
 * node combines defining both a (possibly spatial) object and an associated
 * implicit arrival event type, possibly with an "onArrival" event rule method.
 *
 * Entry node object definitions may include (1) a "successorNode" attribute slot
 * for assigning a successor node to which processing objects are routed; (2) a
 * "maxNmrOfArrivals" attribute slot for defining a maximum number of arrival
 * events after which no more arrival events will be created (and, consequently,
 * the simulation may run out of future events); (3) either an "arrivalRate"
 * attribute slot for defining the event rate parameter of an exponential pdf
 * used for computing the time between two consecutive arrival events, or a per-
 * instance-defined "arrivalRecurrence" method slot for computing the recurrence
 * of arrival events; (4) a per-instance-defined "outputType" slot for defining
 * a custom output type (instead of the default "pROCESSINGoBJECT"). If neither an
 * "arrivalRate" nor an "arrivalRecurrence" method are defined, the exponential
 * distribution with an event rate of 1 is used as a default recurrence.
 *
 * Entry nodes have a built-in (read-only) statistics attribute "nmrOfArrivedObjects"
 * counting the number of objects that have arrived at the given entry node.
 *
 * TODO: If no successor node is defined for an entry node, an output queue is
 * automatically created.
 */
oes.EntryNode = new cLASS({
  Name: "eNTRYnODE", label: "Entry node", shortLabel: "Entry",
  supertypeName: "oBJECT",
  properties: {
    "outputType": {range: "oBJECTtYPE", optional:true},  // default: "pROCESSINGoBJECT"
    "successorNode": {range: "pROCESSINGnODE", optional:true},
    "maxNmrOfArrivals": {range: "PositiveInteger", optional:true},
    "arrivalRate": {range: "Decimal", optional:true},
    "nmrOfArrivedObjects": {range: "NonNegativeInteger", shortLabel: "arrObj", optional:true}
  }
});
/**
 * Exit nodes are objects that participate in departure events leading to the
 * destruction of the departing object. The definition of an exit node combines
 * defining both a (possibly spatial) object and an associated implicit departure
 * event type, possibly with an "onDeparture" event rule method.
 *
 * Exit nodes have two built-in statistics attributes: (1) "nmrOfDepartedObjects"
 * counting the number of objects that have departed at the given exit node, and
 * (2) "cumulativeTimeInSystem" for adding up the times in system of all departed
 * objects.
 */
oes.ExitNode = new cLASS({
  Name: "eXITnODE", label: "Exit node", shortLabel: "Exit",
  supertypeName: "oBJECT",
  properties: {
    "inputQueue": {range:"oBJECT", minCard: 0, maxCard: Infinity, isOrdered:true,
      label:"Input Queue", shortLabel:"inpQ"},
    "nmrOfDepartedObjects": {range: "NonNegativeInteger", shortLabel: "depObj", optional:true},
    "cumulativeTimeInSystem": {range: "NonNegativeDecimal", optional:true}
  }
});
/**
 * Set up PN statistics
 * - for any entry node, define the implicit statistics variable "arrivedObjects"
 * - for any exit node, define the implicit statistics variables "departedObjects"
 *   and "meanTimeInSystem"
 */
oes.setupProcNetStatistics = function () {
  var entryNodes = oes.EntryNode.instances,
      exitNodes = oes.ExitNode.instances;
  if (!sim.model.statistics) sim.model.statistics = {};
  // define default statistics variables for PN entry node statistics
  Object.keys( entryNodes).forEach( function (nodeIdStr) {
    var suppressDefaultEntry=false,
        entryNode = entryNodes[nodeIdStr],
        varName = Object.keys( entryNodes).length === 1 ?
            "arrivedObjects" : entryNode.name +"_arrivedObjects";
    entryNode.nmrOfArrivedObjects = 0;
    if (sim.model.statistics[varName] && !sim.model.statistics[varName].label) {
      // model-defined suppression of default statistics
      suppressDefaultEntry = true;
    }
    if (!suppressDefaultEntry) {
      if (!sim.model.statistics[varName]) sim.model.statistics[varName] = {};
      sim.model.statistics[varName].range = "NonNegativeInteger";
      if (!sim.model.statistics[varName].label) {
        sim.model.statistics[varName].label = "Arrived objects";
      }
      sim.model.statistics[varName].entryNode = entryNode;
      sim.model.statistics[varName].computeOnlyAtEnd = true;
    }
  });
  // define default statistics variables for PN exit node statistics
  Object.keys( exitNodes).forEach( function (nodeIdStr) {
    var suppressDefaultEntry=false,
        exitNode = exitNodes[nodeIdStr],
        varName = Object.keys( exitNodes).length === 1 ?
            "departedObjects" : exitNode.name +"_departedObjects";
    exitNode.nmrOfDepartedObjects = 0;
    if (sim.model.statistics[varName] && !sim.model.statistics[varName].label) {
      // model-defined suppression of default statistics
      suppressDefaultEntry = true;
    }
    if (!suppressDefaultEntry) {
      if (!sim.model.statistics[varName]) sim.model.statistics[varName] = {};
      sim.model.statistics[varName].range = "NonNegativeInteger";
      if (!sim.model.statistics[varName].label) {
        sim.model.statistics[varName].label = "Departed objects";
      }
      sim.model.statistics[varName].exitNode = exitNode;
      sim.model.statistics[varName].computeOnlyAtEnd = true;
    }
    exitNode.cumulativeTimeInSystem = 0;
    varName = Object.keys( exitNodes).length === 1 ?
        "meanTimeInSystem" : exitNode.name +"_meanTimeInSystem";
    if (sim.model.statistics[varName] && !sim.model.statistics[varName].label) {
      // model-defined suppression of default statistics
      suppressDefaultEntry = true;
    }
    if (!suppressDefaultEntry) {
      if (!sim.model.statistics[varName]) sim.model.statistics[varName] = {};
      sim.model.statistics[varName].range = "Decimal";
      if (!sim.model.statistics[varName].label) {
        sim.model.statistics[varName].label = "Mean time in system";
      }
      sim.model.statistics[varName].exitNode = exitNode;
      sim.model.statistics[varName].computeOnlyAtEnd = true;
      sim.model.statistics[varName].expression = function () {
        return exitNode.cumulativeTimeInSystem / exitNode.nmrOfDepartedObjects
      };
    }
  });
};

/**
 * Arrival events are associated with an entry node.
 * They may define a quantity of arrived processing objects, which is 1 by default.
 * Viewing an arrival not as an arrival of processing objects, but as an arrival of
 * a customer order, the quantity attribute would allow to define an order
 * quantity that results in the same quantity of processing objects (or production
 * orders) pushed to the entry node's succeeding processing node.
 */
oes.Arrival = new cLASS({
  Name: "aRRIVAL", label: "Arrival", shortLabel: "Arr",
  supertypeName: "eVENT",
  properties: {
    "entryNode": {range: "eNTRYnODE"},
    "quantity": {range: "PositiveInteger", optional:true}
  },
  methods: {
    "onEvent": function () {
      var occT=0, procObj=null, ProcessingObject=null, followupEvents=[];
      if (this.entryNode.outputType) {
        ProcessingObject = cLASS[this.entryNode.outputType];
      } else {  // default
        ProcessingObject = oes.ProcessingObject;
      }
      // update statistics
      this.entryNode.nmrOfArrivedObjects++;
      // create newly arrived processing object
      procObj = new ProcessingObject({arrivalTime: this.occTime});
      sim.addObject( procObj);
      // invoke onArrival event rule method
      if (this.entryNode.onArrival) followupEvents = this.entryNode.onArrival();
      if (this.entryNode.successorNode) {
        // push newly arrived object to the inputQueue of the next node
        this.entryNode.successorNode.inputQueue.push( procObj);
        // is the follow-up processing node available?
        if (this.entryNode.successorNode.status === oes.ResourceStatusEL.AVAILABLE) {
          this.entryNode.successorNode.status = oes.ResourceStatusEL.BUSY;
          followupEvents.push( new oes.ProcessingActivityStart({
            occTime: this.occTime + sim.nextMomentDeltaT,
            procNode: this.entryNode.successorNode,
            resourceRoles: this.entryNode.resourceRoles || {}
          }));
        }
      }
      // implement the recurrence of aRRIVAL events
      if (!this.entryNode.maxNmrOfArrivals ||
          this.entryNode.nmrOfArrivedObjects < this.entryNode.maxNmrOfArrivals) {
        // has an arrival recurrence function been defined for the entry node?
        if (this.entryNode.arrivalRecurrence) {
          occT = this.occTime + this.entryNode.arrivalRecurrence();
        } else {  // use the default recurrence
          occT = this.occTime + oes.Arrival.defaultRecurrence();
        }
        sim.scheduleEvent( new oes.Arrival({
          occTime: occT, entryNode: this.entryNode}));
      }
      return followupEvents;
    }
  }
});
// define the exponential distribution as the default inter-arrival time
oes.Arrival.defaultEventRate = 1;
oes.Arrival.defaultRecurrence = function () {
  return rand.exponential( oes.Arrival.defaultEventRate);
};
/**
 * Departure events have two participants: an exit node and the departing object.
 */
oes.Departure = new cLASS({
  Name: "dEPARTURE", label:"Departure", shortLabel:"Dep",
  supertypeName: "eVENT",
  properties: {
    "exitNode": {range: "eXITnODE"}
  },
  methods: {
    "onEvent": function () {
      var followupEvents = [];
      // pop processing object from the input queue
      var procObj = this.exitNode.inputQueue.shift();
      // update statistics
      this.exitNode.nmrOfDepartedObjects++;
      this.exitNode.cumulativeTimeInSystem += this.occTime - procObj.arrivalTime;
      // invoke onDeparture event rule method
      if (typeof this.exitNode.onDeparture === "function") {
        followupEvents = this.exitNode.onDeparture();
      }
      // remove object from simulation
      sim.removeObject( procObj);
      return followupEvents;
    }
  }
});
/**
 * Check model constraints
 * @method
 * @author Gerd Wagner
 */
oes.checkProcNetConstraints = function (params) {
  var errMsgs=[], msg="", evts=[];
  // PNC1: nmrOfArrObjects = nmrOfObjectsAtProcNodes + nmrOfObjectsAtExitNodes + nmrOfDepObjects
  var nmrOfArrObjects = Object.keys( oes.EntryNode.instances).reduce( function (res, nodeObjIdStr) {
    return res + sim.objects[nodeObjIdStr].nmrOfArrivedObjects
  }, 0);
  var nmrOfObjectsAtProcNodes = Object.keys( oes.ProcessingNode.instances).reduce( function (res, nodeObjIdStr) {
    return res + sim.objects[nodeObjIdStr].inputQueue.length
  }, 0);
  var nmrOfObjectsAtExitNodes = Object.keys( oes.ExitNode.instances).reduce( function (res, nodeObjIdStr) {
    return res + sim.objects[nodeObjIdStr].inputQueue.length
  }, 0);
  var nmrOfDepObjects = Object.keys( oes.ExitNode.instances).reduce( function (res, nodeObjIdStr) {
    return res + sim.objects[nodeObjIdStr].nmrOfDepartedObjects
  }, 0);
  if (nmrOfArrObjects !== nmrOfObjectsAtProcNodes + nmrOfObjectsAtExitNodes + nmrOfDepObjects) {
    msg = "The object preservation constraint is violated at step "+ sim.step +
        (params && params.add ? params.add : "") +
        " (nmrOfArrObjects: "+ nmrOfArrObjects +
        ", nmrOfObjectsInSystem: "+ String(nmrOfObjectsAtProcNodes+nmrOfObjectsAtExitNodes) +
        ", nmrOfDepObjects: "+ nmrOfDepObjects +")";
    if (params && params.log) console.log( msg);
    else errMsgs.push( msg);
  }
  // PNC2: if a proc. node has a proc. end event, its input queue must be non-empty
  evts = sim.FEL.getEventsOfType("pROCESSINGaCTIVITYeND");
  evts.forEach( function (procEndEvt) {
    var pN = procEndEvt.procNode, inpQ = pN.inputQueue;
    if (inpQ.length === 0 || !inpQ[0]) {
      msg = "At step "+ sim.step +" "+ (params && params.add ? params.add : "") +
          ", the proc. node "+ (pN.name||pN.id) +" has an empty input queue.";
      if (params && params.log) console.log( msg);
      else errMsgs.push( msg);
    }
  });
  return errMsgs;
};
/******************************************************************************
 *** Experiment Classes *******************************************************
 ******************************************************************************/
/**
 * A complex datatype for experiment parameter definitions
 * @author Gerd Wagner
 */
oes.ExperimentParamDef = new cLASS({
  Name: "eXPERIMENTpARAMdEF",
  isComplexDatatype: true,  // do not collect instances
  properties: {
    "name": {range: "Identifier", label:"Name"},
    "values": {
      range: cLASS.ArrayList("Number"),
      label:"Values",
      val2str: function (v) {
        return v.toString();  // JSON.stringify( v);
      },
      str2val: function (str) {
        return JSON.parse( str);
      },
    }
  }
});
/**
 * An experiment is defined for a scenario, which is defined for a model.
 */
oes.ExperimentDef = new cLASS({
  Name: "eXPERIMENTdEF",
  properties: {
    "id": {range: "AutoNumber"},
    "model": {range: "NonEmptyString", label:"Model name", optional:true},
    "scenarioNo": {range: "PositiveInteger", label:"Scenario number"},
    "experimentNo": {range: "PositiveInteger", label:"Experiment number",
        hint:"The sequence number relative to the underlying simulation scenario"},
    "experimentTitle": {range: "NonEmptyString", optional:true, label:"Experiment title"},
    "replications": {range:"PositiveInteger", label:"Number of replications"},
    "parameterDefs": {range: "eXPERIMENTpARAMdEF", minCard: 0, maxCard: Infinity,
        isOrdered:true, label:"Parameter definitions"},
    "seeds": {range: Array, optional:true}  // seeds.length = replications
  }
});
oes.ExperimentDef.idCounter = 0;  // retrieve actual value from IDB

oes.ExperimentRun = new cLASS({
  Name: "eXPERIMENTrUN",
  properties: {
    "id": {range: "AutoNumber", label:"ID"},  // possibly a timestamp
    "experimentDef": {range: "eXPERIMENTdEF", label:"Experiment def."},
    "dateTime": {range: "DateTime", label:"Date/time"}
  }
});
oes.ExperimentRun.getAutoId = function () {
  return (new Date()).getTime();
};

oes.ExperimentScenarioRun = new cLASS({
  Name: "eXPERIMENTsCENARIOrUN",
  properties: {
    "id": {range: "AutoNumber"},  // possibly a timestamp
    "experimentRun": {range: "eXPERIMENTrUN"},
    "experimentScenarioNo": {range: "NonNegativeInteger"},
    "parameterValueCombination": {range: Array},
    "outputStatistics": {range: Object,
      label:"Output statistics",
      val2str: function (v) {
        return JSON.stringify( v);
      },
      str2val: function (str) {
        return JSON.parse( str);
      },
    }
  }
});
oes.ExperimentScenarioRun.getAutoId = function () {
  return (new Date()).getTime();
};

/******************************************************************************
 *** Lists of predefined cLASSes as reserved names for constraint checks ******
 ******************************************************************************/
oes.predefinedObjectTypes = ["oBJECT","pROCESSINGoBJECT","pROCESSINGnODE","eNTRYnODE","eXITnODE"];
oes.predefinedEventTypes = ["eVENT","aCTIVITYsTART","aCTIVITYeND","aRRIVAL",
    "pROCESSINGaCTIVITYsTART","pROCESSINGaCTIVITYeND","dEPARTURE"];
oes.predefinedActivityTypes = ["aCTIVITY","pROCESSINGaCTIVITY"];

/******************************************************************************
 *** OES Model Objects scenario/experiment/config/model/statistics/etc. *******
 ******************************************************************************/
sim.scenario = sim.scenario || {};
// Define the schema of the model object "scenario"
sim.scenario.objectName = "scenario";
sim.scenario.properties = {
    // defaults to Number.MAX_SAFE_INTEGER
    "simulationEndTime": {range:"Time", optional: true, label:"Duration:", hint:"Simulation duration"},
    "name": {range:"NonEmptyString", optional: true, label:"Name", hint:"Scenario name"},
    "title": {range:"NonEmptyString", optional: true, label:"Title", hint:"Scenario title"},
    "shortDescription": {range:"String", optional: true, label:"Scenario description",
        hint:"Short description of the simulation scenario"},
    "creator": {range:"String", optional: true, label:"Creator",
        hint:"Creator of simulation model"},
    "created": {range:"String", optional: true, label:"Created on",
        hint:"Creation date"},
    "modified": {range:"String", optional: true, label:"Modified on",
        hint:"Modification date"},
    "idCounter": {range:"NonNegativeInteger", optional: true, label:"ID counter"},
    "randomSeed": {range:"PositiveInteger", optional: true, label:"Random seed"},
    "warmUpTime": {range:"Time", optional: true, label:"Warm-up period:",
        hint:"Statistics collection starts after the warm-up period"}
};

sim.experiment = {
  objectName: "experiment",
  properties: {
    "experimentNo": {range:"AutoNumber", label:"Experiment number",
        hint:"Automatically assigned sequence number for experiment"},
    "experimentTitle": {range:"String", optional: true, label:"Experiment title"},
    "replications": {range:"PositiveInteger", label:"Number of replications",
        hint:"Number of replications/repetitions per experiment scenario"},
    "parameterDefs": {
        range: "eXPERIMENTpARAMdEF", maxCard: Infinity,
        label:"Experiment parameters",
        hint:"Define experiment parameters by name and value set specification"
    },
    "seeds": {range: Array, optional: true},
  },
  replications: 0,
  parameters: [],
  scenarios:[],  // are created by the simulator
  validate: function () {
    var errMsgs=[], exp = sim.experiment;
    if (exp.replications > 0) {
      if (exp.seeds) {
        if (!Array.isArray( exp.seeds)) {
          errMsgs.push("The experiment 'seeds' parameter must have an array value! Illegal value: "+ JSON.stringify(exp.seeds));
        } else if (exp.seeds.length < exp.replications) {
          errMsgs.push("Not enough seeds for number of replications!");
        }
      }
      if (exp.parameterDefs.length > 0) {
        exp.parameterDefs.forEach( function (paramDef) {
          if (!paramDef.values && !(paramDef.startValue && paramDef.endValue)) {
            errMsgs.push("Experiment parameter "+ paramDef.name +" has neither a 'values' " +
                "nor 'startValue'/'endValue' attribute(s)!");
          }
        });
      }
      if (exp.timeSeriesStatisticsVariables) {
        exp.timeSeriesStatisticsVariables.forEach( function (varName) {
          if (!(varName in sim.model.statistics)) {
            errMsgs.push("'timeSeriesStatisticsVariables' contains a name ("+ varName +") that does not " +
                "correspond to a sim.model.statistics variable!");
          }
        });
      }
    }
    return errMsgs;
  }
};

// Define the schema of the export panel
sim.export = {
  objectName: "export",
  properties: {
    "header": { range: "Boolean", optional: false, label: "Field headers", hint: "Export field headers" },
    "sep": { range: "String", optional: true, label: "Field separator", hint: "Export field separator" },
    "timeSeries": { range: "Boolean", optional: true, label: "Export Time Series" },
    "defFilename": { range: "String", optional: true, label: "Experiment Definition", hint: "Export definition of the experiments" },
    "sumFilename": { range: "String", optional: true, label: "Summary Statistics", hint: "Export summary statistics of the experiments" },
    "tsFilename": { range: "String", optional: true, label: "Time Series Data", hint: "Export time series of the experiments" },
  }
};

// Define the schema of the model object "config"
sim.config = {
  objectName: "config",
  properties: {
    "createLog": {range:"Boolean", optional: true, label:"Log", hint:"Create simulation log? (yes/no)"},
    "visualize": {range:"Boolean", optional: true, initialValue: true, label:"Visualization",
        hint:"Visualize a simulation run? (yes/no)"},
    "stepDuration": {range:"NonNegativeInteger", optional: true, label:"Step duration:",
        hint:"How long is a simulation step to take? [ms]"},
    "userInteractive": {range:"Boolean", optional: true, label:"User-interactive",
      hint:"Enable user interactions? (yes/no)"},
    "runInMainThread": {range:"Boolean", optional: true, label:"Run in main thread",
        hint:"Turn off multithreading? (yes/no)"},
    "stopConditionName": {range: "NonEmptyString", optional: true, label:"Stop condition",
      hint:"A stop condition allows to halt a simulation run in a critical situation " +
      "for observing subsequent simulation steps."}
  },
  validate: function () {
    var errMsgs = [];
    if (!Object.keys(sim.model.stopConditions).includes(sim.config.stopCondition)) {
      errMsgs.push("The name of a stop condition must be from sim.model.stopConditions!");
    }
    return errMsgs;
  }
};

// Define the schema of the model object "sim"
sim.objectName = "sim";
sim.properties = {
  "step": {range:"NonNegativeInteger", label:"Step:", hint:"Simulation step"},
  "time": {range:"Number", label:"Time:", hint:"Simulation time"}
};
sim.stepIncrement = 1;
sim.space = {overlayGrid: {}};

// Define the schema of the observationUI
sim.config.observationUI = {
  objectName: "observationUI",
  canvas: {},
  properties: {
    "spaceView": {range: Object, label: "Space view"},
    "objectViews": {range: Object, label: "Object views"}
  }
};
// define the observationUI.monitor
sim.config.observationUI.monitor = {};
// Define the schema of the observationUI.spaceView
sim.config.observationUI.spaceView = {
  objectName: "spaceView",
  properties: {
    "type": {range: "NonEmptyString", label: "Space view type"},
    "gridCellSize": {range: "PositiveInteger", label: "Grid cell size"}
  }
};
// define the visualization record for non-spatial models
sim.config.observationUI.vis = {SVG:{}};
// define the objectViews definition map
sim.config.observationUI.objectViews = {};
// define the map for runtime objectViews
sim.objectViews = {};

// Define the schema of the model object "model"
sim.model = sim.model || {};
sim.model.v = {};  // definitions of (global) model variables available in sim.v
sim.model.f = {};  // (global) model functions

//TODO: can this be dropped?
oes.defineSimModelSchema = function () {
  sim.model.objectName = "model";
  sim.model.properties = {
    "name": {range:"NonEmptyString", label:"Name"},
    "title": {range:"NonEmptyString", label:"Title", hint:"Model title"},
    "shortDescription": {range:"String", optional: true, label:"Model description",
      hint:"Short description of the simulation model"},
    "systemNarrative": {range:"String", optional: true, label:"System narrative",
        hint:"Narrative of the system under investigation"},
    "license": {range:"String", optional: true, label:"License",
      hint:"Copyright license"},
    "creator": {range:"String", optional: true, label:"Creator",
      hint:"Creator of simulation model"},
    "created": {range:"String", optional: true, label:"Created on",
      hint:"Creation date"},
    "modified": {range:"String", optional: true, label:"Modified on",
      hint:"Modification date"},
    "time": {range:["discrete","continuous"], optional: true,
      label:"Time model", hint:"Either 'discrete' (default) or 'continuous'"},
    "timeUnit": {range:["ms","s","m","h","D","W","M","Y"], optional: true,
        label:"Time unit", hint:"A time unit like 'ms', 's' or 'm'"},
    "timeIncrement": {range:"Decimal", optional: true, label:"Time increment",
        hint:"By default: 1"},
    "objectTypes": {range: Array, label:"Object types"},
    "eventTypes": {range: Array, label:"Event types"},
    "activityTypes": {range: Array, optional: true, label:"Activity types"}
  };
  // Define the schema of the model object "model.space"
  sim.model.space.objectName = "spaceModel";
  sim.model.space.properties = {
      "type": {range:["1D-Grid","IntegerGrid","ObjectGrid","3D-Grid","1D","2D","3D"], label:"Space type"},
      "geometry": {range:["TOROIDAL","EUCLIDEAN"], optional: true, label:"Space geometry",
        hint:"Either 'TOROIDAL' (default) or 'EUCLIDEAN'"},
      "xMax": {range:"NonNegativeInteger", label:"Width", hint:"Maximum x value"},
      "yMax": {range:"NonNegativeInteger", optional: true, label:"Height", hint:"Maximum y value"},
      "zMax": {range:"NonNegativeInteger", optional: true, label:"Depth", hint:"Maximum z value"}
  };
};
sim.model.objectName = "model";
sim.model.properties = {
  "name": {range:"NonEmptyString", label:"Name"},
  "title": {range:"NonEmptyString", label:"Title", hint:"Model title"},
  "shortDescription": {range:"String", optional: true, label:"Model description",
    hint:"Short description of the simulation model"},
  "systemNarrative": {range:"String", optional: true, label:"System narrative",
    hint:"Narrative of the system under investigation"},
  "license": {range:"String", optional: true, label:"License",
    hint:"Copyright license"},
  "creator": {range:"String", optional: true, label:"Creator",
    hint:"Creator of simulation model"},
  "created": {range:"String", optional: true, label:"Created on",
    hint:"Creation date"},
  "modified": {range:"String", optional: true, label:"Modified on",
    hint:"Modification date"},
  "time": {range:["discrete","continuous"], optional: true,
    label:"Time model", hint:"Either 'discrete' (default) or 'continuous'"},
  "timeUnit": {range:["ms","s","m","h","D","W","M","Y"], optional: true,
    label:"Time unit", hint:"A time unit like 'ms', 's' or 'm'"},
  "timeIncrement": {range:"Decimal", optional: true, label:"Time increment",
    hint:"By default: 1"},
  "timeRoundingDecimalPlaces": {range:"NonNegativeInteger", optional: true, label:"Time decimal places",
    hint:"Number of decimal places for rounding the simulation time value"},
  "objectTypes": {range: Array, label:"Object types"},
  "eventTypes": {range: Array, label:"Event types"},
  "activityTypes": {range: Array, optional: true, label:"Activity types"},
  "stopConditions": {range: Object, optional: true, label:"Stop conditions", hint:"Stop conditions " +
    "allow to halt a simulation run in a critical situation for observing subsequent simulation steps."},
};
sim.model.validate = function () {
  var errMsgs = [];
  if (sim.model.timeRoundingDecimalPlaces !== undefined) {
    if (!sim.model.time) {
      sim.model.time = "continuous";
      console.warn("Set sim.model.time = 'continuous' for time rounding!");
    } else if (sim.model.time !== "continuous") {
      errMsgs.push("Time rounding can only be enabled for a model with continuous time!");
    }
  } else if (sim.model.time === "continuous") {
    sim.model.timeRoundingDecimalPlaces = oes.defaults.timeRoundingDecimalPlaces;
  }
  return errMsgs;
};

// Define the schema of the model object "model.space"
sim.model.space = sim.model.space || {};
sim.model.space.objectName = "spaceModel";
sim.model.space.properties = {
  "type": {range:["1D-Grid","IntegerGrid","ObjectGrid","3D-Grid","1D","2D","3D"], label:"Space type"},
  "geometry": {range:["TOROIDAL","EUCLIDEAN"], optional: true, label:"Space geometry",
    hint:"Either 'TOROIDAL' (default) or 'EUCLIDEAN'"},
  "xMax": {range:"NonNegativeInteger", label:"Width", hint:"Maximum x value"},
  "yMax": {range:"NonNegativeInteger", optional: true, label:"Height", hint:"Maximum y value"},
  "zMax": {range:"NonNegativeInteger", optional: true, label:"Depth", hint:"Maximum z value"}
};

// Define the schema of the model object "scenario.initialState"
sim.scenario.initialState = {
  objectName: "initialState",
  properties: {
    // a map, and therefore an instance of Object
    "objects": {range: Object, optional: true, label:"Initial objects"},
    // a map, and therefore an instance of Object
    "events": {range: Object, optional: true, label:"Initial events"}
  },
  validate: function () {
    var errors=[];
    var isProcNetSim = this.objects &&
            Object.keys( this.objects).some( function (objIdStr) {
              return this.objects[objIdStr].typeName === "eNTRYnODE";
            }, this);
    if (!this.events && !isProcNetSim &&
        !(sim.model.timeIncrement || sim.model.OnEachTimeStep)) {
      errors.push("There must be at least one initial event when neither " +
          "a time increment nor an 'OnEachTimeStep' method has been defined.");
    }
    if (!this.objects) return;
    Object.keys( this.objects).forEach( function (objIdStr) {
      var slots={}, Class=null;
      // check object IDs
      if (String( parseInt( objIdStr)) !== objIdStr) {
        errors.push("An object has a non-integer ID: "+ objIdStr);
      }
      slots = this.objects[objIdStr];
      Class = cLASS[slots.typeName];
      Object.keys( slots).forEach( function (p) {
        var decl = Class.properties[p], constrVio=null;
        if (decl) {
          constrVio = cLASS.check( p, decl, slots[p]);
          if (!(constrVio instanceof NoConstraintViolation)) {
            errors.push( slots.typeName +"("+ objIdStr +"): "+ constrVio.message);
          }
        }
      });
    }, this);
    return errors;
  }
};
// Define the schema of the model object "scenario.initialStateUI"
sim.scenario.initialStateUI = {
  objectName: "initialStateUI",
  properties: {
    "editableProperties": {range: Object, label:"Editable properties"}
  },
  validate: function () {
    if (!this.editableProperties) return;
    Object.keys( this.editableProperties).forEach( function (className) {
      // ...
    }, this);
  }
};
sim.scenario.initialStateUI.editableProperties = {};

/**
 * Check correctness of scenario/model definitions
 * @method
 * @author Gerd Wagner
 */
oes.verifySimulation = function () {
  var errMsgs=[];

  function checkModelObject( mo) {  // mo = model object
    var props = mo.properties, errors=[];
    if (!props) {
      errors.push("The model object "+ mo.objectName +" does not have a declaration of 'properties'!");
      return;
    }
    // check property slots
    Object.keys( props).forEach( function (prop) {
      var constrVio = cLASS.check( prop, props[prop], mo[prop]);
      if (!(constrVio instanceof NoConstraintViolation)) {
        errors.push( constrVio.constructor.name +": "+ constrVio.message);
      }
    });
    if (mo.validate) {  // invoke specific check method
      errors.merge( mo.validate());
    }
    if (errors.length > 0) {
      errors.forEach( function (err, i) {
        errors[i] = "["+ mo.objectName +"] " + err;
      });
    }
    return errors;
  }
  // check model definition
  errMsgs.merge( checkModelObject( sim.model));
  // check SPACE model definition
  if (sim.model.space.type) {
    errMsgs.merge( checkModelObject( sim.model.space));
  }
  // check scenario definition
  errMsgs.merge( checkModelObject( sim.scenario));
  // check experiment definition
  if (sim.experiment.replications > 0) errMsgs.merge( checkModelObject( sim.experiment));
  // check initial state definition
  errMsgs.merge( checkModelObject( sim.scenario.initialState));
  // check definitions of STATISTICS variables
  if (sim.model.statistics) {
    Object.keys( sim.model.statistics).forEach( function (varName) {
      var statVar = sim.model.statistics[varName],
          OT = statVar.objectType,
          aggrFunc = statVar.aggregationFunction;
      // statistics variable bound to a model variable
      if (statVar.globalVariable) {
        if (sim.model.v[statVar.globalVariable] === undefined)
          errMsgs.push( "[Statistics] Global model variable <var>"+
              statVar.globalVariable +"</var> has not been defined!");
      }
      if (OT && !sim.model.objectTypes.includes( OT) &&
          !oes.predefinedObjectTypes.includes( OT)) {
        errMsgs.push( "[Statistics] Specified object type <var>"+
            OT +"</var> is not included in sim.model.objectTypes!");
      }
      // variable bound to specific object
      if (statVar.objectIdRef && !sim.objects[statVar.objectIdRef]) {
        errMsgs.push( "[Statistics] Invalid definition of statistics variable <var>"+
            varName +"</var>: There is no object with ID "+ statVar.objectIdRef +"!");
      }
      // aggregation function must be defined
      if (aggrFunc && typeof( oes.stat[aggrFunc]) !== 'function') {
        errMsgs.push( "[Statistics] Invalid definition of statistics variable <var>"+
            varName +"</var>: <code>"+ aggrFunc +
            "</code> is not an admissible aggregation function name!");
      }
      // if variable is bound to a property, objectIdRef or objectType must be provided
      if (statVar.property && !statVar.objectIdRef && !(OT && cLASS[OT])) {
        errMsgs.push( "[Statistics] Invalid definition of statistics variable <var>"+
            varName +"</var>:"+ (!OT ? " object type name missing!" :
                                 !cLASS[OT] ? " object type "+ OT +" not defined!" :
                                 !aggrFunc ? " aggregationFunction missing!":""));
      }
      //TODO: add further checks!
    });
  }
  errMsgs.concat( oes.checkModelConstraints());
  return errMsgs;
};
/**
 * Determine if a simulation is based on a PN model
 */
oes.isProcNetModel = function () {
  var initState = sim.scenario.initialState,
      initialObjDefs = initState.objects,
      initialObj= [], keys=[], key="", i=0;
  if (initialObjDefs) {  // a map of object definitions
    keys = Object.keys( initialObjDefs);
    for (i=0; i < keys.length; i++) {
      key = keys[i];
      initialObj = initialObjDefs[key];
      if (initialObj.typeName === "eNTRYnODE") return true;
    }
  }
  return false;
};
/**
 * Check model constraints
 * @method
 * @author Gerd Wagner
 */
oes.checkModelConstraints = function (params) {
  var errMsgs=[];
  if (sim.model.constraints) {
    Object.keys( sim.model.constraints).forEach( function (constrName) {
      var constraint = sim.model.constraints[constrName], msg="";
      if (!constraint()) {
        msg = "The constraint '"+ constrName +"' is violated at step "+ sim.step;
        if (params && params.log) console.log( msg);
        else errMsgs.push( msg);
      }
    })
  }
  return errMsgs;
};
/**
 * Set up Storage Management
 *
 * @method
 * @author Gerd Wagner
 */
oes.setupStorageManagement = function (dbName) {
  var storageAdapter = {dbName: dbName};
  if (!('indexedDB' in self)) {
    console.log("This browser doesn't support IndexedDB. Falling back to LocalStorage.");
    storageAdapter.name = "LocalStorage";
  } else {
    storageAdapter.name = "IndexedDB";
  }
  sim.storeMan = new sTORAGEmANAGER( storageAdapter);
  //sim.storeMan.createEmptyDb().then( oes.setupFrontEndSimEnv);
  // last step in setupFrontEndSimEnv, then wait for user actions
  sim.storeMan.createEmptyDb([oes.ExperimentRun, oes.ExperimentScenarioRun]).then( function () {
    console.log("Empty IndexedDB created.");
  });
};
/**
 * Set up front-end simulation environment
 *
 * @method
 * @author Gerd Wagner
 */
oes.setupFrontEndSimEnv = function () {
  var errors=[], el=null;
  sim.initializeSimulator();
  // set up initial state
  sim.initializeModelVariables();
  sim.createInitialObjEvt();
  if (Object.keys( oes.EntryNode.instances).length > 0) {
    oes.setupProcNetStatistics();
  }
  // initialize statistics
  if (sim.model.statistics) oes.stat.initialize();
  // check simulation definition constraints
  if (oes.loadManager && oes.loadManager.codeLoadingMode !== "deploy") {
    errors = oes.verifySimulation();
    if (errors.length > 0) {
      el = dom.createElement("div", {id:"errors"});
      el.appendChild( dom.createElement("h1", {content: "Errors"}));
      errors.forEach( function (err) {
        el.appendChild( dom.createElement("p", {content: err}));
      });
      document.body.insertBefore( el, document.body.firstElementChild);
    } else {
      console.log("No errors detected in "+ oes.loadManager.codeLoadingMode + " mode.");
    }
  }
  // set up the UI
  oes.ui.setupUI();
  // visualize initial state (step 0)
  if (sim.config.visualize) oes.ui.visualizeStep();
};
