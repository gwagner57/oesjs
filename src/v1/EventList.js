/*******************************************************************************
 * EventList maintains an ordered list of events using Binary Heap
 * 
 * @copyright Copyright 2018 Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Luis Gustavo Nardin
 ******************************************************************************/
var oes = oes || {};
oes.EventList = function EventList ( a ) {
  this.heap = new BinaryHeap( function ( e ) {
    return e.occTime;
  } );
};
oes.EventList.prototype.add = function ( e ) {
  if (sim.model.timeRoundingDecimalPlaces) {
    e.occTime = Math.round( e.occTime * sim.timeRoundingFactor) /
        sim.timeRoundingFactor;
  }
  this.heap.push( e );
};
oes.EventList.prototype.getNextOccurrenceTime = function () {
  if ( !this.heap.isEmpty() ) {
    return this.heap.getFirst().occTime;
  } else {
    return 0;
  }
};
oes.EventList.prototype.getNextEvent = function () {
  if ( !this.heap.isEmpty() ) {
    return this.heap.pop();
  } else {
    return null;
  }
};
oes.EventList.prototype.getAllEvents = function () {
  return this.heap.content;
};
oes.EventList.prototype.isEmpty = function () {
  return this.heap.isEmpty();
};
oes.EventList.prototype.removeNextEvents = function () {
  var nextTime = 0, nextEvents = [];
  if ( this.heap.isEmpty() ) {
    return [];
  }
  nextTime = this.heap.getFirst().occTime;
  while ( !this.heap.isEmpty() &&
      this.heap.getFirst().occTime === nextTime ) {
    nextEvents.push( this.heap.pop() );
  }
  return nextEvents;
};
oes.EventList.prototype.clear = function ( e ) {
  this.heap.clear();
};

oes.EventList.prototype.containsEventOfType = function ( evtType ) {
  return this.heap.getContent().some( function (evt) {
    return evt.constructor.Name === evtType;
  } );
};
oes.EventList.prototype.getEventsOfType = function ( evtType ) {
  return this.heap.getContent().filter( function (evt) {
    return evt.constructor.Name === evtType;
  } );
};
oes.EventList.prototype.toString = function () {
  var str = "";
  if ( !this.heap.isEmpty() ) {
    str = this.heap.getContent().reduce( function ( serialization, e ) {
      return serialization + ", " + e.toLogString();
    }, "" );
    str = str.slice( 1 );
  }
  return str;
};
