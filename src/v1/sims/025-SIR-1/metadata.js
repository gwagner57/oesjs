var sim = sim || {};
sim.model = sim.model || {};
sim.scenario = sim.scenario || {};
sim.config = sim.config || {};

var oes = oes || {};
oes.ui = oes.ui || {};
oes.ui.explanation = {};

/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "SIR-1";
sim.model.title = "Susceptible-Infectious-Recovered";
sim.model.systemNarrative = "<p></p>";
sim.model.shortDescription = "";
sim.model.source = "";
sim.model.license = "CC BY-NC";
sim.model.creator = "Luis Gustavo Nardin";
sim.model.contributors = "Gerd Wagner";
sim.model.created = "2019-02-28";
sim.model.modified = "2019-02-28";