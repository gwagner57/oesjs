/*******************************************************************************
 * The Plant model class.
 *
 * Plants have a harvest time which varies for each plant and plant type.
 * Plants needs water, measured in Liters per Day, at harvest time. The
 * volume of water consumption per day before harvest time is proportional
 * with the time remaining until harvest time, and starts from 1/2 water
 * consumption at harvest time, when the plant is a "baby plant".
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var Plant = new cLASS({
  Name: "Plant",
  supertypeName: "ObjectInGridSpace",
  properties: {
    "name": {range: "NonEmptyString", label:"Name"},
    "harvestTime": {range: "PositiveInteger", min: 30, max: 180, label: "Harvest time (days)"},
    "waterConsumption" : {range: "Decimal", min: 0.01, max: 10, label: "Water consumption (L/day)"}
  },
  methods: {}
});

// every different plant type (subclass) must implement this method
// - t = temperature (°C),
// - sm = soil moisture (%),
// - rh = relative air humidity (%)
// -
Plant.getOptimalConditions = function () {
  return {t: [0,0], sm: [0,0], rh: [0,0]};
};