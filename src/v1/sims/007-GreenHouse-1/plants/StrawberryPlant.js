/*******************************************************************************
 * The StrawberryPlant model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 *
 * A strawberry plant grows good under the following optimal conditions:
 *
 * - temperature: 16-27°C
 *  (source: http://www.harvesttotable.com/2012/02/growing-strawberries-in-hot-summer-climates/)
 *  (source: https://www.mastergardeners.org/growing-strawberries-home-garden)
 *
 * - soil moisture: 65-75%
 *  (source: http://www.fruit.cornell.edu/berry/production/pdfs/strwaterreqstress.pdf)
 *
 * - relative air humidity: 65-75%, non condensed
 *  (source: http://www.actahort.org/books/567/567_101.htm)
 *
 * Tomato plants needs 90 to 100 days from the "baby stage" until harvest time.
 ******************************************************************************/
var StrawberryPlant = new cLASS({
  Name: "StrawberryPlant",
  supertypeName: "Plant",
  properties: {},
  methods: {
    "waterConsumption" : {range: "Decimal", min: 0.01, max: 0.25,
      initialValue: 0.15, label: "Water consumption (L/day)"}
  }
});

// return the number of days from "baby stage" until harvest time.
StrawberryPlant.getHarvestTime = function () {
  return rand.uniformInt(90,100);
};

// every different plant type (subclass) must implement this method
// t = temperature (°C), sm = soil moisture (%),
// rh = relative air humidity (%)
StrawberryPlant.getOptimalConditions = function () {
  return {t: [16,27], sm: [65,75], rh: [65,75]};
};