/*******************************************************************************
 * The ReadSoilMoisture event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var ReadSoilMoisture = new cLASS({
  Name: "ReadSoilMoisture",
  supertypeName: "eVENT",
  properties: {},
  methods: {
    "onEvent": function () {
      var events = [], occTime = this.occTime;
      var getCell = oes.space.grid.getCell;
      sim.model.v.spaceRegion.forEach(function (region, pos) {
        var oc = region.optimalConditions,
            ocAvg = oc.sm[0] + (oc.sm[1] - oc.sm[0]) / 2;
        // reading sensors...
        region.sensors.sm.forEach(function(sensor, pos) {
          var wp = null, wpIdx = 0;
          var plant = region.plants[pos];
          var cell = getCell(sensor.pos[0], sensor.pos[1]);
          if (sensor.pos[0] < 26 || sensor.pos[0] > 50)
            wpIdx = 0;
          else wpIdx = 1;
          wp = region.actuators.wp[wpIdx];
          // map real world soil moisture in the one perceived by the sensor
          // the sensor reads the value on the cell where the plant is located,
          // (not in the cell where the sensor is located!)
          sensor.read(cell.soilMoisture);
          // soil moisture is too low, need to activate the corresponding water
          // pump which is located in the same block with this soil moisture sensor
          if (sensor.value < oc.sm[0] && !wp.active) {
            events.push(new StartWaterPump({
              occTime: occTime + 1,
              waterPumpRef: wp,
              soilMoisture: sensor.value
            }));
          }
          // stop water pump if active, and soil moisture
          // is over the average optimum soil moisture value
          else if (sensor.value > ocAvg && wp.active) {
            events.push(new StopWaterPump({
              occTime: occTime + 1,
              waterPumpRef: wp,
              soilMoisture: sensor.value
            }));
          }
        });
      });
      return events;
    }
  }
});

ReadSoilMoisture.recurrence = function () {
  return 15; // perform temperature readings every 15 minute
};

ReadSoilMoisture.createNextEvent = function (e) {
  return new ReadSoilMoisture({
    occTime: e.occTime + ReadSoilMoisture.recurrence()
  });
};

