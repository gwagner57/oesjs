/*******************************************************************************
 * The StopHeater event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var StopHeater = new cLASS({
  Name: "StopHeater",
  supertypeName: "eVENT",
  properties: {
    "heaterRef": {range: "Heater", label: "Heater"},
    "temperature": {range: "Decimal", label: "Temperature"}
  },
  methods: {
    "onEvent": function () {
      var h = this.heaterRef;
      h.active = false;
      h.workingCapacity = 0;
      console.log("Day: " + sim.model.v.dayNumber + ", minute: " + sim.step,
        ", the heater with id=" + h.id + ", located in region='"
        + sim.model.v.getSpaceRegionAtCoordinate(h.pos[0], h.pos[1]).name
        + "', cell=(" + h.pos[0] + "," + h.pos[1] + "), was deactivated"
        + "', when temperature=" + this.temperature + ".");
      return [];
    }
  }
});

