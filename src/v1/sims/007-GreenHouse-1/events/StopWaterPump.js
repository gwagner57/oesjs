/*******************************************************************************
 * The StopWaterPump event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var StopWaterPump = new cLASS({
  Name: "StopWaterPump",
  supertypeName: "eVENT",
  properties: {
    "waterPumpRef": {range: "WaterPump", label: "Water pump"},
    "soilMoisture": {range: "Decimal", label: "Soil moisture"}
  },
  methods: {
    "onEvent": function () {
      var wp = this.waterPumpRef;
      if (!wp.active) return [];
      wp.active = false;
      wp.workingCapacity = 0;
      console.log("Day: " + sim.model.v.dayNumber + ", minute: " + sim.step
        + ", the water pump with id=" + wp.id + ", located in region='"
        + sim.model.v.getSpaceRegionAtCoordinate(wp.pos[0], wp.pos[1]).name
        + "', cell=(" + wp.pos[0] + "," + wp.pos[1] + "), was deactivated"
        + " when soilMoisture=" + this.soilMoisture + ".");
      return [];
    }
  }
});