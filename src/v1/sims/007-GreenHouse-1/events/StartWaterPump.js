/*******************************************************************************
 * The StartWaterPump event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var StartWaterPump = new cLASS({
  Name: "StartWaterPump",
  supertypeName: "eVENT",
  properties: {
    "waterPumpRef": {range: "WaterPump", label: "Water pump"},
    "soilMoisture": {range: "Decimal", label: "Soil moisture"}
  },
  methods: {
    "onEvent": function () {
      var wp = this.waterPumpRef;
      if (wp.active) return [];
      wp.active = true;
      wp.workingCapacity = 100;
      console.log("Day: " + sim.model.v.dayNumber + ", minute: " + sim.step
        + ", the water pump with id=" + wp.id + ", located in region='"
        + sim.model.v.getSpaceRegionAtCoordinate(wp.pos[0], wp.pos[1]).name
        + "', cell=(" + wp.pos[0] + "," + wp.pos[1] + "), was activated"
        + " when soilMoisture=" + this.soilMoisture + ".");
      return [];
    }
  }
});

