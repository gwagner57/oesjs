/*******************************************************************************
 * The StartHumidifier event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var StartHumidifier = new cLASS({
  Name: "StartHumidifier",
  supertypeName: "eVENT",
  properties: {
    "humidifierRef": {range: "Humidifier", label: "Humidifier"},
    "humidity": {range: "Decimal", label: "Humidity"},
    "reduceMode": {range: "Boolean", label: "Reduce mode"}
  },
  methods: {
    "onEvent": function () {
      var hum = this.humidifierRef;
      hum.active = true;
      hum.workingCapacity = 100;
      hum.reduceMode = this.reduceMode || false;
      console.log("Day: " + sim.model.v.dayNumber + ", minute: " + sim.step,
        ", the humidifier with id=" + hum.id + ", located in region='"
        + sim.model.v.getSpaceRegionAtCoordinate(hum.pos[0], hum.pos[1]).name
        + "', cell=(" + hum.pos[0] + "," + hum.pos[1] + "), was activated in mode='"
        + (this.reduceMode ? "dehumidification" : "humidification")
        + "', when humidity=" + this.humidity + ".");
      return [];
    }
  }
});

