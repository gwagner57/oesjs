/*******************************************************************************
 * The ReadTemperature event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var ReadTemperature = new cLASS({
  Name: "ReadTemperature",
  supertypeName: "eVENT",
  properties: {},
  methods: {
    "onEvent": function () {
      var events = [], occTime = this.occTime;
      var getCell = oes.space.grid.getCell;
      sim.model.v.spaceRegion.forEach(function (region, pos) {
        var oc = region.optimalConditions,
            ocAvg = oc.t[0] + (oc.t[1] - oc.t[0]) / 2;
        // reading sensors...
        region.sensors.t.forEach(function(sensor, pos) {
          var h = region.actuators.h[pos];
          // map real world temperature in the one perceived by the sensor
          var cell = getCell(sensor.pos[0], sensor.pos[1]);
          sensor.read(cell.temperature);
          // temperature is too low, need to activate the corresponding heater
          // which is located in the same block with this temperature sensor
          if (sensor.value < oc.t[0] && !h.active)
            events.push(new StartHeater({
              occTime: occTime + 1,
              heaterRef: h,
              temperature: sensor.value
            }));
          // stop heater, if active, reaches the middle of the optimum
          // temperature conditions interval
          else if (sensor.value > ocAvg && h.active)
            events.push(new StopHeater({
              occTime: occTime + 1,
              heaterRef: h,
              temperature: sensor.value
            }));
        });
      });
      return events;
    }
  }
});

ReadTemperature.recurrence = function () {
  return 3; // perform temperature readings every 3 minute
};

ReadTemperature.createNextEvent = function (e) {
  return new ReadTemperature({
    occTime: e.occTime + ReadTemperature.recurrence()
  });
};

