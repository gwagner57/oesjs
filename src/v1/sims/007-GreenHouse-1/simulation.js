/*******************************************************
 * Green House.
 * @copyright Copyright 2016 Mircea Diaconescu, BTU (Germany)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
 ********************************************************/
/*******************************************************
 Simulation Parameters
 ********************************************************/
sim.scenario.simulationEndTime = 129600; // one step simulates 1 minute, we simulate ~3 months (90 days).
sim.config.visualize = true;
sim.config.createLog = false;
/*******************************************************
 Simulation Model
 ********************************************************/
sim.scenario.timeUnit = "min";
sim.model.timeIncrement = 1;
//sim.scenario.randomSeed = 7;
// object and event types used by this simulation scenario
sim.model.objectTypes = ["Sensor", "AnalogSensor", "DigitalSensor", "HumiditySensor",
  "SoilMoistureSensor", "TemperatureSensor", "Actuator", "Heater", "Humidifier", "WaterPump",
  "Plant", "StrawberryPlant", "TomatoPlant"];
sim.model.objectTypeFilesInSubfolders = ["sensors/Sensor.js", "sensors/AnalogSensor.js",
  "sensors/DigitalSensor.js", "sensors/HumiditySensor.js", "sensors/SoilMoistureSensor.js",
  "sensors/TemperatureSensor.js", "actuators/Actuator.js", "actuators/Heater.js", "actuators/Humidifier.js",
  "actuators/WaterPump.js", "plants/Plant.js", "plants/StrawberryPlant.js", "plants/TomatoPlant.js"];
sim.model.eventTypes = ["NewDay", "ReadHumidity", "ReadSoilMoisture", "ReadTemperature",
  "StartHeater", "StartHumidifier", "StartWaterPump", "StopHeater", "StopHumidifier", "StopWaterPump"];
sim.model.eventTypeFilesInSubfolders = ["events/NewDay.js", "events/ReadHumidity.js",
  "events/ReadSoilMoisture.js", "events/ReadTemperature.js", "events/StartHeater.js", "events/StartHumidifier.js",
  "events/StartWaterPump.js", "events/StopHeater.js", "events/StopHumidifier.js", "events/StopWaterPump.js"];
// Space model
sim.model.space.type = "ObjectGrid";
// 7 cells per section, 1 cell wall, 1 cell section border,
// total: 9 sections => 77 cells
// Every cell has a width of 10cm
sim.model.space.xMax = 77;
// 7 cells per section, 1 cell wall , 1 cell section border,
// total: 3 sections => 27 cells
// Every cell has a height of 10cm
sim.model.space.yMax = 27;
/*******************************************************
 Global variables
 ********************************************************/
// reference temperature value, used to compute high/low 
// temperatures, also used to compute water vaporisation
// NOTE: this is also used as the start temperature for 
//       every grid cell.
// Measurement unit: °C
sim.model.v.refT = 20;
// soil moisture reference value, used as the start 
// soil moisture value for each cell.
// Measurement unit: %
sim.model.v.refSM = 70;
// relative air humidity reference value, used as the 
// start relative air humidity value for each cell.
// Measurement unit: %
sim.model.v.refRH = 60;
// reference temperature value used to compute water vaporization
// Measurement unit: °C
sim.model.v.refWVT = 20;
// reference water vaporization per minute at a temperature 
// of sim.model.v.refWVT. Value: 1.5% per day (1440 minutes)
// Measurement unit: %
sim.model.v.refWVPM = 0.0010417;
// the lowest temperature value to be reached for the current day
// NOTE: this represents the external world (outside GH) temperature.
// Measurement unit: °C
sim.model.v.lowT = 0;
// the highest temperature value to be reached for the current day
// NOTE: this represents the external world (outside GH) temperature.
// Measurement unit: °C
sim.model.v.highT = 0;
// the thermal isolation factor of the GH (the factor for temperature
// variation in the GH for each 1°C down variation outside the GH
// NOTE: 1 means perfect isolation without any losses
//       0 means no isolation
sim.model.v.thermalIsolationFactor = 0.85;
// the thermal green house effect factor (the factor for temperature
// variation in the GH for each 1°C up variation outside the GH
// NOTE: value = 1 means +1°C outside => +1°C inside
//       value > 1 means a positive factor
//       0 < value < 1 means a negative factor
sim.model.v.greenHouseEffectFactor = 1.25;
// the number of the current day 
//(1 simulation step = 1 minute, 1440 minutes = 1 day)
sim.model.v.dayNumber = 0;
//the number of regions
sim.model.v.numberOfRegions = 2;
// stores data organized per GH region for being easy to access it.
// - the region bounds
// - the sensors
// - the actuators
// - the optimal conditions
sim.model.v.spaceRegion = [];
// given the coordinate of an object, return the 
// index of the zone where the object is located
sim.model.v.getSpaceRegionAtCoordinate = function (x) {
  var region = null;
  sim.model.v.spaceRegion.some(function (r) {
    if (x >= r.bounds[0][0] && x <= r.bounds[1][0]) {
      region = r;
      return true;
    }
  });
  return region;
};
// compute the high and low temperature values for the next day
// NOTE: a Gaussian distribution is used to obtain temperature variations.
sim.model.f.computeNextDayTemperatures = function () {
  // The maximum temperature, during day (20, 38)°C, when
  // the reference temperature (sim.model.v.refT) is 20C
  // mean = 9, standardDeviation = 3
  sim.model.v.highT = Math.round(sim.model.v.refT + rand.normal(9, 3));
  // The minimum temperature, during night (8, 20)°C, when
  // the reference temperature (sim.model.v.refT) is 20C
  // mean = 6, standardDeviation = 2
  sim.model.v.lowT = Math.round(sim.model.v.refT - rand.normal(6, 2));
  // the temperature difference between day and night can't be greater than 20°C
  // NOTE: normally this should not happen anyway, but just in case.
  if (sim.model.v.highT - sim.model.v.lowT > 20)
    sim.model.v.lowT = sim.model.v.highT - 20;
};
/*******************************************************
 Grid cell properties
 ********************************************************/
sim.model.space.gridCellProperties = {
  "soilMoisture": {
    range: "Decimal", min: 0, max: 100,
    initialValue: 0, label: "Soil moisture"
  },
  "temperature": {
    range: "Decimal", min: 0, max: 50,
    initialValue: 0, label: "Temperature"
  },
  "lastDayTemperature": {
    range: "Decimal", min: 0, max: 50,
    initialValue: 0, label: "Last day temperature"
  },
  "humidity": {
    range: "Decimal", min: 0, max: 100,
    initialValue: 0, label: "Air humidity"
  },
  "wall": {range: "Boolean", initialValue: false, label: "Is wall"},
  "grass": {range: "Boolean", initialValue: false, label: "Is grass"}
};
/*******************************************************
 Define the observation UI (space view)
 ********************************************************/
sim.config.observationUI.spaceView = {
  type: "gridDom",
  gridCellSize: function () { // returns a positive integer which represents the number of px for a grid cell
    var xMax = sim.model.space.xMax, cellSize = 0,
        bodyWidth = parseInt(getComputedStyle(document.body).getPropertyValue("width"));
    if (bodyWidth - 6 * xMax > 1) cellSize = parseInt((bodyWidth - 3 * xMax - 1) / xMax);
    // return the cell size, the minimum allowed value is 3
    return Math.max( cellSize, 3);
  },
  gridCellColor: {
    R: ["soilMoisture", function (m, cell) {
      if (cell.wall) return 0;
      else if (cell.grass) return 50;
      // compute color based on the soil moisture value
      return 120 - 0.6 * m;
    }],
    G: ["soilMoisture", function (m, cell) {
      if (cell.wall) return 0;
      else if (cell.grass) return 60;
      // compute color based on the soil moisture value
      return 120 - 0.6 * m;
    }],
    B: ["soilMoisture", function (m, cell) {
      if (cell.wall || cell.grass) return 0;
      // compute color based on the soil moisture value
      return 140 - 0.6 * m;
    }]
  },
  showGridCellInfoOnFlyOver: true
};
/*******************************************************
 Define the observation UI (objects view)
 ********************************************************/
sim.config.observationUI.objectViews = {
  "TomatoPlant": {
    content: "&#127793;",
    color: "green",
    backgroundColor: "white",
    showObjectInfoOnFlyOver: true
  },
  "StrawberryPlant": {
    content: "&#127793;",
    color: "green",
    backgroundColor: "white",
    showObjectInfoOnFlyOver: true
  },
  "WaterPump": {
    roundedCorners: "7px",
    content: "&#128167;",
    color: "yellow",
    backgroundColor: function (o) {
      if (o.active && sim.step % 5 === 0) return "#EC7063";
      else return "blue";
    },
    showObjectInfoOnFlyOver: true
  },
  "Humidifier": {
    roundedCorners: "7px",
    content: "RH",
    backgroundColor: function (o) {
      if (o.active && sim.step % 5 === 0) return "#17A589";
      else return "#D6EAF8";
    },
    color: "black",
    showObjectInfoOnFlyOver: true
  },
  "Heater": {
    roundedCorners: "7px",
    content: "H",
    backgroundColor: function (o) {
      if (o.active && sim.step % 5 === 0) return "#17A589";
      else return "#78281F";
    },
    color: "#F9E79F",
    showObjectInfoOnFlyOver: true
  },
  "SoilMoistureSensor": {
    content: "&#128167;",
    color: "yellow",
    backgroundColor: function (s) {
      var r = sim.model.v.getSpaceRegionAtCoordinate(s.pos[0], s.pos[1]);
      var ocSM = r.optimalConditions.sm;
      if (s.value < ocSM[0]) return "#3498DB"; // too low
      else if (s.value >= ocSM[0] && s.value <= ocSM[1]) return "green"; // optimal
      else return "red"; // too high
    },
    showObjectInfoOnFlyOver: true
  },
  "HumiditySensor": {
    content: "&#9748;",
    color: "yellow",
    backgroundColor: function (s) {
      var r = sim.model.v.getSpaceRegionAtCoordinate(s.pos[0], s.pos[1]);
      var ocRH = r.optimalConditions.rh;
      if (s.value < ocRH[0]) return "#3498DB"; // too low
      else if (s.value >= ocRH[0] && s.value <= ocRH[1]) return "green"; // optimal
      else return "red"; // too high
    },
    showObjectInfoOnFlyOver: true
  },
  "TemperatureSensor": {
    content: "°C",
    backgroundColor: function (s) {
      var r = sim.model.v.getSpaceRegionAtCoordinate(s.pos[0], s.pos[1]);
      var ocT = r.optimalConditions.t;
      if (s.value < ocT[0]) return "#3498DB"; // too low
      else if (s.value >= ocT[0] && s.value <= ocT[1]) return "green"; // optimal
      else return "red"; // too high
    },
    color: "yellow",
    showObjectInfoOnFlyOver: true
  }
};
/*******************************************************
 Define the initial state
 ********************************************************/
sim.scenario.setupInitialState = function () {
  var i = 0, j = 0, pX = 0, pY = 0,
    sIdx = 1, sMSIdx = 1, o = null, c = null;
  var tomatoOC = TomatoPlant.getOptimalConditions(),
    strawberryOC = StrawberryPlant.getOptimalConditions();
  var bounds_r1 = [[2, 2], [50, 26]], r1 = null,
      bounds_r2 = [[53, 2], [76, 26]], r2 = null;
  var getCell = oes.space.grid.getCell;

  // set grid cell properties: moisture, humidity and temperature.
  oes.space.grid.forAllCells( function (x, y, cell) {
    var oc = null;
    // the cells which acts as wall or grass does not have 
    // temperature, soil moisture or relative air humidity
    if (x === 1 || x === 51 || x === 77 || y === 1 || y === 27) cell.wall = true;
    else if (x < 51 && ((x - 2) % 8 === 0 || (y - 2) % 8 === 0)) cell.grass = true;
    else if (x > 51 && ((x - 4) % 8 === 0 || (y - 2) % 8 === 0)) cell.grass = true;
    else {
      if (x >= bounds_r1[0][0] && x <= bounds_r1[1][0])
        oc = tomatoOC;
      else if (x >= bounds_r2[0][0] && x <= bounds_r2[1][0])
        oc = strawberryOC;
      // initially, the soil moisture is at the optimum level for each reach,
      // which means, the middle of the optimal soil moisture interval
      cell.soilMoisture = (oc.sm[0] + oc.sm[1]) / 2;
      // initially, the air temperature is at the optimum level for each reach,
      // which means, the middle of the optimal air temperature interval
      cell.temperature = (oc.t[0] + oc.t[1]) / 2;
      cell.lastDayTemperature = cell.temperature;
      // initially, the relative air humidity is at the optimum level for each reach,
      // which means, the middle of the optimal relative air humidity interval
      cell.humidity = (oc.rh[0] + oc.rh[1]) / 2;
    }
  });

  // tomato plants region ( 6 x 3 blocks, 1 block = 7 x 7 cells + 1 border cell)
  // [left-bottom-x, left-bottom-y], [right-top-x, right-top-y]
  r1 = sim.model.v.spaceRegion[0] = {name: "Tomato-Region", bounds: bounds_r1};
  // optimal conditions for this region
  r1.optimalConditions = TomatoPlant.getOptimalConditions();
  // sensors from this region
  r1.sensors = {t: [], rh: [], sm: []};
  // actuators from this region
  r1.actuators = {h: [], wp: [], hum: []};
  // plants from this region
  r1.plants = [];
  // effects that applies to region
  r1.effects = {t: 0, rh: 0};
  for (j = 0; j < 3; j++) {
    pY = j * 8 + 5;
    for (i = 0; i < 6; i++) {
      pX = i * 8 + 5;
      if (j === 1 && (i === 1 || i === 4)) {
        o = new WaterPump({id: 100 + sIdx, pos: [pX - 1, pY - 1],
          waterFlow: 15, ratedPower: 550});
        sim.addObject(o);
        getCell(o.pos[0], o.pos[1]).addObject(o);
        r1.actuators.wp.push(o);
        o = new Humidifier({id: 110 + sIdx, pos: [pX + 3, pY - 1],
          ratedPower: 1200});
        sim.addObject(o);
        getCell(o.pos[0], o.pos[1]).addObject(o);
        r1.actuators.hum.push(o);
        o = new Heater({id: 120 + sIdx, pos: [pX + 1, pY + 1], ratedPower: 1800});
        sim.addObject(o);
        getCell(o.pos[0], o.pos[1]).addObject(o);
        r1.actuators.h.push(o);
        o = new TemperatureSensor({id: 130 + sIdx, pos: [pX - 1, pY + 3]});
        sim.addObject(o);
        (c = getCell(o.pos[0], o.pos[1])).addObject(o);
        r1.sensors.t.push(o);
        o.read(c.temperature);
        o = new HumiditySensor({id: 140 + sIdx, pos: [pX + 3, pY + 3]});
        o.read(sim.model.v.refRH);
        sim.addObject(o);
        (c = getCell(o.pos[0], o.pos[1])).addObject(o);
        r1.sensors.rh.push(o);
        o.read(c.humidity);
        sIdx++;
      } else {
        o = new TomatoPlant({
          id: sMSIdx,
          harvestTime: TomatoPlant.getHarvestTime(),
          pos: [pX + 1, pY + 1]
        });
        sim.addObject(o);
        getCell(o.pos[0], o.pos[1]).addObject(o);
        r1.plants.push(o);
        o = new SoilMoistureSensor({id: 150 + sMSIdx, pos: [pX + 1, pY]});
        sim.addObject(o);
        (c = getCell(o.pos[0], o.pos[1])).addObject(o);
        r1.sensors.sm.push(o);
        o.read(c.soilMoisture);
        sMSIdx++;
      }
    }
  }
  // strawberry plants - grow rate high: (0.7, 0.9)
  // strawberry plants region ( 3 x 3 blocks, 1 block = 7 x 7 cells + 1 border cell)
  // [left-bottom-x, left-bottom-y], [right-top-x, right-top-y]
  r2 = sim.model.v.spaceRegion[1] = {name: "Strawberry-Region", bounds: bounds_r2};
  // optimal conditions for this region
  r2.optimalConditions = StrawberryPlant.getOptimalConditions();
  // sensors from this region
  r2.sensors = {t: [], rh: [], sm: []};
  // actuators from this region
  r2.actuators = {h: [], wp: [], hum: []};
  // plants from this region
  r2.plants = [];
  // effects that applies to region
  r2.effects = {t: 0, rh: 0};
  for (j = 0; j < 3; j++) {
    pY = j * 8 + 5;
    for (i = 6; i < 9; i++) {
      pX = i * 8 + 5;
      if (j === 1 && i === 7) {
        o = new WaterPump({id: 100 + sIdx, pos: [pX + 1, pY - 1],
          waterFlow: 15, ratedPower: 550});
        sim.addObject(o);
        getCell(o.pos[0], o.pos[1]).addObject(o);
        r2.actuators.wp.push(o);
        o = new Humidifier({id: 110 + sIdx, pos: [pX + 5, pY - 1],
          ratedPower: 1200});
        sim.addObject(o);
        getCell(o.pos[0], o.pos[1]).addObject(o);
        r2.actuators.hum.push(o);
        o = new Heater({id: 120 + sIdx, pos: [pX + 3, pY + 1],
          ratedPower: 1800});
        sim.addObject(o);
        getCell(o.pos[0], o.pos[1]).addObject(o);
        r2.actuators.h.push(o);
        o = new TemperatureSensor({id: 130 + sIdx, pos: [pX + 1, pY + 3]});
        sim.addObject(o);
        (c = getCell(o.pos[0], o.pos[1])).addObject(o);
        r2.sensors.t.push(o);
        o.read(c.temperature);
        o = new HumiditySensor({id: 140 + sIdx, pos: [pX + 5, pY + 3]});
        sim.addObject(o);
        (c = getCell(o.pos[0], o.pos[1])).addObject(o);
        r2.sensors.rh.push(o);
        o.read(c.humidity);
        sIdx++;
      } else {
        o = new StrawberryPlant({
          id: sMSIdx,
          harvestTime: StrawberryPlant.getHarvestTime(),
          pos: [pX + 3, pY + 1]
        });
        sim.addObject(o);
        getCell(o.pos[0], o.pos[1]).addObject(o);
        r2.plants.push(o);
        o = new SoilMoistureSensor({id: 150 + sMSIdx, pos: [pX + 3, pY]});
        sim.addObject(o);
        (c = getCell(o.pos[0], o.pos[1])).addObject(o);
        r2.sensors.sm.push(o);
        o.read(c.soilMoisture);
        sMSIdx++;
      }
    }
  }

};
/********************************************************
 The actions that executes every simulation step
 NOTE: the day starts at 5:00AM
 ********************************************************/
sim.model.OnEachTimeStep = function () {
  var minute = sim.step % 1440;
  var ghIF = sim.model.v.thermalIsolationFactor,
      ghEF = sim.model.v.greenHouseEffectFactor;
  var hT = sim.model.v.highT * ghEF,
      lT = sim.model.v.lowT * (2 - ghIF);

  //apply the actuator effects on the environment
  sim.model.v.spaceRegion.forEach(function (region, pos) {
    var plantsNmr = region.plants.length;
    // heater actuators
    region.effects.t = 0;
    region.actuators.h.forEach(function (heater) {
      // heaters are able to uniformly increase the temperature
      // with 1 degree every 15 minutes, for each 1000w used power,
      // for a region of 8.82m^2, which represents a 42 x 21 cells
      // space (10cm cell size)
      if (heater.active) {
        // 0.00000067 = 1 / (100% * 1000w * 15 minutes)
        region.effects.t += 0.00000067 * heater.ratedPower * heater.workingCapacity;
        // second region is half the size
        if (pos === 1) region.effects.t *= 2;
      }
    });
    // humidifier actuators
    region.effects.rh = 0;
    region.actuators.hum.forEach(function (humidifier) {
      var rh = 0;
      // humidifiers are able to uniformly increase/decrease
      // the humidity with 1% every 6 minutes, for
      // each 1000w used power, for a region of 8.82m^2,
      // which represents a 42 x 21 cells space (10cm cell size)
      if (humidifier.active) {
        rh = 0.00000166667 * humidifier.ratedPower * humidifier.workingCapacity;
        region.effects.rh = humidifier.reduceMode ? -rh : rh;
        // second region is half the size
        if (pos === 1) region.effects.rh *= 2;
      }
    });

    // compute the RH consumption for the plants
    region.plants.forEach(function (plant) {
      var rhPC = 0, pWC = plant.waterConsumption / 1440,
        pHT = plant.harvestTime * 1440;
      // Plants "consumes" also air humidity, by using their leaves.
      // Every plant consume humidity, based on its maturity stage. At harvest time, it
      // 100% of the value of its waterConsumption property, and at start time 50%.
      // The computations is for a number of cells equals with plant width x height,
      // so 42 * 21 cells for tomato region and 21 x 21 for strawberry region,
      // while one plant is 3 x 3 (logical) cells =>  a factor of 0.01 respectively 0.02
      // NOTE1: the formula used to compute RH based on water consumption is
      //        RH = RH + pWC / 3, where pWC represents the
      //        quantity of water that is consumed by the plant.
      // NOTE2: this is just a simplification of reality, where the RH computation
      //        is highly complex, and depends on a multitude of factors (most of
      //        which are unknown in our simulation scenario).
      rhPC = (0.5 * pWC * (1 + sim.step / pHT) * (pos === 0 ? 0.01 : 0.02)) / 3;
      region.effects.rh -= rhPC;
    });
    // compute the soil moisture as the result of watering and
    // using water pumps, and water consumption by the plants
    region.actuators.wp.forEach(function (wp) {
      // Every 1L of water means an increase of 60% soil moisture for one cell (10 x 10 cm).
      // Water distribution in soil is radial, and decreases with 10% every 1 cell increase in radius.
      // The water pump has small pipe connection with every plant in its radius (21 x 21 cells).
      // NOTE: water pumps works at 100% capacity when started, and cannot be adjusted.
      // Every plant consume water, based on its maturity stage. At harvest time, it
      // 100% of the value of its waterConsumption property, and at start time only 50%.
      // The water consumption is also radial, and decreases with 10% every 1 cell increase in radius.
      // Also, 1L water consumption, means 60% decrease in soil moisture.
      var smDecPC = 0.9, range = 3, i = 0;
      var lWPC = 0, wFactor = 0;
      for (i = 0; i <= range; i++) wFactor += Math.pow(smDecPC, i);
      // liters of water in the cell where the plant is located
      // which is the cell that gets the 60% soil moisture for 1L water
      lWPC = wp.active ? ((wp.waterFlow / plantsNmr) / wFactor) * 0.6 : 0;
      region.plants.forEach(function (plant) {
        var pipeAtX = plant.pos[0],
            pipeAtY = plant.pos[1];
        var pWC = plant.waterConsumption / 1440,
            pHT = plant.harvestTime * 1440,
            pWCDec = 0.5 * 0.6 * pWC * (1 + sim.step / pHT) / wFactor;
        var i = 0, n = 1, smInc = 0, getCell = oes.space.grid.getCell;
        // identify the cells containing plants in the reach of this pump
        if (Math.abs(wp.pos[0] - pipeAtX) < 12 && Math.abs(wp.pos[1] - pipeAtY) < 12) {
          getCell(pipeAtX, pipeAtY).soilMoisture += lWPC - pWCDec;
          while (n <= range) {
            smInc = (lWPC - pWCDec) * Math.pow(smDecPC, n);
            for (i = -n; i <= n; i++) {
              getCell(pipeAtX + i, pipeAtY + n).soilMoisture += smInc;
              getCell(pipeAtX + i, pipeAtY - n).soilMoisture += smInc;
              if (i > -n && i < n) {
                getCell(pipeAtX + n, pipeAtY + i).soilMoisture += smInc;
                getCell(pipeAtX - n, pipeAtY + i).soilMoisture += smInc;
              }
            }
            n++;
          }
        }
      });
      // compute water consumption (cubic meters)
      if (wp.active) sim.stat.waterConsumption += wp.waterFlow / 1000; // m^3
    });
  });

  // compute the changes for cell properties
  oes.space.grid.forAllCells(function (x, y, cell) {
    // delimiter cells does not have "real" temperature, humidity
    // or soil moisture values, these are just stone/grass walls
    if (cell.wall || cell.grass) return;
    var region = sim.model.v.getSpaceRegionAtCoordinate(x, y);
    var t = cell.temperature,
      refT = sim.model.v.refWVT,
      refWVPM = sim.model.v.refWVPM;
    // Soil moisture decreases because of the temperature
    // variation, which produces water vaporization.
    // Each 1°C plus/minus results in a 10% up/down
    // variation relative to the reference value.
    var wVPM = refWVPM + (t - refT) * refWVPM / 10.0,
      rhFPM = wVPM / 3.0;
    // water vaporization is uniform all over the grid,
    // but is relative to the temperature of the cell
    if (cell.soilMoisture >= wVPM) {
      cell.soilMoisture -= wVPM * cell.soilMoisture / 100;
      // the relative air humidity increases, because of the
      // water vaporization. There is a complex formula to
      // compute the value, but we use a simplified form:
      // RH = RH + wVPM / 3, where vVPM represents the
      // quantity of water that is lost from the soil moisture.
      if (cell.humidity - rhFPM < 100) cell.humidity += rhFPM;
    }

    // The temperature variations during the day:
    // - increases between 5:00AM - 5:00PM and
    // - decreases between 5:00PM - 5:00AM.
    if (minute < 720) { // first part of the day: 5:00AM - 5:00 PM
      // Max_GH_T = Max_Outer_T * GH_Effect_Factor
      // Maximum GH temperature not reached, so the temperature increases,
      // or possibly decreases, if today's max outside temperature
      // is lower than the temperature in the GH at the moment of computation
      if (t < hT) cell.temperature += (hT - cell.lastDayTemperature) / 720;
    } else { // last part of the day: 5:00PM - 5:00AM
      // Min_GH_T = GH_T - Min_Outer_T * (2 - insulationFactor)
      // Minimum GH temperature not reached, so the temperature
      // decreases, or possibly increases if today's min outside
      // temperature is higher than the temperature inside the GH
      // at the moment of computation
      if (t > lT) cell.temperature -= (hT - lT) / 720;
    }
    // increase of temperature due to heaters
    cell.temperature += region.effects.t;
    // increase/decrease of air humidity due to humidifiers
    if (cell.humidity - Math.abs(region.effects.rh) > 0)
      cell.humidity += region.effects.rh;
  });
  /****************************************************************
   * compute power consumption for the currently active actuators *
   ****************************************************************/
  sim.stat.energyConsumption = 0;
  sim.stat.energyConsumptionH = 0;
  sim.stat.energyConsumptionWP = 0;
  sim.stat.energyConsumptionRH = 0;
  sim.model.v.spaceRegion.forEach(function (region) {
    Object.keys(region.actuators).forEach(function (k) {
      region.actuators[k].forEach(function (actuator) {
        var ec = 0;
        if (actuator.active) {
          // compute the power consumption for the actuator
          // - One step represents 1 minute (1/60 hours)
          // - PowerConsumption(kWh) = ((RatedPower * ActuatorCurrentCapacity / 100) / 60 ) / 1000
          ec =  actuator.ratedPower * actuator.workingCapacity / 100000;
          sim.stat.energyConsumption += ec;
          if (k === 'h') {
            sim.stat.heatersOnTime++;
            sim.stat.energyConsumptionH += ec;
          }
          if (k === 'wp') {
            sim.stat.waterPumpsOnTime++;
            sim.stat.energyConsumptionWP += ec;
          }
          if (k === 'hum') {
            sim.stat.humidifiersOnTime++;
            sim.stat.energyConsumptionRH += ec;
          }
        }
      });
    });
  });
  sim.stat.totalEnergyConsumption += sim.stat.energyConsumption / 60;
};
/********************************************************
 Output Statistics Variables
 ********************************************************/
sim.model.statistics = {
  "waterConsumption": {
    range: "Decimal", label: "Water consumption",
    decimalPlaces: 2, unit: "m^3"
  },
  "energyConsumptionH": {
    range: "Decimal", label: "Heating EC",
    decimalPlaces: 2, unit: "kWh", showTimeSeries: true
  },
  "energyConsumptionWP": {
    range: "Decimal", label: "Watering EC",
    decimalPlaces: 2, unit: "kWh", showTimeSeries: true
  },
  "energyConsumptionRH": {
    range: "Decimal", label: "Humidification EC",
    decimalPlaces: 2, unit: "kWh", showTimeSeries: true
  },
  "energyConsumption": {
    range: "Decimal", label: "Actual EC",
    decimalPlaces: 2, unit: "kWh"
  },
  "totalEnergyConsumption": {
    range: "Decimal", label: "Total EC",
    decimalPlaces: 2, unit: "kWh"
  },
  "heatersOnTime": {
    range: "Decimal", label: "Heaters active time",
    decimalPlaces: 2, unit: "h",
    computeOnlyAtEnd: true,
    expression: function () {
      // translate to hours
      return sim.stat.heatersOnTime / 60.0;
    }
  },
  "waterPumpsOnTime": {
    range: "Decimal", label: "Water pumps active time",
    decimalPlaces: 2, unit: "h",
    computeOnlyAtEnd: true,
    expression: function () {
      // translate to hours
      return sim.stat.waterPumpsOnTime / 60.0;
    }
  },
  "humidifiersOnTime": {
    range: "Decimal", label: "Humidifiers active time",
    decimalPlaces: 2, unit: "h",
    computeOnlyAtEnd: true,
    expression: function () {
      // translate to hours
      return sim.stat.humidifiersOnTime / 60.0;
    }
  },
  "minOTemperature": {range: "Decimal", label: "Min outer temperature", unit: "°C"},
  "maxOTemperature": {range: "Decimal", label: "Max outer temperature", unit: "°C"},
  "avgOTemperature": {range: "Decimal", label: "Avg out temperature",
    unit: "°C", decimalPlaces: 2}
};

sim.scenario.initialState.events = [
  {typeName: "NewDay", occTime: 1, dayNumber: 1},
  {typeName: "ReadTemperature", occTime: 1},
  {typeName: "ReadHumidity", occTime: 1},
  {typeName: "ReadSoilMoisture", occTime: 1}
];