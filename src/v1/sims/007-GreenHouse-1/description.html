<!-- ===========================================================================
 * Green House with 2D isometric visualization.
 * @copyright Copyright 2016 Mircea Diaconescu, BTU (Germany)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
================================================================================ -->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale = 1.0" />
  <link rel="stylesheet" href="../../../../css/normalize.min.css" />
  <link rel="stylesheet" href="../../../../css/description.css" />
  <script src="metadata.js"></script>
    <script src="../../../ui/description.js"></script>
</head>
<body onload="oes.ui.setupDescription()">
<div id="frontMatter">
  <h1>Green House with Grid Space Visualization</h1>
  <section id="shortDescription"></section>
  <p><strong>Classification tags</strong>: environment simulation, Green House, sensor, actuator.</p>
</div>
<section class="collapsed"><h1><span>►</span>Background</h1>
    <p>Growing vegetables, and in general doing agriculture, is one of the oldest occupations of humanity.
        Over many decades was observed that it is possible to grow vegetables in a climate unfriendly region,
        as long as some of the specific environment characteristics are kept under control, providing optimal
        or close to optimal conditions. It is important to notice, that while some small deviations from the
        optimal conditions does not necessarily mean a low or no production, it still affects some of the plants,
        more sensitive than others, resulting in a decrease (not necessarily proportional in quantity) of the final
        vegetables production quantity.
    </p>
    <p>One interesting concept is that, during the daytime, the temperature is increased because of the sunlight, thus
        reducing the electricity consumption. For this, the Green House walls are made of transparent materials, such as
        special glass or polymeric glass, which allows the infrared radiation to enter the Green House, thus increasing
        the inside temperature. However, during the night, the temperature may drop under the required limits, specially
        outside the season or in regions with a harsh climate. For such cases, a heater is used to maintain the lower
        limit of the temperature.</p>
    <p>In this scenario, we simulate a Green House environment, where the temperature, air humidity and soil moisture
        characteristics are observed. For being able to control these environment qualities, water pumps,
        humidifiers/de-humidifiers and heaters are used. </p>
    <figure>
        <img src="media/img/greenhouseeffects.jpg"/>
        <figcaption>The Green House Effect.</figcaption>
    </figure>
</section>

<section class="mbd collapsed"><h1><span>►</span>Conceptual Model <sup class="expl-req">?</sup></h1>
  <div id="expl-CM" class="expl"></div>

    <p>The conceptual model for studying the Green House environment requires to model the behavior of the
        environment with respect to three important qualities: temperature, relative air humidity and soil moisture.
        Those factors are monitored and "partially" (e.g., heating is possible, but cooling is not) controlled by
        the mean of actuators, whenever sub-optimal conditions affecting the good evolution of the plants
        are detected.</p>

    <section><h1>Conceptual Information Model <sup class="expl-req">?</sup></h1>
      <div id="expl-CIM" class="expl"></div>

        <p>The relevant object types are:</p>
        <ol>
            <li>temperature, relative air humidity and soil moisture sensors,</li>
            <li>water pump, humidifier / de-humidifier and heater actuators,</li>
            <li>the plant, and in particular the tomato and strawberry plant types.</li>
        </ol>
        <p>The relevant types of events are:</p>
        <ol>
            <li>the water pump can be activated or deactivated</li>
            <li>the humidifier is turned ON in humidification / de-humidification mode, or it is turned OFF,</li>
            <li>the heater is turned ON or OFF.</li>
        </ol>
    </section>

    <section>
     <h1>Conceptual Process Model <sup class="expl-req">?</sup></h1>
      <div id="expl-CPM" class="expl"></div>

        <table>
            <caption>Event rules.</caption>
            <thead>
            <tr>
                <td>ON (event type)</td><td>DO (event routine)</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Periodically enable sensor reading</td>
                <td>If the read quantities are in the low side of the optimal ranges, then enable the associated
                    actuator, responsible with the control of that specific monitored environment quality.
                    For the case of relative air humidity, if the actuator is already started in de-humidification
                    mode, then a "turn OFF" action is required.</td>
            </tr>
            <tr>
                <td>Making a actuator switch OFF decision</td>
                <td>If the actuator is turned ON and the corresponding sensor reads a value "near the middle" of
                    the optimal conditions interval is reached, then switch it OFF.</td>
            </tr>
            <tr>
                <td>Making a actuator switch ON decision</td>
                <td>If the actuator is turned OFF and the corresponding sensor reads a value below or over the
                    optimal range, switch it ON.</td>
            </tr>
            <tr>
                <td>Compute the environmental effects of the plants</td>
                <td>On each step, representing a minute of real time, the effect of the environment changes due to
                    plants is computed. Thus, the plants consumes water from soil as well as from the relative
                    air humidity</td>
            </tr>
            </tbody>
        </table>
    </section>
</section>

<section class="mbd collapsed"><h1><span>►</span>Simulation Design Model <sup class="expl-req">?</sup></h1>

  <div id="expl-DM" class="expl"></div>

    <section><h1>Information Design Model <sup class="expl-req">?</sup></h1>
      <div id="expl-IDM" class="expl"></div>
        <p>The simulation design of the Green House Model is based on choosing a <i>discrete grid 2D space</i>. In such
            a space the computations of the (continuous) environment quality variations are greately simplified.
            In particular, computing the water dissipation within the ground is made in a radial way, which means that
            the soil moisture increase is inverse proportional with the distance from the water pump position and the
            position on which the soil moisture is read by the sensor.</p>
    </section>

    <section>
      <h1>Process Design Model <sup class="expl-req">?</sup></h1>
      <div id="expl-PDM" class="expl"></div>

        <table>
            <caption>Event rules.</caption>
            <thead>
            <tr>
                <td>ON (event type)</td><td>DO (event routine)</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>EachSimulationTimeStep</td>
                <td>
                    Check if the a specific sensor needs to be read. <br/>
                    Various sensors are read at various time intervals.<br/><br/>
                    <strong>DO</strong> compute statistics: e.g., water and electricity consumption<br/>
                    <strong>IF</strong> ( sensor.read() < optimalConditionsRange * allowedDeviation)<br/>
                    <strong>THEN</strong> turn ON the corresponding actuator.<br/>
                    <strong>ELSE IF</strong> ( sensor.read() > optimalConditionsRange * allowedDeviation)<br/>
                    <strong>THEN</strong> turn OFF the corresponding actuator.<br/><br/>
                    <em><strong>Note 1:</strong> corresponding actuator means the actuator in charge with the
                        control or variation of the specific environment quality monitored by the specific
                        sensor.</em><br/><br/>
                    <em><strong>Note 2:</strong> a special case occurs when the relative air humidity is below the
                        threshold and the humidifier actuator is already turned ON in de-humidification mode. In this
                        case, the humidifier actuator needs to be switched OFF.</em>
                </td>
            </tr>
            </tbody>
        </table>
    </section>
</section>
</body>
</html>

