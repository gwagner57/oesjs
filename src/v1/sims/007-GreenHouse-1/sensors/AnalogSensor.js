/*******************************************************************************
 * The AnalogSensor model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var AnalogSensor = new cLASS({
  Name: "AnalogSensor",
  supertypeName: "Sensor",
  properties: {},
  methods: {}
});