/*******************************************************************************
 * The HumiditySensor model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var HumiditySensor = new cLASS({
  Name: "HumiditySensor",
  supertypeName: "DigitalSensor",
  properties: {
    "accuracy": {range: "Decimal", min:0, max: 100, initialValue: 3, label: "Accuracy"},
    "precision": {range: "Decimal", min:0, max: 100, initialValue: 5, label: "Precision"},
    "resolution": {range: "Decimal", min:0, max: 100, initialValue: 1, label: "Resolution"}
  },
  methods: {}
});