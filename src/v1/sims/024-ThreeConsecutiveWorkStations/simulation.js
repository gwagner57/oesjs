/*******************************************************
 Simulation Parameters
********************************************************/
sim.scenario.simulationEndTime = 168 * 60;
sim.scenario.idCounter = 11;  // start value of auto IDs
sim.scenario.randomSeed = 12345;  // optional
/*******************************************************
 Simulation Configuration
 ********************************************************/
sim.config.createLog = false;
sim.config.isConstraintCheckingTurnedOn = true;
//sim.config.suppressInitialStateUI = true;
//sim.config.runInMainThread = true;
/*******************************************************
 Simulation Model
********************************************************/
sim.model.time = "continuous";
sim.model.timeUnit = "m";
sim.model.timeRoundingDecimalPlaces = 2;  // like in 3.81

// model variables
sim.model.v.bufferCapacity =  {
  range:"PositiveInteger", initialValue: 4,
  label:"Inter-station buffer capacity",
  hint:"The capacity of the input buffers of the 2nd and 3rd workstations"
};
// stop conditions
sim.model.stopConditions =  {
  "Zero Inventory": function () {
  },
  "After Breakdown": function () {
    var currentStepEvtTypes = sim.currentStepEvents.map( function (e) {
      return e.constructor.Name;
    });
    return currentStepEvtTypes.includes("Breakdown");
  }
};
//sim.config.stopConditionName = "After Breakdown";
/*******************************************************
 Define Initial State
********************************************************/
sim.scenario.initialState.objects = {
  "1": {typeName: "eNTRYnODE", name:"tray entry", successorNode: 2,
        arrivalRecurrence: function () { return rand.exponential( 1/3);}},
  /*
    "2": {typeName: "pROCESSINGnODE", name:"solder deposition station", successorNode: 3,
          duration: function () { return rand.triangular( 3, 8, 4);}},
    "3": {typeName: "pROCESSINGnODE", name:"component placement station", successorNode: 4,
          inputBufferCapacity: function () {return sim.v.bufferCapacity;},
          duration: function () { return rand.frequency({"6.3":0.6, "9.3":0.4});}},
    "4": {typeName: "pROCESSINGnODE", name:"solder reflow station", successorNode: 5,
          inputBufferCapacity: function () {return sim.v.bufferCapacity;},
          fixedDuration: 7.0},
  */
    "5": {typeName: "eXITnODE", name:"tray exit"}
};
sim.scenario.setupInitialState = function () {
  var solderDepositionStation = new oes.ProcessingNode({id: 2, name:"solder deposition station", successorNode: 3,
    duration: function () { return rand.triangular( 3, 8, 4);}});
  sim.addObject( solderDepositionStation);
  var componentPlacementStation = new oes.ProcessingNode({id: 3, name:"component placement station", successorNode: 4,
    inputBufferCapacity: sim.v.bufferCapacity,
    duration: function () { return rand.frequency({"6.3":0.6, "9.3":0.4});}});
  sim.addObject( componentPlacementStation);
  var solderReflowStation = new oes.ProcessingNode({id: 4, name:"solder reflow station", successorNode: 5,
    inputBufferCapacity: sim.v.bufferCapacity,
    duration: 7});
  sim.addObject( solderReflowStation);
};
  /*******************************************************
   Define Output Statistics Variables
   ********************************************************/
sim.model.statistics = {
  "arrivedObjects": {label:"Arrived trays"},  // define a custom label
  "departedObjects": {label:"Departed trays"}  // define a custom label
};
/*******************************************************
 Define Experiment
 ********************************************************/
sim.experiment.id = 1;
sim.experiment.experimentNo = 1;
sim.experiment.title = "Find best buffer capacity";
sim.experiment.replications = 5;  // 20
sim.experiment.seeds = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
sim.experiment.parameterDefs = [{name:"bufferCapacity", values:[2, 4, 8]}];  // 1, 2, 4, 8, 16
