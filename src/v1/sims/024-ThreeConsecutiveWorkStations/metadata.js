var sim = sim || {};
sim.model = sim.model || {};
sim.scenario = sim.scenario || {};
sim.config = sim.config || {};

var oes = oes || {};
oes.ui = oes.ui || {};
oes.ui.explanation = {};

/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "ThreeConsecutiveWorkstations-1";
sim.model.title = "Three Manufacturing Workstations in an Electronics Assembly Line";
sim.model.systemNarrative = "<p>A new electronics assembly manufacturing system consisting of three workstations " +
    "in a serial line (a solder deposition station, a component placement station and a solder reflow station) " +
    "is to be designed. The line produces one type of circuit card with some small design " +
    "differences between cards allowed. Circuit cards are batched into relatively large trays for processing " +
    "before entering the line. All cards in the tray are processed simultaneously by each machine. </p>" +
    "<p>Only a small amount of space is available in the plant for the new line, so inter-station " +
    "buffer space must be kept to a minimum. The objective is to determine the relationship between buffer capacity " +
    "and throughput time noting the amount of blocking that occurs. This knowledge will aid management in determining " +
    "the final amount of buffer space to allocate.</p>";
sim.model.shortDescription = "<p>The assembly line is modeled as a Processing Network with an entry node for arriving parts, " +
    "three consecutive processing nodes representing workstations, and an exit node for departing products. " +
    "The time between the arrival of circuit card trays to the first workstation is modeled as exponentially distributed " +
    "with mean 3.0 minutes. The processing time distribution is the same at each workstation: uniformly distributed " +
    "between 0.7 and 4.7 minutes. </p>"+
    "<p>Processing objects have four attributes for statistics collection. One attribute is arrival time to the system. " +
    "The other three attributes store the processing times at each station. The inter-arrival time and the three processing times " +
    "are sampled from four different random number streams.</p>";
sim.model.license = "CC BY-NC";
sim.model.creator = "Gerd Wagner";
sim.model.created = "2019-09-12";
sim.model.modified = "2019-09-12";
sim.model.source = "Section 7.3 of <a href='https://scholarworks.gvsu.edu/cgi/viewcontent.cgi?article=1006&context=books'>Beyond Lean: " +
    "Simulation in Practice</a> by Charles R. Standridge, Second Edition: April, 2013.";
sim.model.sourceAuthors = "C.R. Standridge";
