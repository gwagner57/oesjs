<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale = 1.0" />
  <link rel="stylesheet" href="../../../../css/normalize.min.css" />
  <link rel="stylesheet" href="../../../../css/description.css" />
  <script src="metadata.js"></script>
  <script src="../../../ui/description.js"></script>
</head>
<body onload="oes.ui.setupDescription()">
 <div id="frontMatter">
  <h1>A Processing Network Model of an Electronics Assembly Line Consisting of Three Workstations</h1>
  <p><strong>Classification tags</strong>: business operations management, DES, next-event time progression, processing network model</p>
  <figure class="right">
    <img src="media/img/theme-image.svg" alt="manufacturing machine" width="240"
         title="[Public domain] Martin von Nathusius, derivative work: Frédéric, via Wikimedia Commons" />
  </figure>
  <section id="shortDescription"></section>
 </div>

 <section class="mbd collapsed"><h1><span>►</span>Conceptual Model <sup class="expl-req">?</sup></h1>
   <div id="expl-CM" class="expl"></div>

   <p>A new electronics assembly manufacturing system consisting of three workstations in a serial
    line (a solder deposition station, a component placement station and a solder reflow station) is to be designed.
    The line produces circuit cards, which are batched into relatively large trays for processing
     before entering the line.</p>
   <p>Only a small amount of space is available in the plant for the new line, so inter-station buffer
     space must be kept to a minimum. A general storage area with sufficient space can be used for
     circuit card trays prior to processing at the first workstation. Engineers are worried that the small
     inter-station buffer space will result in severe blocking that will prevent the throughput time target from
     being reached. The objective is to determine the relationship between buffer size and throughput time
     noting the amount of blocking that occurs. This knowledge will aid management in determining
     the final amount of buffer space to allocate.</p>

   <section><h2>Conceptual Information Model <sup class="expl-req">?</sup></h2>
     <div id="expl-CIM" class="expl"></div>
     <p>The potentially relevant object types are:</p>
<!--
     <figure class="right-float">
       <img src="CIM-ObjT.svg" width="450"/>
       <figcaption>A conceptual model describing object types.</figcaption>
     </figure>
-->
     <ol>
       <li>circuit cards,</li>
       <li>circuit card trays,</li>
       <li>workstations,</li>
       <li>inter-station buffer.</li>
     </ol>
     <p>Potentially relevant types of events are:</p>
     <ol>
       <li>arrivals of circuit card trays,</li>
       <li>processing of trays at each workstations,</li>
       <li>departures of trays.</li>
     </ol>
<!--     
     <p>Both object types and event types, with their participation associations, can be visually described
       in a UML class diagram, as shown below.</p>
     <figure>
       <img src="media/img/CIM.svg" width="600"/>
       <figcaption>A conceptual information model describing object types and event types.</figcaption>
     </figure>
-->     
   </section>

   <section>
     <h1>Conceptual Process Model <sup class="expl-req">?</sup></h1>
     <div id="expl-CPM" class="expl"></div>
     <table>
       <caption>Event rules.</caption>
       <thead>
       <tr>
         <td>ON (event type)</td><td>DO (event routine)</td>
       </tr>
       </thead>
       <tbody>
       <tr>
         <td>arrival of circuit card tray</td>
         <td>IF deposition station is available<br/>
          THEN start a new solder deposition activity<br/>
          ELSE place tray in input buffer</td>
       </tr>
       <tr>
         <td>end of solder deposition activity</td>
         <td>IF component placement station available<br/>
          THEN start a new component placement activity<br/>
          ELSE IF input buffer of component placement station not full<br/>
            THEN place the tray in the buffer and,<br/>
              IF it fills the buffer, THEN block the deposition station</td>
       </tr>
       </tbody>
     </table>
<!--     
     <figure>
       <img src="media/img/CPM.svg" width="800"/>
       <figcaption>A conceptual process model describing the conditional succession of events and activities.</figcaption>
     </figure>
-->
   </section>
 </section>

 <section class="mbd collapsed"><h1><span>►</span>Simulation Design Model <sup class="expl-req">?</sup></h1>
   <div id="expl-DM" class="expl"></div>

   <p>The assembly line is modeled as a Processing Network with an Entry Node for arriving trays,
     three consecutive Processing Nodes representing workstations, and an Exit Node for departing trays.
     The time between the arrival of circuit card trays to the first workstation is modeled as exponentially distributed
     with mean 3.0 minutes. The processing time distribution is the same at each workstation: uniformly distributed
     between 0.7 and 4.7 minutes.</p>

   <p>Processing objects have four attributes for statistics collection. One attribute is arrival time to the system.
     The other three attributes store the processing times at each station. Assigning all processing times when the
     entity arrives assures that the first entity has the first
     processing time sample at each station, the second the second and so forth. This assignment is
     the best way to ensure that the use of random number streams is most effective in reducing the
     variation between system alternative scenarios, which aids in finding statistical differences
     between the cases. In general, the nth arriving entity may not be the nth entity processed at a
     particular station. In the serial line model, this will be the case since entities can’t “pass” each
     other between stations.</p>

   <section><h2>Information Design Model <sup class="expl-req">?</sup></h2>
     <div id="expl-IDM" class="expl"></div>
     <p>A computational design for the purpose of computing the statistics <i>average time in system</i>
       and <i>machineActivity utilizations</i> is obtained by modeling the following types of objects, events and activities:</p>
  <ul>
    <li>Two object types: <b><code>Machine</code></b> and <b><code>Order</code></b>, such that
	machines (1) have a waiting line represented as a multi-valued reference property <code>waitingOrders</code>
	defined by a corresponding association end, and (2) may have a successor machine represented as a 
	reference property <code>nextMachine</code>	defined by a corresponding association end.</li>
    <li>One event type: <b><code>OrderArrival</code></b> with a reference property <code>machine</code>
      for expressing that an order arrives at a certain machine. As an exogenous event type, <code>OrderArrival</code>
      has a <code>recurrence</code> function representing a random variable for computing the time in-between
      two subsequent order arrival events.</li>
    <li>One activity type: <b><code>MachineActivity</code></b>, having a resource association with the object type
      <code>Machine</code> (for expressing that a machine activity is performed at a certain machine) and
      having a function <code>randomDuration</code> representing a random variable that models the random
      variation of duration of machine activities.</li>
  </ul>
<!--
     <p>The object types, event types and activity types described above, together with their participation
       associations, are visually described in the following UML class diagram:</p>
     <figure>
       <img src="media/img/IDM.svg" width="600"/>
       <figcaption>An information design model describing object types, event types and activity types.</figcaption>
     </figure>
-->
   </section>
   <section><h2>Process Design Model <sup class="expl-req">?</sup></h2>
     <div id="expl-PDM" class="expl"></div>
     <table>
       <caption>Event rule design table.</caption>
       <thead>
       <tr>
         <td>ON (event type)</td><td>DO (event routine)</td>
       </tr>
       </thead>
       <tbody>
       <tr>
         <td>OrderArrival( sP) @ t</td>
         <td>
           CREATE order{ arrivalTime: t}<br/>
           PUSH order TO sP.waitingOrders<br/>
           IF sP.waitingOrders.length = 1<br/>
           THEN SCHEDULE ActivityStart("MachineActivity", sP) @ t'
         </td>
       </tr>
       <tr>
         <td>ActivityEnd("MachineActivity", a) @ t</td>
         <td>
           SET sP = a.machine<br/>
           POP order FROM sP.waitingOrders<br/>
           IF HAS-VALUE( sP.nextMachine)<br/>
           THEN SET nextSP = sP.nextMachine<br/>
           &nbsp;&nbsp;&nbsp;PUSH order TO nextSP.waitingOrders<br/>
           &nbsp;&nbsp;&nbsp;IF nextSP.waitingOrders.length = 1<br/>
           &nbsp;&nbsp;&nbsp;THEN SCHEDULE ActivityStart("MachineActivity", nextSP) @ t'<br/>
           ELSE REMOVE order FROM SIMULATION<br/>
           IF sP.waitingOrders.length > 0<br/>
           THEN SCHEDULE ActivityStart("MachineActivity", sP) @ t'
         </td>
       </tr>
       </tbody>
     </table>
     <p>Notice that <i>t'</i> denotes the next moment in simulation time after <i>t</i>. If the simulation is using discrete time, then <i>t' = t+1</i>,
       else (if the simulation is using continuous time), then <i>t' = t + &Delta;t</i> where <i>&Delta;t</i> denotes the smallest time increment
       defined by the simulation model's time granularity.</p>
   </section>

  <section><h2>Model Verification and Validation</h2>
   <p>Verification evidence is obtained by running the simulation and comparing the sum of the initial WiP
    and the number of arrivals with the sum of the number of departures and the WiP at the end of the run.</p>
   <p>Validation evidence is obtained by comparing the percent busy time of the deposition station as
    estimated from the simulation results with the expected value computed as follows. The average
    operation time is (0.7 + 4.7)/2 = 2.7 minutes. The average time between arrivals is 3 minutes.
    Thus, the expected percent busy time is 2.7 / 3.0 = 90%.</p>
   <p>If the approximate 99% confidence interval for the true percent busy time computed from the simulation results
    is, for instance, 86.7 % - 91.7%, then validation evidence is obtained, since this interval contains the
    analytically determined value for the percent busy time.</p>
  </section>

  <section><h2>Experiment Design</h2>
   <p>Since management assesses production weekly, it is natural to choose a simulation duration
    of one week. The size of each of the two buffers is of interest. Note that the
    workstations in this study have the same processing time distribution, indicating that the line is
    balanced as it should be. Analytic and empirical research have shown that for serial systems
    whose workstations have the same operation time distribution that buffers of equal size are
    preferred (Askin and Standridge, 1993; Conway et al., 1988).</p>
   <table><caption>Summary of Experiment Design</caption>
    <thead><tr><th>Element of the Experiment</th><th>Values</th></tr></thead>
    <tbody>
     <tr><td>Experiment parameters and their values</td><td>Size of each buffer: 1, 2, 4, 8, 16</td></tr>
     <tr><td>Performance measures</td><td><ol><li>Throughput time</li>
      <li>Percent blocked time at deposition station</li>
      <li>Percent blocked time at placement station</li></ol></td></tr>
     <tr><td>Random number streams</td><td><ol><li>Time between arrivals</li>
      <li>Processing time at deposition station</li>
      <li>Processing time at placement station</li>
      <li>Processing time at reflow station</li></ol></td></tr>
     <tr><td>Initial state</td><td>One circuit card tray in each inter-station buffer with the
      following station busy</td></tr>
     <tr><td>Number of replicates</td><td>20</td></tr>
     <tr><td>Simulation duration</td><td>2400 minutes (one week)</td></tr>
    </tbody>
   </table>
    <p>The relationship between buffer size and throughput time is to be assessed. Thus, various values
     of buffer size will be simulated and the throughput time observed.
     Average throughput time is one performance measure of interest along with the percent blocked
     time of each station.</p>
    <p>Since all circuit card trays that arrive also depart eventually the arrival rate is the same
     as the throughput rate, at least in the long term. Note that by Little’s Law, the ratio of the
     Work-in-Process (WiP) to the throughput time (TT) is equal to the (known) throughput rate (TR): WiP / TT = TR. Thus, either
     the total WiP, including that preceding the first workstation, or the throughput time could be measured.</p>
    <p>The throughput time is easy to observe since computing it only requires recording the time of arrivals in
     an attribute of processing objects. Thus, the throughput time can be computed when a processing object leaves the system.
     Computing the WiP requires counting the number of processing object within the line. This also is
     straightforward. We will choose to compute the processing object time for this study.</p>
   <p>For a discussion of this experiment, see Section 7.3.3 in
    <a href='https://scholarworks.gvsu.edu/cgi/viewcontent.cgi?article=1006&context=books'>Beyond Lean: Simulation in Practice</a>.</p>
  </section>
 </section>

<!--
 <section class="mbd collapsed"><h1><span>►</span>See also</h1>
   <p>Other OES models for the same system/problem/domain:</p>
   <ol>
     <li><a href="../001-MachineActivityDesk-1/simulation.html">MachineActivityDesk-1</a>: A machineActivity queue model
       (one machineActivity and one queue) with two statistics: <i>maximum queue length</i> and <i>machineActivity utilization</i>.
       The model abstracts away from individual orders and from the composition of the queue.</li>
   </ol>
 </section>
-->
</body>
</html>

