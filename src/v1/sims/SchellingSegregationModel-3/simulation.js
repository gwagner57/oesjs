/*******************************************************
 * Schelling's Segregation Model. A resident is selected, checked if the
 * its tolerance level is below the proportion of different neighbors, and if
 * so, it moves to an empty location in the grid. After moving, the happiness
 * of all residents are recalculated.
 * @copyright Copyright 2017 Luis Gustavo Nardin and Gerd Wagner, BTU (Germany)
 * @author Gerd Wagner, Luis Gustavo Nardin
 * @license The MIT License (MIT)
 ********************************************************/
sim.scenario.simulationEndTime = 10;
sim.config.stepDuration = 200;  // observation time per step in ms
sim.config.visualize = true;
sim.config.createLog = false;
/*******************************************************
 Simulation Model
 ********************************************************/
sim.model.objectTypes = ["GroupOfResidents"];
// Space model
sim.model.space.type = "IntegerGrid";
sim.model.space.xMax = 100;
sim.model.space.yMax = 50;
// model variables
sim.model.v.updateStrategy = true; // true: All, false: Single
sim.model.v.moveStrategy = MoveTypeEL.RANDOM_RADIUS;
sim.model.v.neigborhoodMoveRadius = 2;
sim.model.v.neigborHoodRadius = 1;
sim.model.v.populationDensity = 0.6;
sim.model.v.uncontentResidents = []; // a list used as a global store

/**
 * Return the index of an Array in another Array
 * @param val Array to check
 * @return {number} Index of the Array in the Array, -1 otherwise
 */
Array.prototype.indexOfArray = function ( val ) {
  let i = 0,
      len = this.length;
  
  if (Array.isArray( val )) {
    for (i = 0; i < len; i += 1) {
      if ((this[i].length === val.length) &&
          (this[i].every( ( v, i ) => v === val[i] ))) {
        return i;
      }
    }
  }
  
  return -1;
};

// model procedures/functions
/*
 * Compute the percentage of neighbors from different groups
 */
sim.model.f.neighbDiffLevel = function ( posX, posY, groupNo ) {
  let neighboursCount = 0,
      neighboursOfDifferentGroupCount = 0,
      cellValue = 0,
      x = 0,
      y = 0;
  let xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax,
      radius = sim.v.neigborHoodRadius || 1;
  
  // scan all residents in the neighbourhood not considering the current
  // location
  for (x = Math.max( posX - radius, 1 ); x <= Math.min( posX + radius, xMax );
       x += 1) {
    for (y = Math.max( posY - radius, 1 ); y <= Math.min( posY + radius, yMax );
         y += 1) {
      if (!(x === posX && y === posY)) {
        cellValue = sim.space.grid[(y - 1) * xMax + x - 1] & 15; //lower 4 bits
        if (cellValue !== 0) {
          neighboursCount += 1;
          if (cellValue !== groupNo) {
            neighboursOfDifferentGroupCount += 1;
          }
        }
      }
    }
  }
  if (neighboursCount === 0) {
    return 0; // no different neighbors
  }
  else {
    return neighboursOfDifferentGroupCount / neighboursCount;
  }
};

/*
 * Move operation
 */
sim.model.f.moveR = function ( xR, yR ) {
  let x = 0,
      y = 0,
      g = 0,
      t = 0,
      bestT = 0,
      tol = 0,
      bestPos = [],
      newPos = [],
      xMin = 0,
      yMin = 0,
      xMax = 0,
      yMax = 0,
      groups = cLASS["GroupOfResidents"].instances;
  let getCellVal = oes.space.grid.i.getCellValue,
      setCellVal = oes.space.grid.i.setCellValue,
      neighbDiffLevel = sim.model.f.neighbDiffLevel;
  
  switch (sim.v.moveStrategy) {
      // Random
    case MoveTypeEL.RANDOM:
      xMin = 1;
      yMax = 1;
      xMax = sim.model.space.xMax;
      yMax = sim.model.space.yMax;
      do {
        newPos[0] = rand.uniformInt( xMin, xMax );
        newPos[1] = rand.uniformInt( yMin, yMax );
      } while (getCellVal( newPos[0], newPos[1] ) !== 0);
      break;
      // Random radius
    case MoveTypeEL.RANDOM_RADIUS:
      xMin = Math.max( 1, xR - sim.v.neigborhoodMoveRadius );
      yMin = Math.max( 1, yR - sim.v.neigborhoodMoveRadius );
      xMax = Math.min( sim.model.space.xMax,
          xR + sim.v.neigborhoodMoveRadius );
      yMax = Math.min( sim.model.space.yMax,
          yR + sim.v.neigborhoodMoveRadius );
      do {
        newPos[0] = rand.uniformInt( xMin, xMax );
        newPos[1] = rand.uniformInt( yMin, yMax );
      } while (getCellVal( newPos[0], newPos[1] ) !== 0);
      break;
      // Random satisfiability
    case MoveTypeEL.RANDOM_SATISFIABILITY:
      xMin = 1;
      yMin = 1;
      xMax = sim.model.space.xMax;
      yMax = sim.model.space.yMax;
      g = getCellVal( xR, yR );
      tol = groups[String( g )].toleranceLevel;
      do {
        newPos[0] = rand.uniformInt( xMin, xMax );
        newPos[1] = rand.uniformInt( yMin, yMax );
      } while ((getCellVal( newPos[0], newPos[1] ) !== 0) &&
               (neighbDiffLevel( newPos[0], newPos[1], g ) >= tol ));
      break;
      // Random radius satisfiability
    case MoveTypeEL.RANDOM_RADIUS_SATISFIABILITY:
      xMin = Math.max( 1, xR - sim.v.neigborhoodMoveRadius );
      yMin = Math.max( 1, yR - sim.v.neigborhoodMoveRadius );
      xMax = Math.min( sim.model.space.xMax,
          xR + sim.v.neigborhoodMoveRadius );
      yMax = Math.min( sim.model.space.yMax,
          yR + sim.v.neigborhoodMoveRadius );
      g = getCellVal( xR, yR );
      tol = groups[String( g )].toleranceLevel;
      do {
        newPos[0] = rand.uniformInt( xMin, xMax );
        newPos[1] = rand.uniformInt( yMin, yMax );
      } while ((getCellVal( newPos[0], newPos[1] ) !== 0) &&
               (neighbDiffLevel( newPos[0], newPos[1], g ) >= tol ));
      break;
    case MoveTypeEL.FIRST_BEST_LOCATION:
      xMin = 1;
      yMin = 1;
      xMax = sim.model.space.xMax;
      yMax = sim.model.space.yMax;
      g = getCellVal( xR, yR );
      tol = groups[String( g )].toleranceLevel;
      bestT = 1;
      for (x = xMin; x < xMax; x += 1) {
        for (y = yMin; y <= yMax; y += 1) {
          if ((x !== xR) && (y !== yR) && (getCellVal( x, y ) === 0)) {
            t = neighbDiffLevel( x, y, g );
            if ((t < tol) && (t < bestT)) {
              newPos[0] = x;
              newPos[1] = y;
              bestT = t;
            }
          }
        }
      }
      break;
    case MoveTypeEL.RANDOM_BEST_LOCATION:
      xMin = 1;
      yMin = 1;
      xMax = sim.model.space.xMax;
      yMax = sim.model.space.yMax;
      g = getCellVal( xR, yR );
      tol = groups[String( g )].toleranceLevel;
      bestT = 1;
      for (x = xMin; x < xMax; x += 1) {
        for (y = yMin; y <= yMax; y += 1) {
          if ((x !== xR) && (y !== yR) && (getCellVal( x, y ) === 0)) {
            t = neighbDiffLevel( x, y, g );
            if (t < tol) {
              if (t < bestT) {
                bestPos = [];
                bestPos.push( [x, y] );
                bestT = t;
              } else if (t === bestT) {
                bestPos.push( [x, y] );
              }
            }
          }
        }
      }
      
      newPos = bestPos[rand.uniformInt( 0, bestPos.length )];
      break;
  }
  
  if (newPos) {  // move to new location
    g = getCellVal( xR, yR );
    setCellVal( newPos[0], newPos[1], g );
    setCellVal( xR, yR, 0 );
  } else {  // leave grid
    setCellVal( xR, yR, 0 );
    sim.stat.residentsWhoLeftTheGrid += 1;
  }
  
  return ( newPos );
}
;

/*
 * Update the happiness of the [posX, posY] neighbors
 */
sim.model.f.neighborsUpdate = function ( posX, posY ) {
  let x = 0,
      y = 0,
      g = 0,
      tol = 0,
      radius = 1,
      index = -1;
  let xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax,
      neighbDiffLevel = sim.model.f.neighbDiffLevel,
      groups = cLASS["GroupOfResidents"].instances,
      getCellVal = oes.space.grid.i.getCellValue;
  
  // scan all residents in the neighbourhood not considering the current
  // location
  for (x = Math.max( posX - radius, 1 ); x <= Math.min( posX + radius, xMax );
       x += 1) {
    for (y = Math.max( posY - radius, 1 ); y <= Math.min( posY + radius, yMax );
         y += 1) {
      g = getCellVal( x, y );
      index = sim.v.uncontentResidents.indexOfArray( [x, y] );
      if (g > 0) {
        tol = groups[String( g )].toleranceLevel;
        if (neighbDiffLevel( x, y, g ) > tol) {
          if (index === -1) {
            sim.v.uncontentResidents.push( [x, y] );
          }
        } else {
          if (index > -1) {
            sim.v.uncontentResidents.splice( index, 1 );
          }
        }
      } else if (index > -1) {
        sim.v.uncontentResidents.splice( index, 1 );
      }
    }
  }
};

sim.model.OnEachTimeStep = function () {
  let i = 0,
      N = 0,
      pos = [],
      newPos = [];
  let neighbDiffLevel = sim.model.f.neighbDiffLevel,
      groups = cLASS["GroupOfResidents"].instances,
      getCellVal = oes.space.grid.i.getCellValue;  // function reference
  
  N = sim.v.uncontentResidents.length;
  
  // Update all residents per timestep
  if (sim.v.updateStrategy) {
    for (i = 0; i < N; i += 1) {
      pos = sim.v.uncontentResidents[i];
      sim.model.f.moveR( pos[0], pos[1] );
    }
    
    // collect all residents that are unhappy
    sim.v.uncontentResidents = [];
    oes.space.grid.forAllCells( function ( x, y ) {
      let g = getCellVal( x, y ),
          tol = 0;
      if (g > 0) {
        tol = groups[String( g )].toleranceLevel;
        if (neighbDiffLevel( x, y, g ) > tol) {
          sim.v.uncontentResidents.push( [x, y] );
        }
      }
    } );
    
    // Update one resident per timestep
  } else {
    if (N > 0) {
      i = rand.uniformInt( 0, N - 1 );
      pos = sim.v.uncontentResidents[i];
      newPos = sim.model.f.moveR( pos[0], pos[1] );
      sim.model.f.neighborsUpdate( pos[0], pos[1] );
      if (newPos) {
        sim.model.f.neighborsUpdate( newPos[0], newPos[1] );
      }
    }
  }
};
/**********************************************************
 Define the initial state of the simulation system
 ***********************************************************/
sim.scenario.setupInitialState = function () {
  var xMax = sim.model.space.xMax;
  var neighbDiffLevel = sim.model.f.neighbDiffLevel,
      groups={}, nmrOfGroups=0, obj=null;

  obj = new GroupOfResidents({
      id:1, name:"A", toleranceLevel: 0.5
  });
  sim.addObject( obj);
  obj = new GroupOfResidents({
    id:2, name:"B", toleranceLevel: 0.9
  });
  sim.addObject( obj);

  groups = cLASS["GroupOfResidents"].instances;
  nmrOfGroups = Object.keys( groups ).length;

  // populate a portion of the grid cells
  oes.space.grid.forAllCells( function ( x, y ) {
    let groupNo = 0;
    if (Math.random() < sim.v.populationDensity) {
      // pick a random group number
      groupNo = rand.uniformInt( 1, nmrOfGroups );
      // mark cell as occupied
      sim.space.grid[(y - 1) * xMax + x - 1] = groupNo;
    }
  } );
  
  // collect all uncontent residents
  sim.v.uncontentResidents = [];
  
  oes.space.grid.forAllCells( function ( x, y ) {
    let g = oes.space.grid.i.getCellValue( x, y ),  // group number
        tol = 0.0;
    if (g > 0) {  // cell is occupied by a resident
      tol = groups[String( g )].toleranceLevel;
      if (neighbDiffLevel( x, y, g ) > tol) {
        sim.v.uncontentResidents.push( [x, y] );
      }
    }
  } );
  sim.stat.nmrOfUncontentResidents = sim.v.uncontentResidents.length;
  console.log( sim.step, " ", sim.stat.nmrOfUncontentResidents );
};
/*******************************************************
 Define Output Statistics Variables
 ********************************************************/
sim.model.statistics = {
  "residentsWhoLeftTheGrid": {
    range: "NonNegativeInteger",
    label: "Residents gone"
  },
  "nmrOfUncontentResidents": {
    range: "NonNegativeInteger",
    label: "Uncontent residents",
    showTimeSeries: true
  }
};
