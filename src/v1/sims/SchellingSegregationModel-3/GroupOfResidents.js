var MoveTypeEL = new eNUMERATION( "MoveTypeEL",
    ["random", "random radius", "random satisfiability",
      "random radius satisfiability", "first best location",
      "random best location"] );
var GroupOfResidents = new cLASS( {
  Name: "GroupOfResidents",
  supertypeName: "oBJECT",
  properties: {
    "toleranceLevel": {
      range: "ClosedUnitInterval",
      label: "Tolerance level",
      hint: "Percentage of different-neighbors tolerated to be happy"
    }
  },
  methods: {}
} );