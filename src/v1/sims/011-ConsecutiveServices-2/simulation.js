/*******************************************************
 Simulation Scenario Parameters
********************************************************/
sim.scenario.simulationEndTime = 500;
sim.scenario.idCounter = 11;  // start value of auto IDs
sim.scenario.randomSeed = 15;  // optional
/*******************************************************
 Simulation Model
********************************************************/
sim.model.time = "continuous";
sim.model.timeRoundingDecimalPlaces = 2;  // like in 3.85
/*******************************************************
 Simulation Configuration
 ********************************************************/
sim.config.createLog = true;
sim.config.isConstraintCheckingTurnedOn = true;
//sim.config.suppressInitialStateUI = true;

/*******************************************************
 Define Initial State
********************************************************/
sim.scenario.initialState.objects = {
  "1": {typeName: "eNTRYnODE", name:"entry", successorNode: 2, maxNmrOfArrivals: 500},
  "2": {typeName: "pROCESSINGnODE", name:"recD", successorNode: 3},
  "3": {typeName: "pROCESSINGnODE", name:"caseD", successorNode: 4,
        // define a non-default duration
        randomDuration: function () {return rand.exponential( 1);}},
  "4": {typeName: "eXITnODE", name:"exit"}
};
/*******************************************************
 Define Output Statistics Variables
 ********************************************************/
sim.model.statistics = {
  "arrivedObjects": {label:"Arrived clients"},  // redefine default label
  "departedObjects": {label:"Departed clients"} // redefine default label
};
/*******************************************************
 Define Experiment
 ********************************************************/
sim.experiment.id = 1;
sim.experiment.experimentNo = 1;
sim.experiment.title = "Test";
sim.experiment.replications = 5;
sim.experiment.seeds = [11,12,13,14,15,16,17,18,19,20];
