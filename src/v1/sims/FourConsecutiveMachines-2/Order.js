var Order = new cLASS({
  Name: "Order",
  shortLabel: "Order",
  supertypeName: "oBJECT",
  properties: {
    "arrivalTime": { range: "NonNegativeInteger", label: "Arrival time",
        shortLabel: "arrT"}
  }
});
