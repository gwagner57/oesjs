/*******************************************************************************
 * The OrderArrival event class
 *
 * @copyright Copyright 2015-2016 Gerd Wagner
 *   Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 ******************************************************************************/

/**
 * @type {cLASS}
 */
var OrderArrival = new cLASS({
  Name: "OrderArrival",
  shortLabel: "Arr",
  supertypeName: "eVENT",
  properties: {
    "machine": {range: "Machine"},
    "quantity": {
      range: "PositiveInteger",
      initialValue: function () {
        return rand.uniformInt(3,7);
      }
    }
  },
  methods: {
    "onEvent": function () {
      var followupEvents=[], order=null;
      // create new order object
      order = new Order({arrivalTime: this.occTime});
      sim.addObject( order);
      // push new order to the queue
      this.machine.waitingOrders.push( order);
      // update statistics
      sim.stat.arrivedOrders++;
      // if the machineActivity desk is not busy
      if (this.machine.waitingOrders.length === 1) {
        followupEvents.push( new oes.ActivityStart({
          occTime: this.occTime + 1,
          activityType: "MachineActivity",
          duration: this.machine.activityDuration(),
          // on activity creation resource roles are copied to corresp. property slots
          resources: {"machine": this.machine}
        }));
      }
      return followupEvents;
    }
  }
});
// Any exogenous event type needs to define a static function "recurrence"
OrderArrival.recurrence = function () {
  return rand.exponential( sim.model.v.orderEventRate);
};
// Any exogenous event type needs to define a static function "createNextEvent"
OrderArrival.createNextEvent = function (e) {
 return new OrderArrival({
     occTime: e.occTime + OrderArrival.recurrence(),
     machine: e.machine
  });
};
