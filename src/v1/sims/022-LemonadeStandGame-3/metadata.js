var sim = sim || {};
sim.model = sim.model || {};
sim.scenario = sim.scenario || {};
sim.config = sim.config || {};

var oes = oes || {};
oes.ui = oes.ui || {};
oes.ui.explanation = {};

/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "LemonadeStand-3";
sim.model.title = "Lemonade Stands as Manufacturing Companies in a Competitive Market Dominated by Weather Conditions";
sim.model.systemNarrative = "Several lemonade stands compete with each other in a market dominated " +
    "by the weather: the more sunny and warm its is, the more customers show up for buying lemonade. Each" +
    "customer has specific preferences concerning the price and quality of the lemonade. When they are dissatisfied" +
    "with the lemonade of their preferred supplier, they switch to another stand that becomes their new preferred supplier.";
sim.model.shortDescription = "This model modifies and extends the LemonadeStand-2 model by implementing " +
    "customers with individual price and quality preferences who place customer orders at their preferred supplier.";
sim.model.license = "CC BY-NC";
sim.model.creator = "Gerd Wagner";
sim.model.contributors = "Ke Xu";
sim.model.created = "2018-03-19";
sim.model.modified = "2018-03-21";
