var CustomerOrder = new cLASS({
    Name: "CustomerOrder",
    shortLabel: "CO",
    supertypeName: "eVENT",
    properties: {
      "customer": {range: "Customer"},
      "supplier": {range: "SingleProductsupplier"}
    },
    methods: {
      "onEvent": function () {
        var events=[], suppliers={}, supplierIds=[], supId="", newSupId="",
            prodType = this.supplier.productType,
            qtyPerSupplyUnit = prodType.quantityPerSupplyUnit,
            availSupplyUnits = Math.floor( prodType.stockQuantity / qtyPerSupplyUnit);
        // deduct demand from quantity in stock
        if (availSupplyUnits === 0) {
          this.supplier.dailyLostSales++;
          this.supplier.totalLostSales++;
        } else {
          prodType.stockQuantity -= qtyPerSupplyUnit;
          // update daily revenue of supplier
          this.supplier.dailyRevenue += prodType.salesPrice;
          // update liquidity of supplier
          this.supplier.money += prodType.salesPrice;
          // check customer satisfaction
          if (this.customer.maxPrice < prodType.salesPrice) {
            suppliers = cLASS["SingleProductCompany"].instances;
            supplierIds = Object.keys( suppliers);
            supId = customer.preferredSupplier.id;
            newSupId = (supplierIds.indexOf( supId) + 1) % supplierIds.length;
            // assign new preferred supplier
            customer.preferredSupplier = suppliers[newSupId];
          }
        }
        return events;
      }
    }
});
