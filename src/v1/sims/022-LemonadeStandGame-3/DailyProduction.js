/*******************************************************************************
 * @copyright Copyright 2018 Gerd Wagner
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 ******************************************************************************/
var DailyProduction = new cLASS({
  Name: "DailyProduction",
  shortLabel: "Prod",
  supertypeName: "eVENT",
  properties: {},
  methods: {
    "onEvent": function () {
      var events=[],
          companies = cLASS["SingleProductCompany"].instances,
          customers = cLASS["Customer"].instances,
          customerIds = Object.keys( customers),
          market = sim.namedObjects["Market"],
          nmrOfCustomerOrders = market.getDailyDemandQuantity(),
          i=0, cust=null;
      // perform production
      Object.keys( companies).forEach( function (compId) {
        companies[compId].performProduction();
      });
      // create random permutation of the customer IDs array
      rand.shuffleArray( customerIds);
      // schedule CustomerOrder events for a random subset of customers
      for (i=0; i < nmrOfCustomerOrders; i++) {
        cust = customers[customerIds[i]];
        events.push( new CustomerOrder({
          occTime: this.occTime + 5,  // 5 hours later
          customer: cust,
          supplier: cust.preferredSupplier
        }));
      }
      return events;
    }
  }
});
