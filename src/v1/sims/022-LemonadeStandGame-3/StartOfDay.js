/*******************************************************************************
 * @copyright Copyright 2018 Gerd Wagner
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 ******************************************************************************/
var StartOfDay = new cLASS( {
  Name: "StartOfDay",
  shortLabel: "SoD",
  supertypeName: "eVENT",
  properties: {},
  methods: {
    // parameters may be delivered by user action form
    "onEvent": function (slots) {
      var events=[],
          companies = cLASS["SingleProductCompany"].instances,
          dayNo = Math.ceil( sim.time / 24);
      // do company stuff
      Object.keys( companies).forEach( function (compId) {
        var comp = companies[compId],
            inpInvItems = comp.inputInventoryItems,
            prodType = comp.productType;
        // demand forecasting
        comp.dailyDemandForecast = comp.getDemandForecast()
        // production planning and replenishment
        if (slots && slots.planProdQty && compId === "3") {  // slots provided by UI
          prodType.plannedProductionQuantity = slots.planProdQty;
        } else {
          comp.planProductionQuantity();
        }
        // sales price planning
        if (slots && slots.planSalesPrice && compId === "3") {  // slots provided by UI
          prodType.salesPrice = slots.planSalesPrice;
        } else {
          comp.planSalesPrice();
        }
        // reset object-specific daily statistics variables
        comp.dailyRevenue = 0;
        comp.dailyCosts = 0;
        comp.dailyProfit = 0;
        comp.dailyLostSales = 0;
        // review inventory and create replenishment deliveries
        Object.keys( inpInvItems).forEach( function (inpItemName) {
          var inpItem = sim.namedObjects[inpItemName], del={}, x=0;
          if ((inpItem.reorderPeriod && dayNo % inpItem.reorderPeriod === 0) ||
              (inpItem.reorderPoint && !inpItem.outstandingOrder) &&
              inpInvItems[inpItemName] <= inpItem.reorderPoint) {
            // compute order quantity (in supply units)
            x = (inpItem.targetInventory - inpInvItems[inpItemName]) / inpItem.quantityPerSupplyUnit;
            del[inpItemName] = Math.ceil( x);
            del.cost = del[inpItemName] * inpItem.purchasePrice;
            events.push( new Delivery({
              occTime: this.occTime + inpItem.leadTime() * 24 + 2,  // 2 hours later
              receiver: comp,
              deliveredItems: del
            }));
            inpItem.outstandingOrder = true;
          }
        }, this);
      }, this);
      // schedule DailyProduction event
      events.push( new DailyProduction({
        occTime: this.occTime + 3  // 1 hour after deliveries
      }));
      // schedule EndOfDay event
      events.push( new EndOfDay({
        occTime: this.occTime + 10  // 10 hours later
      }));
      return events;
    }
  }
} );
// Any exogenous event type needs to define a static function "recurrence"
StartOfDay.recurrence = function () {
  return 24;
};
// Any exogenous event type needs to define a static function "createNextEvent"
StartOfDay.createNextEvent = function ( e) {
  return new StartOfDay( {
    occTime: e.occTime + StartOfDay.recurrence()
  } );
};