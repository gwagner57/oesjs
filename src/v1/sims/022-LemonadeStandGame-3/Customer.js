/*******************************************************************************
 * @copyright Copyright 2018 Gerd Wagner
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 ******************************************************************************/
var Customer = new cLASS( {
  Name: "Customer",
  supertypeName: "oBJECT",
  properties: {
    "maxPrice": {range: "Decimal", label: "Maximal price accepted"},
    "prefProductComposition": {range: Object, label: "Preferred lemonade quality"},
    "suppliers": {range: "SingleProductCompany", minCard: 0, maxCard: Infinity},
    "preferredSupplier": {range: "SingleProductCompany", label: "Preferred lemonade stand"},
    "market": {range: "DailyDemandMarket", label: "Daily demand market"}
  }
});

Customer.changePrefVendor = function ( customer ) {
  var prefId = customer.preferredSupplier.id,
      suppliers = [];
  
  if (customer.suppliers.length > 1) {
    suppliers = customer.suppliers.slice( 0, customer.suppliers.length );
    
    suppliers.splice( suppliers.indexOf( prefId.toString() ), 1 );
    
    customer.preferredSupplier =
        cLASS["SingleProductCompany"].instances[Customer.prefVendor( suppliers )];
  } else {
    sim.removeObject( customer );
    delete cLASS["Customer"].instances[customer.id];
  }
};

Customer.changeMaxPrice = function ( customer ) {
  
  console.log( "Customer:" + customer );
  var price = customer.maxPrice;
  switch (customer.weather.weatherState) {
    case WeatherStateEL.SUNNY:
      price = price * (1 + rand.normal( 0.4, 0.1 ));
      break;
    case WeatherStateEL.RAINY:
      price = price * (1 - rand.normal( 0.2, 0.1 ));
      break;
  }
  if (customer.weather.temperature < 20) {
    price = parseFloat( (price * (1 - rand.normal( 0.2, 0.1 ))).toFixed( 1 ) );
  } else if (this.forecastTemp < 30) {
    price = parseFloat( price.toFixed( 1 ) );
  } else {
    price = parseFloat( (price * (1 + rand.normal( 0.2, 0.1 ))).toFixed( 1 ) );
  }
  console.log( "new maxPrice: " + price + " old maxPrice " +
               customer.maxPrice );
  return price;
};

Customer.changePrefProdComp = function ( customer ) {
  
  console.log( "Customer:" + customer );
  var prefProdComp = customer.prefProdComp;
  
  if (customer.weather.temperature < 25) {
    prefProdComp["Lemon"] = Math.max( 1, prefProdComp["Lemon"] - 1 );
    prefProdComp["Sugar"] = Math.max( 0, prefProdComp["Sugar"] - 0.1 );
  } else if (customer.weather.temperature >= 30) {
    prefProdComp["Lemon"] += 1;
    prefProdComp["Sugar"] += 0.1;
  }
  
  console.log( "new prefProdComp: " + prefProdComp.Lemon );
  return prefProdComp;
};
