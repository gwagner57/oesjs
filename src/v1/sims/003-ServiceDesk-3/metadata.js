var sim = sim || {};
sim.model = sim.model || {};
sim.scenario = sim.scenario || {};
sim.config = sim.config || {};

var oes = oes || {};
oes.ui = oes.ui || {};
oes.ui.explanation = {};

/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "ServiceDesk-3";
sim.model.title = "An Activity-Based Service Queue Model";
sim.model.systemNarrative = "The customers arriving at a service desk (or service station) have to wait " +
    "in a queue when the service desk is busy. Otherwise, when the queue is empty and the service desk is not busy, " +
    "they are immediately served by the service clerk. Whenever a service is completed, the served customer " +
    "departs and the next customer from the queue, if there is any, will be served.";
sim.model.shortDescription = "A service queue model where the service is modeled as an activity with " +
    "the service desk as its resource, for which the utilization statistics is computed automatically. " +
    "The model includes one object type: ServiceDesk, one event type: CustomerArrival " +
    "and one activity type: PerformService.";
sim.model.license = "CC BY-NC";
sim.model.creator = "Gerd Wagner";
sim.model.created = "2016-10-04";
sim.model.modified = "2016-10-18";
