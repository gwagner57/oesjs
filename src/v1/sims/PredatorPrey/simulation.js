/*******************************************************
 * Predator-Prey - An example of a discrete event simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
********************************************************/

/*******************************************************
 Simulation Parameters
********************************************************/
sim.scenario.simulationEndTime = 10;
sim.config.stepDuration = 500;  // 500 ms observation time per step
sim.config.visualize = true;
sim.config.createLog = false;
//logOnly: ["Species"];
/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "Predator-Prey-Population-Dynamics";
sim.model.title = "A Simple Model of a Predator-Prey Population Dynamics";
sim.model.shortDescription = "Predator and Prey are positioned and visualized in a grid space.";
sim.model.objectTypes = ["Species"];
// Space model
sim.model.space.type = "IntegerGrid";
sim.model.space.xMax = 100;
sim.model.space.yMax = 50;

sim.model.OnEachTimeStep = function () {
  var k=0, m, tE, tW, tN, tS, x, y, xE, xW, yN, yS, cell=[], newPos=[], dir;
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax;
  var Prey = sim.objects["1"],
      Predator = sim.objects["2"];
  // move objects by one cell at random
  k = 0; m = Prey.cells.length;
  while (k < m) {
    cell = Prey.cells[k];
    x = cell[0]; y = cell[1];
    // check if this prey animal has been removed from the grid, meanwhile
    if (sim.space.grid[(y-1)*xMax + x - 1] !== 1) {
      Prey.cells.splice( k, 1);  // drop cell element
      m = m - 1;  // continue loop with the same k
      continue;
    }
    dir = oes.space.grid.getRandomDirection();
    newPos = oes.space.grid.getTranslationPosition( x, y, dir);
    newPos = oes.space.grid.i.findFreeCell( newPos);
    oes.space.grid.i.move( cell, newPos);
    cell[0] = newPos[0];
    cell[1] = newPos[1];
    x = cell[0]; y = cell[1];
    // compute N,E,S,W coordinates
    yN = y < yMax ? y+1 : 1;
    xE = x < xMax ? x+1 : 1;
    yS = y > 1 ? y-1 : yMax;
    xW = x > 0 ? x-1 : xMax;
    // get (type) value of neighbor cell West
    tE = sim.space.grid[(y-1)*xMax + xW - 1];
    // get (type) value of neighbor cell East
    tW = sim.space.grid[(y-1)*xMax + xE - 1];
    // get (type) value of neighbor cell North
    tN = sim.space.grid[(yN-1)*xMax + x - 1];
    // get (type) value of neighbor cell South
    tS = sim.space.grid[(yS-1)*xMax + x - 1];
    if (tE===2 || tW===2 || tN===2 || tS===2) {  // predator on neighbor cell
      sim.space.grid[(y-1)*xMax + x-1] = 0;  // remove prey from grid
      Prey.popSize--;  // decrement population size attribute
      Prey.cells.splice( k, 1);  // drop cell element
      m = m-1;  // continue loop with the same k
    } else k = k+1;
  }
  //console.log( Prey.cells.length +" = "+ Prey.popSize);
  k = 0; m = Predator.cells.length;
  while (k < m) {
    cell = Predator.cells[k];
    x = cell[0]; y = cell[1];
    dir = oes.space.grid.getRandomDirection();
    newPos = oes.space.grid.getTranslationPosition( x, y, dir);
    newPos = oes.space.grid.i.findFreeCell( newPos);
    oes.space.grid.i.move( cell, newPos);
    cell[0] = newPos[0];
    cell[1] = newPos[1];
    x = cell[0]; y = cell[1];
    k = k+1;
    // compute N,E,S,W coordinates
    yN = y < yMax ? y+1 : 1;
    xE = x < xMax ? x+1 : 1;
    yS = y > 1 ? y-1 : yMax;
    xW = x > 0 ? x-1 : xMax;
    // inspect neighbor cell East
    if (sim.space.grid[(y-1)*xMax + xE - 1] === 1) {  // prey on neighbor cell
      sim.space.grid[(y-1)*xMax + xE - 1] = 0;  // remove prey from grid
      Prey.popSize--;  // decrement population size attribute
      continue;
    }
    // inspect neighbor cell West
    if (sim.space.grid[(y-1)*xMax + xW - 1] === 1) {  // prey on neighbor cell
      sim.space.grid[(y-1)*xMax + xW - 1] = 0;  // remove prey from grid
      Prey.popSize--;  // decrement population size attribute
      continue;
    }
    // inspect neighbor cell North
    if (sim.space.grid[(yN-1)*xMax + x - 1] === 1) {  // prey on neighbor cell
      sim.space.grid[(yN-1)*xMax + x - 1] = 0;  // remove prey from grid
      Prey.popSize--;  // decrement population size attribute
      continue;
    }
    // inspect neighbor cell South
    if (sim.space.grid[(yS-1)*xMax + x - 1] === 1) {  // prey on neighbor cell
      sim.space.grid[(yS-1)*xMax + x - 1] = 0;  // remove prey from grid
      Prey.popSize--;  // decrement population size attribute
    }
  }
};
/*******************************************************
 Define Space View/Visualization
********************************************************/
/*
 set backgroundColor of grid cell  "hsl(120, 50%, 50%)"
 1) 0 (or 360) is red, 120 is green, 240 is blue.
 2) Saturation is a percentage value; 0% means a shade of gray and 100% is the full color.
 3) Lightness is also a percentage; 0% is black, 100% is white.
 */
sim.config.observationUI = {
  canvasColor: "white",  //TODO: implement!
  gridCellValueColors: ["white","blue","red"],
  gridCellSize: 4  // px
};
/**********************************************************
 Define the initial state of the simulation system
***********************************************************/
sim.scenario.initialState.objects = {
  "1": {type: "Species", name: "Prey", popSize: 50, cells:[]},
  "2": {type: "Species", name: "Predator", popSize: 20, cells:[]}
};
sim.scenario.setupInitialState = function () {
  var Prey = sim.objects["1"];
  var Predator = sim.objects["2"];
  var i=0, x=0, y=0;
  var xMax = sim.model.space.xMax;
  var grid = sim.space.grid;
  var freeCell=[];
  // define animal individuals as a list of grid cells with random positions
  Prey.cells = []; Predator.cells = [];
  for (i=0; i < Prey.popSize; i++) {
    freeCell = oes.space.grid.i.findFreeCell();
    Prey.cells.push( freeCell);
    x = freeCell[0]; y = freeCell[1];
    // mark cell as occupied
    grid[(y-1)*xMax + x - 1] = Prey.id;
  }
  for (i=0; i < Predator.popSize; i++) {
    freeCell = oes.space.grid.i.findFreeCell();
    Predator.cells.push( freeCell);
    x = freeCell[0]; y = freeCell[1];
    // mark cell as occupied
    grid[(y-1)*xMax + x - 1] = Predator.id;
  }
};
/*******************************************************
 Define UI for initial state modifications
 ********************************************************/
sim.scenario.initialStateUI.editableProperties["Species"] = ["popSize"];
/*******************************************************
 Define Output Statistics Variables
 ********************************************************/
sim.model.statistics = {
  "preyPopulationSize": {property:"popSize", objectType:"Species", objectIdRef: 1,
    showTimeSeries: true, label:"Prey population"},
  "predatorPopulationSize": {property:"popSize", objectType:"Species", objectIdRef: 2,
    showTimeSeries: true, label:"Predator population"}
};
