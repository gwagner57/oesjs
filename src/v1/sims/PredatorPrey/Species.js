/* Species is a powertype, so its instances are object types.
   This will be supported by a future OES version.
 */
var Species = new cLASS({
  Name: "Species",
  supertypeName: "oBJECT",  //TODO: a species is, in fact, not an object
  properties: {
    "popSize": {range: "NonNegativeInteger", label:"Population size"},
    "birthRate": {range: "Decimal"},
    "deathRate": {range: "Decimal"},
    "cells": {range: Array}
  },
  methods: {
  }
});
