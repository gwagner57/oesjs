<!-- ===========================================================================
 * SchellingSegregationModel-2 - An example of an Object Event Simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
================================================================================ -->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8" />
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale = 1.0" />
  <link rel="stylesheet" href="../../../../css/normalize.css" />
  <link rel="stylesheet" href="../../../../css/description.css" />
  <script src="../../../ui/description.js"></script>
  <script src="scenario.js"></script>
  <script>window.addEventListener('load', oes.ui.setupDescription);</script>
</head>
<body>
<div id="frontMatter">
  <p>Available on <a href="https://sim4edu.com">Sim4edu</a></p>
  <section id="shortDescription"></section>
  <p><strong>Classification tags</strong>: social science, grid space model, fixed-increment time progression.</p>
  <p>Other OES models for the same system/problem/domain:
    <a href="../006-SchellingSegregationModel-1/simulation.html">SchellingSegregationModel-1</a></p>
</div>
 <section class="collapsed"><h1><span>►</span>Background</h1>
   <p>Thomas C. Schelling, who is a co-recipient of the 2005 Nobel Prize in Economics, published a paper
     proposing a theory about the persistence of racial or ethnic segregation despite an environment of
     growing tolerance ("Dynamic Models of Segregation", Journal of Mathematical Sociology 1, 1971, 143-186.).
     He suggested that even if individuals tolerate racial diversity, if they also remain uneasy
     about being a minority in the locality, segregation will still be the equilibrium situation.</p>
   <p>Schelling placed pennies and dimes on a chess board and moved them around according to various rules.
     He interpreted the board as a city, with each square of the board representing a house or a lot.
     He interpreted the pennies and dimes as residents representing any two groups in society,
     such as two different races of people, boys and girls, smokers and non-smokers, etc.
     The neighborhood of a resident occupying any location on the board consisted of the squares
     adjacent to this location. Thus, interior (non-edge) residents could have up to	eight neighbors,
     non-corner edge residents could have a maximum of five neighbors, and corner edge residents could have
     a maximum of three neighbors. Rules could be specified that determined whether a particular resident
     was content in her current location. If she was not, she would try to move to another location on
     the board, or possibly just exit the board entirely. As can be expected, Schelling found that
     the board quickly evolved into a strongly segregated location pattern if the residents'
     &quot;contentness rules&quot; were specified so that segregation was heavily favored. Surprisingly,
     however, he also found that initially integrated boards tipped into full segregation even if the
     residents' contentness rules expressed only a mild preference for having neighbors of their own group.</p>
   <p>This problem description has been extracted from the paper
     <a href="http://cress.soc.surrey.ac.uk/~scs1ng/ngpub/paper148_NG.pdf">Varieties of Emergence</a> by Nigel Gilbert
     and from the page <a href="http://www.econ.iastate.edu/tesfatsi/abmread.htm#Modeling">On-Line Guide for Newcomers to
       Agent-Based Modeling in the Social Sciences</a> by Robert Axelrod and Leigh Tesfatsion.</p>
 </section>

  <section class="mbd collapsed"><h1><span>►</span>Conceptual Model <sup class="expl-req">?</sup></h1>
    <div id="expl-CM" class="expl"></div>
    <p>The conceptual model for studying seggregation as a social phenomena describes the behavior of residents as members
    of some group, being either content or uncontent with their neighborhood, and moving to another place
      as a consequence of being uncontent.</p>

    <section><h1>Conceptual Information Model <sup class="expl-req">?</sup></h1>
      <div id="expl-CIM" class="expl"></div>
      <p>Residential segregation involves residents living at some address as members of some group
        and their neighborhoods as spatial structures. Consequently, potentially relevant object types are:</p>
      <figure class="right-float">
        <img src="CIM-ObjT.svg" width="450"/>
        <figcaption>A conceptual model describing object types.</figcaption>
      </figure>
      <ol>
        <li>residential addresses,</li>
        <li>residents,</li>
        <li>groups of residents.</li>
      </ol>
      <p>Potentially relevant types of events are:</p>
      <ol>
        <li>residents periodically checking their neihborhood for determining if they are content with it,</li>
        <li>residents making a decision to move,</li>
        <li>residents searching for an acceptable place,</li>
        <li>residents moving.</li>
      </ol>
      <p>Both object types and event types, with their participation associations, can be visually described in
      a UML class diagram, as shown below.</p>
      <figure>
        <img src="CIM.svg" width="600"/>
        <figcaption>A conceptual information model describing object types and event types.</figcaption>
      </figure>
    </section>

    <section>
      <h1>Conceptual Process Model <sup class="expl-req">?</sup></h1>
      <div id="expl-CPM" class="expl"></div>
      <table>
        <caption>Event rules.</caption>
        <thead>
        <tr>
          <td>ON (event type)</td><td>DO (event routine)</td>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>Periodical neighborhood check</td>
          <td>If there is not a sufficient number of neighbors of the same group,
            then make a decision to move away.</td>
        </tr>
        <tr>
          <td>Making a decision to move</td>
          <td>Search a place with a sufficient number of neighbors of the same group.</td>
        </tr>
        <tr>
          <td>Search ends</td>
          <td>If the search was successful, then move to the place found, otherwise leave the area.</td>
        </tr>
        </tbody>
      </table>
    </section>
  </section>

<section class="mbd collapsed"><h1><span>►</span>Simulation Design Model <sup class="expl-req">?</sup></h1>
  <div id="expl-DM" class="expl"></div>

  <section><h1>Information Design Model <sup class="expl-req">?</sup></h1>
    <div id="expl-IDM" class="expl"></div>

    <p>The simulation design of the Schelling Segregation Model is based on choosing a <i>grid space</i> as a
      simplification of the real space, in which residents live in some neighborhood.
      Each resident occupies a specific grid cell with a neighborhood consisting of the 8 adjacent cells surrounding it,
      both vertically/horizontally (N/E/S/W) and diagonally (NE/SE/SW/NW).</p>
    <p>The <i>Object Event</i> simulation paradigm, supported by Sim4edu, provides two forms of grid spaces:
      (1) a simple <i>integer grid</i>, where a grid cell has an integer value that can, for instance, represent the group
      of the resident at this location, and (2) an <i>object grid</i>, where a grid cell is a special type of object
      such that one can define properties for it and associate it with a set of objects that are positioned
      on this grid cell. Obviously, choosing an integer grid as the space model of a simulation implies limitations on
      the information that can be represented per grid cell, but allows larger grid spaces compared to object grids
      because of its smaller memory footprint.</p>
    <p>In the <i>SchellingSegregationModel-2</i>, an object grid has been chosen as the space model such that each
      grid cell may have an associated resident object (in its <code>objects</code> list) representing the resident
      that occupies the cell. All resident objects are instances of the object class <code>Resident</code>
      and belong to a group, which is an instance of the object type <code>GroupOfResidents</code>. There is no need
      to model addresses as objects since the chosen space model provides grid cells as abstract locations.</p>
    <p>There are no explicit events (only implicit time step events). Consequently, the model does not contain any event types.</p>
  </section>

  <section>
    <h1>Process Design Model <sup class="expl-req">?</sup></h1>
    <div id="expl-PDM" class="expl"></div>

    <table>
      <caption>Event rules.</caption>
      <thead>
      <tr>
        <td>ON (event type)</td><td>DO (event routine)</td>
      </tr>
      </thead>
      <tbody>
      <tr>
        <td>Each simulation time step</td>
        <td>
          Pick a resident r that is uncontent and search for a free cell<br/>
          &nbsp;&nbsp;&nbsp;where r would be content<br/>
          IF there is such a cell<br/>
          THEN move r to ths cell<br/>
          ELSE remove r from the grid<br/>
          Re-evaluate the contentness of all residents affected by this change
        </td>
      </tr>
      </tbody>
    </table>
  </section>
</section>

</body>
</html>

