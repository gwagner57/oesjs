/*******************************************************
 * Predator-Prey - An example of a discrete event simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
********************************************************/

/*******************************************************
 Simulation Parameters
********************************************************/
sim.scenario.simulationEndTime = 10;
sim.config.stepDuration = 200;  // observation time per step in ms
sim.config.visualize = true;
sim.config.createLog = false;
/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "SchellingSegregationModel-2";
sim.model.title = "The Schelling Segregation Model with Residents as Objects";
sim.model.systemNarrative = "Residential segregation results from the behavior of residents as members of some group, being either content or uncontent with their neighborhood in terms of it including a sufficient number of residents of the same group, and moving to another place as a consequence of being uncontent. A residential space is populated by residents belonging to some group. Periodically, all residents check if they are content with their neighborhood, based on their degree of tolerating neighbors of a different group. If they are not, they move to a location where they are content, or leave the space if they don't find such a location.";
sim.model.shortDescription = "In the simulation model, the space is represented by an object grid (a 2-dimensional array of objects) with 'neighborhood' defined to consist of the 8 adjacent cells surrounding it, such that each grid cell has a resident object.";
sim.model.objectTypes = ["Resident", "GroupOfResidents"];
// meta data
sim.model.license = "CC BY-NC";
sim.model.creator = "Gerd Wagner";
sim.model.created = "2016-09-29";
sim.model.modified = "2016-09-29";
// Space model
sim.model.space.type = "ObjectGrid";
sim.model.space.gridCellMaxOccupancy = 1;  // at most 1 object per cell
sim.model.space.xMax = 50;
sim.model.space.yMax = 25;

// model variables
sim.model.v.neigborHoodRadius = 1;
sim.model.v.populationDensity = 0.6;  // how densely the grid is populated
sim.model.v.uncontentResidents = [];  // the list of residents that want to move
// model procedures/functions
/*
 * Compute the percentage of neighbors from different groups
 */
sim.model.f.neighbDiffLevel = function (posX, posY, groupNo) {
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax;
  var neighboursCount = 0, neighboursOfDifferentGroupCount = 0;
  var cellValue = 0, x = 0, y = 0;
  var radius = sim.model.v.neigborHoodRadius || 1;
  // scan all residents in the neighbourhood not considering the current location
  for (x = Math.max( posX-radius, 1); x <= Math.min( posX+radius, xMax); x++) {
    for (y = Math.max( posY-radius, 1); y <= Math.min( posY+radius, yMax); y++) {
      if (!(x === posX && y === posY)) {
        cellValue =  sim.space.grid[(y-1)*xMax + x-1] & 15;  //lower 4 bits
        if (cellValue !== 0) {
          neighboursCount++;
          if (cellValue !== groupNo) neighboursOfDifferentGroupCount++;
        }
      }
    }
  }
  if (neighboursCount === 0) return 0;  // no different neighbors
  else return neighboursOfDifferentGroupCount / neighboursCount;
};

/*******************************************************
 Grid cell properties
 ********************************************************/
sim.model.space.gridCellProperties = {};
/*******************************************************
 Define the observation UI (space view)
 ********************************************************/
sim.config.observationUI.spaceView = {
  type: "gridDom",
  gridCellSize: function () { // returns a positive integer which represents the number of px for a grid cell
    var xMax = sim.model.space.xMax, cellSize = 0,
        bodyWidth = parseInt( getComputedStyle( document.body).getPropertyValue("width"));
    if (bodyWidth - 6 * xMax > 1) cellSize = parseInt( (bodyWidth - 3 * xMax - 1) / xMax);
    return Math.max( cellSize, 3);  // the minimum cell size is 3
  },
  // gridCellColor: {},
  showGridCellInfoOnFlyOver: false
};
/*******************************************************
 Define the observation UI (objects view)
 ********************************************************/
sim.config.observationUI.objectViews = {
  "Resident": {
    content: "&#127793;",
    color: "brown",
    showObjectInfoOnFlyOver: false
  }
};

// ****************************************************************************
sim.model.OnEachTimeStep = function () {
  var i=0, xR=0, yR=0, g=0, N=0, pos=[], newPos=[], resident=null;
  var xMax = sim.model.space.xMax,
      yMax = sim.model.space.yMax,
      neighbDiffLevel = sim.model.f.neighbDiffLevel,
      groups = cLASS["GroupOfResidents"].instances;
  //-------------------------------------------------------------
  function findAcceptableLocation( resident) {
    var x = 0, y = 0, d = 0;
    var g = resident.groupNo,
        tol = groups[String(g)].toleranceLevel;
    // the maximum distance of the current grid cell to one of the border lines
    var maxDistance = Math.max( Math.max( xMax-x0, x0-1), Math.max( yMax-y0, y0-1));
    for (d=1; d <= maxDistance; d++) {
      // start North clockwise
      y = y0 + d;  // inspect the line North
      if (y <= yMax) {
        for (x = Math.max( x0-d, 1); x <= Math.min( x0 + d, xMax); x++) {
          if (cellVal(x,y) === 0 && neighbDiffLevel(x,y,g) <= tol) return [x,y];
        }
      }
      x = x0 + d;  // inspect the line East
      if (x <= xMax) {
        for (y = Math.min( y0+d-1, yMax); y >= Math.max( y0-d, 1); y--) {
          if (cellVal(x,y) === 0 && neighbDiffLevel(x,y,g) <= tol) return [x,y];
        }
      }
      y = y0 - d;  // inspect the line South
      if (y >= 1) {
        for (x = Math.min( x0+d-1, xMax); x >= Math.max( x0-d, 1); x--) {
          if (cellVal(x,y) === 0 && neighbDiffLevel(x,y,g) <= tol) return [x,y];
        }
      }
      x = x0 - d;  // inspect the line West
      if (x >= 1) {
        for (y = Math.max( y0-d+1, 1); y <= Math.min( y0+d-1, yMax); y++) {
          if (cellVal(x,y) === 0 && neighbDiffLevel(x,y,g) <= tol) return [x,y];
        }
      }
    }
    return null;
  }
  //-------------------------------------------------------------
  // Loop over all uncontent residents
  N = sim.model.v.uncontentResidents.length;
  for (i=0; i < N; i++) {
    resident = sim.model.v.uncontentResidents[i];
    // search for a free cell where the resident would be content
    newPos = findAcceptableLocation( resident);
    if (newPos) {  // move to new location
      sim.space.grid[newPos[0]][newPos[1]].addObject( resident);
      sim.space.grid[resident.pos[0]][resident.pos[1]].removeObject( resident);
    } else {  // leave grid
      sim.space.grid[resident.pos[0]][resident.pos[1]].removeObject( resident);
      sim.stat.residentsWhoLeftTheGrid++;
    }
  }
  // collect all residents that are still uncontent
  sim.model.v.uncontentResidents = [];
  oes.space.grid.forAllCells( function (x,y,c) {
    var g = cellVal(x,y), tol = 0;
    if (g > 0) {
      tol = groups[String(g)].toleranceLevel;
      if (neighbDiffLevel(x,y,g) > tol) {
        sim.model.v.uncontentResidents.push( resident);
      }
    }
  });
  sim.stat.nmrOfUncontentResidents = sim.model.v.uncontentResidents.length;
  console.log(sim.step, " ", sim.stat.nmrOfUncontentResidents);
};
/**********************************************************
 Define the initial state of the simulation system
***********************************************************/
sim.scenario.initialState.objects = {
  "1": {typeName: "GroupOfResidents", name: "A", toleranceLevel: 0.6},
  "2": {typeName: "GroupOfResidents", name: "B", toleranceLevel: 0.6}
};
sim.scenario.setupInitialState = function () {
  var xMax = sim.model.space.xMax;
  var groups = cLASS["GroupOfResidents"].instances,
      nmrOfGroups = Object.keys( groups).length;
  var populationDensity = sim.model.v.populationDensity;
  var neighbDiffLevel = sim.model.f.neighbDiffLevel;
  sim.model.v.uncontentResidents = [];
  // populate a portion of the grid cells
  oes.space.grid.forAllCells( function (x,y,c) {
    var g=0, resident=null;
    if (Math.random() < populationDensity) {
      // pick a random group number
      g = rand.uniformInt( 1, nmrOfGroups);
      // create resident object with this group no
      resident = new Resident({groupNo: g, pos: [x,y]});
      // add resident object to simulation objects
      sim.addObject( resident);
      // add resident object to grid cell
      c.addObject( resident);
    }
  });
  // collect all uncontent residents
  oes.space.grid.forAllCells( function (x,y,c) {
    var objIDsOfObjectsOnCell = Object.keys( c.objects),
        resObj=null, tol = 0.0, g=0;
    if (objIDsOfObjectsOnCell.length > 0) {  // cell is occupied by a resident
      resObj = c.objects[objIDsOfObjectsOnCell[0]];
      g = resObj.groupNo;
      if (!groups[String(g)]) throw "No group for number "+ String(g);
      tol = groups[String(g)].toleranceLevel;
      if (neighbDiffLevel(x,y,g) > tol) {
        sim.model.v.uncontentResidents.push( resObj);
      }
    }
  });
  sim.stat.nmrOfUncontentResidents = sim.model.v.uncontentResidents.length;
  console.log( sim.step, " ", sim.stat.nmrOfUncontentResidents);
};
/*******************************************************
 Define Output Statistics Variables
 ********************************************************/
sim.model.statistics = {
  "residentsWhoLeftTheGrid": {range:"NonNegativeInteger", label:"Residents gone"},
  "nmrOfUncontentResidents": {range:"NonNegativeInteger", label:"Uncontent resid.", showTimeSeries: true}
};
