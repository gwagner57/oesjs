var Resident = new cLASS({
  Name: "Resident",
  supertypeName: "ObjectInGridSpace",
  properties: {
    "groupNo": {range: "PositiveInteger", label:"Group number",
        hint:"The number of the group the resident belongs to"}
  },
  methods: {
  }
});
