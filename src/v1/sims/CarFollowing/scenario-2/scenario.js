/*********************************************************************
 * Car Following - An example of a continuous state change simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 *********************************************************************/

/*******************************************************
 Simulation Parameters
 ********************************************************/
sim.scenario.simulationEndTime = 100;
sim.model.timeIncrement = 0.01;   // 100 ms
sim.config.stepDuration = 10;  // observation time per step in ms
sim.config.visualize = true;
sim.config.createLog = false;

sim.model.objectTypes = ["Car"];

/*******************************************************
 Observation User Interface
 ********************************************************/
// the following visualization settings are the defaults for a "1D" space
sim.config.observationUI.spaceView = {
  type: "threeDim",  // must be compatible with space model type
  // ground texture - if not set, then groundColor param is used
  groundImage: "ground.jpg",
  // texture applied on the track
  trackImage: "street.jpg",
  // ground texture - if not set, defaults to #606060 (dark gray)
  // NOTE: used only if groundImage param is not set
  groundColor: "#606060",
  // if not set (or set to 0), the full available width is used
  width: 0,
  // if not set (or set to 0), default value is 400px
  height: 600,
  // Canvas color (i.e., the empty 3D space color)
  // if not set the default value is #B6DCF6 (light blue)
  // NOTE: only HTML color codes are allowed, it does not work with CSS color names!
  canvasColor: "#B6DCF6",
  // the width of the track
  trackWidth: 10,
  // if not set, the default value is #BFBFBF (gray)
  // NOTE: use CSS color names or HTML hex color format (e.g., #FF0058)
  trackColor: "#bfbfbf"
};
sim.config.observationUI.objectViews["Car"] = {
  shape: "circle",  // default - in 3D view it displays a sphere
  // object-specific views
  "1": {
    shape: "square",
    fillColor: "blue"
  }
};
sim.config.observationUI.monitor["distance"] = {
  type: "Meter",
  variable: {type:"statistics"}
};

/**********************************************************
 Define the initial state of the simulation system
 ***********************************************************/
sim.scenario.initialState.objects = {
  "1": {typeName: "Car", name: "leader", pos:[50,0], vel:[17,0], acc:[5,0]},
  "2": {typeName: "Car", name: "follower1", pos:[25,0], vel:[17,0], acc:[0,0], leadingCar: 1},
  "3": {typeName: "Car", name: "follower2", pos:[0,0], vel:[17,0], acc:[0,0], leadingCar: 2}
};
//sim.scenario.setupInitialState = function () {};

