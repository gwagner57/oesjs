var Car = new cLASS({
  Name: "Car",
  supertypeName: "ObjectInOneDimSpace",
  properties: {
    "leadingCar": {range:"Car", optional: true}
  },
  methods: {
    "getDistance": function () {
      if (!this.leadingCar) return 0;
      else return this.leadingCar.pos[0] - this.pos[0];
    },
    "getNextAcceleration": function () {
      if (this.leadingCar) {  // a following car
        return Car.reactionSensitivity *
          (this.getDistance() - this.vel[0] * Car.securityTimeDistance);
      } else {
        if (this.vel[0] > 33.33) return -5.0;  // 120 km/h
        else if (this.vel[0] < 16.66) return 5.0;
        else return this.acc[0];
      }
    }
  }
});
/*
 *  class-level (static) attributes
 */
Car.securityTimeDistance = 2.0;
Car.reactionSensitivity = 5.0;

