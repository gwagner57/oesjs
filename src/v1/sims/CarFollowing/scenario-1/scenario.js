/*********************************************************************
 * Car Following - An example of a continuous state change simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
*********************************************************************/

/*******************************************************
 Simulation Parameters
********************************************************/
sim.scenario.simulationEndTime = 100;
sim.model.timeIncrement = 0.05;   // 50 ms
sim.config.stepDuration = 50;  // observation time per step in ms
sim.config.visualize = true;
sim.config.createLog = false;
sim.scenario.suppressInitialStateUI = true;

sim.model.objectTypes = ["Car"];

// the following visualization settings are the defaults for a "1D" space
sim.config.observationUI.spaceView = {
  type: "oneDimSVG",  // must be compatible with space model type
  // space-view-type-specific properties
  canvasColor: "white",
  // trackDiameter: 500,
  trackWidth: 10,
  trackColor: "grey"
};
sim.config.observationUI.objectViews["Car"] = {
  suppressIdName: true,  // by default ID and Name are shown
  //showInfo: "fixedText",  // shown next to view
  //showProperties: [prop1, prop2, ...],  // shown next to view
  shape: "circle",  // default
  // object-specific views
  "1": {
    fillColor: "blue"
  }
};
sim.config.observationUI.monitor["distance"] = {
  type: "Meter",
  variable: {type:"statistics"}
};
/**********************************************************
 Define the initial state of the simulation system
***********************************************************/
sim.scenario.initialState.objects = {
  "1": {typeName: "Car", name: "leader", pos:[70,0], vel:[0,0], acc:[6,0]},
  "2": {typeName: "Car", name: "follower1", pos:[30,0], vel:[0,0], acc:[0,0], leadingCar: 1},
  "3": {typeName: "Car", name: "follower2", pos:[0,0], vel:[0,0], acc:[0,0], leadingCar: 2}
};
//sim.scenario.setupInitialState = function () {};

