/*********************************************************************
 * Car Following - An example of a continuous state change simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
*********************************************************************/

/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "CarFollowing-1";
sim.model.title = "Car Following";
sim.model.systemNarrative = "A car following behavior in a situation where one car follows another car consists of the following car adapting its speed to the speed of the car in front of it.";
sim.model.shortDescription = "An example of a continuous state change simulation with cars as moving objects that are subject to continuous changes of their acceleration, velocity and position in a simple 1-dimensional space.";
sim.model.objectTypes = ["Car"];
// meta data
sim.model.license = "CC BY-NC";
sim.model.creator = "Gerd Wagner";
sim.model.created = "2016-09-01";
sim.model.modified = "2016-11-15";
// Space model
sim.model.space.type = "1D";  // 1-dimensional continuous space
sim.model.space.xMax = 500;  // distance of the simulated track (in m)

sim.model.OnEachTimeStep = function () {
  var i= 0, o=null, objIdStr="";
  var keys = Object.keys( ObjectInOneDimSpace.instances);
  for (i=0; i < keys.length; i++) {
    objIdStr = keys[i];
    o = ObjectInOneDimSpace.instances[objIdStr];
    // compute next state on basis of current state
    o.acc[1] = o.getNextAcceleration();
    o.vel[1] = o.computeNextVelocity();
    o.pos[1] = o.computeNextPosition();
    // update current state
    o.acc[0] = o.acc[1];
    o.vel[0] = o.vel[1];
    o.pos[0] = o.pos[1];
    //if (sim.step % 10 === 0) console.log( o.vel[0], o.getDistance());
  }
};