/*******************************************************************************
 * SingleEvent event class
 * 
 * @copyright Copyright 2018 Brandenburg University of Technology, Germany
 * @license The MIT License (MIT)
 * @author Luis Gustavo Nardin
 * @author Gerd Wagner
 ******************************************************************************/
var SingleEvent = new cLASS( {
  Name: "SingleEvent",
  shortLabel: "D",
  supertypeName: "eVENT",
  
  // Properties
  properties: {},
  
  // Methods
  methods: {
    "onEvent": function () {
      var followupEvents = [];
      
      var newEvts = rand.uniformInt( 1, 2 );
      
      for ( i = 0; i < newEvts; i += 1 ) {
        followupEvents.push( new SingleEvent( {
          occTime: this.occTime + rand.uniformInt( 1, 10000 )
        } ) );
      }
      
      return followupEvents;
    }
  }
} );
SingleEvent.priority = 1;
