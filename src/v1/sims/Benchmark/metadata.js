var sim = sim || {};
sim.model = sim.model || {};
sim.scenario = sim.scenario || {};
sim.config = sim.config || {};

var oes = oes || {};
oes.ui = oes.ui || {};
oes.ui.explanation = {};

/*******************************************************************************
 * Simulation Model
 ******************************************************************************/
sim.model.name = "Benchmark-1";
sim.model.title = "Event List Benchmark Model";
sim.model.systemNarrative = "";
sim.model.shortDescription = "";
sim.model.source = "";
sim.model.license = "CC BY-NC";
sim.model.creator = "Luis Gustavo Nardin";
sim.model.contributors = "Gerd Wagner";
sim.model.created = "2018-05-15";
sim.model.modified = "2018-05-15";
