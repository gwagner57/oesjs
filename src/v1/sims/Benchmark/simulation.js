/*******************************************************************************
 * Event List Benchmark Model
 * 
 * @copyright Copyright 2018 Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Luis Gustavo Nardin
 * @author Gerd Wagner
 ******************************************************************************/
/*******************************************************************************
 * Simulation Parameters
 ******************************************************************************/
// sim.scenario.title = "..."; // optional
// sim.scenario.name = "..."; // optional
// sim.scenario.shortDescription = "<p></p>"; // optional
// sim.scenario.creator = "..."; // optional
// sim.scenario.created = "..."; // optional
// sim.scenario.modified = "..."; // optional
sim.scenario.simulationEndTime = 1000000;
sim.scenario.idCounter = 0; // optional
sim.scenario.randomSeed = 1234; // optional
/*******************************************************************************
 * Simulation Configuration
 ******************************************************************************/
sim.config.createLog = false;
sim.config.visualize = false;
sim.config.userInteractive = false;
/*******************************************************************************
 * Simulation Model
 ******************************************************************************/
sim.model.time = "discrete";
sim.model.timeUnit = "D"; // days

sim.model.objectTypes = [];
sim.model.eventTypes = ["SingleEvent"];

/*******************************************************************************
 * Define Initial State
 ******************************************************************************/
// Objects
sim.scenario.initialState.objects = {};

// Events
sim.scenario.initialState.events = [{
  typeName: "SingleEvent",
  occTime: 1
}];

// Setup the initial state of the simulation scenario
sim.scenario.setupInitialState = function () {
};

/*******************************************************************************
 * Define Output Statistics Variables
 ******************************************************************************/
sim.model.statistics = {};
