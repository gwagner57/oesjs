<!-- ===========================================================================
 * Green House with 2D isometric visualization.
 * @copyright Copyright 2016 Mircea Diaconescu, BTU (Germany)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
================================================================================ -->
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta charset="utf-8">
  <title></title>
  <meta name="viewport" content="width=device-width, initial-scale = 1.0" />
</head>
<body>
<div id="frontMatter">
  <h1>Green House with 2D Isometric Visualization</h1>
  <section id="shortDescription"></section>
  <p><strong>Classification tags</strong>: environment simulation, Green House, sensor, actuator.</p>
</div>
<section class="collapsed"><h1><span>►</span>Background</h1>
    <p>Growing vegetables, and in general doing agriculture, is one of the oldest occupations of humanity.
        Over many decades was observed that it is possible to grow vegetables in a climate unfriendly region,
        as long as some of the specific environment characteristics are kept under control, providing optimal
        or close to optimal conditions. It is important to notice, that while some small deviations from the
        optimal conditions does not necessarily mean a low or no production, it still affects some of the plants,
        more sensitive than others, resulting in a decrease (not necessarily proportional in quantity) of the final
        vegetables production quantity.
    </p>
    <p>One interesting concept is that, during the daytime, the temperature is increased because of the sunlight, thus
    reducing the electricity consumption. For this, the Green House walls are made of transparent materials, such as
    special glass or polymeric glass, which allows the infrared radiation to enter the Green House, thus increasing
        the inside temperature. However, during the night, the temperature may drop under the required limits, specially
        outside the season or in regions with a harsh climate. For such cases, a heater is used to maintain the lower
        limit of the temperature.</p>
    <p>In this scenario, we simulate a Green House environment, where the temperature, air humidity and soil moisture
    characteristics are observed. For being able to control these environment qualities, water pumps,
        humidifiers/de-humidifiers and heaters are used. </p>
    <figure>
        <img src="media/img/greenhouseeffects.jpg"/>
        <figcaption>The Green House Effect.</figcaption>
    </figure>
</section>

<section class="mbd collapsed"><h1><span>►</span>Conceptual Model <sup class="expl-req">?</sup></h1>
    <div class="expl">The conceptual model, also called <i>domain model</i>, describes the real-world <i>system
        under investigation</i> by identifying the relevant types of objects and events, and describing their dynamics,
        allowing to understand what's going on in the system.</div>
    <p>The conceptual model for studying the Green House environment requires to model the behavior of the
        environment with respect to three important qualities: temperature, relative air humidity and soil moisture.
        Those factors are monitored and "partially" (e.g., heating is possible, but cooling is not) controlled by
        the mean of actuators, whenever sub-optimal conditions affecting the good evolution of the plants
        are detected.</p>

    <section><h1>Conceptual Information Model <sup class="expl-req">?</sup></h1>
        <div class="expl">
            <p>A conceptual information model describes the subject matter vocabulary used, e.g.,
                in the system narrative, in a (semi-)formal way. Such a vocabulary essentially consists of names for</p>
            <ul>
                <li><strong>types</strong>, corresponding to <i>classes</i> in OO modeling, or <i>unary predicates</i> in formal logic,</li>
                <li><strong>properties</strong> corresponding to <i>binary predicates</i> in formal logic,</li>
                <li><strong>associations</strong> corresponding to <i>n-ary predicates</i> in formal logic.</li>
            </ul>
            <p>The main categories of types are <i>object types</i> and <i>event types</i>. A simple form of
                conceptual information model is obtained by providing a list of each of them, while a more
                elaborated model, preferably in the form of a UML class diagram, also defines properties and associations,
                including the <strong>participation</strong> of objects (of certain types) in events (of certain types).</p>
        </div>
        <p>The relevant object types are:</p>
        <ol>
            <li>temperature, relative air humidity and soil moisture sensors,</li>
            <li>water pump, humidifier / de-humidifier and heater actuators,</li>
            <li>the plant, and in particular the tomato plant type.</li>
        </ol>
        <p>The relevant types of events are:</p>
        <ol>
            <li>the water pump can be activated or deactivated</li>
            <li>the humidifier is turned ON in humidification / de-humidification mode, or it is turned OFF,</li>
            <li>the heater is turned ON or OFF.</li>
        </ol>
    </section>

    <section>
        <h1>Conceptual Process Model <sup class="expl-req">?</sup></h1>
        <div class="expl">
            <p>In a conceptual process model, we describe the dynamics of the system under investigation. We can do this by</p>
            <ol>
                <li>identifying the <b><i>relevant types of events</i></b>, which account for the causation of relevant state changes
                    and follow-up events;</li>
                <li>describing, for each of these event types, the <b><i>causal regularity</i></b> associated with it in the form of an
                    <b><i>event rule</i></b> that defines the <b><i>state changes</i></b> and <b><i>follow-up events</i></b>
                    triggered by events of that type.</li>
            </ol>
            <p>Any event type modeled in the information model could potentially trigger a causal regularity. For simplicity,
                however, we may merge (and omit) those types of events, which can be considered to temporally coincide
                with events of another type.</p>
        </div>
        <table>
            <caption>Event rules.</caption>
            <thead>
            <tr>
                <td>ON (event type)</td><td>DO (event routine)</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Periodically enable sensor reading</td>
                <td>If the read quantities are in the low side of the optimal ranges, then enable the associated
                    actuator, responsible with the control of that specific monitored environment quality.
                    For the case of relative air humidity, if the actuator is already started in de-humidification
                    mode, then a "turn OFF" action is required.</td>
            </tr>
            <tr>
                <td>Making a actuator switch OFF decision</td>
                <td>If the actuator is turned ON and the corresponding sensor reads a value "near the middle" of
                    the optimal conditions interval is reached, then switch it OFF.</td>
            </tr>
            <tr>
                <td>Making a actuator switch ON decision</td>
                <td>If the actuator is turned OFF and the corresponding sensor reads a value below or over the
                    optimal range, switch it ON.</td>
            </tr>
            <tr>
                <td>Compute the environmental effects of the plants</td>
                <td>On each step, representing a minute of real time, the effect of the environment changes due to
                plants is computed. Thus, the plants consumes water from soil as well as from the relative
                    air humidity</td>
            </tr>
            </tbody>
        </table>
    </section>
</section>

<section class="mbd collapsed"><h1><span>►</span>Simulation Design Model <sup class="expl-req">?</sup></h1>

    <div class="expl">The simulation design model defines a computational design for a simulation based on a conceptual model.
        Unlike the conceptual model, the design is tailored towards the purpose of the simulation project
        (e.g., for answering certain research questions in a social system analysis project or in
        a technical system engineering project, or for teaching certain facts about a system in an
        educational simulation project). Although the design model is independent of a specific
        technology platform, it is typically based on object-oriented modeling (e.g., with UML diagrams).
        It can be implemented in different ways with any specific technology choice, typically using an
        object-oriented programming approach.</div>

    <section><h1>Information Design Model <sup class="expl-req">?</sup></h1>
        <div class="expl">
            <p>An information design model is normally derived from a conceptual information model
                by <b><i>choosing the design-relevant types</i></b> of objects and events and enrich them with design details,
                while dropping other object types and event types not deemed relevant for the simulation design. Adding design details
                includes specifying <b><i>property ranges</i></b> as well as adding multiplicity and other types of
                <b><i>constraints</i></b>.</p>
            <p>In addition to these general information modeling issues, there are also a few issues, which are
                specific for simulation modeling:</p>
            <ol>
                <li>If the simulation is to deal with <b><i>objects in space</i></b>, the design model must be based on a
                    choice of <b><i>space model</i></b>: one-dimensional (1D) discrete space, two-dimensional (2D) discrete space
                    (also called <i>grid space</i>), three-dimensional (3D) discrete space, and 1D/2D/3D continuous space.
                    The chosen space model implies a corresponding form of spatial <i>positions</i> (or <i>locations</i>):
                    a 1-, 2- or 3-tuple of integers or decimal numbers.</li>
                <li>The information design model must distinguish between <b><i>exogenous</i></b> and <b><i>caused</i></b>
                    (or <i>endogenous</i>) event types. For any exogenous event type, the <b><i>recurrence</i></b> of events
                    of that type must be specified, typically in the form of a random variable, but in some cases it may be
                    a constant (like "on each Monday"). The recurrence defines the elapsed time between two consecutive events
                    of the given type (their inter-occurrence time). It can be specified within
                    the event class concerned in the form of a special method with the predefined name "recurrence".</li>
                <li>Certain simulation variables, such as the <i>quantity</i> attribute of <i>Demand</i> events,
                    or the occurrence delay of <i>Delivery</i> events, may be subject to random variation, which
                    can be modeled with <b><i>random variables</i></b> having a suitable probability distribution.
                    In the case of a random variable attribute, we can use the UML stereotype «rva» for categorizing the
                    attribute as a random variable attribute. In the case of another kind of simulation variable, we can
                    use a method stereotyped «rvm» for categorizing it as a <i>random variate (sampling) method</i> realizing
                    a corresponding random variable. In both cases, we can indicate the underlying probability distribution
                    in the model diagram by appending a symbolic expression denoting a distribution to the attribute or
                    method definition clause.</li>
            </ol>
        </div>
        <p>The simulation design of the Green House Model is based on choosing a <i>continuous 2D space</i>. A special
            overlay grid (discrete grid space) is applied on top of it. The overlay space is used to simplify the
            computation of the (continuous) environment quality variations. In particular, computing the water
            dissipation within the ground is made in a radial way, which means that the soil moisture increase
            is inverse proportional with the distance from the water pump position and the position on which the
            soil moisture is read by the sensor.</p>
    </section>

    <section>
        <h1>Process Design Model <sup class="expl-req">?</sup></h1>
        <div class="expl">In the process design model, we refine the conceptual process model. We can do this by
            identifying those types of events that account for the causation of relevant state changes and follow-up
            events by triggering a causal regularity. Any event type modeled in the information model could potentially
            trigger a causal regularity. For simplicity, however, we may omit those types of events, which can be
            considered to temporally coincide with events of another type.</div>
        <table>
            <caption>Event rules.</caption>
            <thead>
            <tr>
                <td>ON (event type)</td><td>DO (event routine)</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>EachSimulationTimeStep</td>
                <td>
                    Check if the a specific sensor needs to be read. <br/>
                    Various sensors are read at various time intervals.<br/><br/>
                    <strong>DO</strong> compute statistics: e.g., water and electricity consumption<br/>
                    <strong>IF</strong> ( sensor.read() < optimalConditionsRange * allowedDeviation)<br/>
                    <strong>THEN</strong> turn ON the corresponding actuator.<br/>
                    <strong>ELSE IF</strong> ( sensor.read() > optimalConditionsRange * allowedDeviation)<br/>
                    <strong>THEN</strong> turn OFF the corresponding actuator.<br/><br/>
                    <em><strong>Note 1:</strong> corresponding actuator means the actuator in charge with the
                        control or variation of the specific environment quality monitored by the specific
                        sensor.</em><br/><br/>
                    <em><strong>Note 2:</strong> a special case occurs when the relative air humidity is below the
                        threshold and the humidifier actuator is already turned ON in de-humidification mode. In this
                        case, the humidifier actuator needs to be switched OFF.</em>
                </td>
            </tr>
            </tbody>
        </table>
    </section>
</section>
<!-- load all the required files, including scenario.js - it also triggers the initialization -->
<script src="../loadManager.js"></script>

</body>
</html>

