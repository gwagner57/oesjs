/*******************************************************************************
 * The StopHumidifier event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var StopHumidifier = new cLASS({
  Name: "StopHumidifier",
  supertypeName: "eVENT",
  properties: {
    "humidifierRef": {range: "Heater", label: "Humidifier"},
    "humidity": {range: "Decimal", label: "Humidity"},
    "reduceMode": {range: "Boolean", label: "Reduce mode"}
  },
  methods: {
    "onEvent": function () {
      var hum = this.humidifierRef;
      hum.active = false;
      hum.workingCapacity = 0;
      hum.reduceMode = this.reduceMode || false;
      console.log("Day: " + sim.model.v.dayNumber + ", minute: " + sim.step
        + ", the humidifier with id=" + hum.id + ", located in cell="
        + oes.space.overlayGrid.getCell(hum.pos[0], hum.pos[1]).pos
        + ", was deactivated when HumiditySensor.value=" + this.humidity[1]
        + ", and Cell.humidity=" + this.humidity[0] + ".");
      return [];
    }
  }
});

