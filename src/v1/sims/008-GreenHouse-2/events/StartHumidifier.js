/*******************************************************************************
 * The StartHumidifier event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var StartHumidifier = new cLASS({
  Name: "StartHumidifier",
  supertypeName: "eVENT",
  properties: {
    "humidifierRef": {range: "Humidifier", label: "Humidifier"},
    "humidity": {range: "Array", label: "Humidity"},
    "reduceMode": {range: "Boolean", label: "Reduce mode"}
  },
  methods: {
    "onEvent": function () {
      var hum = this.humidifierRef;
      hum.active = true;
      hum.workingCapacity = 100;
      hum.reduceMode = this.reduceMode || false;
      console.log("Day: " + sim.model.v.dayNumber + ", minute: " + sim.step
        + ", the humidifier with id=" + hum.id + ", located in cell="
        + oes.space.overlayGrid.getCell(hum.pos[0], hum.pos[1]).pos
        + ", was activated in mode='" + (hum.reduceMode ? 'dehumidification' : 'humidification')
        + "', when HumiditySensor.value=" + this.humidity[1]
        + ", and Cell.humidity=" + this.humidity[0] + ".");
      return [];
    }
  }
});

