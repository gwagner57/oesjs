/*******************************************************************************
 * The NewDay event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var NewDay = new cLASS({
  Name: "NewDay",
  supertypeName: "eVENT",
  properties: {
    "dayNumber": {range: "PositiveInteger", label: "Day number"}
  },
  methods: {
    "onEvent": function () {
      var avgDayTemperature = 0;
      // set the current day number
      sim.model.v.dayNumber = this.dayNumber;
      // compute the temperatures for the new day
      sim.model.f.computeNextDayTemperatures();
      // compute max outside temperature
      sim.stat.maxOTemperature =
          Math.max(sim.stat.maxOTemperature, sim.model.v.highT);
      // compute min outside temperature
      if (sim.stat.minOTemperature === 0)
        sim.stat.minOTemperature = sim.model.v.lowT;
      else
        sim.stat.minOTemperature = Math.min(sim.stat.minOTemperature, sim.model.v.lowT);
      // compute the average temperature outside GH
      avgDayTemperature = (sim.model.v.highT + sim.model.v.lowT) / 2.0;
      sim.stat.avgOTemperature +=
        (avgDayTemperature - sim.stat.avgOTemperature) / sim.model.v.dayNumber;
      // store the latest temperature value for each cell, which is used then
      // to compute the temperature variation each minute, so that the min/max
      oes.space.overlayGrid.forAllCells(function (x, y, cell) {
          cell.lastDayTemperature = cell.temperature;
      });
      console.log("Day: " + sim.model.v.dayNumber + ", minute: " + sim.step 
        + " ---> Weather prognosis for today: outer(max: " + sim.model.v.highT + ", min: "
        + sim.model.v.lowT + ")°C, GreenHouse("
        + (sim.model.v.highT * sim.model.v.greenHouseEffectFactor).toFixed(2) + ", "
        + (sim.model.v.lowT * ( 2 - sim.model.v.thermalIsolationFactor)).toFixed(2) + ")°C.");
      return [];
    }
  }
});

NewDay.recurrence = function () {
  return 1440; // 1 simulation step = 1 minute, 1 day = 1440 minutes
};

NewDay.createNextEvent = function (e) {
  return new NewDay({
    occTime: e.occTime + NewDay.recurrence(),
    dayNumber: parseInt(e.occTime / NewDay.recurrence()) + 2
  });
};

