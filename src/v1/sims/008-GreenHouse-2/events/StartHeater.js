/*******************************************************************************
 * The StartHeater event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var StartHeater = new cLASS({
  Name: "StartHeater",
  supertypeName: "eVENT",
  properties: {
    "heaterRef": {range: "Heater", label: "Heater"},
    "temperature": {range: Array, label: "Temperature"}
  },
  methods: {
    "onEvent": function () {
      var h = this.heaterRef;
      h.active = true;
      h.workingCapacity = 100;
      console.log("Day: " + sim.model.v.dayNumber + ", minute: " + sim.step
        + ", the heater with id=" + h.id + ", located in cell="
        + oes.space.overlayGrid.getCell(h.pos[0], h.pos[1]).pos
        + ", was activated when TemperatureSensor.value=" + this.temperature[1]
        + ", and Cell.temperature=" + this.temperature[0] + ".");
      return [];
    }
  }
});