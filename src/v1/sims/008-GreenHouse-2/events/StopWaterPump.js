/*******************************************************************************
 * The StopWaterPump event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var StopWaterPump = new cLASS({
  Name: "StopWaterPump",
  supertypeName: "eVENT",
  properties: {
    "waterPumpRef": {range: "WaterPump", label: "Water pump"},
    "soilMoisture": {range: "Array", label: "Soil moisture"}
  },
  methods: {
    "onEvent": function () {
      var wp = this.waterPumpRef;
      if (!wp.active) return []; // already turned OFF
      wp.active = false;
      wp.workingCapacity = 0;
      console.log("Day: " + sim.model.v.dayNumber + ", minute: " + sim.step
        + ", the water pump with id=" + wp.id + ", located in cell="
        + oes.space.overlayGrid.getCell(wp.pos[0], wp.pos[1]).pos
        + ", was deactivated when SoilMoistureSensor.value=" + this.soilMoisture[1]
        + ", and Cell.soilMoisture=" + this.soilMoisture[0] + ".");
      return [];
    }
  }
});