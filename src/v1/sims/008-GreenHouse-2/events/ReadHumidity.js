/*******************************************************************************
 * The ReadHumidity event class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var ReadHumidity = new cLASS({
  Name: "ReadHumidity",
  supertypeName: "eVENT",
  properties: {},
  methods: {
    "onEvent": function () {
      var events = [], occTime = this.occTime;
      var getCell = oes.space.overlayGrid.getCell;
      var spaceData = sim.model.v.spaceData;
      var oc = spaceData.optimalConditions,
          ocAvg = oc.rh[0] + (oc.rh[1] - oc.rh[0]) / 2;
      // reading sensors...
      spaceData.sensors.rh.forEach(function(sensor, pos) {
        var hum = spaceData.actuators.hum[pos];
        // map real world humidity in the one perceived by the sensor
        var cell = getCell(sensor.pos[0], sensor.pos[1]);
        sensor.read(cell.humidity);
        if (!hum.active) {
          // humidity is too low, need to activate the corresponding humidifier
          // which is located in the same block with this humidity sensor
          if (sensor.value < oc.rh[0]) {
            events.push(new StartHumidifier({
              occTime: occTime + 1,
              humidifierRef: hum,
              humidity: [cell.humidity, sensor.value],
              reduceMode: false
            }));
          }
          // humidity is too high, need to activate the corresponding humidifier
          // in "reduce" mode, which is located in the same block with this
          // humidity sensor
          else if (sensor.value > oc.rh[1]) {
            events.push(new StartHumidifier({
              occTime: occTime + 1,
              humidifierRef: hum,
              humidity: [cell.humidity, sensor.value],
              reduceMode: true
            }));
          }
        }
        // stop humidifier, if active, and humidity
        // is close to the average optimal value
        else if ((sensor.value > ocAvg && !hum.reduceMode)
                 || (sensor.value < ocAvg && hum.reduceMode)) {
          events.push(new StopHumidifier({
            occTime: occTime + 1,
            humidifierRef: hum,
            humidity: [cell.humidity, sensor.value],
            reduceMode: false
          }));
        }
      });
      return events;
    }
  }
});

ReadHumidity.recurrence = function () {
  return 5; // perform temperature readings every 5 minute
};

ReadHumidity.createNextEvent = function (e) {
  return new ReadHumidity({
    occTime: e.occTime + ReadHumidity.recurrence()
  });
};

