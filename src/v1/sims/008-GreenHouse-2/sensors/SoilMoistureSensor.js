/*******************************************************************************
 * The SoilMoistureSensor model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var SoilMoistureSensor = new cLASS({
  Name: "SoilMoistureSensor",
  supertypeName: "AnalogSensor",
  properties: {
    "value": {range: "Decimal", min: 0, max: 100, label: "Moisture"},
    "accuracy": {range: "Decimal", min:0, max: 100, initialValue: 5, label: "Accuracy"},
    "precision": {range: "Decimal", min:0, max: 100, initialValue: 5, label: "Precision"},
    "resolution": {range: "Decimal", min:0, max: 100, initialValue: 1, label: "Resolution"}
  },
  methods: {}
});