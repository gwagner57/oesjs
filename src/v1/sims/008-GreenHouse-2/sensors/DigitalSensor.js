/*******************************************************************************
 * The DigitalSensor model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var DigitalSensor = new cLASS({
  Name: "DigitalSensor",
  supertypeName: "Sensor",
  properties: {},
  methods: {}
});