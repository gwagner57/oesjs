/*******************************************************************************
 * The TemperatureSensor model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var TemperatureSensor = new cLASS({
  Name: "TemperatureSensor",
  supertypeName: "DigitalSensor",
  properties: {
    "value": {range: "Decimal", min: -40, max: 80, label: "Temperature"},
    "accuracy": {range: "Decimal", min:0, max: 100, initialValue: 1, label: "Accuracy"},
    "precision": {range: "Decimal", min:0, max: 100, initialValue: 3, label: "Precision"},
    "resolution": {range: "Decimal", min:0, max: 100, initialValue: 0.5, label: "Resolution"}
  },
  methods: {}
});