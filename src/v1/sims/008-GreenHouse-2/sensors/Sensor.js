/*******************************************************************************
 * The Sensor model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var Sensor = new cLASS({
  Name: "Sensor",
  supertypeName: "ObjectInTwoDimSpace",
  properties: {
    "value": {range: "Decimal", label: "Sensed value"},
    "accuracy": {range: "Decimal", label: "Accuracy"},
    "precision": {range: "Decimal", label: "Precision"},
    "resolution": {range: "Decimal", label: "Resolution" }
  },
  methods: {
    "read": function (v) {
      // convert value to resolution, if one is given
      // NOTE: if resolution <=0, then the sensor has a
      //       perfect resolution, that is: close to 1/∞
      this.value = this.resolution ? this.toResolution(v): v;
      // convert value to accuracy, if one is given
      // NOTE: if accuracy is not provided, then the sensor has a
      //       perfect accuracy.
      this.value = this.accuracy ? this.toAccuracy(v) : v;
    },
    "toResolution": function (v) {
      // The result is rounded up or down based on the resolution
      // A resolution factor < 0.5 is rounded down, and >= 0.5 is rounded up.
      // TODO: check when it makes sense to use ceil instead of floor
      return Math.round(v / this.resolution) * this.resolution;
    },
    "toAccuracy": function (v) {
      // use normal (Gaussian) distribution for accuracy
      // - the mean is 0 (middle of the accuracy interval, [-acc, acc]
      // - the standard deviation is 1/3 * acc
      // NOTE: accuracy is given as % value from the read value.
      return v + rand.normal(0, 1/3 * v * this.accuracy / 100.0);
    }
  }
});