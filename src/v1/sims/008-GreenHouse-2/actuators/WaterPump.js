/*******************************************************************************
 * The WaterPump actuator model class
 *
 * When active, a water pump works at 100% capacity, without being possible
 * to adjust its working capacity.
 * A water pump is able to supply 25L of water every minute.
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var WaterPump = new cLASS({
  Name: "WaterPump",
  supertypeName: "Actuator",
  properties: {
    "waterFlow": {range: "Decimal", label:"Water flow (l/min)"}
  },
  methods: {}
});