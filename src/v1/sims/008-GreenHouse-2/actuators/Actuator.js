/*******************************************************************************
 * The Actuator model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var Actuator = new cLASS({
  Name: "Actuator",
  supertypeName: "ObjectInTwoDimSpace",
  properties: {
    "active": {range: "Decimal", label:"Active"},
    "ratedPower": {range: "Decimal", min:0, max: 5000, label:"Rated power (Watts)"},
    "workingCapacity": {range: "Decimal", min:0, max: 100, label:"Current capacity (%)"}
  },
  methods: {
  }
});
