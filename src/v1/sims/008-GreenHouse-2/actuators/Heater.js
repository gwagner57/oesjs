/*******************************************************************************
 * The Heater actuator model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var Heater = new cLASS({
  Name: "Heater",
  supertypeName: "Actuator",
  properties: {},
  methods: {}
});