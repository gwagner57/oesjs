/*******************************************************************************
 * The Humidifier actuator model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var Humidifier = new cLASS({
  Name: "Humidifier",
  supertypeName: "Actuator",
  properties: {
    "reduceMode": {range: "Boolean", label: "Reduce humidity mode"}
  },
  methods: {}
});