/*******************************************************************************
 * The StoneTile model class.
 *
 * Used as delimiter (stone tile), mostly for visualization effects.
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 ******************************************************************************/
var StoneTile = new cLASS({
  Name: "StoneTile",
  supertypeName: "ObjectInTwoDimSpace",
  properties: {},
  methods: {
    "toString": function () {
      return "StoneTile:{id:" + this.id + ", x:"
        + this.pos[0] + ", y:" + this.pos[1] + ", width:"
        + this.width + ", height:" + this.height + "}";
    }
  }
});