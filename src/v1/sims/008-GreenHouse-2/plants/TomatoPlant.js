/*******************************************************************************
 * The TomatoPlant model class
 *
 * @copyright Copyright 2016 Mircea Diaconescu
 * @license The MIT License (MIT)
 * @author Mircea Diaconescu
 *
 * A tomato plant grows good under the following optimal conditions:
 *
 * - temperature: 18-32°C
 *  (source: http://www.harvesttotable.com/2009/02/how_to_grow_tomatoes/)
 *  (source: http://homeguides.sfgate.com/ideal-humidity-indoor-tomato-plants-46330.html)
 *
 * - soil moisture: 65-75%
 *  (source: http://www.ncbi.nlm.nih.gov/pubmed/20136003)
 *
 * - relative air humidity: 80-90% during daytime and 65-75% during nighttime,
 *   non condensed, so in average the optimal value is around 70-80%
 *  (source: http://homeguides.sfgate.com/ideal-humidity-indoor-tomato-plants-46330.html)
 *
 * Tomato plants needs 80 to 90 days from the "baby stage" until harvest time.
 ******************************************************************************/
var TomatoPlant = new cLASS({
  Name: "TomatoPlant",
  supertypeName: "Plant",
  properties: {
    "harvestTime": {range: "PositiveInteger",
      initialValue: function () {return rand.uniformInt(80,90);},
      min: 80, max: 90, label: "Harvest time (days)"},
    "waterConsumption" : {range: "Decimal", min: 0.1, max: 0.5,
      initialValue: 0.3, label: "Water consumption (L/day)"}
  },
  methods: {
    "toString": function () {
      return "TomatoPlant:{id:" + this.id + ", x:"
        + this.pos[0] + ", y:" + this.pos[1] + ", width:"
        + this.width + ", height:" + this.height + "}";
    }
  }
});

// t = temperature (°C), sm = soil moisture (%),
// rh = relative air humidity (%)
TomatoPlant.getOptimalConditions = function () {
  return {t: [18,32], sm: [65,75], rh: [70,80]};
};