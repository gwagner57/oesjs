/*******************************************************
 * Green House with 2D isometric visualization.
 * @copyright Copyright 2016 Mircea Diaconescu, BTU (Germany)
 * @author Mircea Diaconescu
 * @license The MIT License (MIT)
 ********************************************************/
/*******************************************************
 Simulation Parameters
 ********************************************************/
sim.scenario.simulationEndTime = 129600; // one step simulates 1 minute, we simulate ~3 months (90 days).
sim.config.visualize = true;
sim.config.createLog = false;
/*******************************************************
 Simulation Model
 ********************************************************/
sim.scenario.timeUnit = "min";
sim.model.timeIncrement = 1;
sim.config.stepDuration = 33;  // ~ 30 FPS
//sim.scenario.randomSeed = 7;
// object and event types used by this simulation scenario
sim.model.objectTypes = ["Sensor", "AnalogSensor", "DigitalSensor", "HumiditySensor",
  "SoilMoistureSensor", "TemperatureSensor", "Actuator", "Heater", "Humidifier", "WaterPump",
  "Plant", "TomatoPlant", "StoneTile"];
sim.model.objectTypeFilesInSubfolders = ["sensors/Sensor.js", "sensors/AnalogSensor.js", "sensors/DigitalSensor.js",
  "sensors/HumiditySensor.js", "sensors/SoilMoistureSensor.js", "sensors/TemperatureSensor.js",
  "actuators/Actuator.js", "actuators/Heater.js", "actuators/Humidifier.js", "actuators/WaterPump.js",
  "plants/Plant.js", "plants/TomatoPlant.js", "terrain/StoneTile.js"];
sim.model.eventTypes = ["NewDay", "ReadHumidity", "ReadSoilMoisture", "ReadTemperature",
  "StartHeater", "StartHumidifier", "StartWaterPump", "StopHeater", "StopHumidifier", "StopWaterPump"];
sim.model.eventTypeFilesInSubfolders = ["events/NewDay.js", "events/ReadHumidity.js", "events/ReadSoilMoisture.js",
  "events/ReadTemperature.js", "events/StartHeater.js", "events/StartHumidifier.js", "events/StartWaterPump.js",
  "events/StopHeater.js", "events/StopHumidifier.js", "events/StopWaterPump.js"];
// Space model
sim.model.space.type = "2D";
// 9 cells per section, 1 cell border,
// total: 6 sections => 47 cells
// Every cell has a width of 10cm
sim.model.space.xMax = 470;
// 9 cells per section, 1 cell border,
// total: 6 sections => 74 cells
// Every cell has a height of 10cm
sim.model.space.yMax = 470;
// this space has an overlay grid
sim.model.space.overlayGridCellSize = 10;
/*******************************************************
 Global variables
 ********************************************************/
// reference temperature value, used to compute high/low 
// temperatures, also used to compute water vaporisation
// NOTE: this is also used as the start temperature for 
//       every grid cell.
// Measurement unit: °C
sim.model.v.refT = 20;
// soil moisture reference value, used as the start 
// soil moisture value for each cell.
// Measurement unit: %
sim.model.v.refSM = 70;
// relative air humidity reference value, used as the 
// start relative air humidity value for each cell.
// Measurement unit: %
sim.model.v.refRH = 60;
// reference temperature value used to compute water vaporization
// Measurement unit: °C
sim.model.v.refWVT = 20;
// reference water vaporization per minute at a temperature 
// of sim.model.v.refWVT. Value: 1.5% per day (1440 minutes)
// Measurement unit: %
sim.model.v.refWVPM = 0.0010417;
// the lowest temperature value to be reached for the current day
// NOTE: this represents the external world (outside GH) temperature.
// Measurement unit: °C
sim.model.v.lowT = 0;
// the highest temperature value to be reached for the current day
// NOTE: this represents the external world (outside GH) temperature.
// Measurement unit: °C
sim.model.v.highT = 0;
// the thermal isolation factor of the GH (the factor for temperature
// variation in the GH for each 1°C down variation outside the GH
// NOTE: 1 means perfect isolation without any losses
//       0 means no isolation
sim.model.v.thermalIsolationFactor = 0.85;
// the thermal green house effect factor (the factor for temperature
// variation in the GH for each 1°C up variation outside the GH
// NOTE: value = 1 means +1°C outside => +1°C inside
//       value > 1 means a positive factor
//       0 < value < 1 means a negative factor
sim.model.v.greenHouseEffectFactor = 1.25;
// the number of the current day 
//(1 simulation step = 1 minute, 1440 minutes = 1 day)
sim.model.v.dayNumber = 0;
// stores the current energy consumption value
sim.model.v.energyConsumption = 0;
// stores space region data for being easy to access it.
// - the sensors
// - the actuators
// - the optimal conditions
sim.model.v.spaceData = {};
// compute the high and low temperature values for the next day
// NOTE: a Gaussian distribution is used to obtain temperature variations.
sim.model.f.computeNextDayTemperatures = function () {
  // The maximum temperature, during day (20, 38)°C, when
  // the reference temperature (sim.model.v.refT) is 20C
  // mean = 9, standardDeviation = 3
  sim.model.v.highT = Math.round(sim.model.v.refT + rand.normal(9, 3));
  // The minimum temperature, during night (8, 20)°C, when
  // the reference temperature (sim.model.v.refT) is 20C
  // mean = 6, standardDeviation = 2
  sim.model.v.lowT = Math.round(sim.model.v.refT - rand.normal(6, 2));
  // the temperature difference between day and night can't be greater than 20°C
  // NOTE: normally this should not happen anyway, but just in case.
  if (sim.model.v.highT - sim.model.v.lowT > 20)
    sim.model.v.lowT = sim.model.v.highT - 20;
};
/*******************************************************
 Grid cell properties
 ********************************************************/
sim.model.space.gridCellProperties = {
  "soilMoisture": {
    range: "Decimal", min: 0, max: 100,
    initialValue: 0, label: "Soil moisture"
  },
  "temperature": {
    range: "Decimal", min: 0, max: 50,
    initialValue: 0, label: "Temperature"
  },
  "lastDayTemperature": {
    range: "Decimal", min: 0, max: 50,
    initialValue: 0, label: "Last day temperature"
  },
  "humidity": {
    range: "Decimal", min: 0, max: 100,
    initialValue: 0, label: "Air humidity"
  },
  "stone": {range: "Boolean", initialValue: false, label: "Is stone"}
};
/*******************************************************
 Define the observation UI (space view)
 ********************************************************/
sim.config.observationUI.spaceView = {
  canvasColor: ["#7db85c", "#5d8c3e"], // one value means solid color, an array of colors creates gradients
  tileImage: "SoilTile32x32.png",
  type: "Isometric2D",
  height: 600,
  tileSize: 10 // xMax = 470, yMax = 470, tileSize = 10 => 47 x 47 space tiles
};

/*******************************************************
 Define the observation UI (objects view)
 ********************************************************/
sim.config.observationUI.objectViews = {
  "TomatoPlant": {
    images: {
      "baby-plant": "BabyPlant64x64.png",
      "medium-plant": "MediumPlant64x64.png",
      "mature-plant": "MaturePlant128x128.png"
    },
    imageId: function (o) {
      var currentDay = sim.model.v.dayNumber;
      var stageTime = o.harvestTime / 5;
      if (currentDay < stageTime)
        return "baby-plant";
      else if (currentDay >= 2 * stageTime && currentDay <= 4 * stageTime)
        return "medium-plant";
      else return "mature-plant";

      /*if (sim.step <= 500)
        return "baby-plant";
      else if (sim.step >= 500  && sim.step <= 1000)
        return "medium-plant";
      else return "mature-plant";*/
    }
  },
  "StoneTile": {image: "StoneTile32x32.png"}
};
try {
sim.config.observationUI.monitor= {
  "avgSoilMoisture": {
    type: "Meter",
    sourceVariable: {type: "statistics", decimalPlaces: 2},
    // The following are widget specific properties.
    // NOTE: default values are used when not defined, as specified in the comments above.
    pos: [174, 468], // location of the center of the widget, in pixels, defaults to center of the display area
    width: 144, // widget width in pixels (height = width, Meter is a square widget)
    suppressDigitalDisplay: false, // suppress the digital value (optional, defaults to false)
    minDivision: 0.01, // minimum value variation to update widget, defaults to (max - min) / 270
   // unit: "%", // measurement unit - used as label for the meter, defaults to unit used in the property definition
    label: "Avg. Moisture", // override the label defined by the monitored object property
    divisions: { // display colored intervals (up to 6 colors from the set: {white, green, yellow, red, blue, orange})
      green: TomatoPlant.getOptimalConditions().sm, // format: [startColorValue, stopColorValue]
      yellow: [ // format: [[startColorValue1, stopColorValue1], [startColorValue2, stopColorValue2], ...]
        [TomatoPlant.getOptimalConditions().sm[0] - 5, TomatoPlant.getOptimalConditions().sm[0]],
        [TomatoPlant.getOptimalConditions().sm[1], TomatoPlant.getOptimalConditions().sm[1] + 5]
      ],
      red: [ // format: [[startColorValue1, stopColorValue1], [startColorValue2, stopColorValue2], ...]
        [0, TomatoPlant.getOptimalConditions().sm[0] - 5],
        [TomatoPlant.getOptimalConditions().sm[1] + 5, 100]
      ]
    }
  },
  "temperature": {
    type: "Meter",
    sourceVariable: {type: "property", name: "value", object: 5010, decimalPlaces: 2},
    pos: [96, 339],
    width: 144,
    minDivision: 0.01,
    unit: "°C",
    divisions: {
      blue: function(o) { // blue means to low temperature, "cold"
        return [o.constructor.properties.value.min, TomatoPlant.getOptimalConditions().t[0]];
      },
      green: TomatoPlant.getOptimalConditions().t, // green means optimal temperature
      red: function(o) { // red means to high temperature, "hot"
        return [TomatoPlant.getOptimalConditions().t[1], o.constructor.properties.value.max];
      },
      yellow: [ // yellow means temperature at the margins of optimal (±2 degrees over/under optimal value)
        [TomatoPlant.getOptimalConditions().t[0] - 2, TomatoPlant.getOptimalConditions().t[0]],
        [TomatoPlant.getOptimalConditions().t[1], TomatoPlant.getOptimalConditions().t[1] + 2]
      ]
    }
  },
  "humidity": {
    type: "Meter",
    sourceVariable: {type: "property", name: "value", object: 5011, decimalPlaces: 2},
    pos: [252, 339],
    width: 144,
    minDivision: 0.01,
    label: "RH",
    unit: "%",
    divisions: {
      green: TomatoPlant.getOptimalConditions().rh,
      yellow: [
        [TomatoPlant.getOptimalConditions().rh[0] - 5, TomatoPlant.getOptimalConditions().rh[0]],
        [TomatoPlant.getOptimalConditions().rh[1], TomatoPlant.getOptimalConditions().rh[1] + 5]
      ],
      red: [
        [0, TomatoPlant.getOptimalConditions().rh[0] - 5],
        [TomatoPlant.getOptimalConditions().rh[1] + 5, 100]
      ]
    }
  },
  "display1": {
    type: "AlphanumericDisplay",
    sourceVariables: [
      {type: "global", name: "dayNumber", label: "Day"},
      {type: "simulator", name: "step",
        label: function (s) { // NOTE: a new GH day starts at 5:00 AM, when the Sun rises, not at 00:00!
          var dayMinute = s.step % 1440;
          var hour = parseInt(dayMinute / 60) + 5;
          var minute = dayMinute % 60;
          var amPm = dayMinute < 480 || dayMinute > 1139 ? "AM" : "PM";
          hour = dayMinute < 480 ? hour % 13 : hour % 12;
          return (hour < 10 ? ("0" + hour) : hour) + ":" + (minute < 10 ? ("0" + minute) : minute) + amPm;
        }
      },
      {type: "statistics", name: "totalEnergyConsumption"}
    ],
    // The following are widget specific properties.
    // NOTE: default values are used when not defined, as specified in the comments above.
    rows: 2, // optional, specifies the number of display rows (defaults to 2)
    cols: 16, // optional, specifies the number of display columns per row (defaults to 16)
    pos: [174, 73], // location of the center of the widget, in pixels, defaults to center of the display area
    textPosition: [[1,1], [1,10],[2,1]], // text position on the screen - index corresponds to sourceVariables index
    backgroundColor: "#111a69",// optional, defaults to a blue color used by real LCD displays (HTML code: #111a69)
    textColor: "#FFFFFF", // optional, defaults to white (HTML code: #FFFFFF)
    suppressLabel: false, // optional flag to suppress the widget label (defaults to false)
    label: "General Information",// optional, defaults to empty label
    width: 256 // optional, defaults to 256, but can be adjusted to something else (advice: use power of 2 values).
  },
  "display2": {
    type: "AlphanumericDisplay",
    sourceVariables: [
      { type: "property",
        name: "value", object: 5000,
        decimalPlaces: 2,
        label: function (o) {
          if (o.active) return "WP: on ";
          else return "WP: off";
        }
      },
      { type: "property",
        name: "value", object: 5001,
        decimalPlaces: 2,
        label: function (o) {
          if (o.active) return "Hum: on ";
          else return "Hum: off";
        }
      },
      { type: "property",
        name: "value", object: 5002,
        decimalPlaces: 2,
        label: function (o) {
          if (o.active) return "Heater: on ";
          else return "Heater: off";
        }
      }
    ],
    pos: [174, 190],
    textPosition: [[1,1], [1,9], [2,1]], // text position on the screen - related to source variable property
    backgroundColor: "#111a69",// optional, defaults to standard blue used by real LCD displays
    textColor: "#FFFFFF", // optional, defaults to white
    label: "Actuators State",// optional, defaults to empty label
    width: 256 // optional, defaults to 256, but can be adjusted to something else.
  }

};
}catch (e) {};
/*******************************************************
 Define the initial state
 ********************************************************/
sim.scenario.setupInitialState = function () {
  var x = 0, y = 0;
  var idCounter = 0, o = null;
  var xMax = sim.model.space.xMax,
    yMax = sim.model.space.yMax,
    gridCellSize = sim.model.space.overlayGridCellSize,
    gridCellSize2 = gridCellSize / 2;
  var spaceData = sim.model.v.spaceData;
  var oc = TomatoPlant.getOptimalConditions();
  var getCell = oes.space.overlayGrid.getCell;

  // set grid cell properties: moisture, humidity and temperature.
  oes.space.overlayGrid.forAllCells(function (x, y, cell) {
    // the cells which acts as stone does not have
    // temperature, soil moisture or relative air humidity
    if (x === 1 || x === 47 || y === 1 || y === 47) cell.stone = true;
    else {
      // initially, the soil moisture is at the optimum level for each reach,
      // which means, the middle of the optimal soil moisture interval
      cell.soilMoisture = (oc.sm[0] + oc.sm[1]) / 2;
      // initially, the air temperature is at the optimum level for each reach,
      // which means, the middle of the optimal air temperature interval
      cell.temperature = (oc.t[0] + oc.t[1]) / 2;
      cell.lastDayTemperature = cell.temperature;
      // initially, the relative air humidity is at the optimum level for each reach,
      // which means, the middle of the optimal relative air humidity interval
      cell.humidity = (oc.rh[0] + oc.rh[1]) / 2;
    }
  });
  // optimal conditions
  spaceData.optimalConditions = oc;
  // sensors
  spaceData.sensors = {t: [], rh: [], sm: []};
  // actuators
  spaceData.actuators = {h: [], hum: [], wp: []};
  // plants
  spaceData.plants = [];
  // effects
  spaceData.effects = {t: 0, rh: 0};
  // add the stone tiles, plants, actuators and sensors.
  for (x = 0; x < xMax; x += gridCellSize) {
    for (y = yMax; y > 0; y -= gridCellSize) {
      if (x === 0 || x === xMax - gridCellSize) {
        sim.addObject(new StoneTile({
          // auto-generated ID
          pos: [x + gridCellSize2, y - gridCellSize2],
          width: gridCellSize, height: gridCellSize
        }));
      } else {
        if (y === gridCellSize || y === yMax) {
          // auto-generated ID
          sim.addObject(new StoneTile({
            pos: [x + gridCellSize - gridCellSize2, y - gridCellSize2],
            width: gridCellSize, height: gridCellSize
          }));
        } else {
          if ( (x + 4 * gridCellSize) % 90 === 0 && (y - 5 * gridCellSize) % 90 === 0){
            // - there is only one water pump, in the middle of the Green house
            // - there is only one heater, in the middle of the Green house
            // - there is only one humidifier, in the middle of the Green house
            // - there is only one temperature sensor, in the middle of the Green house
            // - there is only one humidity sensor, in the middle of the Green house
            if ( x === 230 && y === 230) {
              // water pump
              o = new WaterPump({id: 5000, pos: [x + gridCellSize2, y + gridCellSize2],
                waterFlow: 15, ratedPower: 550});
              spaceData.actuators.wp.push(sim.addObject(o));
              // humidifier
              o = new Humidifier({id: 5001, pos: [x + gridCellSize2, y + gridCellSize2],
                ratedPower: 1200});
              spaceData.actuators.hum.push(sim.addObject(o));
              // heater
              o = new Heater({id: 5002, pos: [x + gridCellSize2, y + gridCellSize2],
                ratedPower: 1800});
              spaceData.actuators.h.push(sim.addObject(o));
              // temperature sensor
              o = new TemperatureSensor({id: 5010 , pos: [x + gridCellSize2, y + gridCellSize2]});
              spaceData.sensors.t.push(sim.addObject(o));
              o.read(getCell(o.pos[0], o.pos[1]).temperature);
              // humidity sensor
              o = new HumiditySensor({id: 5011, pos: [x + gridCellSize2, y + gridCellSize2]});
              spaceData.sensors.rh.push(sim.addObject(o));
              o.read(getCell(o.pos[0], o.pos[1]).humidity);
            }
            // add Tomato Plant, one every 90 units = 90 cm
            o = new TomatoPlant({
              id: 2000 + idCounter,
              pos: [x + gridCellSize2, y + gridCellSize2],
              // a plant has a size of 3x3 logical cells
              width: 3 * gridCellSize, height: 3 * gridCellSize
            });
            spaceData.plants.push(sim.addObject(o));
            // there is one soil moisture sensor for each plant
            // NOTE: this is an abstract notion, since "a plant" may represent "a group of plants"
            o = new SoilMoistureSensor({id: 5012 + idCounter, pos: [x + gridCellSize2, y + gridCellSize2]});
            o.read(getCell(o.pos[0], o.pos[1]).soilMoisture);
            spaceData.sensors.sm.push(sim.addObject(o));
            idCounter++;
          }
        }
      }
    }
  }
};
/********************************************************
 The actions that executes every simulation step
 NOTE: the day starts at 5:00AM
 ********************************************************/
sim.model.OnEachTimeStep = function () {
  var minute = sim.step % 1440;
  var ghIF = sim.model.v.thermalIsolationFactor,
    ghEF = sim.model.v.greenHouseEffectFactor;
  var hT = sim.model.v.highT * ghEF,
    lT = sim.model.v.lowT * (2 - ghIF);
  var spaceData = sim.model.v.spaceData;
  var getCell = oes.space.overlayGrid.getCell;
  var cellSize = sim.model.space.overlayGridCellSize;
  var plantsNmr = spaceData.plants.length;

  // heater actuators
  spaceData.effects.t = 0;
  spaceData.actuators.h.forEach(function (heater) {
    // heaters are able to uniformly increase the temperature
    // with 1 degree every 15 minutes, for each 1000w used power,
    // for a region of 20.25m^2, which represents a 45 x 45 cells
    // space (10cm cell size)
    if (heater.active) {
      // 0.00000067 = 1 / (100% * 1000w * 15 minutes)
      spaceData.effects.t += 0.00000067 * heater.ratedPower * heater.workingCapacity;
    }
  });
  // humidifier actuators
  spaceData.effects.rh = 0;
  spaceData.actuators.hum.forEach(function (humidifier) {
    var rh = 0;
    // humidifiers are able to uniformly increase/decrease
    // the humidity with 1% every 6 minutes, for
    // each 1000w used power, for a region of 20.25m^2,
    // which represents a 45 x 45 cells space (10cm cell size)
    if (humidifier.active) {
      // 0.00000166667 = 1 / (100% * 1000w * 6 minutes)
      rh = 0.00000166667 * humidifier.ratedPower * humidifier.workingCapacity;
      spaceData.effects.rh = humidifier.reduceMode ? -rh : rh;
    }
  });

  // compute the RH consumption for the plants
  spaceData.plants.forEach(function (plant) {
    var rhPC = 0, pWC = plant.waterConsumption / 1440,
      pHT = plant.harvestTime * 1440;
    // Plants "consumes" also air humidity, by using their leaves.
    // Every plant consume humidity, based on its maturity stage. At harvest time, it
    // 100% of the value of its waterConsumption property, and at start time 50%.
    // The computations is for a number of cells equals with plant width x height,
    // so 45 x 45 cells total, one plant is 3 x 3 cells =>  a factor of 0.004445
    // NOTE1: the formula used to compute RH based on water consumption is
    //        RH = RH + pWC / 3, where pWC represents the
    //        quantity of water that is consumed by the plant.
    // NOTE2: this is just a simplification of reality, where the RH computation
    //        is highly complex, and depends on a multitude of factors (most of
    //        which are unknown in our simulation scenario).
    rhPC = (0.5 * pWC * (1 + sim.step / pHT) * 0.004445) / 3;
    spaceData.effects.rh -= rhPC;
  });

  // compute the soil moisture as the result of watering and
  // using water pumps, and water consumption by the plants
  spaceData.actuators.wp.forEach(function (wp) {
    // Every 1L of water means an increase of 60% soil moisture for one cell (10 x 10 cm).
    // Water distribution in soil is radial, and decreases with 10% every 1 cell increase in radius.
    // The water pump has small pipe connection with every plant in its radius (45 x 45 cells).
    // NOTE: water pumps works at 100% capacity when started, and cannot be adjusted.
    // Every plant consume water, based on its maturity stage. At harvest time, it
    // 100% of the value of its waterConsumption property, and at baby stage, only 50%.
    // The water consumption is also radial, and decreases with 10% every 1 cell increase in radius.
    // Also, 1L water consumption, means 60% decrease in soil moisture.
    var smDecPC = 0.9, range = 5, i = 0;
    var lWPC = 0, wFactor = 0;
    for (i = 0; i <= range; i++) wFactor += Math.pow(smDecPC, i);
    // liters of water in the cell where the plant is located
    // which is the cell that gets the 60% soil moisture for 1L water
    lWPC = wp.active ? ((wp.waterFlow / plantsNmr) / wFactor) * 0.6 : 0;
    spaceData.plants.forEach(function (plant) {
      var pipeAtX = plant.pos[0], pipeAtY = plant.pos[1];
      var pWC = plant.waterConsumption / 1440,
        pHT = plant.harvestTime * 1440,
        pWCDec = 0.5 * 0.6 * pWC * (1 + sim.step / pHT) / wFactor;
      var i = 0, n = 1, smInc = 0, iCs = 0, nCs = 0;
      // the cell located at the root of the plant
      getCell(pipeAtX, pipeAtY).soilMoisture += lWPC - pWCDec;
      while (n <= range) {
        smInc = (lWPC - pWCDec) * Math.pow(smDecPC, n);
        nCs = n * cellSize;
        for (i = -n; i <= n; i++) {
          iCs = i * cellSize;
          getCell(pipeAtX + iCs, pipeAtY + nCs).soilMoisture += smInc;
          getCell(pipeAtX + iCs, pipeAtY - nCs).soilMoisture += smInc;
          if (i > -n && i < n) {
            getCell(pipeAtX + nCs, pipeAtY + iCs).soilMoisture += smInc;
            getCell(pipeAtX - nCs, pipeAtY + iCs).soilMoisture += smInc;
          }
        }
        n++;
      }
    });
    // compute water consumption (cubic meters)
    if (wp.active) sim.stat.waterConsumption += wp.waterFlow / 1000; // m^3
  });

  // compute the changes for cell properties
  oes.space.overlayGrid.forAllCells(function (x, y, cell) {
    // delimiter cells does not have "real" temperature, humidity
    // or soil moisture values, these are just stone/grass walls
    if (cell.stone) return;
    var t = cell.temperature,
      refT = sim.model.v.refWVT,
      refWVPM = sim.model.v.refWVPM;
    // Soil moisture decreases because of the temperature
    // variation, which produces water vaporization.
    // Each 1°C plus/minus results in a 10% up/down
    // variation relative to the reference value.
    var wVPM = refWVPM + (t - refT) * refWVPM / 10.0,
      rhFPM = wVPM / 3.0;
    // water vaporization is uniform all over the grid,
    // but is relative to the temperature of the cell
    if (cell.soilMoisture >= wVPM) {
      cell.soilMoisture -= wVPM * cell.soilMoisture / 100;
      // the relative air humidity increases, because of the
      // water vaporization. There is a complex formula to
      // compute the value, but we use a simplified form:
      // RH = RH + wVPM / 3, where vVPM represents the
      // quantity of water that is lost from the soil moisture.
      if (cell.humidity - rhFPM < 100) cell.humidity += rhFPM;
    }
    // The temperature variations during the day:
    // - increases between 5:00AM - 5:00PM and
    // - decreases between 5:00PM - 5:00AM.
    if (minute < 720) { // first part of the day: 5:00AM - 5:00 PM
      // Max_GH_T = Max_Outer_T * GH_Effect_Factor
      // Maximum GH temperature not reached, so the temperature increases,
      // or possibly decreases, if today's max outside temperature
      // is lower than the temperature in the GH at the moment of computation
      if (t < hT) cell.temperature += (hT - cell.lastDayTemperature) / 720;
    } else { // last part of the day: 5:00PM - 5:00AM
      // Min_GH_T = GH_T - Min_Outer_T * (2 - insulationFactor)
      // Minimum GH temperature not reached, so the temperature
      // decreases, or possibly increases if today's min outside
      // temperature is higher than the temperature inside the GH
      // at the moment of computation
      if (t > lT) cell.temperature -= (hT - lT) / 720;
    }
    // increase of temperature due to heaters
    cell.temperature += spaceData.effects.t;
    // increase/decrease of air humidity due to humidifiers
    // and consumption from the plants
    if (cell.humidity - Math.abs(spaceData.effects.rh) > 0)
      cell.humidity += spaceData.effects.rh;
  });
  /****************************************************************
   * compute power consumption for the currently active actuators *
   ****************************************************************/
  sim.model.v.energyConsumption = 0;
  sim.stat.energyConsumptionH = 0;
  sim.stat.energyConsumptionWP = 0;
  sim.stat.energyConsumptionRH = 0;
  Object.keys(spaceData.actuators).forEach(function (k) {
    spaceData.actuators[k].forEach(function (actuator) {
      var ec = 0;
      if (actuator.active) {
        // compute the power consumption for the actuator
        // - One step represents 1 minute (1/60 hours)
        // - PowerConsumption(kWh) = ((RatedPower * ActuatorCurrentCapacity / 100) / 60 ) / 1000
        ec = actuator.ratedPower * actuator.workingCapacity / 100000;
        sim.model.v.energyConsumption += ec;
        if (k === 'h') {
          sim.stat.heatersOnTime++;
          sim.stat.energyConsumptionH += ec;
        }
        if (k === 'wp') {
          sim.stat.waterPumpsOnTime++;
          sim.stat.energyConsumptionWP += ec;
        }
        if (k === 'hum') {
          sim.stat.humidifiersOnTime++;
          sim.stat.energyConsumptionRH += ec;
        }
      }
    });
  });
  sim.stat.totalEnergyConsumption += sim.model.v.energyConsumption / 60;
};
/********************************************************
 Output Statistics Variables
 ********************************************************/
sim.model.statistics = {
  "waterConsumption": {
    range: "Decimal", label: "Water consumption",
    decimalPlaces: 2, unit: "m^3"
  },
  "energyConsumptionH": {
    range: "Decimal", label: "Heating EC",
    decimalPlaces: 2, unit: "kWh", showTimeSeries: true
  },
  "energyConsumptionWP": {
    range: "Decimal", label: "Watering EC",
    decimalPlaces: 2, unit: "kWh", showTimeSeries: true
  },
  "energyConsumptionRH": {
    range: "Decimal", label: "Humidification EC",
    decimalPlaces: 2, unit: "kWh", showTimeSeries: true
  },
  "totalEnergyConsumption": {
    range: "Decimal", label: "EC",
    decimalPlaces: 2, unit: "kWh"
  },
  "heatersOnTime": {
    range: "Decimal", label: "Heaters active time",
    decimalPlaces: 2, unit: "h",
    computeOnlyAtEnd: true,
    expression: function () {
      // translate to hours
      return sim.stat.heatersOnTime / 60.0;
    }
  },
  "waterPumpsOnTime": {
    range: "Decimal", label: "Water pumps active time",
    decimalPlaces: 2, unit: "h",
    computeOnlyAtEnd: true,
    expression: function () {
      // translate to hours
      return sim.stat.waterPumpsOnTime / 60.0;
    }
  },
  "humidifiersOnTime": {
    range: "Decimal", label: "Humidifiers active time",
    decimalPlaces: 2, unit: "h",
    computeOnlyAtEnd: true,
    expression: function () {
      // translate to hours
      return sim.stat.humidifiersOnTime / 60.0;
    }
  },
  "minOTemperature": {range: "Decimal", label: "Min outer temperature", unit: "°C"},
  "maxOTemperature": {range: "Decimal", label: "Max outer temperature", unit: "°C"},
  "avgOTemperature": {
    range: "Decimal", label: "Avg. out temperature",
    unit: "°C", decimalPlaces: 2
  },
  "avgSoilMoisture": {
    gridCellProperty:"humidity",
    initialValue: 70,
    /*initialValue: (TomatoPlant.getOptimalConditions().sm[0]
      + TomatoPlant.getOptimalConditions().sm[1])/2,*/
    aggregationFunction:"avg",
    label:"Avg. soil moisture",
    unit: "%"
  }
};

sim.scenario.initialState.events = [
  {typeName: "NewDay", occTime: 1, dayNumber: 1},
  {typeName: "ReadTemperature", occTime: 1},
  {typeName: "ReadHumidity", occTime: 1},
  {typeName: "ReadSoilMoisture", occTime: 1}
];