var sim = sim || {};
sim.model = sim.model || {};
sim.scenario = sim.scenario || {};
sim.config = sim.config || {};

var oes = oes || {};
oes.ui = oes.ui || {};
oes.ui.explanation = {};

/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "Green-House-in-Continuous-2D-Space";
sim.model.title = "Green House with 2D Isometric Visualization";
sim.model.shortDescription = "Simulate a Green House, where tomato plants are grown (variable grow rates are used)."
    + " Temperature, relative air humidity and soil moisture sensors are used to sense the environment qualities."
    +  " Water pumps humidifiers/de-humidifiers and heater actuators are responsible, with the change of the Green House "
    + " climate, thus maintaining optimal conditions for the tomato plants, resulting in an optimal tomato production.";
sim.model.license = "CC BY-SA";
sim.model.creator = "Mircea Diaconescu";
sim.model.created = "2016-07-25";
sim.model.modified = "2016-09-29";
