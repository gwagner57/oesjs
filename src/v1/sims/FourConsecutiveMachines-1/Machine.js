var Machine = new cLASS({
  Name: "Machine",
  supertypeName: "oBJECT",
  properties: {
    "waitingOrders": { range: "Order", label: "Waiting orders",
        shortLabel: "queue", minCard: 0, maxCard: Infinity},
    "nextMachine": { range: "Machine", optional: true}
  }
});
