/*******************************************************************************
 * The MachineActivity activity class
 *
 * @copyright Copyright 2015-2016 Gerd Wagner
 *   Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 ******************************************************************************/

/**
 * @type {cLASS}
 *
 * When a new MachineActivity activity is created (in the form of a JS object) by an
 * ActivityStart event with a resource allocation {"machine": this.machine}
 * scheduled, e.g., by the OrderArrival event rule, a corresponding "machine"
 * property slot is created. Consequently, the expression "this.machine" can be
 * used for accessing the associated machine object.
 */

var MachineActivity = new cLASS({
  Name: "MachineActivity",
  supertypeName: "aCTIVITY",
  // resource roles are special reference properties
  resourceRoles: {"machine": {range:"Machine", exclusive: true}},
  properties: {},  // there are no additional properties
  methods: {
/*
    // event rule for ActivityStart
    "onActivityStart": function () {
    },
*/
    // event rule for ActivityEnd
    "onActivityEnd": function () {
      var followupEvents = [], order=null,
          nextMachine = this.machine.nextMachine;
      // remove order from queue
      order = this.machine.waitingOrders.shift();
      // is there a successor machineActivity desk?
      if (nextMachine) {
        // add order to the queue of the next machineActivity desk
        nextMachine.waitingOrders.push( order);
        // is the next machineActivity desk available/idle?
        if (nextMachine.waitingOrders.length === 1) {
          // start new machineActivity at next machineActivity desk
          followupEvents.push( new oes.ActivityStart({
            occTime: sim.time + 1,
            activityType: "MachineActivity",
            duration: nextMachine.activityDuration(),
            // on activity creation resource roles are copied to corresp. property slots
            resources: {"machine": nextMachine}
          }));
        }
      } else {  // there is no successor machine
        // update the financial variables
        sim.stat.revenue += sim.model.v.revenuePerOrder;
        sim.stat.manuCosts += sim.model.v.manuCostsPerOrder;
        sim.stat.backlogCosts += sim.model.v.backlogCostsPerOrderPerTimeUnit *
            (sim.stat.arrivedOrders - sim.stat.departedOrders);
        // add the time the order has spent in the system
        sim.stat.cumulativeTimeInSystem += sim.time - order.arrivalTime;
        // remove order from simulation
        sim.removeObject( order);
        // update departedOrders statistics
        sim.stat.departedOrders++;
      }
      // if there are still orders waiting?
      if (this.machine.waitingOrders.length > 0) {
        // schedule next machineActivity start event
        followupEvents.push( new oes.ActivityStart({
          occTime: sim.time + 1,
          activityType: "MachineActivity",
          duration: this.machine.activityDuration(),
          /* a property "machine" is created automatically from the corresponding
           resource role defined in the resources map of the ActivityStart event  */
          resources: {"machine": this.machine}
        }));
      }
      return followupEvents;
    }
  }
});

