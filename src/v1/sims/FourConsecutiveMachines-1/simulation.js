/*******************************************************
 * FourConsecutiveMachines-1 - An example of a discrete event simulation.
 *
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 ********************************************************/
/*******************************************************
 Simulation Parameters
********************************************************/
sim.scenario.simulationEndTime = 100;
sim.scenario.idCounter = 11;  // start value of auto IDs
sim.scenario.randomSeed = 2345;  // optional
sim.config.createLog = false;
//sim.scenario.suppressInitialStateUI = true;
/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "FourConsecutiveMachines";
sim.model.title = "An Activity-Based Model of Four Consecutive Machines";
sim.model.systemNarrative = "A manufacturing company uses 4 consecutive machines.";
sim.model.shortDescription = "The model includes two object types: Machine and Order, " +
    "one event type: OrderArrival, and one activity type: MachineActivity.";
// meta data
sim.model.license = "CC BY-NC";
sim.model.creator = "Gerd Wagner";
sim.model.created = "2017-01-09";
sim.model.modified = "2017-01-09";

sim.model.objectTypes = ["Machine", "Order"];
sim.model.eventTypes = ["OrderArrival"];
sim.model.activityTypes = ["MachineActivity"];

sim.model.time = "continuous";
sim.model.timeRoundingDecimalPlaces = 1;  // like in 3.8

// fixed model parameters
sim.model.v.revenuePerOrder = 15;
sim.model.v.backlogCostsPerOrderPerTimeUnit = 0.1;
// variable model parameters
sim.model.v.manuCostsPerOrder = 10;
sim.model.v.orderEventRate = 1.5;

/*******************************************************
 Define Initial State
********************************************************/
sim.scenario.initialState.objects = {
  "1": {typeName: "Machine", name:"M1", nextMachine: 2,
        activityDuration: function () { return rand.triangular(1,2,3)}},
  "2": {typeName: "Machine", name:"M2", nextMachine: 3,
        activityDuration: function () { return rand.triangular(0.5, 1, 1.5)}},
  "3": {typeName: "Machine", name:"M3", nextMachine: 4,
        activityDuration: function () { return rand.triangular(2,3,4)}},
  "4": {typeName: "Machine", name:"M4",
        activityDuration: function () { return rand.triangular(0.5, 1, 1.5)}}
};
sim.scenario.initialState.events = [
  {typeName: "OrderArrival", occTime: 1, machine: 1}
];
/*******************************************************
 Define Output Statistics Variables
 ********************************************************/
sim.model.statistics = {
  "revenue": {range:"Decimal", label:"Revenue", unit: "T€"},
  "manuCosts": {range:"Decimal", label:"Manuf. costs", unit: "T€"},
  "backlogCosts": {range:"Decimal", label:"Backlog costs", unit: "T€"},
  "profit": { range: "Decimal",  label:"Profit",
    computeOnlyAtEnd: true, decimalPlaces: 1, unit: "T€",
    expression: function () {
      return sim.stat.revenue - (sim.stat.manuCosts + sim.stat.backlogCosts)}
  },
  "arrivedOrders": {range:"NonNegativeInteger", label:"Arrived orders"},
  "departedOrders": {range:"NonNegativeInteger", label:"Departed orders"},
  "cumulativeTimeInSystem": {range:"Decimal"},
  "meanTimeInSystem": { range: "Decimal",  label:"Average time in system",
    computeOnlyAtEnd: true, decimalPlaces: 1, unit: "days",
    expression: function () {
      return sim.stat.cumulativeTimeInSystem / sim.stat.departedOrders}
  }
};
