var ServiceProvider = new cLASS({
  Name: "ServiceProvider",
  shortLabel: "P",
  supertypeName: "oBJECT",
  properties: {
    "waitingCustomers": { range: "Customer", label: "Waiting customers",
        shortLabel: "q", minCard: 0, maxCard: Infinity},
    "nextServiceProvider": { range: "ServiceProvider", optional: true}
  }
});
