var Customer = new cLASS({
  Name: "Customer",
  shortLabel: "C",
  supertypeName: "oBJECT",
  properties: {
    "arrivalTime": { range: "NonNegativeInteger", label: "Arrival time",
        shortLabel: "aT"}
  }
});
