/*******************************************************
 * ConsecutiveServices - An example of a discrete event simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 ********************************************************/
/*******************************************************
 Simulation Parameters
********************************************************/
sim.scenario.simulationEndTime = 200;
sim.scenario.idCounter = 11;  // start value of auto IDs
//sim.scenario.randomSeed = 2345;  // optional
sim.config.createLog = true;
//sim.scenario.suppressInitialStateUI = true;
/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "ConsecutiveServices";
sim.model.title = "An Activity-Based Model of Consecutive Services";
sim.model.systemNarrative = "The customers of the Department of Motor Vehicles first have to " +
    "queue up at the reception for their request being recorded. Then they have to wait for a " +
    "clerk who will handle their case.";
sim.model.shortDescription = "Both the reception and the case handling are modeled as service " +
    "providers with waiting lines, resulting in a model of two consecutive services with queues. For both " +
    "service providers, utilization statistics are computed automatically by the simulator. " +
    "The model includes two object types: ServiceProvider and Customer, one event type: " +
    "CustomerArrival, and one activity type: Service, which is instantiated both by the reception " +
    "service and the case handling service.";
// meta data
sim.model.license = "CC BY-NC";
sim.model.creator = "Gerd Wagner";
sim.model.created = "2016-12-07";
sim.model.modified = "2017-01-02";

sim.model.objectTypes = ["ServiceProvider", "Customer"];
sim.model.eventTypes = ["CustomerArrival"];
sim.model.activityTypes = ["Service"];

/*******************************************************
 Define Initial State
********************************************************/
sim.scenario.initialState.objects = {
  "1": {typeName: "ServiceProvider", nextServiceProvider: 2},
  "2": {typeName: "ServiceProvider"}
};
sim.scenario.initialState.events = [
  {typeName: "CustomerArrival", occTime: 1, serviceProvider: 1}
];
/*******************************************************
 Define Output Statistics Variables
 ********************************************************/
sim.model.statistics = {
  "arrivedCustomers": {range:"NonNegativeInteger", label:"Arrived customers"},
  "departedCustomers": {range:"NonNegativeInteger", label:"Departed customers"},
  "cumulativeTimeInSystem": {range:"Decimal"},
  "meanTimeInSystem": { range: "Decimal",  label:"Average time in system",
    computeOnlyAtEnd: true, decimalPlaces: 1, unit: "min",
    expression: function () {
      return sim.stat.cumulativeTimeInSystem / sim.stat.departedCustomers}
  }
};
