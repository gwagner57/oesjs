/*******************************************************
 * Predator-Prey - An example of a discrete event simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
********************************************************/

/*******************************************************
 Setting the model
 ********************************************************/
sim.model = gol_model;

sim.scenario.shortDescription = "The space of the Game of Life is modeled " +
    "as an integer grid such that the integer value of 0 (zero) represents " +
    "a dead cell and a value of 1 represents a living cell. The model " +
    "does not define any object type nor any event type. The model's " +
	"grid space is visualized in 3D.";

/*******************************************************
 Simulation Parameters
********************************************************/
sim.scenario.simulationEndTime = 100;
sim.config.stepDuration = 200;  // 200 ms observation time per step
sim.config.visualize = true;

/**********************************************************
 Define the initial state
***********************************************************/
sim.scenario.setupInitialState = function () {
  var i=0, x=0, y=0;
  var N = 5000;  // number of cells initially alive
  var xMax = sim.model.space.xMax;
  var grid = sim.space.grid;
  var freeCell=[];
  // define animal individuals as a list of grid cell with random positions
  for (i=0; i < N; i++) {
    freeCell = oes.space.grid.i.findFreeCell();
    x = freeCell[0]; y = freeCell[1];
    // mark cell as alive
    grid[(y-1)*xMax + x - 1] = 1;
  }
};

/**********************************************************
 Define an observation user interface
***********************************************************/
sim.config.observationUI.spaceView = {
  type: "threeDim",
  // if not set (or set to 0), the full available width is used
  width: 0,
  // if not set (or set to 0), default value is 400px
  height: 600,
  // the ground color is used to fill the ground plane
  // if not set, it defaults to "#606060" (dark grey)
  groundColor: "#606060"
};