/*******************************************************
 * Game of Life - An example of a discrete event simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
********************************************************/

/*******************************************************
 Simulation Model
********************************************************/
gol_model = {};  // define namespace object
gol_model.name = "GameOfLife";
gol_model.title = "Conway's Game of Life &ndash; with a Random Initial State";
gol_model.systemNarrative = "In this famous Cellular Automata (CA) model, " +
    "grid cells may become alive or die depending on their neighborhood: " +
    "(1) a living cell dies if it has less than 2 or more than 3 living neighbours; " +
    "(2) a dead cell becomes alive if it has exactly three living neighbours.";
gol_model.shortDescription = "The space of the Game of Life is modeled as an " +
    "integer grid such that the integer value of 0 (zero) represents " +
    "a dead cell and a value of 1 represents a living cell. The model " +
    "does not define any object type nor any event type.";
// define space model
gol_model.space = {
  type: "IntegerGrid",
  xMax: 150,
  yMax: 75
};
gol_model.v = {a: 10};
// meta data
gol_model.license = "CC BY-NC";
gol_model.creator = "Gerd Wagner";
gol_model.created = "2016-07-05";
gol_model.modified = "2016-11-22";

// define time event rule for fixed-increment time progression 
gol_model.OnEachTimeStep = function () {
  var cellsToLive=[], nmrOfCellsToLive=0,
      cellsToDie=[], nmrOfCellsToDie= 0,
      i=0;
  var nmrOfLivingNeighborCells = oes.space.grid.i.getNmrOfNeighborCellsWithBit0;
  var isCellAlive = oes.space.grid.i.isSetBit0;
  var cellDies = oes.space.grid.i.unsetBit0;
  var cellBecomesAlive = oes.space.grid.i.setBit0;
  oes.space.grid.forAllCells( function (x,y) {
    var liveN = nmrOfLivingNeighborCells(x,y);
    if (isCellAlive(x,y) && (liveN < 2 || liveN > 3)) cellsToDie.push([x,y]);
    if (!isCellAlive(x,y) && liveN === 3) cellsToLive.push([x,y]);
  });
  nmrOfCellsToLive = cellsToLive.length;
  for (i=0; i < nmrOfCellsToLive; i++) cellBecomesAlive( cellsToLive[i][0], cellsToLive[i][1]);
  nmrOfCellsToDie = cellsToDie.length;
  for (i=0; i < nmrOfCellsToDie; i++) cellDies( cellsToDie[i][0], cellsToDie[i][1]);
};
