/*******************************************************
 * Game of Life - An example of a discrete event simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
********************************************************/

/*******************************************************
 Setting the model
********************************************************/
scenario.model = gol_model;
// or
// sim.model = gol_model;
// sim.scenario = ...  or sim.scenarios[1] = ...

/*******************************************************
 Simulation Parameters
********************************************************/
scenario.simulationEndTime = 100;
config.stepDuration = 200;  // 200 ms observation time per step
scenario.visualize = true;

/**********************************************************
 Define the initial state 
***********************************************************/
scenario.setupInitialState = function () {
  var i=0, x=0, y=0;
  var N = 5000;  // number of cells initially alive
  var xMax = sim.model.space.xMax;
  var grid = sim.space.grid;
  var freeCell=[];
  // define animal individuals as a list of grid cell with random positions
  for (i=0; i < N; i++) {
    freeCell = oes.space.grid.i.findFreeCell();
    x = freeCell[0]; y = freeCell[1];
    // mark cell as alive
    grid[(y-1)*xMax + x - 1] = 1;
  }
};
