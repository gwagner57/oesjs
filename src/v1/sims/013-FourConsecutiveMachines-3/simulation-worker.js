
//self.importScripts("../../../../../cLASSjs/dist/cLASSjsWorkerLib.js");
self.importScripts("../../../../../cLASSjs/lib/browserShims.js");
self.importScripts("../../../../../cLASSjs/lib/errorTypes.js");
self.importScripts("../../../../../cLASSjs/lib/util.js");
self.importScripts("../../../../../cLASSjs/lib/idb.js");
self.importScripts("../../../../../cLASSjs/src/eNUMERATION.js");
self.importScripts("../../../../../cLASSjs/src/cLASS.js");
self.importScripts("../../../../../cLASSjs/src/storage/sTORAGEmANAGER.js");
self.importScripts("../../../../../cLASSjs/src/storage/sTORAGEmANAGER_IndexedDB.js");

self.importScripts("../../../../lib/rand.js", "../../../../lib/BinaryHeap.js");
self.importScripts("../../OES.js", "../../EventList.js", "../../statistics.js", "../../simulator.js");

self.importScripts("simulation.js");

if (sim.model.objectTypes) {
  sim.model.objectTypes.forEach( function (objT) {
    self.importScripts( objT + ".js");
  });
}
if (sim.model.eventTypes) {
  sim.model.eventTypes.forEach( function (evtT) {
    self.importScripts( evtT + ".js");
  });
}
if (sim.model.activityTypes) {
  sim.model.activityTypes.forEach( function (actT) {
    self.importScripts( actT + ".js");
  });
}

//=================================================================

onmessage = function (e) {
  // receive parameter/variable values changed via the UI
  if (e.data.endTime) sim.scenario.simulationEndTime = e.data.endTime;
  if (e.data.changedModelVarValues) {
    Object.keys( e.data.changedModelVarValues).forEach( function (varName) {
      sim.model.v[varName].value = e.data.changedModelVarValues[varName];
    });
  }
  if (e.data.runExperiment) {
    sim.initializeSimulator( e.data.dbName);
    if (e.data.expReplications) sim.experiment.replications = e.data.expReplications;
    sim.runExperiment();
  } else {
    sim.initializeSimulator();
    if (e.data.createLog !== undefined) sim.config.createLog = e.data.createLog;
    sim.runScenario( true);  // run in worker thread
  }
};