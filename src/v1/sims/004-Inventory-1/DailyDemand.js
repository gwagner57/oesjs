/*******************************************************************************
 * The DailyDemand event class
 *
 * @copyright Copyright 2015-2016 Gerd Wagner
 *   Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 ******************************************************************************/
var DailyDemand = new cLASS({
  Name: "DailyDemand",
  shortLabel: "Dem",
  supertypeName: "eVENT",
  properties: {
    "quantity": {range: "PositiveInteger", label:"Quantity", shortLabel: "q"},
    "shop": {range: "SingleProductShop"}
  },
  methods: {
    "onEvent": function () {
      var followupEvents=[],
          sh = this.shop,
          stQ = sh.stockQuantity,
          newStQ = stQ - this.quantity,
          rP = sh.reorderPoint;
      // update stockQuantity
      this.shop.stockQuantity = Math.max( 0, newStQ);
      // update statistics
      if (newStQ < 0) {
        // increment the stock-out counter by 1
        sim.stat.nmrOfStockOuts++;
        // update lostSales and total stock-out costs
        sim.stat.lostSales += Math.abs( newStQ);
        sim.stat.stockoutCosts += Math.abs( newStQ) * 2;
        newStQ = 0;
      }
      sim.stat.totalSales += Math.min( this.quantity, stQ);
      // schedule new Delivery if stock level falls below reorder level
      if (stQ > rP && newStQ <= rP) {
        followupEvents.push( new Delivery({
          delay: Delivery.leadTime(),
          quantity: sh.targetInventory - newStQ,
          receiver: sh
        }));
      }
      return followupEvents;
    },
    "createNextEvent": function () {
      return new DailyDemand({
        delay: DailyDemand.recurrence(),
        quantity: DailyDemand.sampleQuantity(),
        shop: this.shop
      });
    }
  }
});
DailyDemand.recurrence = function () {
  return 1;
};
DailyDemand.sampleQuantity = function () {
  return rand.uniformInt( 5, 30);
};
