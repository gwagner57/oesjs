/*******************************************************
 * Inventory - An example of a discrete event simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
********************************************************/

/*******************************************************
 Simulation Parameters
********************************************************/
sim.scenario.simulationEndTime = 100;
//sim.scenario.randomSeed = 5;  // optional
sim.config.createLog = true;

/*******************************************************
 Simulation Model
********************************************************/
sim.model.time = "discrete"; // implies using only discrete random variables
sim.model.timeUnit = "D";

sim.model.objectTypes = ["SingleProductShop"];
sim.model.eventTypes = ["DailyDemand", "Delivery"];

/*******************************************************
 Define the initial state
 ********************************************************/
// Either declaratively:
sim.scenario.initialState.objects = {
  "1": {typeName: "SingleProductShop", name:"TV Shop", shortLabel:"shop",
    stockQuantity: 60, reorderPoint: 50, targetInventory: 80}
};
sim.scenario.initialState.events = [
  {typeName: "DailyDemand", occTime:1, quantity:25, shop:"1"}
];
// Or with a procedure:
/*
sim.scenario.setupInitialState = function () {
  var tvShop = new SingleProductShop({
    id: 1, name:"TV",
    stockQuantity: 80,
    reorderPoint: 50,
    targetInventory: 100
  });
  sim.addObject( tvShop);
  sim.scheduleEvent( new DailyDemand({occTime:1, quantity:25, shop: tvShop}));
}
*/
/*******************************************************
 Define Output Statistics Variables
 ********************************************************/
sim.model.statistics = {
  "nmrOfStockOuts": {range:"NonNegativeInteger", label:"Stockouts", hint:"Number of stockout events"},
  "lostSales": {range:"NonNegativeInteger", label:"Lost"},
  "totalSales": {range:"NonNegativeInteger", label:"Total sales"},
  "holdingCosts": {range:"NonNegativeInteger", label:"Holding costs", unit: "€"},
  "orderingCosts": {range:"NonNegativeInteger", label:"Ordering costs", unit: "€"},
  "stockoutCosts": {range:"NonNegativeInteger", label:"Stockout costs", unit: "€"},
  "totalCosts": {range:"NonNegativeInteger", label:"Total costs",
    computeOnlyAtEnd: true, unit: "€",
    expression: function () {
      return sim.stat.holdingCosts + sim.stat.orderingCosts + sim.stat.stockoutCosts
    }
  },
  "serviceLevel": {range:"Decimal", label:"Service level",
    computeOnlyAtEnd: true, decimalPlaces: 1, unit: "%",
    expression: function () {
      return sim.stat.totalSales / (sim.stat.totalSales + sim.stat.lostSales) * 100
    }
  },
  "averageInventory": {objectType:"SingleProductShop", objectIdRef: 1,
    property:"stockQuantity", aggregationFunction:"avg", label:"Avg. inventory"}
};
