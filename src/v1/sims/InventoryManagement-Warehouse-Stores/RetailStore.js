var RetailStore = new cLASS({
  Name: "RetailStore",
  supertypeName: "oBJECT",
  properties: {
    "quantityInStock": {range:"NonNegativeInteger", label:"Stock", shortLabel: "stock"},
    "reorderPoint": {range:"NonNegativeInteger"},
    "targetInventory": {range:"PositiveInteger"}
  }
});
