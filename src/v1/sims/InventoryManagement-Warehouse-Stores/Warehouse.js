var Warehouse = new cLASS({
  Name: "Warehouse",
  supertypeName: "oBJECT",
  properties: {
    "quantityInStock": {range:"NonNegativeInteger", shortLabel:"stock", label:"Stock"},
    "reorderInterval": {range:"NonNegativeInteger"},
    "targetInventory": {range:"PositiveInteger"}
  }
});
