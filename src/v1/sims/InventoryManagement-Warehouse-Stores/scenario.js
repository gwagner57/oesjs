/*******************************************************
 * InventoryManagement-Warehouse-Stores - An example of a discrete event simulation.
 * @copyright Copyright 2016 Gerd Wagner, BTU (Germany) + ODU (VA, USA)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
********************************************************/
/*******************************************************
 General Variables and Methods
********************************************************/

/*******************************************************
 Simulation Parameters
********************************************************/
sim.scenario.simulationEndTime = 100;
//sim.scenario.randomSeed = 5;  // optional
sim.scenario.createLog = true;

/*******************************************************
 Simulation Model
********************************************************/
sim.model.name = "InventoryManagement-Warehouse-Stores";
sim.model.title = "Inventory Management in a Warehouse-Store Supply Chain";
sim.model.systemNarrative = "<p>A single-product company (e.g., selling one model of TVs, only) " +
    "has several retail stores and a central warehouse. On each business day, " +
    "customers come to the stores and place their orders. If the ordered product quantity is in stock, " +
    "the customer pays for the order and the ordered products are provided. Otherwise, the store has missed " +
    "a business opportunity and the difference bteween order quantity and stock level counts as a " +
    "lost sale. The order may still be partially fulfilled, if there are still some items in stock, else " +
    "the customer has to leave the store without any item. The percentage of lost sales is an important " +
    "performance indicator.</p>" +
    "<p>Stores use a continuous replenishment policy. Consequently, whenever their reorder point has been reached, " +
    "they place a replenishment order with a quantity computed as the difference between an upper " +
    "threshold (called <i>target inventory</i>) and the current stock level.</p>" +
    "<p>The warehose uses a periodic replenishment policy. Consequently, whenever its reorder interval has passed, " +
    "it places a replenishment order with a quantity computed as the difference between its " +
    "target inventory and the current stock level.</p>";
sim.model.shortDescription = "<p>The model defines a Warehouse-Store supply chain. For simplicity, " +
    "customer orders are treated in an abstract way by aggregating them into daily demand events, " +
    "such that the random variation of the daily order quantity is modeled by a random variable.</p>" +
    "<p>Likewise, the random variation of the <i>delivery lead time</i>, which is the time in-between a " +
    "replenishment order and the corresponding delivery, is modeled in the form of random variables.</p>";
sim.model.objectTypes = ["RetailStore", "Warehouse"];
sim.model.eventTypes = ["DailyDemand", "Delivery"];
// meta data
sim.model.license = "CC BY-NC";
sim.model.creator = "Gerd Wagner";
sim.model.created = "2016-06-01";
sim.model.modified = "2016-11-14";

/*******************************************************
 Define the initial state
 ********************************************************/
sim.scenario.initialState.objects = {
  "1": {typeName: "RetailStore", name:"store1",
    quantityInStock: 100, reorderPoint: 50, targetInventory: 100},
  "2": {typeName: "RetailStore", name:"store2",
    quantityInStock: 200, reorderPoint: 100, targetInventory: 200},
  "3": {typeName: "Warehouse", name:"warehouse",
    quantityInStock: 500, reorderInterval: 3, targetInventory: 500}
};
sim.scenario.initialState.events = [
  {typeName: "DailyDemand", occTime:1, quantity:20, store:"1"},
  {typeName: "DailyDemand", occTime:1, quantity:40, store:"2"},
];
/*******************************************************
 Define Output Statistics Variables
 ********************************************************/
sim.model.statistics = {
  "lostSales": {range:"NonNegativeInteger", label:"Lost"},
  "averageInventory": {objectType:"RetailStore", objectIdRef: 1,
    property:"quantityInStock", aggregationFunction:"avg", label:"Avg. inventory"}
};
