/*******************************************************************************
 * The DailyDemand event class
 *
 * @copyright Copyright 2015-2016 Gerd Wagner
 *   Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 ******************************************************************************/
var DailyDemand = new cLASS({
  Name: "DailyDemand",
  supertypeName: "eVENT",
  properties: {
    "quantity": {range: "PositiveInteger", label:"quant"},
    "store": {range: "RetailStore"}
  },
  methods: {
    "onEvent": function () {
      var q = this.quantity,
          prevStockLevel = this.store.quantityInStock;
      // update lostSales if demand quantity greater than stock level
      if (q > prevStockLevel) {
        sim.stat.lostSales += q - prevStockLevel;
      }
      // update quantityInStock
      this.store.quantityInStock = Math.max( prevStockLevel-q, 0);
      // periodically schedule new Delivery events
      if (sim.time % this.store.reorderInterval === 0) {
        return [new Delivery({
          occTime: this.occTime + Delivery.sampleLeadTime(),
          quantity: this.store.targetInventory - this.store.quantityInStock,
          receiver: this.store
        })];
      } else return [];  // no follow-up events
    }
  }
});
DailyDemand.recurrence = function () {
  return 1;
};
DailyDemand.sampleQuantity = function () {
  return rand.uniformInt( 5, 30);
};
DailyDemand.createNextEvent = function (e) {
 return new DailyDemand({
    occTime: e.occTime + DailyDemand.recurrence(),
    quantity: DailyDemand.sampleQuantity(),
    store: e.store
  });
};
