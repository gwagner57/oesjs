/**
 * @fileOverview A JavaScript implementation of an Object-Event Simulator defined as
 * a JS object "sim". The simulator is associated with a simulation model (sim.model)
 * and one or more simulation scenarios (sim.scenarios).
 *
 * @copyright Copyright 2016 Gerd Wagner and Mircea Diaconescu, BTU (Germany)
 * @author Gerd Wagner
 * @license The MIT License (MIT)
 *
 * Integrity constraints that must be satisfied at any simulation step can be defined
 * as Boolean functions in the map "model.constraints" with constraint names being the
 * keys.
 */
/*
Improvements/extensions
v1
 - Add support for more stop conditions:
   (a) sim.durationInCpuTime, (b) sim.durationInSteps
 - Define set/get for scenario.visualize and use the setter for dropping/setting-up the visualization (canvas)
 - Rename "eXPERIMENTdEF" to "eXPERIMENTtYPE", and (b) drop the "id" property and the eXPERIMENTdEF.idCounter
   (c) drop "scenarioNo" (d) make "model" optional

 - improve clock-time measuring and support real-time simulation enabled by realtimeFactor set to 1

 -  (5) introduce processOwner (?) and resourcePools
 - Implement support for the "recurrence" attribute of entry nodes
 - Allow setting a waiting timeout for the input queues of processing nodes (corresponding
   to AnyLogic's "Enable exit on timeout")
 - Implement support for the "processingCapacity" attribute of processing nodes (by popping/forwarding
   more than one processing objects)

v2
 - replace the "load manager" by using the new dynamic "import" JS command:
     const calculator = await import('./calculator.js');
 - Distinguish between "model variable" versus "model parameter" (as in AnyLogic)
 - refactor createInitialObjEvt into a create and a reset procedure such that already created initial objects
    are not deleted, but reset, when rerunning a simulation
 - New model constraint checks:
   + prevent the use of pre-defined cLASS names ("aRRIVAL", etc.) and predefined property names (oes.predfinedProperties)
 - support the definition of a "warm-up period"
 - Split up the current initialize statistics into a setup and a (trimmed) initialize procedure,
   such that the setup is called before an experiment and only the initialize is called for each run
 - improve the initial state definition UI:
   + support value changes via IndexedDB
   + allow adding/dropping objects in the ClassPopulationWidget
   + support enumeration attributes in the ClassPopulationWidget
 - Add observation UIs for visualizing variables in "monitors"

 - run experiment scenarios in parallel worker threads using the navigator.hardwareConcurrency information
   (see https://developer.mozilla.org/en-US/docs/Web/API/NavigatorConcurrentHardware/hardwareConcurrency)
 - Add exploration model
 - Support using variants of the same model (sim.models)
 - UI for defining ex-post statistics

v3
 - extend mODELcLASS with object pools
 - use ES6 modules
 - improve add/delete performance by using Maps instead of plain JS objects (consider weak Maps for better Garbage Collection)
 - concurrent event processing with deferred state changes
 - add agents
 - add participation model
 */

/*******************************************************
 Initializations
 ********************************************************/
sim.ui = sim.ui || {}; // runtime UI components
/*******************************************************
 Add object to simulation objects
 *******************************************************
 * @author Gerd Wagner
 * @method
 * @param o  the object to be added
 */
sim.addObject = function (o) {
  if (!(o instanceof oes.Object)) {
    console.log( o.toString() +" is not an oBJECT!");
    return;
  }
  if (!o.id) o.id = sim.idCounter++;
  sim.objects[String(o.id)] = o;
  if (o.name) {
    if (typeof o.name !== "string" ) {
      console.log("oBJECT "+ o.toString() +" has a non-string name"+ o.name);
      return;
    } else sim.namedObjects[o.name] = o;
  }
  return o;
};
sim.addObjects = function (objArr) {
  objArr.forEach( function (o) {sim.addObject(o)});
  return objArr;
};
/*******************************************************
 Remove an object from the set of simulation objects
 *******************************************************
 * @author Gerd Wagner
 * @method
 * @param o  the object to be removed
 */
sim.removeObject = function (o) {
  var ObjectClass=null;
  if (!(o instanceof oes.Object)) {
    console.error("Failure of removeObject: "+ JSON.stringify(o) +" is not an OES object!");
    return;
  }
  if (!sim.objects[String(o.id)]) {
    console.error("Failure of removeObject: "+ JSON.stringify(o) +"@"+ sim.time +
        " has not been registered as a simulation object!");
    return;
  }
  ObjectClass = o.constructor;
  delete ObjectClass.instances[o.id];
  delete sim.objects[o.id];
};
sim.removeObjectById = function (id) {
  var ObjectClass=null;
  if (typeof id === "string") id = parseInt(id);
  if (!Number.isInteger( id)) {
    console.error("Failure of removeObjectById: "+ JSON.stringify(id) +" is not an integer!");
    return;
  }
  if (!sim.objects[id]) {
    console.error("Failure of removeObjectById: "+ JSON.stringify(id) +
        " is not an ID of a registered simulation object!");
    return;
  }
  ObjectClass = sim.objects[id].constructor;
  delete ObjectClass.instances[id];
  delete sim.objects[id];
};
/*******************************************************
 Schedule an event by adding it to the FEL
 *******************************************************
 * @author Gerd Wagner
 * @method
 * @param e  the event to be scheduled
 */
sim.scheduleEvent = function (e) {
  if (e instanceof oes.Event) {
    if (e.delay) e.occTime = sim.time + e.delay;
    else if (!e.occTime) e.occTime = sim.time + sim.nextMomentDeltaT;
    sim.FEL.add( e);
  } else {
    console.log( e.toString() +" is not an OES event (not an eVENT instance)!");
  }
};
/********************************************************
 * Initialize Model Variables
 ********************************************************/
sim.initializeModelVariables = function (expParamSlots) {
  sim.v = Object.create( null);  // a map of global variables (accessible by name)
  // set up the map of model variables
  sim.model.v = sim.model.v || Object.create( null);
  Object.keys( sim.model.v).forEach( function (varName) {
    var mv = sim.model.v[varName];
    if (typeof expParamSlots === "object" && expParamSlots[varName]) {
      // assign experiment parameter value
      sim.v[varName] = expParamSlots[varName];
    } else {
      sim.v[varName] = (mv.value !== undefined) ? mv.value : mv.initialValue;
    }
  });
}
/********************************************************
 * Create Initial Objects and Events
 ********************************************************/
sim.createInitialObjEvt = function () {
  var initState = sim.scenario.initialState,
      initialEvtDefs=null, initialObjDefs=null, entryNodes={}, procNodes={};
  // clear initial state data structures
  sim.objects = Object.create( null);  // a map of all objects (accessible by ID)
  sim.namedObjects = Object.create( null);  // a map of objects accessible by a unique name
  sim.FEL.clear();
  sim.ongoingActivities = Object.create( null);  // a map of all ongoing activities accessible by ID
  // clear the cLASS populations of model-specific object types
  sim.model.objectTypes.forEach( function (objTypeName) {
    cLASS[objTypeName].instances = Object.create( null);
  });
  // clear the cLASS populations of pre-defined object and activity types
  ["eNTRYnODE","pROCESSINGnODE","eXITnODE","pROCESSINGoBJECT","pROCESSINGaCTIVITY"].
      forEach( function (objTypeName) {cLASS[objTypeName].instances = {};});
  // allow parametrized object/event definitions
  if (typeof sim.scenario.setupInitialState === "function") {
    sim.scenario.setupInitialState();
  }
  // register initial objects
  initialObjDefs = initState.objects;
  if (initialObjDefs) {  // a map of object definitions
    Object.keys( initialObjDefs).forEach( function (objIdStr) {
      var objSlots = util.cloneObject( initialObjDefs[objIdStr]),
          objTypeName = objSlots.typeName,
          ObjType = cLASS[objTypeName], obj=null;
      // fatal error: object type class not found
      if (!ObjType) throw "Missing object type class '" + objTypeName + "'!";
      objSlots.id = parseInt( objIdStr);
      delete objSlots.typeName;  // remove typeName slot
      try {obj = new ObjType( objSlots);}
      catch (e) {
        if (typeof e !== "object") console.log( e);
        else console.log( e.constructor.name +": "+ e.message);
        obj = null;
      }
      if (obj) sim.addObject( obj);
    })
  }
  // convert ID references to object references (in a second pass)
  Object.keys( sim.objects).forEach( function (objIdStr) {
    var obj = sim.objects[objIdStr],
        propDefs = cLASS[obj.constructor.Name].properties;
    Object.keys( obj).forEach( function (p) {
      if (!propDefs[p]) {
        if (typeof obj[p] !== "function" && !oes.predfinedProperties.includes(p)) {
          console.log("Undeclared prop: "+ p +" for obj "+ objIdStr);
        }
        return;
      }
      var range = propDefs[p].range, val = obj[p], rangeClasses=[];
      if (typeof range === "string" && typeof val !== "object" &&
          (cLASS[range] || range.includes("|"))) {
        if (range.includes("|")) {
          rangeClasses = range.split("|");
          // check referential integrity: val must be in some range class
          if (!rangeClasses.some( function (rc) {
                return cLASS[rc].instances[String(val)];
              })) {
            throw "Referential integrity violation for property "+ p +": "+ val +" does not reference any of "+
                range +"!";
          }
        } else if (!(sim.objects[String(val)] instanceof cLASS[range])) {  // also allows superclasses
            throw "Referential integrity violation for property "+ p +": "+ val +" does not reference a "+ range +"!";
        }
        obj[p] = sim.objects[String(val)];
      }
    });
  });
  // schedule initial events
  initialEvtDefs = initState.events;
  if (initialEvtDefs) {  // an array of JS object definitions
    initialEvtDefs.forEach( function (evt) {
      var e = util.cloneObject( evt),  // clone event object definition
          evtTypeName = e.typeName,
          EvtType = cLASS[evtTypeName];
      // fatal error: event type class not found
      if (!EvtType) throw Error("Missing class for event type '" + evtTypeName + "'!");
      delete e.typeName;  // remove type slot
      sim.scheduleEvent( new EvtType( e));
    })
  }
  /**************************************************************
   * Special settings for PN models
   **************************************************************/
  // schedule initial arrival events for the entry nodes of a PN
  entryNodes = oes.EntryNode.instances;
  Object.keys( entryNodes).forEach( function (nodeIdStr) {
    var occT=0, arrEvt=null, entryNode = entryNodes[nodeIdStr];
    // has no arrival recurrence function been defined for this entry node?
    if (!entryNode.arrivalRecurrence) {
      // use the default recurrence
      occT = oes.Arrival.defaultRecurrence();
    } else {
      occT = entryNode.arrivalRecurrence();
    }
    arrEvt = new oes.Arrival({ occTime: occT, entryNode: entryNode});
    sim.scheduleEvent( arrEvt);
  });
  // create backlinks in serial PNs
  procNodes = oes.ProcessingNode.instances;
  Object.keys( procNodes).forEach( function (nodeIdStr) {
    var procNode = procNodes[nodeIdStr];
    if (procNode.successorNode) procNode.successorNode.predecessorNode = procNode;
  });
};
/*************************************************************
 * Update initial state objects (after modifications via the UI)
 ************************************************************/
sim.updateInitialStateObjects = function () {
  // reset the initial objects map
  sim.scenario.initialState.objects = Object.create( null);
  // loop over all object types
  sim.model.objectTypes.forEach( function (objTypeName) {
    var objects = cLASS[objTypeName].instances;
    // loop over all instances of this object type
    Object.keys( objects).forEach( function (objIdStr) {
      var obj = objects[objIdStr],
          objRec = util.createRecordFromObject( obj);
      objRec.typeName = objTypeName;
      delete objRec.id;
      sim.scenario.initialState.objects[objIdStr] = objRec;
    });
  });
};
/*************************************************************
 * Initialize the simulator on start up
 * Settings that do not vary across scenario runs in an experiment
 ************************************************************/
sim.initializeSimulator = function (dbName) {
  var x=0;
  sim.time = 0;  // simulation time
  sim.FEL = new oes.EventList();  // the Future Events List (FEL)
  // complete model definition by setting objectTypes and eventTypes if not defined
  if (!sim.model.objectTypes) sim.model.objectTypes = [];
  if (!sim.model.eventTypes) sim.model.eventTypes = [];
  // set timeIncrement for fixed-increment time progression
  if (sim.model.timeIncrement) {
    sim.timeIncrement = sim.model.timeIncrement;
  } else {
    if (sim.model.OnEachTimeStep) sim.timeIncrement = 1;
  }
  if (sim.model.timeRoundingDecimalPlaces !== undefined) {
    if (!sim.model.time) {
      sim.model.time = "continuous";
      console.warn("Set sim.model.time = 'continuous' for time rounding!");
    }
  } else if (sim.model.time === "continuous" && !sim.timeIncrement) {
    sim.model.timeRoundingDecimalPlaces = oes.defaults.timeRoundingDecimalPlaces;
  }
  if (sim.model.time === "continuous") {
    if (sim.model.timeRoundingDecimalPlaces) {
      sim.timeRoundingFactor = Math.pow( 10, sim.model.timeRoundingDecimalPlaces);
    } else {
      if (sim.timeIncrement) {  // fixed-increment time progression
        // determine rounding factor
        x = sim.timeIncrement - Math.trunc( sim.timeIncrement);
        if (x === 0) sim.timeRoundingFactor = 1;
        else if (x >= 0.1) sim.timeRoundingFactor = 10;
        else if (x >= 0.01) sim.timeRoundingFactor = 100;
        else sim.timeRoundingFactor = 1000;
      }
    }
    // define the minimal time delay until the next moment
    if (sim.model.nextMomentDeltaT) {
      sim.nextMomentDeltaT = sim.model.nextMomentDeltaT;
    } else if (sim.timeRoundingFactor) {
      sim.nextMomentDeltaT = 1 / sim.timeRoundingFactor;
    } else {  // default
      sim.nextMomentDeltaT = 0.000001;
    }
  } else {  // discrete time
    sim.nextMomentDeltaT = 1;
  }
  // compute derived property EventClass.participantRoles
  sim.model.eventTypes.forEach( function (evT) {
    var EventClass = cLASS[evT];
    EventClass.participantRoles = Object.keys( EventClass.properties).filter( function (prop) {
      // the range of a participant role must be an object type
      return sim.model.objectTypes.includes( EventClass.properties[prop].range);
    });
  });
  // initialize space model
  if (sim.model.space.type) oes.space.initialize();
  // set up a default random variate sampling method
  if (sim.scenario.randomSeed) {  // use the Mersenne Twister RNG
    rand = new Random( sim.scenario.randomSeed);
  } else {  // use the JS built-in RNG
    rand = new Random();
  }
  // initialize experiment(s)
  if (sim.experiment.replications) {  // an experiment has been defined
    if (!sim.experiment.parameterDefs) sim.experiment.parameterDefs = [];
    sim.experiment.parameterDefs.forEach( function (paramDef, i, a) {
      if (paramDef.constructor !== oes.ExperimentParamDef) {
        a[i] = new oes.ExperimentParamDef( paramDef);
      }
    });
    if (sim.experiment.constructor !== oes.ExperimentDef) {
      sim.experiment = new oes.ExperimentDef( sim.experiment);
    }
  }
  if (dbName) oes.setupStorageManagement( dbName);
};
/*******************************************************************
 * Initialize a (standalone or experiment scenario) simulation run *
 *******************************************************************/
sim.initializeSimulationRun = function (expParamSlots, seed) {
  var logInfo={};
  var isExperimentRun = expParamSlots !== undefined || seed;
  sim.step = 0;  // simulation loop step counter
  sim.time = 0;  // simulation time
  // set default endTime
  if (!sim.scenario.endTime) sim.scenario.endTime = Infinity;
  // set warm-up period for delaying the gathering of statistics
  sim.warmUpTime = sim.scenario.warmUpTime ? sim.scenario.warmUpTime : 0;
  // set stop condition
  if (sim.config.stopConditionName) {
    sim.stopCondition = sim.model.stopConditions[sim.config.stopConditionName];
  } else delete sim.stopCondition;
  // get ID counter from simulation scenario, or set to default value
  sim.idCounter = sim.scenario.idCounter || 1000;
  // set up a default random variate sampling method
  if (!isExperimentRun && sim.scenario.randomSeed) {  // use the Mersenne Twister RNG
    rand = new Random( sim.scenario.randomSeed);
  } else if (seed) {  // experiment-defined replication-specific seed
    rand = new Random( seed);
  } else {  // use the JS built-in RNG
    rand = new Random();
  }
  // set up initial state
  sim.initializeModelVariables( expParamSlots);
  sim.createInitialObjEvt();
  if (Object.keys( oes.EntryNode.instances).length > 0) oes.setupProcNetStatistics();
  if (sim.model.statistics) {
    // initialize statistics
    oes.stat.initialize();
    // create statistics for initial state
    oes.stat.updateStatistics();
  }
  // get stepDuration from simulation config, or set to default value
  sim.stepDuration = sim.config.stepDuration || 0;
  // log initial state (visualized before in oes.setupFrontEndSimEnv)
  if (sim.config.createLog) {
    logInfo = sim.createStepLogInfo();
    if (!sim.useWorker) {  // main thread
      if (typeof sim.logStep === "function") sim.logStep( logInfo);
    } else {  // worker thread
      self.postMessage({  // send log data to main thread
        simStep: sim.step,
        simTime: logInfo.simTime,
        systemStateInfo: logInfo.systemStateInfo,
        evtInfo: logInfo.evtInfo
      });
    }
  }
};
/*******************************************************
 Run a Standalone Scenario
********************************************************/
sim.runScenario = function (useWorker) {
  var simTimeTenth = parseInt( sim.scenario.simulationEndTime / 10),
      nextProgressIncremTime = simTimeTenth,
      areConstraintsToBeChecked = sim.isConstraintCheckingTurnedOn ||
          sim.isConstraintCheckingTurnedOn === undefined && oes.loadManager &&
          oes.loadManager.codeLoadingMode !== "deploy";
  // Turn off property constraint checking for object/event creation with cLASS
  if (!areConstraintsToBeChecked) cLASS.areConstraintsToBeChecked = false;
  if (!useWorker) {  // running in main thread
    sim.useWorker = false;
    sim.initializeSimulationRun();
    sim.runScenarioStep();  // loops by self-invocation via setTimeout
  } else {  // running in worker thread
    sim.useWorker = true;
    sim.initializeSimulationRun();
    while (sim.time < sim.scenario.simulationEndTime) {
      sim.runScenarioStep();
      if (areConstraintsToBeChecked) {
        oes.checkModelConstraints({log:true});
        if (oes.isProcNetModel()) oes.checkProcNetConstraints({log:true});
      }
      // update the progress bar and the simulation step/time
      if (sim.time > nextProgressIncremTime) {
        self.postMessage({
            progressIncrement: 10,
            simStep: sim.step,
            simTime: sim.time
        });
        nextProgressIncremTime += simTimeTenth;
      }
      // end simulation if no time increment and no more events
      if (!sim.timeIncrement && sim.FEL.isEmpty()) break;
    }
    if (sim.model.statistics) oes.stat.computeOnlyAtEndStatistics();
    self.postMessage({simStat: sim.stat});
  }
};
/*******************************************************
 Standalone Scenario Simulation Step
 (when executed in main thread, it loops by self-invocation via setTimeout)
********************************************************/
sim.runScenarioStep = function (followupEvents) {
  var nextEvents=[], i=0, j=0,
      EventClass=null, nextExoEvt=null, e=null,
      stepStartTime = (new Date()).getTime(),
      totalStepTime = 0, stepDiffTimeDelay = 0,
      uia = sim.scenario.userInteractions,  // shortcut
      uiViewModel=null, eventTypeName="", logInfo={};
  //-----------------------------------------------------
  if (!sim.useWorker) {
    if (sim.stopRequested) {   // interrupt simulation
      sim.stopRequested = false;
      oes.ui.updateUiOnStop();
      return;
    }
    if (sim.time >= sim.scenario.simulationEndTime)  {  // terminate simulation
      if (sim.model.statistics) oes.stat.computeOnlyAtEndStatistics();
      oes.ui.updateUiOnSimulationEnd();
      return;
    }
  }
  if (followupEvents) {  // runScenarioStep was called from user action event handler
    // schedule follow-up events
    for (j=0; j < followupEvents.length; j++) {
      sim.scheduleEvent( followupEvents[j]);
    }
    // clear followUpEvents list
    followupEvents = [];
  } else {  // normal invocation of runScenarioStep
    followupEvents = [];
    sim.advanceSimulationTime();
    if (sim.time === sim.nextEvtTime) { // a next-event time progression step
      // extract and process next events
      nextEvents = sim.FEL.removeNextEvents();
      // store current events in global variable for testing stop conditions
      sim.currentStepEvents = nextEvents;
      if (nextEvents.length > 1) nextEvents.sort( oes.Event.rank);  // priority order
      for (i=0; i < nextEvents.length; i++) {
        e = nextEvents[i];
        eventTypeName = e.constructor.Name;
        // retrieve event class
        EventClass = cLASS[eventTypeName];
        // test if EventClass represents an exogenous event type
        if (typeof EventClass.recurrence === "function") {
          // create and schedule next exogenous event
          if (typeof e.createNextEvent === "function") {
            sim.scheduleEvent( e.createNextEvent());
          } else if (EventClass.createNextEvent) {  // old syntax (class-level method)
            sim.scheduleEvent( EventClass.createNextEvent( e));
          } else {
            nextExoEvt = new EventClass();
            nextExoEvt.occTime = e.occTime + EventClass.recurrence();
            // copy event participants
            EventClass.participantRoles.forEach( function (pR) {
              nextExoEvt[pR] = e[pR];
            });
            sim.scheduleEvent( nextExoEvt);
          }
        }
        // check if a user interaction has been triggered
        if (sim.config.userInteractive && uia && uia[eventTypeName]) {
          // check also the triggering event condition, if defined
          if (!uia[eventTypeName].trigEvtCondition || uia[eventTypeName].trigEvtCondition(e)) {
            // make sure that the user interaction triggering event is last in nextEvents list
            if (i === nextEvents.length - 1) {
              sim.currentEvents[eventTypeName] = e;
              uiViewModel = uia[eventTypeName];
              Object.keys( uiViewModel.outputFields).forEach( function (outFldN) {
                var fldEl = uiViewModel.dataBinding[outFldN],
                    val = uiViewModel.outputFields[outFldN].value;
                if (typeof val === "function") fldEl.value = val();
                else fldEl.value = val || "";
              });
              uiViewModel.domElem.style.display = "block";
              return;  // interrupt simulator & transfer control to UI
            } else {
              util.swapArrayElements( nextEvents, i, length-1);
            }
          }
        }
        followupEvents = e.onEvent();
        // render event appearances if defined
        if (sim.config.visualize && sim.ui.animations && sim.ui.animations[eventTypeName]) {
          sim.ui.animations[eventTypeName].play();
        }
        // schedule follow-up events
        for (j=0; j < followupEvents.length; j++) {
          sim.scheduleEvent( followupEvents[j]);
        }
        // clear followUpEvents list
        followupEvents = [];
      }
    } else {  // a fixed-increment time progression step
      if (sim.model.OnEachTimeStep) sim.model.OnEachTimeStep();
    }
  }
  // update statistics
  if (sim.model.statistics && sim.time >= sim.warmUpTime) oes.stat.updateStatistics();
  // create simulation log
  if (sim.config.createLog) {
    logInfo = sim.createStepLogInfo();
    logInfo.simStep = sim.step;
    if (!sim.useWorker) {  // main thread
      if (typeof sim.logStep === "function") sim.logStep( logInfo);
    } else {  // worker thread
      self.postMessage( logInfo);
    }
  }
  if (!sim.useWorker) {  // only in main thread
    // update the sim-control UI via the fields' data binding to UI output elements
    sim.ui["sim"].dataBinding["step"].value = sim.step;
    sim.ui["sim"].dataBinding["time"].value = sim.time;
    // update state visualization (NOT in worker mode)
    if (sim.config.visualize) oes.ui.visualizeStep();
    // compute the time needed for executing this step
    totalStepTime = (new Date()).getTime() - stepStartTime;
    // check if we need some delay, because of the stepDuration parameter
    if (sim.stepDuration > totalStepTime) {
      stepDiffTimeDelay = sim.stepDuration - totalStepTime
    } else {
      stepDiffTimeDelay = 0;
    }
    // end simulation if no time increment and no more events
    if (!sim.timeIncrement && sim.FEL.isEmpty()) {
      if (sim.model.statistics) oes.stat.computeOnlyAtEndStatistics();
      oes.ui.updateUiOnSimulationEnd();
    } else if (sim.mode !== "SingleStepSim") {
      if (typeof sim.stopCondition === "function" && sim.stopCondition()) {
        sim.stopRequested = true;
      }
      // continue simulation loop
      // in the browser, use setTimeout to prevent script blocking
      setTimeout( sim.runScenarioStep, stepDiffTimeDelay);
    }
  }
};
/*******************************************************
 Run an Experiment (in a JS worker)
 ********************************************************/
sim.runExperiment = async function () {
  var exp = sim.experiment, cp = [], valueSets = [], i = 0, j = 0, k = 0, M = 0,
    N = exp.parameterDefs.length, increm = 0, x = 0, expPar = {},
    expRunId = (new Date()).getTime(),
    valueCombination = [], expParamSlots = {},
    tenthRunLength = 0,  // a tenth of the total run time
    nextProgressIncrementStep = 0;  // thresholds for updating the progress bar
  try {
    await sim.storeMan.add( oes.ExperimentRun, {
      id: expRunId,
      experimentDef: exp.id,
      dateTime: (new Date()).toISOString(),
    });
  } catch (e) {
    console.log( JSON.stringify(e));
  }
  // create preliminary definitions of implicit PN statistics variables
  if (oes.isProcNetModel()) {
    if (!sim.model.statistics["arrivedObjects"]) {
      sim.model.statistics["arrivedObjects"] = {label:"Arrived objects"};
    }
    if (!sim.model.statistics["departedObjects"]) {
      sim.model.statistics["departedObjects"] = {label:"Departed objects"};
    }
    if (!sim.model.statistics["meanTimeInSystem"]) {
      sim.model.statistics["meanTimeInSystem"] = {label:"Mean time in system"};
    }
  }
  if (N === 0) {  // simple experiment (without parameters)
    cp = [[]];  // only 1 empty parameter value combination
    M = cp.length;  // size of cartesian product
    // initialize replication statistics record
    exp.replicStat = {};
    Object.keys( sim.model.statistics).forEach( function (varName) {
      var statVar = sim.model.statistics[varName];
      // is output statistics with simple numeric values?
      statVar.isSimpleOutputStatistics = statVar.label && (!statVar.range || cLASS.numericTypes.includes( statVar.range));
      if (statVar.isSimpleOutputStatistics) {
        exp.replicStat[varName] = [];  // an array per statistics variable
      }
    });
    tenthRunLength = exp.replications > 10 ? Math.floor( exp.replications / 10) : 1;
  } else {
    for (i=0; i < N; i++) {
      expPar = exp.parameterDefs[i];
      if (!expPar.values) {
        // create value set
        expPar.values = [];
        increm = expPar.stepSize || 1;
        for (x = expPar.startValue; x <= expPar.endValue; x += increm) {
          expPar.values.push( x);
        }
      }
      valueSets.push( expPar.values);
    }
    cp = util.cartesianProduct( valueSets);
    M = cp.length;  // size of cartesian product
    tenthRunLength = Math.min( 1, Math.floor((M * exp.replications) / 10));
  }
  nextProgressIncrementStep = tenthRunLength;
  // loop over all combinations of experiment parameter values
  for (i=0; i < M; i++) {
    valueCombination = cp[i];  // a JS array
    // initialize the scenario record
    exp.scenarios[i] = {stat:{}};
    exp.scenarios[i].parameterValues = valueCombination;
    // initialize experiment scenario statistics
    Object.keys( sim.model.statistics).forEach( function (varName) {
      var statVar = sim.model.statistics[varName];
      if (statVar.label) {  // output statistics
        if (statVar.property && statVar.objectType &&
            !statVar.aggregationFunction) {
          exp.scenarios[i].stat[varName] = {};
        } else exp.scenarios[i].stat[varName] = 0;
      }
    });
    // create experiment parameter slots for assigning corresponding model variables
    for (j=0; j < N; j++) {
      expParamSlots[exp.parameterDefs[j].name] = valueCombination[j];
    }
    // run experiment scenario replications
    for (k=0; k < exp.replications; k++) {
      if (exp.seeds) {
        sim.initializeSimulationRun( expParamSlots, exp.seeds[k]);
      } else {
        sim.initializeSimulationRun( expParamSlots);
      }
      while (sim.time < sim.scenario.simulationEndTime) {
        sim.runExperimentScenarioStep();
        if (sim.config.isConstraintCheckingTurnedOn) {
          oes.checkModelConstraints({log:true});
          if (oes.isProcNetModel()) {
            oes.checkProcNetConstraints({log:true, add:" in repl. "+ String(k+1)});
          }
        }
        // end simulation if no time increment and no more events
        if (!sim.timeIncrement && sim.FEL.isEmpty()) break;
      }
      oes.stat.computeOnlyAtEndStatistics();
      if (N > 0) {  // experiment with parameters
        // add up replication statistics from sim.stat to sim.experiment.scenarios[i].stat
        Object.keys( sim.model.statistics).forEach( function (varName) {
          if (sim.model.statistics[varName].isSimpleOutputStatistics) {
            exp.scenarios[i].stat[varName] += sim.stat[varName];
          }
        });
        if (exp.storeEachExperimentScenarioRun) {
          await sim.storeMan.add( oes.ExperimentScenarioRun, {
            id: expRunId + i * exp.replications + k + 1,
            experimentRun: expRunId,
            experimentScenarioNo: i,
            parameterValueCombination: exp.scenarios[i].parameterValues,
            outputStatistics: Object.assign({}, sim.stat)  // clone
          });
        }
        // update progress bar
        if ((i+1) * exp.replications >= nextProgressIncrementStep) {
          self.postMessage({progressIncrement: 10});
          nextProgressIncrementStep += tenthRunLength;
        }
      } else {  // simple experiment
        // store replication statistics
        Object.keys( exp.replicStat).forEach( function (varName) {
          exp.replicStat[varName][k] = sim.stat[varName];
          /*
          if (sim.model.statistics[varName].isSimpleOutputStatistics) {
            exp.replicStat[varName][k] = sim.stat[varName];
          }
          */
        });
        await sim.storeMan.add( oes.ExperimentScenarioRun, {
          id: expRunId + k + 1,
          experimentRun: expRunId,
          outputStatistics: Object.assign({}, sim.stat)  // clone
        });
        // update progress bar
        if (k+1 >= nextProgressIncrementStep) {
          self.postMessage({progressIncrement: 10});
          nextProgressIncrementStep += tenthRunLength;
        }
      }
    }
    if (N === 0) {  // simple experiment (without parameters)
      // aggregate replication statistics in sim.experiment.scenarios[0].stat
      Object.keys( exp.replicStat).forEach( function (varName) {
        if (sim.model.statistics[varName].isSimpleOutputStatistics) {
          if (!sim.model.statistics[varName].hasRecordRange) {
            exp.scenarios[i].stat[varName] = {};
            Object.keys( oes.stat.summary).forEach( function (aggr) {
              var aggrF = oes.stat.summary[aggr].f;
              exp.scenarios[i].stat[varName][aggr] = aggrF( exp.replicStat[varName]);
            });
          } else {
            //TODO: loop over record fields
          }
        }
      });
    }
    if (N > 0) {  // experiment with parameters
      // compute averages
      Object.keys( sim.model.statistics).forEach( function (varName) {
        if (sim.model.statistics[varName].isSimpleOutputStatistics) {
          exp.scenarios[i].stat[varName] /= exp.replications;
        }
      });
      // send statistics to main thread
      self.postMessage({
        expScenNo: i,
        expScenParamValues: exp.scenarios[i].parameterValues,
        expScenStat: exp.scenarios[i].stat
      });
      // store the summary statistics aggregated over all exp. scenario runs
      if (!exp.storeEachExperimentScenarioRun) {
        try {
          await sim.storeMan.add( oes.ExperimentScenarioRun, {
            experimentRun: expRunId,
            experimentScenarioNo: i,
            parameterValueCombination: exp.scenarios[i].parameterValues,
            outputStatistics: exp.scenarios[i].stat
          });
        } catch (e) {
          console.log( JSON.stringify(e));
        }
      }
    } else {  // simple experiment (without parameters)
      // send statistics to main thread
      self.postMessage({
        expReplicStat: exp.replicStat,
        expScenStat: exp.scenarios[i].stat
      });
    }
  }
  self.postMessage({endOfExp: true});
};
/*******************************************************
 Advance Simulation Time
 ********************************************************/
sim.advanceSimulationTime = function () {
  sim.nextEvtTime = sim.FEL.getNextOccurrenceTime();  // 0 if there is no next event
  // increment the step counter
  sim.step += 1;
  // advance simulation time
  if (sim.timeIncrement) {  // fixed-increment time progression
    // fixed-increment time progression simulations may also have events
    if (sim.nextEvtTime > sim.time && sim.nextEvtTime < sim.time + sim.timeIncrement) {
      sim.time = sim.nextEvtTime;  // an event occurring before the next incremented time
    } else {
      sim.time += sim.timeIncrement;
    }
  } else if (sim.nextEvtTime > 0) {  // next-event time progression
    sim.time = sim.nextEvtTime;
  }
  if (sim.model.time === "continuous" && sim.timeRoundingFactor) {
    sim.time = Math.round( sim.time * sim.timeRoundingFactor) /
        sim.timeRoundingFactor;
    sim.nextEvtTime = Math.round( sim.nextEvtTime * sim.timeRoundingFactor) /
        sim.timeRoundingFactor;
  }
}
/*******************************************************
 Experiment Scenario Simulation Step
 ********************************************************/
sim.runExperimentScenarioStep = function () {
  var nextEvents=[], i=0, j=0,
      EventClass=null, nextExoEvt=null, e=null,
      eventTypeName="", followupEvents=[];
  //-----------------------------------------------------
  sim.advanceSimulationTime();
  if (sim.time === sim.nextEvtTime) { // a next-event time progression step
    // extract and process next events
    nextEvents = sim.FEL.removeNextEvents();
    if (nextEvents.length > 1) nextEvents.sort( oes.Event.rank);  // priority order
    for (i=0; i < nextEvents.length; i++) {
      e = nextEvents[i];
      eventTypeName = e.constructor.Name;
      // retrieve event class
      EventClass = cLASS[eventTypeName];
      // does EventClass represent an exogenous event type?
      if (EventClass.recurrence) {
        // create and schedule next exogenous event
        if (e.createNextEvent) {  // new syntax
          sim.scheduleEvent( e.createNextEvent());
        } else if (EventClass.createNextEvent) {  // old syntax (class-level method)
          sim.scheduleEvent( EventClass.createNextEvent( e));
        } else {
          nextExoEvt = new EventClass();
          nextExoEvt.occTime = e.occTime + EventClass.recurrence();
          // copy event participants
          EventClass.participantRoles.forEach( function (pR) {
            nextExoEvt[pR] = e[pR];
          });
          sim.scheduleEvent( nextExoEvt);
        }
      }
      followupEvents = e.onEvent();
      // schedule follow-up events
      for (j=0; j < followupEvents.length; j++) {
        sim.scheduleEvent( followupEvents[j]);
      }
      // clear followUpEvents list
      followupEvents = [];
    }
  } else {  // a fixed-increment time progression step
    if (sim.model.OnEachTimeStep) sim.model.OnEachTimeStep();
  }

  // update statistics
  if (sim.model.statistics && sim.time >= sim.warmUpTime) oes.stat.updateStatistics();
};
/*******************************************************
 Create step log info
 ********************************************************/
sim.createStepLogInfo = function () {
  var simTime = sim.model.time === "continuous" && sim.timeRoundingFactor ?
      Math.round( sim.time * sim.timeRoundingFactor) / sim.timeRoundingFactor :
      sim.time;
  var systemStateInfo = Object.keys( sim.v).reduce( function (serialization, varName, i) {
    var varDecl = sim.model.v[varName], slotSerialization="";
    if (varDecl.shortLabel) {
      slotSerialization = varDecl.shortLabel +": "+ sim.v[varName];
      return i>0 ? serialization +", "+ slotSerialization : slotSerialization;
    } else return serialization;
  }, "");
  if (systemStateInfo && Object.keys( sim.objects).length > 0) systemStateInfo += ", ";
  systemStateInfo += Object.keys( sim.objects).reduce( function (serialization, objIdStr, i) {
    var o = sim.objects[objIdStr];
    if (o.shortLabel || o.constructor.shortLabel) {
      return i>0 ? serialization +", "+ o.toLogString() : o.toLogString();
    } else return serialization;
  }, "");
  return {simTime: String(simTime), systemStateInfo: systemStateInfo, evtInfo: sim.FEL.toString()}
};