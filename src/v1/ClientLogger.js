/*******************************************************
 Simulation Log
********************************************************/
sim.logStep = function (stepLog) {
  // create new row and insert as first row in table
  var rowEl = sim.ui.logEl.insertRow(0);
  rowEl.insertCell().textContent = stepLog.simStep;
  rowEl.insertCell().textContent = stepLog.simTime;
  rowEl.insertCell().textContent = stepLog.systemStateInfo;
  rowEl.insertCell().textContent = stepLog.evtInfo;
};