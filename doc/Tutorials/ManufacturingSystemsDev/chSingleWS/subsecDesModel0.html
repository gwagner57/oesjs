<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <title id="secDesM1.1">Design Model 1.1: Abstracting away from individual parts and processing start events</title>
  </head>
  <body>For making a simulation design model for the single workstation system described in the conceptual model presented in <a
  href="secSingleWsCM.html"></a>, a possible simplification, and simulation design choice, is to abstract away from individual parts and the
  composition of the input buffer, and instead only consider the length of the input buffer.<p>There are two situations when processing can be
  started: either when the input buffer is empty and a new part arrives, or when the input buffer is not empty and processing ends. Therefore, any
  processing start event immediately follows either a part arrival or a processing end event, and we may abstract away from processing start events
  and drop the corresponding event type from the design model.</p><p>These two simplifications result in the simulation design model shown in <a
  href="#figWS0IDM"></a> and <a href="#figWS0PDM"></a>.</p><figure id="figWS0IDM"><figcaption style="text-align:center;">Information design model 1.1
  does neither consider individual parts nor processing start events.</figcaption><img src="../img/WS0_IDM.svg" width="350" /></figure><p>A simulation
  information design model must distinguish between <i>exogenous</i> and <i>caused</i> (or <i>endogenous</i>) event types. For any exogenous event
  type, the recurrence of events of that type must be specified, typically in the form of a random variable, but in some cases it may be a constant
  (like 'on each Monday'). The recurrence defines the elapsed time between two consecutive events of the given type (their inter-occurrence time). It
  can be specified within the event class concerned in the form of a special method with the predefined name <i>recurrence</i>.</p><p>Notice that the
  underlining of the random variate functions <i>recurrence()</i> and <i>processingTime()</i> in the class diagram of <a href="#figWS0IDM"></a> means
  that they are defined as class-level (“static”) operations, which are invoked by using the class name as a prefix, like in
  <code>PartArrival.recurrence()</code>.</p><figure id="figWS0PDM"><figcaption style="text-align:center;">Process design model 1.1 based on the
  information design model 1.1 shown above.</figcaption><img src="../img/WS0_PDM.svg" width="500" /></figure><p>Each Event circle in a DPMN Process
  Diagram defines an <strong>event rule</strong> consisting of the Data Objects and the outgoing event scheduling arrows attached to it. Each attached
  Data Object defines an object variable that is bound to a specific object reference passed to the rule invocation via the triggering event
  expression. It allows accessing the property values of the referenced object both for querying and changing the object state. The second compartment
  of a DPMN Data Object contains one or more <em>state change statements</em>. All state change statements specified in attached Data Objects are
  executed first, before any follow-up event is scheduled according to the attached (possibly conditional) event scheduling arrows.</p><p>Process
  design model 1.1, shown in <a href="#figWS0PDM"></a>, defines two event rules:</p><ol>
      <li>On each <em>part arrival</em>, the input buffer length is incremented by 1 and, if it's equal to 1, a new <em>PartDeparture</em> event is
      scheduled to occur with a delay provided by invoking the <i>processingTime</i> function defined in the <i>WorkStation</i> object class.</li>
      <li>On each <em>part departure</em> (or <em>processing end</em>), the input buffer length is decremented by 1 and, if the input buffer length is
      still greater than 0, a new <i>PartDeparture</i> event is scheduled to occur with a delay provided by invoking the function
      <code>WorkStation.processingTime()</code>.</li>
    </ol><p>The following window allows running an implementation of design model 1.1.</p> <div class="iframe-container"> <iframe
  id="iframe1"></iframe> </div> <p>The dynamics of a simulation model can be illustrated with the help of a simulation log that shows the sequence of
  simulation steps of a particular run. Such a log can be created either manually (with paper and pencil), as often taught in simulation courses, or
  with a simulation tool. The following log has been created by running an OESjs implementation of design model 1.1 (presented in <a
  href="../appOESjs/secModel1.html"></a>) with a workstation that has initially 3 parts in its input buffer and two initial events: an arrival event
  and a departure event.</p><table border="1" id="simLogTbl" style="overflow-x: auto;">
      <thead>
        <tr>
          <th colspan="4">Simulation Log</th>
        </tr>
        <tr>
          <th colspan="2">Time</th>
          <th>System State</th>
          <th>Future Events</th>
        </tr>
      </thead>
      <tbody id="simLog">
        <tr>
          <td>20</td>
          <td>87.88</td>
          <td>workStation1{ bLen: 0}</td>
          <td>Arr@88.74</td>
        </tr>
        <tr>
          <td>19</td>
          <td>83.72</td>
          <td>workStation1{ bLen: 1}</td>
          <td>Dep@87.88, Arr@88.74</td>
        </tr>
        <tr>
          <td>18</td>
          <td>77.62</td>
          <td>workStation1{ bLen: 0}</td>
          <td>Arr@83.72</td>
        </tr>
        <tr>
          <td>17</td>
          <td>71.21</td>
          <td>workStation1{ bLen: 1}</td>
          <td>Dep@77.62, Arr@83.72</td>
        </tr>
        <tr>
          <td>16</td>
          <td>65.97</td>
          <td>workStation1{ bLen: 0}</td>
          <td>Arr@71.21</td>
        </tr>
        <tr>
          <td>15</td>
          <td>61.51</td>
          <td>workStation1{ bLen: 1}</td>
          <td>Dep@65.97, Arr@71.21</td>
        </tr>
        <tr>
          <td>14</td>
          <td>60.28</td>
          <td>workStation1{ bLen: 2}</td>
          <td>Dep@61.51, Arr@71.21</td>
        </tr>
        <tr>
          <td>13</td>
          <td>56.87</td>
          <td>workStation1{ bLen: 1}</td>
          <td>Arr@60.28, Dep@61.51</td>
        </tr>
        <tr>
          <td>12</td>
          <td>52.04</td>
          <td>workStation1{ bLen: 0}</td>
          <td>Arr@56.87</td>
        </tr>
        <tr>
          <td>11</td>
          <td>47.86</td>
          <td>workStation1{ bLen: 1}</td>
          <td>Dep@52.04, Arr@56.87</td>
        </tr>
        <tr>
          <td>10</td>
          <td>47.02</td>
          <td>workStation1{ bLen: 2}</td>
          <td>Dep@47.86, Arr@56.87</td>
        </tr>
        <tr>
          <td>9</td>
          <td>41.03</td>
          <td>workStation1{ bLen: 1}</td>
          <td>Arr@47.02, Dep@47.86</td>
        </tr>
        <tr>
          <td>8</td>
          <td>40.08</td>
          <td>workStation1{ bLen: 0}</td>
          <td>Arr@41.03</td>
        </tr>
        <tr>
          <td>7</td>
          <td>33.78</td>
          <td>workStation1{ bLen: 1}</td>
          <td>Dep@40.08, Arr@41.03</td>
        </tr>
        <tr>
          <td>6</td>
          <td>24.93</td>
          <td>workStation1{ bLen: 0}</td>
          <td>Arr@33.78</td>
        </tr>
        <tr>
          <td>5</td>
          <td>18.92</td>
          <td>workStation1{ bLen: 1}</td>
          <td>Dep@24.93, Arr@33.78</td>
        </tr>
        <tr>
          <td>4</td>
          <td>12.79</td>
          <td>workStation1{ bLen: 2}</td>
          <td>Dep@18.92, Arr@33.78</td>
        </tr>
        <tr>
          <td>3</td>
          <td>6.69</td>
          <td>workStation1{ bLen: 3}</td>
          <td>Dep@12.79, Arr@33.78</td>
        </tr>
        <tr>
          <td>2</td>
          <td>6.25</td>
          <td>workStation1{ bLen: 4}</td>
          <td>Dep@6.69, Arr@33.78</td>
        </tr>
        <tr>
          <td>1</td>
          <td>1</td>
          <td>workStation1{ bLen: 3}</td>
          <td>Arr@6.25, Dep@6.69</td>
        </tr>
        <tr>
          <td>0</td>
          <td>0</td>
          <td>workStation1{ bLen: 3}</td>
          <td>Arr@1, Dep@1</td>
        </tr>
      </tbody>
    </table> <blockquote class="role-attention"><p>A process model has to provide for the proper processing of simultaneous (or parallel) events.
  Special attention is required in cases of parallel events for which different processing orders yield different results.</p><p>In the case of model
  1.1, there may be situations with parallel arrival and departure events. Such situations are more likely when the granularity of simulation time is
  low, as in models with discrete time. For instance, using a discrete time model, there may be event pairs like {PartArrival@3, PartDeparture@3} in
  the Future Events List. However, this does not create any problem for model 1.1, since both possible parallel event processing orders yield the same
  successor simulation state, as shown below.</p></blockquote><p>A particular processing order for a set of parallel events is called a <em>parallel
  event serialization (PES)</em>. A process model is <em>PES-independent</em> if for any set of parallel events scheduled during a simulation run,
  i.e., added to the <em>Future Events List (FEL)</em>, their processing order doesn't matter, that is, different serializations do not create
  different successor simulation states. This is a desirable property for a process model. If a process model is not PES-independent, one may have to
  define processing order priorities for event types in order to achieve the intended effects of a set of parallel events.</p><p>The process model
  defined by <a href="#figWS0PDM"></a> is PES-independent. This can be proven by considering a simulation state with a set of current events to be
  processed by the simulator and then show that all possible serializations yield the same successor simulation state. Let's consider the simulation
  state SS = ⟨{inputBufferLength:1}, {A@3, D@3}⟩ at simulation time 3, where S={inputBufferLength:1} represents the system state and FEL = {A@3, D@3}
  represents the Future Events List. There are two possible serializations of the parallel events in FEL: ⟨A@3, D@3⟩ and ⟨D@3, A@3⟩. For the first of
  them we obtain the following sequence of rule applications: </p><ol>
      <li>SS' = r<sub>1</sub>(SS) = ⟨{inputBufferLength:2}, {D@3, A@6}⟩, assuming that invoking <i>PartArrival.recurrence()</i> returns 3 as the delay
      for the next arrival event.</li>
      <li>SS'' = r<sub>2</sub>(SS') = ⟨{inputBufferLength:1}, {A@6, D@5}⟩, assuming that invoking <i>WorkStation.processingTime()</i> returns 2 as the
      delay for the next departure event.</li>
    </ol><p>The second serialization yields</p><ol>
      <li>SS' = r<sub>2</sub>(SS) = ⟨{inputBufferLength:0}, {A@3, D@5}⟩, assuming that invoking <i>WorkStation.processingTime()</i> returns 2 as the
      delay for the next departure event .</li>
      <li>SS'' = r<sub>1</sub>(SS') = ⟨{inputBufferLength:1}, {D@5, A@6}⟩, assuming that invoking <i>PartArrival.recurrence()</i> returns 3 as the
      delay for the next arrival event.</li>
    </ol><p>For both serializations, the resulting simulation state SS'' is the same.</p></body>
</html>
