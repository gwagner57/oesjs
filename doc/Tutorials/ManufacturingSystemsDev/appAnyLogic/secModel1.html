<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <title>Implementing Design Model 1.1</title>
  </head>
  <body><p>The simplest approach to event-based simulation with AnyLogic is to use the <em>Event</em> element from the Agent Palette. However, the
  <em>Event</em> element does not support event types with properties. In example 1, this prevents part arrival and departure events having a
  reference to a particular workstation as the value of a reference property <i>workStation</i>, unlike what is specified by the design model shown in
  <a href="#figWS0PDM_2"></a>. We will therefore first show how to make a simplified simulation model with <code>PartArrival</code> and
  <code>PartDeparture</code> modeled as <em>Events</em> without a reference to a workstation, thus leaving the participating workstation implicit.
  </p><section><figure id="figWS0IDM_2"><figcaption style="text-align:center;">Information design model 1.1.</figcaption><img src="../img/WS0_IDM.svg"
  width="350" /></figure><h4>Implementing the object type <code>WorkStation</code></h4><p>The object type <code>WorkStation</code> defined in
  information design model 1.1 can be implemented in AnyLogic in the form of an Agent Type. </p><p>After selecting the <em>Agent</em> Palette,
  drag-and-drop the <em>Agent</em> element, choose the option "A single agent", keep the setting "I want to create a new agent type", call the
  resulting object type "WorkStation" and the resulting object "workStation1", and click Finish. Then double-click the <code>workStation1</code>
  object, such that the <code>WorkStation</code> Agent Type tab is opened for defining the attribute <code>inputBufferLength</code> by
  dragging-and-dropping the AnyLogic <em>Variable</em> element and setting its <em>Type</em> to "int".</p><p>Right-click on the
  <code>inputBufferLength</code> attribute and choose "Create chart" and then "Create time plot" for adding a variable monitor that will show a time
  series of the input buffer length values when the simulation is executed.</p><blockquote class="role-notice"><p>In AnyLogic, an object type is
  defined as an "Agent Type", and an object is defined as an "Agent". A property of an object type is defined as a "Variable" of the "Agent Type"
  concerned, if the property's value is changed by a state change statement of an event rule. Otherwise, if the property's value is not changed during
  a simulation run, it is defined as a "Parameter" of the "Agent Type" concerned.</p></blockquote></section><figure id="figWS0PDM_2"><figcaption
  style="text-align:center;">Process design model 1.1.</figcaption><img src="../img/WS0_PDM.svg" width="500" /></figure><section><h4>Implementing a
  simplified version of design model 1.1 using AnyLogic's <em>Event</em> element</h4><p>AnyLogic's <em>Event</em> element only supports simple event
  types without properties. Consequently, using it only allows implementing a simplified version of the design model without workstation
  references.</p><p>Drag-and-drop the <em>Event</em> element twice and call the resulting event types "PartArrival" and "PartDeparture". Go on with
  defining these event types in the following way:</p><ol>
      <li><p>Click on <code>PartArrival</code>, choose "Timeout" as <i>Trigger type</i>, set <em>Mode</em> to "Cyclic" for defining events of this
      type to be <em>exogenous</em> (i.e., recurring), with a "First occurrence time" of 1 and an expression <code>exponential(0.1666)</code> as the
      value of "Recurrence time", and add the following Java statements in the <em>Action</em> panel:</p><pre>workStation1.inputBufferLength++;
if (workStation1.inputBufferLength == 1) {
  PartDeparture.restart( triangular( 3, 8, 4));
}</pre></li>
      <li><p>Click on <code>PartDeparture</code>, choose "Timeout" as <i>Trigger type</i> and set <em>Mode</em> to "User control" for defining events
      of this type to be <em>endogenous</em> (i.e., caused by invoking AnyLogic's <code>restart</code> method), and add the following Java statements
      in the <em>Action</em> panel:</p><pre>workStation1.inputBufferLength--;
if (workStation1.inputBufferLength &gt; 0) {
  PartDeparture.restart( triangular( 3, 8, 4));
}</pre></li>
    </ol><p>After performing the above simulation definition steps, we see the defined elements in the Main Agent window, as shown in <a
  href="#figDesM1_AnyLogic"></a>.</p><figure id="figDesM1_AnyLogic"><figcaption style="text-align:center;">The Main Agent window showing the first
  AnyLogic implementation of design model 1.1.</figcaption><img src="../img/WS0_AnyLogic.png" width="500" /></figure><blockquote
  class="role-notice"><p>An event type without properties can be implemented as an "Event", distinguishing the following two cases:</p><ol>
      <li><p>For <em>exogenous</em> event types, set <i>Trigger type</i> to "Timeout" and <em>Mode</em> to "Cyclic". Then, define the "First
      occurrence time" of an event of this type, and set the "Recurrence time" to a fixed value or to a probability distribution function such as
      <code>exponential(0.5)</code>.</p></li>
      <li>For other (caused) event types, set <i>Trigger type</i> to "Timeout" and <em>Mode</em> to "User control", which means that events of such a
      type need to be scheduled with AnyLogic's predefined <code>restart</code> <em>Event</em> method.</li>
    </ol></blockquote></section><section><h4>Implementing design model 1.1 using AnyLogic's <em>Dynamic Event</em> element</h4><p>AnyLogic's
  <em>Dynamic Event</em> element allows implementing an event type E with properties by implementing these properties in the form of event creation
  parameters. A Dynamic Event of type E has to be scheduled with a special method, which is automatically generated by AnyLogic with the name
  "create_E", having two fixed parameters, the occurrence time and the time unit, followed by parameters corresponding to the properties of the event
  type. For instance, the following method invocation expression includes an occurrence time (obtained by invoking the <code>exponential</code>
  distribution function) and the time unit <code>MINUTE</code>, followed by a reference to a particular <code>WorkStation</code> as arguments for
  invoking the <code>create_PartArrival</code> method:</p><pre>create_PartArrival( exponential(0.5), MINUTE, workStation1);</pre><p>As in the
  implementation with <em>Event</em> elements described above, an object type <code>WorkStation</code> with an attribute
  <code>inputBufferLength</code> has first to be defined and instantiated with an object <code>workStation1</code>. Also, as before, a variable
  monitor for <code>inputBufferLength</code> is needed for obtaining a simulation output.</p><p>Then drag-and-drop the <em>Dynamic Event</em> element
  twice to the <em>Main</em> panel and call the resulting event types "PartArrival" and "PartDeparture". Go on with defining these event types in the
  following way:</p><ol>
      <li><p>Click on <code>PartArrival</code>, define under "Arguments" the (event creation) parameter <code>workStation</code> of type
      <code>WorkStation</code>, and add the following Java statements using this parameter in the <em>Action</em> text area:</p><pre>workStation.inputBufferLength++;
if (workStation.inputBufferLength == 1) {
  create_PartDeparture( triangular(3,8,4), MINUTE, workStation);
}
// create next arrival event
create_PartArrival( exponential(0.1666), MINUTE, workStation);</pre><p>Notice that since a <em>Dynamic Event</em> does not allow a direct recurrence
      definition, it is necessary to schedule the next event in the <em>Action</em> of that event type for keeping the flow of exogenous events going
      on.</p></li>
      <li><p>Click on <code>PartDeparture</code>, define under "Arguments" the property <code>workStation</code> of type <code>WorkStation</code>, and
      add the following Java statements in the <em>Action</em> panel:</p><pre>workStation.inputBufferLength--;
if (workStation.inputBufferLength &gt; 0) {
  create_PartDeparture( triangular(3,8,4), MINUTE, workStation);
}</pre></li>
    </ol><p>Finally, for any exogenous event type, like <code>PartArrival</code>, an initial event has to be scheduled in the "On startup" action of
  <code>Main</code>:</p><pre>// create first arrival event
create_PartArrival( 1, MINUTE, workStation1);</pre><p>Notice that the initial <code>PartArrival</code> event refers to <code>workStation1</code>,
  which has been created as an object of type <code>WorkStation</code>. For obtaining a simulation of two workstations operating in parallel, we would
  have to create another <code>WorkStation</code> object, say <code>workStation2</code>, and also schedule a part arrival event for it, as in</p><pre>// create first arrival event at workStation1
create_PartArrival( 1, MINUTE, workStation1);
// create first arrival event at workStation2
create_PartArrival( 1, MINUTE, workStation2);</pre><blockquote class="role-notice"><p>Normally, an event of a certain type has one or more objects of
  certain types as its participants playing certain roles in the event. For instance, a part arrival event has two participants: a <em>part</em>
  object and a <em>workstation</em> object. The participation roles of an event type can be represented in the form of reference properties of the
  event type. For instance, the event type <code>PartArrival</code> would have the reference properties <code>part</code> (with range
  <code>Part</code>) and <code>workstation</code> (with range <code>Workstation</code>).</p><p>AnyLogic's <em>Dynamic Event</em> element does not
  allow defining properties for a certain type of Dynamic Event, but event properties can be implemented with the help of event creation parameters,
  called "Arguments" in the Dynamic Event definition panel, such that certain values for these event creation parameters (representing properties) can
  be provided when creating an event. For instance, the Dynamic Event <code>PartArrival</code> would have the event creation parameters
  <code>part</code> (with range <code>Part</code>) and <code>workstation</code> (with range
  <code>Workstation</code>).</p></blockquote></section></body>
</html>
