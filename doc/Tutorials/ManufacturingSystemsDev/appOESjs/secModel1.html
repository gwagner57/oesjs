<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />
    <title>Implementing Design Model 1.1</title>
  </head>
  <body><p>Design model 1.1, presented in <a href="../chSingleWS/subsecDesModel0.html#secDesM1.1"></a>, defines one object type,
  <code>WorkStation</code>, and two event types, <code>PartArrival</code> and <code>PartDeparture</code>. It can be run as an OESjs online simulation
  at <a href="https://sim4edu.com/sims/101">https://sim4edu.com/sims/101</a>.</p><p>An OESjs simulation consists of a <kbd>simulation.js</kbd> file
  that essentially defines simulation parameters and an initial state for a model, which is coded in a number of object class files and event class
  files, such as <kbd>WorkStation.js</kbd>, <kbd>PartArrival.js</kbd> and <kbd>PartDeparture.js</kbd>. </p><section><h4>Implementing object and event
  types</h4><p>The information design model 1.1 shown below defines one object type and two event types. </p><figure id="figWS0IDM_1"><figcaption
  style="text-align:center;">Information design model 1.1.</figcaption><img src="../img/WS0_IDM.svg" width="350" /></figure><p>These three type
  definitions are implemented in the form of three corresponding classes in OESjs. An object type like <code>WorkStation</code> is implemented as a
  class that extends the predefined class <code>oBJECT</code> in a class definition file like <kbd>WorkStation.js</kbd>, as shown in the following
  program listing:</p><pre class="role-listing-1">var <b>WorkStation</b> = new cLASS({
  Name: "WorkStation",
  supertypeName: "oBJECT",
  properties: {
    "<b>inputBufferLength</b>": { range: "NonNegativeInteger"},
    "currentProcessingTime": {range: "Decimal"}
  }
});
WorkStation.<b>processingTime</b> = function () {
  return rand.exponential(1/6);
};</pre><p>In addition to the attribute <code>inputBufferLength</code>, as specified in the class diagram of <a
  href="../chSingleWS/subsecDesModel0.html#figWS0IDM"></a>, the object type <code>WorkStation</code> also defines an attribute
  <code>currentProcessingTime</code>, which is used for the purpose of computing the utilization statistics.</p><p>The two event types,
  <code>PartArrival</code> and <code>PartDeparture</code>, are implemented as classes that extend the predefined class <code>eVENT</code> in
  corresponding class definition files <kbd>PartArrival.js</kbd> and <kbd>PartDeparture.js</kbd>, as shown in the following program listings:</p><pre
  class="role-listing-1">var <b>PartArrival</b> = new cLASS({
  Name: "PartArrival",
  supertypeName: "eVENT",
  properties: {
    "<b>workStation</b>": {range: "WorkStation"}
  },
  methods: {
    "<b>onEvent</b>": function () {...}
  }
});
PartArrival.<b>recurrence</b> = function () {
  return rand.triangular( 3, 8, 4);  // min,max,mode
};</pre><pre class="role-listing-1">var <b>PartDeparture</b> = new cLASS({
  Name: "PartDeparture",
  supertypeName: "eVENT",
  properties: {
    "<b>workStation</b>": {range: "WorkStation"}
  },
  methods: {
    "<b>onEvent</b>": function () {...}
  }
});</pre><p>Notice that in order to show up in the simulation log, object types and event types, and their properties, need to have a
  <code>label</code> attribute defined for them.</p></section><section><h4>Implementing event rules</h4><p>In OESjs, any event class must have an
  "onEvent" method, which implements the <em>event routine</em> of the event type's event rule. The event rules of the event types
  <code>PartArrival</code> and <code>PartDeparture</code> are defined in the following DPMN diagram .</p><figure id="figWS0PDM_1"><figcaption
  style="text-align:center;">Process design model 1.1.</figcaption><img src="../img/WS0_PDM.svg" width="500" /></figure><p>In the case of the event
  type <code>PartArrival</code>, the code of its event routine is shown in the following program listing:</p><pre class="role-listing-1">"<b>onEvent</b>": function () {
  var events=[], ws = this.workStation;
  // add part to buffer
  ws.inputBufferLength++;
  // update statistics
  sim.stat.arrivedParts++;
  // if the work station is available
  if (ws.inputBufferLength === 1) {
    // compute random processing time
    ws.currentProcessingTime = WorkStation.processingTime();
    // schedule the part's departure event
    events.push( new <b>PartDeparture</b>({
      delay: ws.currentProcessingTime,
      workStation: ws
    }));
  }
  return events;
}</pre><p>In the case of the event type <code>PartDeparture</code>, defined in <kbd>PartDeparture.js</kbd>, the code of its event routine is shown in
  the following program listing:</p><pre class="role-listing-1">"<b>onEvent</b>": function () {
  var events=[], ws = this.workStation;
  // remove part from buffer
  ws.inputBufferLength--;
  // update statistics
  sim.stat.departedParts++;
  sim.stat.totalProcessingTime += ws.currentProcessingTime;
  // if there are still parts waiting
  if (ws.inputBufferLength &gt; 0) {
    // compute random processing time
    ws.currentProcessingTime = WorkStation.processingTime();
    // schedule the next departure event
    events.push( new PartDeparture({
      delay: ws.currentProcessingTime,
      workStation: ws
    }));
  }
  return events;
}</pre></section><section><h4>Defining the initial state</h4><p>The initial state is defined in the <kbd>simulation.js</kbd> file like so:</p><pre
  class="role-listing-1">sim.scenario.initialState.objects = {
  "1": {typeName: "WorkStation", name:"workStation1", 
    inputBufferLength: 3}
};
sim.scenario.initialState.events = [
  {typeName: "PartArrival", occTime: 1, workStation: 1},
  {typeName: "PartDeparture", occTime: 1, workStation: 1}
];</pre><p>Notice that the initial <code>PartArrival</code> and <code>PartDeparture</code> events refer to the <code>workStation</code> with ID=1,
  which has been created as an object of type <code>WorkStation</code> with an <code>inputBufferLength</code> of 3. For obtaining a simulation of two
  workstations operating in parallel, we would have to create another <code>WorkStation</code> object, say with ID=2, and also schedule initial events
  for it, as in</p><pre>sim.scenario.initialState.objects = {
  "1": {typeName: "WorkStation", name:"workStation1", 
    inputBufferLength: 3},
  "2": {typeName: "WorkStation", name:"workStation2", 
    inputBufferLength: 3}
};
sim.scenario.initialState.events = [
  {typeName: "PartArrival", occTime: 1, workStation: 1},
  {typeName: "PartDeparture", occTime: 1, workStation: 1},
  {typeName: "PartArrival", occTime: 1, workStation: 2},
  {typeName: "PartDeparture", occTime: 1, workStation: 2}
];</pre></section></body>
</html>
