<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Resource-Constrained Activities in Simulation Design Models</title>
  </head>

  <body><p>In simulation design models, resource-constrained activities can be
  modeled in two ways: </p><ol>
      <li>either abstracting away from the structure of resource object types
      and individual resource objects, and only representing a resource object
      type in the form of a named resource pool with a quantity (or
      <em>counter</em>) attribute holding the number of available resources,
      or</li>

      <li>explicitly representing resource object types and individual
      resource objects of that type as members of a collection representing a
      resource pool.</li>
    </ol><p>While the first approach is simpler, the second approach allows
  modeling various kinds of non-availability of specific resources (e.g., due
  to failures or due to not being in the shift plan).</p><p>For any resource
  object type <i>Res</i>, the three operations described in the following
  table <a href="#tblResManOps"></a> are needed.</p><table border="1"
      id="tblResManOps">
      <thead>
        <tr>
          <th>Resource management operation</th>

          <th>General meaning</th>

          <th>Resource counter approach</th>

          <th>Resource pool approach</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <th><i>isResAvailable</i></th>

          <td>test if a resource of type <i>Res</i> is available and return
          <i>true</i> or <i>false</i></td>

          <td>test if the corresponding resource counter attribute has a value
          that is greater than 0</td>

          <td>test if the number of available resource objects in the resource
          pool is greater than 0</td>
        </tr>

        <tr>
          <th><i>allocateRes</i></th>

          <td>allocate a resource object of type <i>Res</i></td>

          <td>decrement resource counter attribute</td>

          <td>select (and return) a resource object from the set of available
          resource objects in the resource pool (using an allocation policy)
          and designate it as BUSY</td>
        </tr>

        <tr>
          <th><i>releaseRes</i></th>

          <td>de-allocate a resource object of type <i>Res</i></td>

          <td>increment resource counter attribute</td>

          <td>take a resource object of type <i>Res</i> as argument and
          designate it as AVAILABLE</td>
        </tr>
      </tbody>
    </table><p>In both approaches, it is natural to add these operations to
  the object type representing the process owner of the activities concerned,
  as in the models shown in <a href="#figMedDep1IDM1"></a> and <a
  href="#figMedDep1IDM2"></a>.</p><p>In the first approach, for each resource
  object type in the conceptual model, a resource counter attribute is added
  to the object type representing the process owner and the conceptual model's
  resource object types are dropped.</p><p>In the second approach, the
  conceptual model's resource object types are elaborated by adding an
  enumeration attribute <em>status</em> holding a resource status value such
  as AVAILABLE or BUSY. For each resource object type, a collection-valued
  property (such as <em>rooms</em> or <em>doctors</em>) representing a
  resource pool is added to the object type representing the process
  owner.</p><section><h4>A simple model with resource counters</h4><p>Using
  the conceptual information model shown in <a
  href="secCM.html#figResRoles"></a> as a starting point, we first rename all
  classes and properties according to OO naming conventions and replace each
  of the two (conceptual) operations <em>allocate a room</em> and <em>allocate
  a doctor</em> with a triple of
  <i>isAvailable</i>/<i>allocate</i>/<i>release</i> operations for the two
  resource object classes <em>Room</em> and <em>Doctor</em> in the
  <em>MedicalDepartment</em> class, where we also add the counter attributes
  <em>nmrOfRooms</em> and <em>nmrOfDoctors</em>. Then, the two resource object
  classes <em>Room</em> and <em>Doctor</em> are dropped. The result of this
  elaboration is the information design model shown in <a
  href="#figMedDep1IDM1"></a>.</p><figure id="figMedDep1IDM1"><figcaption>An
  information model for the simplified design with the resource counters
  nmrOfRooms and nmrOfDoctors.</figcaption><div><img
  src="../img/MedExam1_IDM1.svg" width="500" /></div></figure><p>Using the
  conceptual process model shown in <a href="secCM.html#figMedDepCPM1"></a> as
  a starting point and based on the type definitions of the information design
  model of <a href="#figMedDep1IDM1"></a>, we get the following process
  design.</p><figure id="figMedDep1PDM1"><figcaption>A process design model
  based on the information design model of <a
  href="#figMedDep1IDM1"></a>.</figcaption><div><img
  src="../img/MedDep1_PDM1.png" width="500" /></div></figure><p>This process
  model defines the following two event rules. </p><table border="1">
      <thead>
        <tr>
          <th>ON pa: PatientArrival</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>md : MedicalDepartment <br />resAllocated : Boolean<br />md :=
          pa.medicalDepartment</td>
        </tr>

        <tr>
          <td>IF md.isRoomAvailable() AND md.isDoctorAvailable()<br />THEN
          md.allocateRoom(); md.allocateDoctor(); resAllocated :=
          true<br />ELSE md.waitingPatients.push( pa.patient); resAllocated :=
          false</td>
        </tr>

        <tr>
          <td>IF resAllocated SCHEDULE Examination( patient:=pa.patient,
          medicalDepartment:=md)</td>
        </tr>
      </tbody>
    </table><table border="1">
      <thead>
        <tr>
          <th>ON ex: Examination</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>md : MedicalDepartment <br />anotherPatientFetched :
          Boolean<br />p: Patient<br />md := ex.medicalDepartment</td>
        </tr>

        <tr>
          <td>IF md.waitingPatients.length = 0<br />THEN md.releaseRoom();
          md.releaseDoctor(); anotherPatientFetched := false<br />ELSE p :=
          md.waitingPatients.pop(); anotherPatientFetched := true</td>
        </tr>

        <tr>
          <td>IF anotherPatientFetched SCHEDULE Examination( patient:=p,
          medicalDepartment:=md)</td>
        </tr>
      </tbody>
    </table><p>Notice that the event scheduling arrows of <a
  href="#figMedDep1PDM1"></a>, and also the SCHEDULE statements of the
  corresponding event rule tables, do not contain assignments of the duration
  of activities, since it is assumed that, by default, whenever an activity
  type has an operation <em>duration()</em>, the duration of activities of
  this type are assigned by invoking this
  operation.</p></section><section><h4>A general model with resource objects
  as members of resource pools</h4><p>In a more general approach, instead of
  using resource counter attributes, explicitly modeling resource object
  classes (like <em>Room</em> and <em>Doctor</em>) allows representing
  resource roles (stereotyped with «res») and resource pools (stereotyped with
  «pool») in the form of collections (like <em>md.rooms</em> and
  <em>md.doctors</em>) and modeling various forms of non-availability of
  resources (such as machines being defective or humans not being in the shift
  plan) with the help of corresponding resource status values (such as
  OUT_OF_ORDER). The result of this elaboration is the information design
  model shown in <a href="#figMedDep1IDM2"></a>.</p><figure
  id="figMedDep1IDM2"><figcaption>An <abbr title="Object Event">OE</abbr> class model with resource object
  types for modeling resource roles and pools.</figcaption><div><img
  src="../img/MedExam1_IDM2.svg" /></div></figure><p>For an <abbr title="Object Event">OE</abbr> class model,
  like the one shown in <a href="#figMedDep1IDM2"></a>, the following
  completeness constraint must hold: when an object type <i>O</i> (like
  <i>Doctor</i>) participates in a «res» association (a resource role
  association) with an activity type <i>A</i> (like <i>Examination</i>), the
  process owner object type of <i>A</i> (<i>MedicalDepartment</i>) must have a
  «pool» association with <i>O</i>.</p><figure
  id="figMedDep1PDM2"><figcaption>A process design model based on the
  information design model of <a
  href="#figMedDep1IDM2"></a>.</figcaption><div><img
  src="../img/MedDep1_PDM2.png" /></div></figure></section><section><h4>Extending
  OE Class Diagrams by adding a «resource type» category</h4><p>The
  information design model of <a href="#figMedDep1IDM2"></a> contains two
  object types, <i>Room</i> and <i>Doctor</i>, which are the range of resource
  role and resource pool properties (association ends stereotyped «res» and
  «pool»). Such object types can be categorized as «resource type» with the
  implied meaning that they inherit a resource status attribute from a
  pre-defined class <code>Resource</code>, as shown in <a
  href="#figMedDep1IDM3"></a>.</p><figure><figcaption>Any resource type
  <i>R</i> extends the pre-defined object type
  <code>Resource</code></figcaption><div><img
  src="../img/Resource-Object.svg" /></div></figure><p>The introduction of
  resource types to OEM class models allows simplifying models by dropping the
  following modeling items from <abbr title="Object Event">OE</abbr> class models, making them part of the
  implicit semantics:</p><ol>
      <li>the <i>status</i> attributes of object types representing resource
      types, which are implicitly inherited;</li>

      <li>the pre-defined enumeration <i>ResourceStatusEL</i>;</li>

      <li>the resource management operations <i>isAvailable</i>,
      <i>allocate</i> and <i>release</i>, which are implicitly inherited by
      any resource type; and</li>

      <li>the planned activity queues may possibly be implicitly represented
      for any resource-constrained activity type in the form of ordered
      multi-valued reference properties of its process owner object type.</li>
    </ol> This is shown in <a href="#figMedDep1IDM3"></a>.<figure
  id="figMedDep1IDM3"><figcaption>A simplified version of the model of <a
  href="#figMedDep1IDM2"></a> </figcaption><div><img
  src="../img/MedExam1-DM3.svg" /></div></figure></section><section
  id="RevisitingManufacturingWS"><h4>Revisiting the manufacturing workstation
  example</h4><p>A manufacturing workstation, or a "server" in the terminology
  of Operation Research, represents a resource for the processing activities
  performed at/by it. This was left implicit in the <abbr title="Object Event">OE</abbr> class model shown on
  the right-hand side of <a
  href="../chSimpleActivities/secDesM.html#figWorkstationIDM"></a>. Using the
  new modeling elements (resource types, resource roles and resource pools),
  the processing activities of a workstation can be explicitly modeled as
  resource-constrained activities, leading to the <abbr title="Object Event">OE</abbr> class model shown in
  <a href="#figWS3IDM3"></a> and to a more high-level and more readable
  process model compared to the process model of <a
  href="../chSimpleActivities/secDesM.html#figWorkstationPDM"></a>.
  </p><figure id="figWS3IDM3"><figcaption>An OE Class Diagram modeling a
  single workstation system with resource-constrained processing activities
  </figcaption><div><img
  src="../img/WS3_IDM3.svg" /></div></figure></section><section><h4>Decoupling
  the allocation of multiple resources</h4><p>In a simplified simulation
  design for the extended scenario (with patients and nurses first walking to
  examination rooms before doctors are allocated for starting the
  examinations) described by the conceptual models of <a
  href="secCM.html#figMedDepCIM4"></a> and <a
  href="secCM.html#figMedDepCPM4"></a>, we do not consider the walks of
  doctors, but only the walks of nurses and patients. For simplicity, we drop
  the superclass <i>people</i> and associate the activity type
  <i>WalkToRoom</i> with the <i>Patient</i> and Nurse classes. The result of
  this elaboration is the information design model shown in <a
  href="#figMedDep2IDM2"></a>.</p><figure id="figMedDep2IDM2"><figcaption>An
  information design model for decoupling the allocation of rooms and
  doctors.</figcaption><div><img
  src="../img/MedExam2_IDM2.svg" /></div></figure><figure
  id="figMedDep2PDM1"><figcaption>A process design model based on the
  information design model of <a
  href="#figMedDep2IDM2"></a>.</figcaption><div><img
  src="../img/MedDep2_PDM1.png" /></div></figure><p>This process design model
  defines three event rules. Notice that the <i>Examination</i> event rule
  either re-allocates the doctor to the next planned examination and schedules
  it, if there is one, or it releases the doctor and re-allocates the room to
  the next planned walk-to-room and schedules it, if there is
  one.</p></section><section><h4>Centralizing the re-allocation of
  resources</h4><p>As shown before, in the conceptual process models of <a
  href="secCM.html#figMedDepCPM3"></a> and <a
  href="secCM.html#figMedDepCPM4"></a>, the re-allocation of resources can be
  centralized with the help of resource release request events and the process
  owner and the involved performers can be displayed by using a Pool that is
  partitioned into Lanes for the involved activity performers, resulting in
  the model shown in <a href="#figMedDep4PDM1"></a>.</p><figure
  id="figMedDep4PDM1"><figcaption>Representing the process owner as a Pool and
  activity performers as Lanes in a process design
  model.</figcaption><div><img
  src="../img/MedDep4_PDM1.png" /></div></figure></section></body>
</html>
