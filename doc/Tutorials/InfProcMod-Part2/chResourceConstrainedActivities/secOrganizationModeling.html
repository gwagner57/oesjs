<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Organization Modeling Concepts</title>
  </head>

  <body><p>The activities of a business process are typically performed by
  human resources holding certain <em>organizational positions</em> that
  authorize them to play certain <em>organizational roles</em> within an
  organization or organizational unit. It is, therefore, natural that
  <em>Activity-Based DES</em> modeling includes organization modeling
  concepts. In particular, the concept of <em>resource roles</em> and the
  related concepts of <em>organizational roles</em> and <em>organizational
  positions</em> are paramount to Activity-Based DES modeling.</p><p>Many
  <em>resource modeling</em> concepts, such as <em>resource roles</em>
  (including <em>performer roles</em>) and <em>resource pools</em>, can be
  captured in information models in the form of categorized ("stereotyped")
  UML class modeling elements such as «rr» for <em>resource role</em>
  association ends, «pr» for <em>performer role</em> association ends, and
  «rp» for <em>resource pool</em> association ends, leading to an extension of
  UML Class Diagrams, called <abbr title="Object Event">OE</abbr> Class
  Diagrams, such as the following one: </p><figure><div
  style="display:flex; justify-content: space-between; "><img
  alt="OE class model of medical departments" src="../img/MedDep_CIM2a.svg"
  width="500" /></div></figure><p>This conceptual <abbr
  title="Object Event">OE</abbr> class model describes two activity types,
  "walks to rooms" and "examinations", each one associated with</p><ol>
      <li>the object type "medical departments", providing the (business)
      <em>process owner</em>; implying that each specific walk-to-room or
      examination activity is performed by human resources of, and within a
      business process owned by, that department;</li>

      <li>a <em>performer</em> object type ("nurses" or "doctors") via a
      <em>performer role</em> («pr») association end, providing the activity
      performer (as a special type of resource);</li>

      <li>another <em>resource</em> object type via a <em>resource role</em>
      («rr») association end, providing another resource of each activity (a
      "room").</li>
    </ol><p>In addition, the model describes three types of <em>resource
  pools</em> by means of «rp» association ends, such that each resource pool
  is associated with a <em>process owner</em> maintaining the pool and
  providing the resources corresponding to one of the three resource
  roles.</p><p>Notice that in a <em>conceptual</em> <abbr
  title="Object Event">OE</abbr> class model, each activity type should have a
  <em>performer role</em> («pr») association, while in an <abbr
  title="Object Event">OE</abbr> class <em>design</em> model (being part of a
  simulation design model), it is an option to abstract away from the
  distinction of performers among the resources of an activity and simply
  consider all its resources (including its performer) as plain resources.
  This is common in management science and Operations Research, where the main
  interest is in obtaining general performance statistics for resources, such
  as their utilization, no matter if they are active resources (performers) or
  not.</p><p>We show how further organization modeling concepts can be
  introduced to <abbr
  title="Object Event Modeling and Simulation">OEM&amp;S</abbr>.</p><h4>Refining
  Object Types to Resource (Role) Types and Performer (Role)
  Types</h4><p>While a <em>resource role</em> of an activity type denotes an
  association end, corresponding to a reference property of the class
  implementing the activity type, a <em>resource</em> (or <em>performer</em>)
  <em>role type</em> denotes an object type that is the range of a
  <em><em>resource</em> (or <em>performer</em>) role</em>. For simplicity, we
  say "resource type" (or "performer type") instead of "resource role type"
  (or "performer role type").</p><p>For all «object type» classes of an <abbr
  title="Object Event">OE</abbr> class model that are the range of a
  <em>performer role</em> or a <em>resource role</em>, it may be an option to
  replace their «object type» category with a «performer type» or «resource
  type» category, as shown for the object types "nurses", "doctors" and
  "rooms" in the following refined model:</p><figure><div><img alt="???"
  src="../img/MedDep_CIM2b.svg" width="500" /></div></figure><p>Notice that
  typically each resource type is the range of one resource role and
  represents a resource pool for it. A resource type may be the target class
  of more than one resource role association end and its population may be
  segmented into several resource pools such that at least one pool is
  assigned to each resource role. </p><p>However, the replacement of the
  «object type» category of a class with a (performer or resource) role type
  category is only an option, if all instances of the class are playing that
  role. In the above example, if there are doctors who do not perform
  examinations (but only surgical operations), then the object type "doctors"
  cannot be turned into a performer type "doctors" for examinations, but would
  have to be partitioned into two subtypes, "surgeons" and "examination
  doctors", such that the latter would be a performer type for
  examinations.</p><p>In <abbr
  title="Object Event Modeling and Simulation">OEM&amp;S</abbr>, a human
  performer type is implicitly a subtype of the pre-defined object type
  <i>Person</i>. This makes it possible that a doctor, as a person that
  performs examinations, may also be a patient in an examination (of course,
  with the constraint that a person cannot be the same examination's doctor
  and patient).</p><h4>Refining Performer Types to Organizational
  Positions</h4><p>An <em>organizational position</em> is defined by a name
  and a set of human performer types representing the roles of the position
  holders. For instance, in a medical department, the organizational position
  <i>Nurse</i> may be defined by the set of performer types <i>Guide</i> and
  <i>ExaminationAssistant</i>, where a <i>Guide</i> is required for guiding a
  patient to an examination room and an <i>ExaminationAssistant</i> is
  required for assisting a doctor in a medical
  examination.</p><p>Computationally, an <em>organizational position</em> is
  an object type that is co-extensional with all of its performer types,
  implying that its instances (position holders) are human resources that can
  be allocated for performing any activity associated with any of these
  performer types. This implies that any instance (or "holder") of an
  organizational position is a person and that any organizational position is
  a subtype of the kind <i>Person</i>. </p><p>For instance, the organizational
  position <i>Nurse</i> is co-extensional with the performer types
  <i>Guide</i> and <i>ExaminationAssistant</i>, with the meaning that nurses
  may play the role of guides or the role of examination assistants, and any
  guide, as well as any examination assistant, is a nurse.</p><p>In simple
  cases, an organizational position is defined by just one performer type for
  one particular performer role. </p><p>In <abbr
  title="Object Event Modeling and Simulation">OEM&amp;S</abbr>, for each
  organizational position (like <i>Clerk</i>) consisting of the performer
  roles p<sub>1</sub>,...,p<sub>n</sub>, there is a default resource pool, or
  better: performer pool, (like <i>clerks</i>) that includes all employees
  holding that position. This pool will be used for allocating the performers
  of activities with a performer role p<sub>i</sub>.</p><p>These
  considerations lead to the following refined version of the medical
  department model shown in the diagram above:</p><figure><div><img alt="???"
  src="../img/MedDep-Positions_CIM.svg" width="500" /></div></figure><p>Notice
  that in this refined model, we have also categorized the object type
  "patients" as «person role type» and the object type "medical departments"
  as «organizational unit type». In <abbr
  title="Object Event Modeling and Simulation">OEM&amp;S</abbr>, a person role
  type, like <i>Patient</i>, is implicitly a subtype of the pre-defined object
  type <i>Person</i>, allowing a doctor to be a patient. An organizational
  unit, as an instance of an organizational unit type, may own one or more
  business processes.</p><h4>Assigning business process types and business
  goals to organizational agents</h4><p>An organizational agent, which is
  either an organization, an organizational unit or an organizational role
  player, can own <em>business process types</em> and can have <em>business
  goals</em>, which allow improving business processes by refactoring business
  process models, including resource allocation policies, and by changing the
  populations of resource pools, in such a way that the goals of the process
  owner (and those of its super-agents) are satisfied.</p><p>Consider the
  following example: </p><figure class="role-example"><figcaption>Example from
  <a href="https://www.signavio.com/post/business-goals-archimate/">Modeling
  Business Goals with ArchiMate</a>.</figcaption><p>A business company has a
  Marketing department owning a Lead Generation process, a Business
  Development unit owning a Lead Qualification process, a Sales department
  owning a Sales process and a Customer Success department owning a Customer
  Success process. The company has the overall top-level goals "increase
  profit", "increase customer satisfaction", and "increase employee
  motivation". </p><p>Without the overall company goals in mind, each
  organizational unit is prone to optimize their subprocess without keeping an
  eye on the long-term implications:</p><ul>
      <li>Marketing maximizes lead output and ignores that most leads it
      generates won’t be qualifiable.</li>

      <li>In its struggle to identify high-quality leads in a mass of
      low-quality lead data, Business development is overly eager to interpret
      leads as ‘qualified’, even if it is clear that many leads they forward
      to Sales can’t be turned into long-term customers.</li>

      <li>Sales tries to close as many deals as possible, without considering
      churn and missed up-sell opportunities if the product can’t deliver on
      the promises made.</li>

      <li>Customer success can’t do more than fight the fires of customer
      dissatisfaction.</li>
    </ul><p>This behavior is often encouraged by middle management that sets
  simple and easily quantifiable short-term performance goals for the team
  that reports to them, without keeping the big picture in
  mind.</p></figure><p>There are different ways of expressing business goals.
  In simulation modeling, we need operational forms of business goals, that
  is, expressions in a formal language. We consider three forms of business
  goals: </p><ol>
      <li><em>Definite goals</em> (like "raise the overall revenue by at least
      25%") are defined by a logical condition (like relativeRevenueRise &gt;=
      0.25) that can be checked by a simulator.</li>

      <li><em>Variable change goals</em> are defined by a combination of one
      of the verbs "increase" or "decrease" with the name of a variable (like
      "profit", "customer satisfaction" or "costs").</li>

      <li><em>Optimization goals</em> are (more ambitious) special variable
      change goals defined by a combination of one of the verbs "maximize" or
      "minimize" with the name of a variable (like "profit", "customer
      satisfaction" or "costs").</li>
    </ol><p></p></body>
</html>
