<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Conceptual Modeling of Resource-Constrained Activities</title>
  </head>

  <body>Modeling resource-constrained activities has been a major issue in the
  field of <em>Discrete Event Simulation (DES)</em> since its inception in the
  nineteen-sixties, while it has been neglected and is still considered an
  advanced topic in the field of <em>Business Process Modeling (BPM)</em>. The
  concept of resource-constrained activities is at the center of both DES and
  BPM. But both fields have developed different, and even incompatible,
  concepts of business process simulation.<p>In the <abbr
  title="Discrete Event Simulation">DES</abbr> paradigm of <em>Processing
  Networks</em>, Gordon (1961) has introduced the resource management
  operations <em>Seize</em> and <em>Release</em> in the simulation language
  GPSS for allocating and de-allocating (releasing) resources. Thus, GPSS has
  established a standard modeling pattern for resource-constrained activities,
  which has become popular under the name of
  <em>Seize</em>-<em>Delay</em>-<em>Release</em> indicating that for
  simulating a resource-constrained activity, its resources are first
  allocated ("seized"), and then, after some delay representing the duration
  of the simulated activity, they are de-allocated ("released").</p><section
  id="secResPools"><h4>Resource roles, process owners and resource
  pools</h4><p>As an illustrative example, we consider a hospital consisting
  of medical departments where patients arrive for getting a medical
  examination performed by a doctor. A medical examination, as an activity,
  has three participants: a patient, a medical department, and a doctor, but
  only one of them plays a resource role: doctors. This can be indicated in an
  <abbr title="Object Event">OE</abbr> Class Diagram by using the stereotype
  «resource role» for categorizing the association ends that represent
  resource roles, as shown in <a href="#figResRoles"></a>.</p><figure
  id="figResRoles"><figcaption>A conceptual information model of the activity
  type "examinations" with resource roles.</figcaption><div><img alt="???"
  src="../img/MedDep_CIM1.svg" width="500" /></div></figure><p>Notice that
  both the event type <em>patient arrivals</em> and the activity type
  <em>examinations</em> have a (mandatory functional) reference property
  <em>process owner</em>. This implies that both patient arrival events and
  examination activities happen at a specific medical department, which is
  their process owner in the sense that it owns the process types composed of
  them. A process owner is called "Participant" in BPMN (in the sense of a
  collaboration participant) and visually rendered in the form of a container
  rectangle called "Pool".</p><p>In <a href="#figResRoles"></a>, the resource
  role of doctors corresponds to the <em>performer</em> role. In BPMN,
  <em>Performer</em> is considered to be a special type of resource role.
  According to (BPMN 2011), a performer can be "a specific individual, a
  group, an organization role or position, or an organization".<span
  class="role-footnote">See Section 10.2.2 in the BPMN 2.0 specification. This
  enumeration should be extended by adding artificial agents, such as robots,
  embedded systems and software systems.</span>In BPMN, <em>Performer</em> is
  specialized into the <em>HumanPerformer</em> of an activity, which is, in
  turn, specialized into <em>PotentialOwner</em> denoting the "persons who can
  claim and work" on an activity of a given type. "A potential owner becomes
  the actual owner [...] by explicitly claiming" an activity. Allocating a
  human resource to an activity by leaving the choice to those humans that
  play a suitable resource role is characteristic for workflow management
  systems, while in traditional DES approaches to resource handling, as in
  Arena, Simio and AnyLogic, (human) resources are assigned to a task (as its
  performer) based on certain policies.</p><p>Thus, the term "performer"
  subsumes several types of performers. We will, by default, use it in the
  sense of a human performer.</p><p>One of the main reasons for considering
  certain objects as resources is the need to collect <em>utilization
  statistics</em> (either in an operational information system, like a
  workflow management system, or in a simulation model) by recording the use
  of resources over time (their <em>utilization</em>) per activity type. By
  designating resource roles in information models, these models provide the
  information needed in simulations and information systems for automatically
  collecting utilization statistics.</p><p>In the hospital example, a medical
  department, as the process owner, is the organizational unit that is
  responsible for reacting to certain events (here: patient arrivals) and
  managing the performance of certain processes and activities (here: medical
  examinations), including the allocation of resources to these processes and
  activities. For being able to allocate resources to activities, a process
  owner needs to manage <em>resource pools</em>, normally one for each
  resource role of each type of activity (if pools are not shared among
  resource roles). A resource pool is a collection of resource objects of a
  certain type. For instance, the three X-ray rooms of a diagnostic imaging
  department form a resource pool of that department.</p><p>Resource pools can
  be modeled in an OE Class Diagram by means of special associations between
  object classes representing process owners (like <em>medical
  departments</em>) and resource classes (like <em>doctors</em>), where the
  association ends, corresponding to collection-valued properties representing
  resource pools, are stereotyped with «resource pool», as shown in <a
  href="#figResRoles"></a>. At any point in time, the resource objects of a
  resource pool may be <em>out of order</em> (like a defective machine or a
  doctor who is not on schedule), <em>busy</em> or <em>available</em>.</p><p>A
  process owner has special procedures for allocating available resources from
  resource pools to activities. For instance, in the model of <a
  href="#figResRoles"></a>, a medical department has the procedure "allocate a
  doctor" for allocating a doctor to a medical examination. These resource
  allocation procedures may use various policies, especially for allocating
  human resources, such as first determining the suitability of potential
  resources (e.g., based on expertise, experience and previous performance),
  then ranking them and finally selecting from the most suitable ones (at
  random or based on their turn). See also (Arias et al 2018).</p><p>The
  conceptual process model shown in <a href="#figMedDepCPM1"></a> is based on
  the information model above. It refers to a medical department as the
  <em>process owner</em>, visualized in the form of a <em>Pool</em> container
  rectangle, and to <i>doctor objects</i>, as well as to the event type
  <i>patient arrivals</i> and to the activity type
  <i>examinations</i>.</p><figure id="figMedDepCPM1"><figcaption>A conceptual
  process model based on the information model of <a
  href="#figResRoles"></a>.</figcaption><div><img alt="???"
  src="../img/MedDep_CPM1.svg" /></div></figure><p>This process model
  describes two causal regularities in the form of the following two event
  rules, each stated with two bullet points: one for describing all the state
  changes and one for describing all the follow-up events brought about by
  applying the rule.</p><ol>
      <li><p>When a new patient arrives:</p><ul>
          <li>if a doctor is available, then she is allocated to the
          examination of that patient; otherwise, a new planned examination is
          queued up;</li>

          <li>if a doctor has been allocated, then start an examination of the
          patient.</li>
        </ul></li>

      <li><p>When an examination is completed by a doctor:</p><ul>
          <li>if the queue of planned examinations is empty, then the doctor
          is released;</li>

          <li>otherwise, the next planned examination by that doctor is
          scheduled to start immediately.</li>
        </ul></li>
    </ol><p>These conceptual event rules describe the real-world dynamics of a
  medical department according to business process management decisions.
  Changes of the waiting line and (de-)allocations of doctors are considered
  to be state changes (in the, not necessarily computerized, information
  system) of the department, as they are expressed in Data Object rectangles,
  which represent state changes of affected objects caused by an event in
  DPMN.</p><p>Notice that the model of <a href="#figMedDepCPM1"></a> abstracts
  away from the fact that after allocating a doctor, patients first need to
  walk to the room before their examination can start. Such a simplification
  may be justified if the walking time can be neglected or if there is no need
  to maximize the productive utilization of doctors who, according to this
  process model, have to wait until the patient arrives at the room. Below,
  this model is extended for allowing to allocate rooms and doctors such that
  patients have to wait for doctors, and not the other way
  around.</p></section><section><h4>Switching roles: doctors as
  patients</h4><p>The same person who is a doctor at a diagnostic imaging
  department may be treated as a patient of that department. It's a well-known
  fact that in the real world people may switch roles and may play several
  roles at the same time, but many modeling approaches/platforms fail to admit
  this. For instance, the simulation language (SIMAN) of the well-known DES
  modeling tool Arena does not treat resources and processing objects
  ("entities") as roles, but as strictly separate categories. This language
  design decision was a meta-modeling mistake, as admitted by Denis Pegden,
  the main creator of SIMAN/Arena, in (Drogoul et al 2018) where he says "it
  was a conceptualization mistake to view Entities and Resources as different
  constructs". </p><p>In <a href="#figKindsRoles"></a>, the above model is
  extended by categorizing the classes <i>doctors</i> and <i>patients</i> as
  «role type» classes and adding the «kind» class <i>people</i> as a supertype
  of <i>doctors</i> and <i>patients</i>, we create the possibility that a
  person may play both roles: the role of a doctor and the role of a patient,
  albeit not at the same time. The object type categories «kind» and «role
  type» have been introduced to conceptual modeling by Guizzardi
  (2005).</p><figure id="figKindsRoles"><figcaption>A conceptual information
  model with doctors and patients as people.</figcaption><div><img alt="???"
  src="../img/MedExam_CM3.svg"
  width="600" /></div></figure></section><section><h4>Queueing planned
  activities</h4><p>Whenever an activity is to be performed but cannot start
  due to a required resource not being available, the <em>planned
  activity</em> is placed in a queue as a waiting job. Thus, in the case of a
  medical examination of a patient, as described in the model of <a
  href="#figKindsRoles"></a>, the <em>waiting line</em> represents, in fact, a
  queue of planned examinations (involving patients), and not a queue of
  waiting patients. </p><p>This consideration points to a general issue:
  modeling resource-constrained activities implies modeling queues of planned
  activities, while there is no need to consider (physical) queues of
  (physical) objects. Consequently, even if a real-world system includes a
  physical queue (of physical objects), an OEM-A model may abstract away from
  its physical character and consider it as a queue of planned activities
  (possibly including pre-allocated resources). While a physical queue implies
  that there is a maximum capacity, a queue of planned activities does not
  imply this. For instance, when a medical department does not require
  patients to queue up in a waiting area for obtaining an examination, but
  accepts their registration for an examination by phone, the resulting queue
  of waiting patients is not a physical queue (but rather a queue of planned
  examinations) and there is no need to limit the number of waiting patients
  in the same way as in the case of queuing up in a waiting area with limited
  space.</p><p>A planned activity can only start, when all required resources
  have been allocated to it. Thus, a planned examination of a patient can only
  start, when both a room and a doctor have been allocated to it. Let's assume
  that when a patient <i>p</i> arrives, only a room is available, but not a
  doctor. In that case, the available room is allocated to the planned
  examination, which is then placed in a queue since it still has to wait for
  the availability of a doctor. Only when a doctor becomes available, e.g.,
  via the completion of an examination of another patient or via an arrival of
  a doctor, the doctor can be allocated as the last resource needed to start
  the planned examination of patient <i>p</i>. </p><p>As a consequence of
  these considerations, the <em>waiting line</em> of a medical department
  modeled in <a href="#figKindsRoles"></a> as an ordered collection of
  patients is renamed to <em>planned walks</em> in <a
  href="#figMedDepCIM4"></a>. In addition, a property <em>planned
  examinations</em>, which holds an ordered collection of patient-room pairs,
  is added to the class <em>medical departments</em>. These model elements
  reflect the hospital's business process practice to maintain a list of
  patients waiting for the allocation of a room to walk to and a list of
  planned examinations, each with a patient waiting for a doctor in an
  examination room.</p></section><section><h4>Decoupling the allocation of
  multiple resources</h4><p>For being more realistic, we consider the fact
  that patients first need to be walked by nurses to the room allocated to
  their examination before the examination can start. Thus, in the model of <a
  href="#figMedDepCIM4"></a>, we add a second activity type, <em>walks to
  room</em>, involving people (typically, nurses and patients) walking to an
  examination room. </p><figure id="figMedDepCIM4"><figcaption>Adding the
  activity type "walks to room" to the conceptual information
  model.</figcaption><div><img alt="???" src="../img/MedExam_CIM4.svg"
  width="600" /></div></figure> <figure id="figMedDepCPM2"><figcaption>A
  conceptual process model based on the information model of <a
  href="#figMedDepCIM4"></a>.</figcaption><div><img alt="???"
  src="../img/MedDep_CPM2.svg" /></div></figure><p>This process model
  describes three causal regularities in the form of the following three event
  rules:</p><ol>
      <li><p>When a new patient arrives:</p><ul>
          <li>if a room and a nurse are available, they are allocated to the
          walk of that patient to that room, otherwise a new planned walk is
          placed in the corresponding queue;</li>

          <li>if a room has been allocated, then the nurse starts walking the
          patient to the room.</li>
        </ul></li>

      <li><p>When a walk of a patient and nurse to a room is completed:</p><ul>
          <li>if there is still a planned walk in the queue and a room is
          available, then the room is allocated and the nurse is re-allocated
          to the walk of the next patient to that room.<br />if a doctor is
          available, she is allocated to the examination of that patient, else
          a new planned examination of that patient is queued up;</li>

          <li>if a doctor has been allocated, then the examination of that
          patient starts<br />if the nurse has been re-allocated, she starts
          walking the next patient to the allocated room.</li>
        </ul></li>

      <li><p>When an examination of a patient is completed by a doctor in a
      particular room:</p><ul>
          <li>if there is still a planned examination (of another patient in
          another room), then re-allocate the doctor to that planned
          examination, else release the doctor;<br />if the waiting line is
          not empty, re-allocate the room to the next patient, else release
          the room;</li>

          <li>if the doctor has been re-allocated to a planned examination,
          that examination starts;<br />if the room has been re-allocated to
          another patient, that patient starts walking to the room.</li>
        </ul></li>
    </ol><p>Notice that the process type described in <a
  href="#figMedDepCPM2"></a> does not consider the fact that doctors have to
  walk to the examination room too, which could be modeled by adding a
  <em>doctors' walks to room</em> Activity rectangle after the patients'
  <em>walks to room</em> Activity rectangle.</p><p>For being able to collect
  informative utilization statistics, it is required to distinguish the total
  time a resource is allocated (its 'gross utilization') from the time it is
  allocated for productive activities (its 'net utilization'). Thus, only
  <em>examinations</em> would be classified as productive activities, while
  <em>walks to room</em> would rather be considered a kind of set-up
  activities.</p><section><h4>Re-engineering the process type by centralizing
  the re-allocation of resources</h4><p>In the process type described in <a
  href="#figMedDepCPM2"></a>, the re-allocation of released resources is
  handled in the event rules of activity end events:</p><ul>
      <li>when a nurse's and patient's walk to a room ends, the nurse is free
      to be re-allocated; so if there is another planned walk and a room is
      available, the nurse is re-allocated to a walk of the next patient to
      that room;</li>

      <li>when an examination ends, its resources (a doctor and a room) are
      re-allocated, if planned activities are waiting for them.</li>
    </ul><p>This approach requires that the same re-allocation logic is
  repeated in the event rules of all activity types associated with that type
  of resource, implying that all performers involved would have to know and
  execute the same re-allocation logic. It is clearly preferable to centralize
  this logic in a single event rule, which can be achieved by introducing
  <em>release resource request</em> events following activities that do not
  need to keep resources allocated, as shown in <a href="#figMedDepCPM3"></a>
  where the re-allocation of doctors and rooms is decoupled from the
  examination activities and centralized (e.g., in a special resource
  management unit) by adding the two event types <em>room release
  requests</em> and <em>doctor release requests</em> modeling simultaneous
  events that follow examinations.</p><figure
  id="figMedDepCPM3"><figcaption>An improved process model based on the
  information model of <a href="#figMedDepCIM4"></a>.</figcaption><div><img
  alt="???" src="../img/MedDep_CPM3.svg" width="700" /></div></figure><p>This
  process model describes an improved business process with six event
  rules:</p><ol>
      <li><p>When a new patient arrives:</p><ul>
          <li>if a room and a nurse are available, they are allocated to the
          walk of that patient to that room, otherwise a new planned walk is
          placed in the corresponding queue;</li>

          <li>if a room has been allocated, then the nurse starts walking the
          patient to the room.</li>
        </ul></li>

      <li><p>When a walk of a patient and nurse to a room is completed:</p><ul>
          <li>if a doctor is available, she is allocated to the examination of
          that patient, else a new planned examination of that patient is
          queued up;</li>

          <li>if a doctor has been allocated, then the examination of that
          patient starts; in addition, a nurse release request is issued.</li>
        </ul></li>

      <li><p>When a nurse release request has been issued:</p><ul>
          <li>if the waiting line is not empty and a room is available,
          allocate the room and re-allocate the nurse to the next patient,
          else release the nurse;</li>

          <li>if the nurse has been re-allocated to another patient, she
          starts walking that patient to the room.</li>
        </ul></li>

      <li><p>When an examination is completed:</p><ul>
          <li>[no state change]</li>

          <li>a room release request is issued (e.g., by notifying a resource
          management clerk or the department's information system), and, in
          parallel, a doctor release request is issued.</li>
        </ul></li>

      <li><p>When a room release request is received by a resource
      manager:</p><ul>
          <li>if the waiting line is not empty and a nurse is available,
          allocate the nurse and re-allocate the room to the next patient,
          else release the room;</li>

          <li>if the room has been re-allocated to another patient, the nurse
          starts walking that patient to the room.</li>
        </ul></li>

      <li><p>When a doctor release request is received by a resource
      manager:</p><ul>
          <li>if there is still a planned examination (of another patient in
          another room), then re-allocate the doctor to that planned
          examination, else release the doctor;</li>

          <li>if the doctor has been re-allocated to a planned examination,
          that examination starts.</li>
        </ul></li>
    </ol><p>Notice that, in the general case, instead of scheduling several
  simultaneous release requests, each for a single resource, when an activity
  completes, a single joint release request for all used resources should be
  scheduled, allowing to re-allocate several of the released resources
  jointly.</p></section><section><h4>Displaying the process owner and activity
  performers</h4><p>The process owner and the involved performers can be
  displayed in an OEM process model by using a rectangular <em>Pool</em>
  container for the process owner and Pool partitions called <em>Lanes</em>
  for the involved activity performers, as shown in <a
  href="#figMedDepCPM4"></a>. Notice that, as opposed to BPMN, where lanes do
  not have a well-defined meaning, but can be used for any sort of arranging
  model elements, DPMN Lanes represent organizational actors playing the
  resource role of <em>performer</em>.</p><figure
  id="figMedDepCPM4"><figcaption>Displaying the process owner and activity
  performers in a conceptual process model.</figcaption><div><img alt="???"
  src="../img/MedDep_CPM4.svg" width="650" /></div></figure></section><section
  id="secNonExclusiveRes"><h4>Non-exclusive resources</h4><p>In OEM, a
  resource is exclusive by default, that is, it can be used in at most one
  activity at the same time, if no other <em>multitasking multiplicity</em> is
  specified. For instance, in all information models above (e.g., in <a
  href="#figResRoles"></a>), the participation associations between the
  resource classes <i>rooms</i> and <i>doctors</i> and the activity classes
  <i>walks to room</i> and <i>examinations</i> do not specify any multitasking
  multiplicity for the association end at the side of the activity class, but
  just the historical participation multiplicity of zero-to-many (∗)
  expressing that resources participate in zero or more activities over
  time.</p><p>OEM allows expressing multitasking multiplicities with the help
  of snapshot multiplicities of the form "S:m", where m is a multiplicity
  expression, added to the history multiplicity of the association end
  concerned.</p><p>A non-exclusive resource can be simultaneously used in more
  than one activity. The maximum number of activities, in which a
  non-exclusive resource can participate at the same time, is normally
  specified at the type level for all resource objects of that type using the
  upper bound of a multitasking multiplicity. In general, there may be cases
  where it should be possible to specify this at the level of individual
  resource objects. For instance, larger examination rooms may accommodate
  more examinations than smaller ones.</p><p>A resource can be exclusive with
  respect to all types of activities (which is the default case) or it can be
  exclusive with respect to specific types of activities. For instance, in the
  model of <a href="#figExclusiveResByType"></a>, a multitasking multiplicity
  of 0..1 is defined both for the participation of rooms in walks and in
  examinations. This means a room can participate in at most one walk and in
  at most one examination at a time, which is a different business rule,
  allowing to walk patients to a room even if it is currently used for an
  examination, compared to the model of <a href="#figMedDepCIM4"></a>,
  allowing to walk patients to a room only if it is currently not being used
  for an examination.</p><figure id="figExclusiveResByType"><figcaption>Adding
  multitasking multiplicities for rooms participating both in walks and
  examinations, possibly at the same time.</figcaption><div><img alt="???"
  src="../img/MedExam_CIM5.svg"
  width="500" /></div></figure></section></section></body>
</html>
