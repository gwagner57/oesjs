<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Resource-Constrained Activities</title>
  </head>

  <body><p>A <em>Resource-Constrained Activity</em> is an activity that
  requires certain resources for being executed. Such an activity can only be
  started when the required resources are available and can be allocated to
  it. </p><p>At any point in time, a resource required for performing an
  activity may be <em>available</em> or not. A resource is not available, for
  instance, when it is <em>busy</em> or when it is <em>out of
  order</em>.</p><aside class="rightbox40"><h1>Summary</h1><ol>
      <li>A Resource-Constrained Activity can only be started when the
      required resources are available.</li>

      <li>An <abbr title="Object Event">OE</abbr> Class Diagram includes
      <em>Resource Role</em> associations between Resource-Constrained
      Activity types <i>A</i> and resource object types <i>R</i>. Their
      association ends at the side of <i>R</i> correspond to special
      properties of <i>A</i> called <em>Resource Roles</em>.</li>

      <li>The resources that are available for being allocated to a planned
      activity are provided by <em>Resource Pools</em> managed by <em>Process
      Owners</em>. A Resource Pool can be explicitly modeled in an OE Class
      Diagram as a collection-valued reference property of the object type
      representing the Process Owner.</li>

      <li>A simulation model assigns a Resource Pool to each Resource Role,
      either implicitly or explicitly. An implicit assignment is provided by
      the default assumption that there is one pool for each resource object
      type.</li>

      <li>A <em>Resource Type</em> is defined in an <abbr
      title="Object Event">OE</abbr> Class Diagram as a special object type
      that has a resource <em>status</em> attribute and is the range of a
      Resource Role and possibly also of a Resource Pool property.</li>

      <li><em>Resource Cardinality Constraints</em> and <em>Multitasking
      Constraints</em> are defined in an <abbr title="Object Event">OE</abbr>
      Class Diagram in the form of multiplicities of the respective
      association ends of Resource Role associations.</li>

      <li>A <em>Resource-Dependent Activity Scheduling (RDAS)</em> arrow is a
      high-level modeling element of DPMN Process Diagrams, merging the
      semantics of event scheduling arrows with the Allocate-Release modeling
      pattern.</li>

      <li>OEM-A extends basic OEM by adding activities, resource roles,
      resource cardinality constraints, resource pools, resource types and
      RDAS arrows. Notice that most of these resource modeling elements are
      expressed in an <abbr title="Object Event">OE</abbr> Class Diagram, and
      only the possible event flows are expressed in a DPMN Process Diagram
      with the help of event scheduling arrows and <abbr
      title="Resource-Dependent Activity Scheduling">RDAS</abbr> arrows.</li>
    </ol></aside><p>Resources are objects of a certain type. The resource
  objects of an activity include its <em>performer</em>, as expressed in the
  diagram shown in <a href="#figActRes"></a>. While in the real-world, any
  activity has a performer, a conceptual domain model or a simulation design
  model may abstract away from the performer of an activity.</p><p>For
  instance, a consultation activity may require a consultant and a room. Such
  <em>resource cardinality constraints</em> are defined at the type level.
  When defining the activity type <code>Consultation</code>, these resource
  cardinality constraints are defined in the form of two mandatory
  associations with the object types <code>Consultant</code> and
  <code>Room</code> such that both associations' ends have the multiplicity 1
  ("exactly one"). Then, in a simulation run, a new <code>Consultation</code>
  activity can only be started, when both a <code>Consultant</code> object and
  a <code>Room</code> object are available.</p><p>In an OE Class Diagram, an
  object type that has a resource <em>status</em> attribute and is the range
  of both a Resource Role and a Resource Pool property can be viewed as a
  <em>resource type</em>.</p><p><em>Activity Networks (ANs)</em> extend <abbr
  title="Object Event Graph">OEG</abbr>s by adding activity nodes (in the form
  of rectangles with rounded corners) and <em>Resource-Dependent Activity
  Scheduling (RDAS)</em> arrows.</p><p>For all types of resource-constrained
  activities, or, more precisely, for all activity nodes of an <abbr
  title="Activity Network">AN</abbr>, a simulator can automatically collect
  the following statistics:</p><ol>
      <li><i>Throughput</i> statistics: the numbers of enqueued and dequeued
      planned activities, and the numbers of started and completed
      activities.</li>

      <li><i>Queue length</i> statistics: the average and maximum length of
      its queue of planned activities.</li>

      <li><i>Cycle time</i> statistics: the minimum, average and maximum cycle
      time, which is the sum of the waiting time and the activity
      duration.</li>

      <li><i>Resource utilization</i> statistics: the percentage of time each
      resource object involved is busy with an activity of that type.</li>
    </ol><p>In addition, a simulator can automatically collect the percentage
  of time each resource object involved is idle or out-of-order.</p><figure
  id="figActRes"><div><img alt="???"
  src="../img/ResourceConstrained-Activities.svg"
  width="600" /></div><figcaption>Typically, the performer of an activity is a
  resource object.</figcaption></figure><p>For modeling resource-constrained
  activities, we need to define their types. As can be seen in <a
  href="#figIndividualsAndTypes"></a>, a resource-constrained activity type is
  composed of </p><ol>
      <li>a set of <em>properties</em> and a set of <em>operations</em>, as
      any entity type,</li>

      <li>a set of <strong>resource roles</strong>, each one having the form
      of a reference property with a name, an object type as range, and a
      multiplicity that may define a <strong>resource cardinality
      constraint</strong> like, e.g., "exactly one resource object of this
      type is required" or "at least two resource objects of this type are
      required".</li>
    </ol><p>The resource roles defined for an activity type may include the
  performer role.</p><figure id="figIndividualsAndTypes"><figcaption>Activity
  types may have special properties representing resource
  roles.</figcaption><div><img alt="???"
  src="../img/Types-with-ResourceRoles.svg"
  width="500" /></div></figure><p>These considerations show that a simulation
  language for simulating activities needs to allow defining activity types
  with two kinds of properties: ordinary properties and resource roles. At
  least for the latter ones, it must be possible to define multiplicities for
  defining resource cardinality constraints. These requirements are fulfilled
  by <abbr title="Object Event">OE</abbr> Class Diagrams where resource roles
  are defined as special properties categorized by the stereotype «resource
  role» or, in short, «rr».</p><p>The extension of basic OEM by adding the
  concepts needed for modeling resource-constrained activities (in particular,
  resource roles with constraints, resource pools, and resource-dependent
  activity scheduling arrows) is called <em>OEM-A</em>.</p></body>
</html>
