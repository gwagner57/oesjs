<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Introducing Resource-Dependent Activity Scheduling Arrows</title>
  </head>

  <body><p>The conceptual process model shown in <a
  href="secCM.html#figMedDepCPM4"></a> and the process design model shown in
  <a href="secDesM.html#figMedDep4PDM1"></a> exhibit a general pattern for
  modeling a sequence of two resource-constrained activities of types
  <i>A</i><sub>1</sub> and <i>A</i><sub>2</sub> shown in <a
  href="#figAllocRelPatternCPM"></a>. For describing this
  <strong>Allocate-Release Modeling Pattern</strong>, we assume that </p><ol>
      <li>the process owner maintains queues for planned activities:
      <i>q</i><sub>1</sub> for for planned activities of type
      <i>A</i><sub>1</sub>, and <i>q</i><sub>2</sub> for planned activities of
      type <i>A</i><sub>2</sub>, both defined as queue-valued (i.e., ordered
      multi-valued) reference properties of the process owner in the
      underlying information model;</li>

      <li>the underlying information model specifies the sets of resources
      <i>R</i><sub>1</sub> and <i>R</i><sub>2</sub> required by
      <i>A</i><sub>1</sub> and <i>A</i><sub>2</sub>;</li>

      <li>the set of resources required by <i>A</i><sub>2</sub> but not by
      <i>A</i><sub>1</sub> is denoted by
      <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub>;</li>

      <li>the set of resources required by <i>A</i><sub>1</sub> and by
      <i>A</i><sub>2</sub> is denoted by
      <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub>.</li>
    </ol><figure id="figAllocRelPatternCPM"><figcaption>A conceptual modeling
  pattern for a sequence of resource-constrained
  activities</figcaption><div><img alt="???"
  src="../img/AllocRelPattern_CPM.svg" /></div></figure><p>We can describe the
  execution semantics of the <em>Allocate-Release Modeling Pattern</em> for
  the case of a succession of an activity of type <i>A</i><sub>1</sub> by
  another activity of type <i>A</i><sub>2</sub> in the following way:</p><ol>
      <li>ON <i>start event</i>:<ol type="a">
          <li>If the resources <i>R</i><sub>1</sub> required by
          <i>A</i><sub>1</sub> are available, they are allocated; otherwise, a
          planned <i>A</i><sub>1</sub> activity is added to the task queue
          <i>q</i><sub>1</sub>.</li>

          <li>If the resources <i>R</i><sub>1</sub> have been allocated, a new
          activity of type <i>A</i><sub>1</sub> is started.</li>
        </ol></li>

      <li>WHEN <i>an activity of type <i>A</i><sub>1</sub></i> is
      completed:<ol type="a">
          <li>If available, the resources
          <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> are allocated; otherwise,
          a planned <i>A</i><sub>2</sub> activity with reserved resources
          <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> is added to the task queue
          <i>q</i><sub>2</sub>.</li>

          <li>If <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> have been
          allocated, a new activity of type <i>A</i><sub>2</sub> (with
          resources <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> and
          <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub>) is started. In addition,
          the release of <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub> is
          requested.</li>
        </ol></li>

      <li>ON <i>release request</i> for
      <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub>:<ol type="a">
          <li>If <i>q</i><sub>1</sub> is not empty and the resources
          <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> required by both
          <i>A</i><sub>1</sub> and <i>A</i><sub>2</sub> are available, they
          are allocated and <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub> are
          re-allocated to head( <i>q</i><sub>1</sub>); otherwise,
          <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub> are released.</li>

          <li>If the resources <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub> have
          been re-allocated, a new activity of type <i>A</i><sub>1</sub> is
          started.</li>
        </ol></li>

      <li>WHEN <i>an activity of type <i>A</i><sub>2</sub></i> completes:<ol
          type="a">
          <li>There is no state change.</li>

          <li>An immediate release request for <i>R</i><sub>2</sub> is
          caused/scheduled.</li>
        </ol></li>

      <li>ON <i>release request</i> for <i>R</i><sub>2</sub>:<ol type="a">
          <li>If <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> is nonempty: if
          <i>q</i><sub>1</sub> is not empty and the resources
          <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub> required by
          <i>A</i><sub>1</sub>, but not yet allocated, are available, they are
          allocated and <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> are
          re-allocated to head(<i> q</i><sub>1</sub>); otherwise,
          <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> are released. If
          <i>q</i><sub>2</sub> is not empty, then re-allocate
          <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> to head(<i>
          q</i><sub>2</sub>); otherwise,
          <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> are released.</li>

          <li>If <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> have been
          re-allocated, a new activity of type <i>A</i><sub>1</sub> is
          started. If <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> have been
          re-allocated, a new activity of type <i>A</i><sub>2</sub> is
          started.</li>
        </ol></li>
    </ol><p>Since the Allocate-Release Modeling Pattern defines a generic
  algorithm for scheduling resource-constrained activities as well as
  allocating and releasing resources, its (pseudo-)code does not have to be
  included in a DPMN Process Diagram, but can be delegated to an OE simulator
  supporting the resource-dependent scheduling of resource-constrained
  activities according to this pattern. This approach allows introducing new
  DPMN modeling elements for expressing the Allocate-Release Modeling Pattern
  in a concise way, either leaving allocate-release steps completely implicit,
  as in the DPMN Process Diagram of <a href="#figAllocRelPatternPDM2"></a>, or
  explicitly expressing them, as in <a
  href="#figAllocRelPatternPDM1"></a>.</p><p>The most important new DPMN
  modeling element introduced are <em>Resource-Dependent </em><em>Activity
  Scheduling</em> (RDAS) arrows pointing to resource-constrained activities,
  as in <a href="#figAllocRelPatternPDM2"></a>. These arrows are high-level
  modeling elements representing the implicit allocate-release logic exhibited
  in <a href="#figAllocRelPatternCPM"></a>. Thus, the meaning of the model of
  <a href="#figAllocRelPatternPDM2"></a> is provided by the model of <a
  href="#figAllocRelPatternCPM"></a>.</p><figure
  id="figAllocRelPatternPDM2"><figcaption>Using <abbr
  title="Resource-Dependent Activity Scheduling">RDAS</abbr> arrows in a
  conceptual process model.</figcaption><div><img alt="???"
  src="../img/AllocRelPattern_PDM2.svg" width="400" /></div></figure><p>It is
  an option to display the implicit allocate-release steps with
  <i>Allocate</i> and <i>Release</i> rectangles together with <em>simple
  control flow</em> arrows, as between the <i>start event</i> circle and the
  <i>Allocate R1</i> rectangle in <a
  href="#figAllocRelPatternPDM1"></a>.</p><figure
  id="figAllocRelPatternPDM1"><figcaption>Displaying the implicit
  allocate-release steps.</figcaption><div><img alt="???"
  src="../img/AllocRelPattern_PDM1.svg" width="600" /></div></figure><p>The
  meaning of the model of <a href="#figAllocRelPatternPDM1"></a> is the same
  as that of <a href="#figAllocRelPatternPDM2"></a>, which is provided by the
  model of <a href="#figAllocRelPatternCPM"></a>. The fact that, using <abbr
  title="Resource-Dependent Activity Scheduling">RDAS</abbr> arrows, the
  allocate-release logic of resource-constrained activities does not have to
  be explicitly modeled and displayed in a process model shows the power of
  founding a process model on an information model, since the entire resource
  management logic can be expressed in terms of resource roles, constraints
  and pools in an information model. This is in contrast to the common
  approach of industrial simulation tools, such as Simio and AnyLogic, which
  require defining resource roles, constraints and pools as well as explicit
  allocate-release steps in the process model, in a similar way as shown in <a
  href="#figAllocRelPatternPDM1"></a>.</p><section><h4>The Standard
  Allocate-Release Algorithm</h4><p>For each activity type, it has to be
  decided, if the resources required for a planned activity of that type are
  allocated at once or successively as soon as they become available. We call
  these two allocation policies <em>Single-Step Allocation (SSA)</em> and
  <em>Multi-Step Allocation (MSA)</em>.</p><p>We can describe the <em>Standard
  Allocate-Release Algorithm</em> for the case of an event of type <i>E</i>
  succeeded by an activity of type <i>A</i><sub>1</sub> succeeded by another
  activity of type <i>A</i><sub>2</sub>, with <i>R</i><sub>i</sub> and
  <i>q</i><sub>i</sub> being the set of resource roles and the task queue of
  <i>A</i><sub>i</sub>, in the following way:</p><ol>
      <li>WHEN an event <i>e</i>@<i>t</i> of type <i>E</i> occurs:<ol type="a">
          <li>A planned activity <i>a</i> of type <i>A</i><sub>1</sub> is
          created. If all <i>R</i><sub>1</sub> resources are available, they
          are allocated to <i>a</i>. Otherwise: (1) if MSA, then all available
          <i>R</i><sub>1</sub> resources are allocated to <i>a</i>; (2)
          <i>a</i> is added to the task queue <i>q</i><sub>1</sub>. </li>

          <li>If all <i>R</i><sub>1</sub> resources have been allocated to
          <i>a</i>, it is started.</li>
        </ol></li>

      <li>WHEN an activity <i>a</i><sub>1</sub> of type
      <i><i>A</i></i><sub>1</sub> is completed:<ol type="a">
          <li>A planned activity <i>a</i><sub>2</sub> of type
          <i>A</i><sub>2</sub> is created. If all
          <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> resources are available,
          they are allocated to <i>a</i><sub>2</sub> and
          <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> are re-allocated from
          <i>a</i><sub>1</sub> to <i>a</i><sub>2</sub> (resulting in an
          allocation of all <i>R</i><sub>2</sub> resources to
          <i>a</i><sub>2</sub>). Otherwise: (1) if MSA, then all available
          <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> resources are allocated to
          <i>a</i><sub>2</sub> and <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub>
          are re-allocated from <i>a</i><sub>1</sub> to <i>a</i><sub>2</sub>;
          (2) <i>a</i><sub>2</sub> is added to <i>q</i><sub>2</sub>.</li>

          <li>If all <i>R</i><sub>2</sub> resources have been allocated to
          <i>a</i><sub>2</sub>, it is started. In addition, if MSA, the
          release of all <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub> resources
          is requested, else the release of all <i>R</i><sub>1</sub> resources
          is requested.</li>
        </ol></li>

      <li>ON <i>release request</i> for
      <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub>:<ol type="a">
          <li>If <i>q</i><sub>1</sub> is not empty and the resources
          <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> required by both
          <i>A</i><sub>1</sub> and <i>A</i><sub>2</sub> are available, they
          are allocated and <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub> are
          re-allocated to head( <i>q</i><sub>1</sub>); otherwise,
          <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub> are released.</li>

          <li>If the resources <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub> have
          been re-allocated, a new activity of type <i>A</i><sub>1</sub> is
          started.</li>
        </ol></li>

      <li>WHEN <i>an activity of type <i>A</i><sub>2</sub></i> completes:<ol
          type="a">
          <li>There is no state change.</li>

          <li>An immediate release request for <i>R</i><sub>2</sub> is
          caused/scheduled.</li>
        </ol></li>

      <li>ON <i>release request</i> for <i>R</i><sub>2</sub>:<ol type="a">
          <li>If <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> is nonempty: if
          <i>q</i><sub>1</sub> is not empty and the resources
          <i>R</i><sub>1</sub>−<i>R</i><sub>2</sub> required by
          <i>A</i><sub>1</sub>, but not yet allocated, are available, they are
          allocated and <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> are
          re-allocated to head(<i> q</i><sub>1</sub>); otherwise,
          <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> are released. If
          <i>q</i><sub>2</sub> is not empty, then re-allocate
          <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> to head(<i>
          q</i><sub>2</sub>); otherwise,
          <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> are released.</li>

          <li>If <i>R</i><sub>1</sub>∩<i>R</i><sub>2</sub> have been
          re-allocated, a new activity of type <i>A</i><sub>1</sub> is
          started. If <i>R</i><sub>2</sub>−<i>R</i><sub>1</sub> have been
          re-allocated, a new activity of type <i>A</i><sub>2</sub> is
          started.</li>
        </ol></li>
    </ol><p>Since the Allocate-Release Modeling Pattern defines a generic
  algorithm for scheduling resource-constrained activities as well as
  allocating and releasing resources, its (pseudo-)code does not have to be
  included in a DPMN Process Diagram, but can be delegated to an OE simulator
  supporting the resource-dependent scheduling of resource-constrained
  activities according to this pattern. This approach allows introducing new
  DPMN modeling elements for expressing the Allocate-Release Modeling Pattern
  in a concise way, either leaving allocate-release steps completely implicit,
  as in the DPMN Process Diagram of <a href="#figAllocRelPatternPDM2"></a>, or
  explicitly expressing them, as in <a
  href="#figAllocRelPatternPDM1"></a>.</p><p>Connecting two Activity
  rectangles with an <abbr
  title="Resource-Dependent Activity Scheduling">RDAS</abbr> arrow in a DPMN
  process diagram implies DPMN's standard allocate-release algorithm according
  to which required resources that have not been allocated before are
  allocated immediately before an activity requiring them is started and
  released immediately after this activity is completed if they are not
  required by a successor activity. More precisely, when an activity of type
  <i>A</i> is completed, any resource that is not required by a successor
  activity of type <i>B</i> will be allocated to the next enqueued activity of
  type <i>A</i>, if there is one, or otherwise released.</p><p>Whenever
  another (non-standard) Allocate-Release logic is needed, it has to be
  expressed explicitly using conditional event scheduling arrows instead of
  <abbr title="Resource-Dependent Activity Scheduling">RDAS</abbr>
  arrows.</p></section><section><h4>Example 1: Simplifying the workstation
  process model</h4><p>We can now simplify the workstation model using the
  resource type category for <i>WorkStation</i> in the <abbr
  title="Object Event">OE</abbr> class model and a resource-dependent activity
  scheduling arrow from the arrival event to the processing activity in the
  DPMN process model. The resulting class model is shown in <a
  href="#figMedDep2IdmRewrite"></a>.</p><figure
  id="figWS3IDM3"><figcaption>Modeling WorkStation as a resource
  type</figcaption><div><img alt="???"
  src="../img/WS3_IDM3.svg" /></div></figure><p>The simplification of the
  process model of <a
  href="../chSimpleActivities/secDesM.html#figWS3PDM2"></a> results in the
  model of <a href="#figWS3PDM3"></a>.</p><figure
  id="figWS3PDM3"><figcaption>A simplified version of the workstation process
  model using an <abbr
  title="Resource-Dependent Activity Scheduling">RDAS</abbr>
  arrow.</figcaption><div><img alt="???" src="../img/WS3_PDM3.svg"
  width="270" /></div></figure></section><section><h4>Example 2: Simplifying
  the medical department process model</h4><p>We can now simplify the medical
  department model using the resource type category for <i>Doctor</i>,
  <i>Room</i> and <i>Nurse</i> in the OEM class model and resource-dependent
  activity scheduling arrows in the DPMN process model. The resulting class
  model is shown in <a href="#figMedDep2IdmRewrite"></a>.</p><figure
  id="figMedDep2IdmRewrite"><figcaption>A simplified version of the medical
  department information model with Doctor and Room as resource
  types</figcaption><div><img alt="???"
  src="../img/MedExam2_IDM2.svg" /></div></figure><p>The simplification of the
  rather complex process model of <a href="secDesM.html#figMedDep4PDM1"></a>
  by using resource-dependent activity scheduling arrows results in the model
  of <a href="#figMedDep2PdmRewrite"></a>.</p><figure
  id="figMedDep2PdmRewrite"><figcaption>A simplified version of the medical
  department process model using <abbr
  title="Resource-Dependent Activity Scheduling">RDAS</abbr>
  arrows.</figcaption><div><img alt="???" src="../img/MedDep4_PDM2.svg"
  width="500" /></div></figure></section></body>
</html>
