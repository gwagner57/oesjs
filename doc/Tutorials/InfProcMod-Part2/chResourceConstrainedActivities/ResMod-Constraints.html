<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Resource Cardinality Constraints and Multitasking
    Constraints</title>
  </head>

  <body><p>Since activities require resources, and resources are allocated to
  planned activities, it is crucial to understand the relationship between
  activities and resources. An important part of its meaning are <em>resource
  cardinality constraints</em> and <em>multitasking
  constraints</em>.</p><p>Business process models do not only describe the
  possible sequences of events and activities, but they also describe the
  dependency of activities on resources. For instance, in a BPMN process
  diagram, a Lane <i>L</i> (such as the <i>doctors</i> lane in the BPMN
  process diagram below) may represent a resource type such that the diagram
  specifies for any Activity rectangle <i>A</i> in <i>L</i> (such as
  <i>examinations</i>) that an activity of type <i>A</i> is performed by a
  resource of type <i>L</i>. </p><figure id="figExamDoc"><figcaption>Doctors
  perform examinations.</figcaption><div><img alt="???"
  src="../img/doctors-perform-examinations_BPMN.svg"
  width="250" /></div></figure><p>While using a container shape, such as a
  BPMN Lane, for assigning elements, such as activities, to the element
  represented by the container shape (viz, a resource role) is a good way of
  visually expressing a many-to-one relationship between activity types and
  resource roles (all activity types within a Lane do have exactly one
  resource role), it does not allow expressing one-to-many or many-to-many
  relationships between activity types and resource roles. </p><p>Therefore,
  the BPMN Lane notation can only be used for specifying a performer resource
  role for activities that do not require resources of other types, and where
  we have a one-to-one association: (1) a resource can be allocated to <em>at
  most one</em> activity, and (2) an activity is performed by <em>exactly
  one</em> resource.</p><p>In general, though, the relationships between
  activities (of some type) and resources (of some type) can be best described
  by binary relationship types in an information model, such as by binary
  associations in a UML class diagram with <em>multiplicity</em> expressions
  at both ends. <a href="#figExamDocRoom"></a> shows two one-to-one
  associations between the activity type <i>examinations</i> and the object
  types <i>rooms</i> and <i>doctors</i> associating a room as a (passive)
  resource and a doctor as a performer resource to an examination
  activity.</p><figure id="figExamDocRoom"><figcaption>An examination requires
  two resources: a room and a doctor.</figcaption><div><img alt="???"
  src="../img/Exam-Doc.svg" width="600" /></div></figure><p>The association
  end at the side of the object type <i>doctors</i> is categorized as a
  <em>resource role</em> with the help of UML’s stereotype notation. The
  multiplicity expression at the resource role end is called <em>resource
  multiplicity</em>. In our example, the resource multiplicity at the
  <i>doctors</i> end is ”1” (an abbreviation of "1..1"), which means that
  exactly one doctor is required for performing an examination, defining a
  <em>resource cardinality constraint</em> that is a conjunction of a
  <em>minimum resource cardinality constraint</em> ("at least one") and a
  <em>maximum resource cardinality constraint</em> ("at most one").</p>In
  general, a multiplicity expression defines both a lower bound and an upper
  bound. When the lower bound of a resource multiplicity is greater than 0, it
  defines a <em>minimum resource cardinality constraint</em>. When the upper
  bound of a resource multiplicity is a natural number greater than 0, it
  defines a <em>maximum resource cardinality constraint</em>. The resource
  multiplicity 0..* (where * stands for <em>unbounded)</em>, which does not
  define any resource cardinality constraint, is rather unusual since normally
  all activities require at least one resource (their performer).<p>The
  multiplicity of the association end at the other side (of the activity type)
  represents a <em>multitasking constraint</em>, which defines if a resource
  is exclusively allocated to an activity, or to how many activities a
  resource can be allocated at the same time. In our example, the multiplicity
  0..1 represents a multitasking constraint stating that, at any point in
  time, a doctor can be associated with, or perform, either no examination or
  at most one examination. </p><p>In an Object-Oriented (OO) programming
  approach, the computational meaning of a resource role is provided by a
  corresponding property in the class implementing the activity type. In the
  above example, the resource role association of "examinations" with
  "doctors" leads to a property <i>doctor</i> in the class <i>Examination</i>,
  as shown in <a href="#figExam"></a>.</p><p>When activities admit using more
  resources than required, this means they can be started as soon as the
  required number of resources are available, but they can also be started
  with a greater number of resources, typically implying a faster
  performance.</p><figure id="figExam"><figcaption>The class
  <i>Examination</i> implementing the corresponding activity
  type.</figcaption><div><img alt="???" src="../img/Examination.svg"
  width="120" /></div></figure><p>While the model shown in <a
  href="#figExamDocRoom"></a> is an example of a <em>one-to-one</em>
  association between an activity type and a resource object type, which is
  the only option in BPMN process models, we consider the cases of
  <em>one-to-many</em>, <em>many-to-one</em> and <em>many-to-many</em>
  associations in the following subsections.</p><section><h4>One-to-Many
  Relationships</h4><p>An example of a one-to-many relationship between
  activity types and resource object types is the association between ”load
  truck” activities and ”wheel loader” resources shown in <a
  href="#figLoadWheelLoaders"></a>.</p><figure
  id="figLoadWheelLoaders"><figcaption>The loading of a truck requires at
  least one, and can be handled by at most two, wheel
  loaders.</figcaption><div><img alt="???" src="../img/Load-Loaders.svg"
  width="320" /></div></figure><p>Notice that in this diagram, the «resource
  role» designation of the association end at the side of “wheel loaders” has
  been abbreviated by the keyword «rr». Its resource multiplicity of 1..2
  specifies a <em>minimum resource cardinality constraint</em> of “at least
  one” and a <em>maximum resource cardinality constraint</em> of “at most
  two”, meaning that one wheel loader is required and a second one is
  admissible. When an activity does not require, but admits of, using more
  than one resource (of the same type), this typically means that the duration
  of the activity is the more decreasing the more resources are being
  used.</p><p>The one-to-many resource role association of ”load truck” with
  ”wheel loaders” leads to a property <i>wheelLoaders</i> in the activity
  class <i>LoadTruck</i>, as shown in <a href="#figLoadTruck"></a>. Notice
  that this property has the multiplicity 1..2, which means that it is
  collection-valued and has either a singleton or a two-element collection
  value.</p><figure id="figLoadTruck"><figcaption>The class LoadTruck
  implementing the corresponding activity type.</figcaption><div><img
  alt="Load Truck activity rectangle" src="../img/LoadTruck.svg"
  width="200" /></div></figure><p>When activities admit using more resources
  than required, this means they can be started as soon as the required number
  of resources are available, but they can also be started with a greater
  number of resources, typically implying a shorter
  duration.</p></section><section><h4>Many-to-One Relationships</h4><p>An
  example of a many-to-one relationship between activity types and resource
  object types is the association between ”examination” activities and
  ”examination room” resources shown in <a
  href="#figExamRoom"></a>.</p><figure id="figExamRoom"><figcaption>An
  examination room may be used by up to 3 examinations at the same
  time.</figcaption><div><img alt="???" src="../img/Exam-Room.svg"
  width="350" /></div></figure><p>In this example, examination rooms (as
  resources) admit of up to three examinations being performed at the same
  time. Such a <em>multitasking constraint</em> can be implemented with the
  help of a (multitasking) <em>capacity</em> attribute of the class
  implementing the resource object type. If such a constraint applies to all
  instances of a resource object type, <em>capacity</em> would be a
  class-level (”static”) attribute, which is rendered underlined in a UML
  class diagram, as shown in the following model:</p><figure><div><img
  alt="Load Truck activity rectangle" src="../img/ExaminationRoom.svg"
  width="150" /></div></figure><p>Alternatively, if different examination
  rooms have different capacities, then <em>capacity</em> would be an ordinary
  (instance-level) attribute of the class
  <i>ExaminationRoom</i>.</p></section><section><h4>Many-to-Many
  Relationships</h4><p>Examples of many-to-many relationships between activity
  types and resource object types are rare and typically involve activities
  going on with interruptions. An example is the association between ”teaching
  a course” activities and ”teacher” resources shown in <a
  href="#figTeaching"></a>. In this example, the teaching of a course admits
  at most two teachers who may be involved in at most 7 course teaching
  activities at the same time.</p><figure id="figTeaching"><figcaption>The
  teaching of a course is performed by teachers.</figcaption><div><img
  alt="???" src="../img/Teaching-Teachers.svg"
  width="400" /></div></figure></section></body>
</html>
