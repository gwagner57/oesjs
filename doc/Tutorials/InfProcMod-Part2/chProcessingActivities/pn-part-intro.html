<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Processing Activities and Processing Networks</title>
  </head>

  <body><p>A <em>Processing Activity</em> is a resource-constrained activity
  that takes one or more objects as inputs and processes them in some way
  (possibly transforming them). The processed objects have been called
  "transactions" in GPSS, "entities" in SIMAN/Arena and also "jobs" in the
  Operations Research literature, while they are called <em>processing
  objects</em> in DPMN. </p><p>Ontologically, there are one or more objects
  participating in an activity, as shown in the diagram below. Some of them
  represent resources, while others represent processing objects. For
  instance, in the information and process models of a medical department
  shown in <a
  href="../chResourceConstrainedActivities/secCM.html#figMedDepCIM4"></a> and
  <a href="../chResourceConstrainedActivities/secCM.html#figMedDepCPM4"></a>,
  there are two processing activity types: <em>walks to room</em> and
  <em>examinations</em>. In <em>walks to room</em>, since nurses are walking
  patients to examination rooms, nurses and rooms are resources, while
  patients are processing objects. In <em>examinations</em>, doctors and rooms
  are resources, while patients are processing objects. If patients would walk
  to an examination room by themselves (without the help of a nurse), patients
  would be the performers of walks to a room, and not processing objects, and,
  consequently, walks to a room would not be processing activities.</p><figure
  id="figProcObjects"><figcaption>Resource-constrained activities involving
  processing objects are processing activities.</figcaption><div><img
  alt="???" src="../img/Processing-Activities.svg"
  width="700" /></div></figure><p>Processing activities typically require
  immobile physical resources, like rooms or workstation machines, which
  define the inner nodes of a <em>Processing Network (PN)</em>. A
  <em>Processing Object</em> enters such a network via an <em>Arrival</em>
  event at an <em>Entry Station</em>, is subsequently routed along a chain of
  <em>Processing Stations</em> where it is subject to <em>Processing
  Activities</em>, and finally exits the network via a <em>Departure</em>
  event at an <em>Exit Station</em>. </p><aside
  class="rightbox40"><h1>Summary</h1><ol>
      <li>A <em>Processing Object</em> enters a <em>Processing Network
      (PN)</em> via an <em>Arrival</em> event at an <em>Entry Station</em>, is
      subsequently routed along a chain of <em>Processing Stations</em> where
      it is subject to <em>Processing Activities</em> involving various
      <em>Resource Objects</em>, and finally exits the PN via a
      <em>Departure</em> event at an <em>Exit Station</em>.</li>

      <li>PNs have been investigated in <em>operations management</em> and the
      mathematical theory of queuing (<a
      href="../Bibliography.html#Loch1998">Loch 1998</a>, Williams 2016) and
      have been the application focus of most industrial simulation software
      products, historically starting with GPSS (Gordon 1961) and SIMAN/Arena
      (Pegden and Davis 1992).</li>

      <li>OEM-PN allows modeling many forms of discrete <i>processing
      processes</i>.</li>

      <li>PN models are <em>spatial</em> simulation models where node objects,
      and other resource objects, are located in space and processing objects
      move (or <em>flow</em>) in space.</li>

      <li>Each node definition in a PN model defines both a spatial object (a
      <em>station</em>) and an event type.</li>

      <li>A <em>Processing Flow Arrow</em> connecting two nodes of a PN model
      represents both an object and an event flow. While events <em>flow in
      time</em>, processing objects <em>flow in space</em> (and time).</li>
    </ol></aside><p>The nodes of a PN define locations in a network space,
  which may be based on a two- or three-dimensional Euclidean space.
  Consequently, OEM-PN models are spatial simulation models, while basic OEM
  and OEM-A allow to abstract away from space. When processing objects are
  routed to a follow-up processing activity, they move to the location of the
  next processing node. The underlying space model allows visualizing a PN
  simulation in a natural way with processing objects as moving
  objects.</p><p>Each node in a PN model represents both an object and an
  event type. An Entry Node represents both an Entry Station (e.g., a
  reception area or an entrance to an inventory) and an arrival event type. A
  Processing Node represents both a Processing Station (e.g., a workstation or
  a room) and a processing activity type. An Exit Node represents both an Exit
  Processing and a departure event type. </p><p>A Processing Flow arrow
  connecting a Processing Node with another Processing Node or with an Exit
  Node represents both an event flow and an object flow. Thus, the node types
  and the flow arrows of a PN are high-level modeling concepts that are
  overloaded with two meanings.</p><p>A PN modeling language should have
  elements for modeling each of the three types of nodes. Consequently, DPMN-A
  has to be extended by adding new visual modeling elements for entry,
  processing and exit nodes, and for connecting them.</p><p>In the field of
  DES, PNs have often been characterized by the narrative of “entities flowing
  through a system”. In fact, while in basic DPMN and in DPMN-A, there is only
  a flow of events, in DPMN-PN this flow of events is over-laid with a flow of
  (processing) objects.</p><p>PNs have been investigated in <em>operations
  management</em> and the mathematical theory of queuing (Loch 1998, Williams
  2016) and have been the application focus of most industrial simulation
  software products, historically starting with GPSS (Gordon 1961) and
  SIMAN/Arena (Pegden and Davis 1992). They allow modeling many forms of
  discrete <i>processing processes</i> as can be found, for instance, in the
  manufacturing industry and the services industry.</p><p>It is remarkable
  that the PN paradigm has dominated the discrete event simulation market
  since the 1990’s and still flourishes today, mainly in the manufacturing and
  services industries, often with object-oriented and “agent-based”
  extensions. Its dominance has led many simulation experts to view it as a
  synonym of DES, which is a conceptual flaw because the concept of DES, even
  if not precisely defined, is clearly more general than the PN
  paradigm.</p><p>The PN paradigm has often been called a “process-oriented”
  DES approach. But unlike the business process modeling language BPMN, it is
  not concerned with a general concept of <i>business process models</i>, but
  rather with the special class of <i>processing process models</i> for
  discrete processing systems. A processing process includes the simultaneous
  handling of several “cases” (processing objects) that may compete for
  resources or have other interdependencies, while a “business process” in
  Business Process Management has traditionally been considered as a
  case-based process that is isolated from other cases.</p><p>For PN models, a
  simulator can automatically collect the following statistics, in addition to
  the resource-constrained activities statistics described in <a
  href="../chResourceConstrainedActivities/ResourceConstrainedActivities-Intro.html"></a>:</p><ol>
      <li>The number of processing objects that arrived at, and departed from,
      the system.</li>

      <li>The number of processing objects in process (that is, either waiting
      in a queue/buffer or being processed)</li>

      <li>The average time a processing object spends in the system (also
      called <em>throughput time</em>).</li>
    </ol><p>During a simulation run, it must hold that the number of
  processing objects that arrived at the system is equal to the sum of the
  number of processing objects in process and the number of processing objects
  that departed from the system, symbolically: </p><div
  style="text-align:center;"><i>arrived</i> = <i>in-process</i> +
  <i>departed</i></div><p> </p></body>
</html>
