<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Modeling a Manufacturing Workstation as a Queueing System</title>
  </head>

  <body><p>A manufacturing workstation receives parts and stores them in its
  input buffer for processing them successively. </p><section><h4>Conceptual
  Model</h4><p>A conceptual information model of a workstation system,
  defining two object types and four event types, is shown in <a
  href="#figWS_CIM"></a>.</p><figure id="figWS_CIM"><figcaption>A conceptual
  information model of a manufacturing workstation
  system</figcaption><div><img alt="???"
  src="../img/WS_CIM.svg" /></div></figure><p>As expressed by the associations
  between the four event types and the two object types, for all four types of
  events, there are the same two types of objects participating in them: parts
  and workstations, implying that each event of these four types involves a
  specific part and a specific workstation.</p><p>Notice that the input buffer
  (filled with waiting parts) is modeled as an association end with name
  <em>waiting parts</em> at the <em>parts</em> side of the association between
  <em>parts</em> and <em>workstations</em>, expressing the fact that at any
  point in time, a workstation has zero or more parts waiting in its input
  buffer for being processed.</p><p>A conceptual process model of this system,
  describing four <em>causal regularities</em> in the form of <em>event
  rules</em>, one for each type of event, is shown in <a
  href="#figWS_CPM"></a> in the form of a <em>BPMN Process Diagram</em> using
  Event circles connected with Sequence Flow arrows expressing (conditional)
  causation, and Data Objects attached to Event circles.</p><figure
  id="figWS_CPM"><figcaption>A conceptual process model of a manufacturing
  workstation system</figcaption><div><img alt="???"
  src="../img/WS_CPM.svg" /></div></figure><p>The four event rules described
  by this model are</p><ol>
      <li>When a part arrives, it is added to the input buffer and, if the
      workstation is available, there will be a processing start event for
      processing the newly arrived part.</li>

      <li>When a processing start event occurs, the next part from the input
      buffer is being processed and a processing end event is caused to occur
      some time later (after the processing time has elapsed).</li>

      <li>When a processing end event occurs, this will cause a part departure
      event and, if the input buffer is not empty, another processing start
      event involving the next part from the buffer.</li>

      <li>When a part departure event occurs, the processed part will be
      removed from the workstation.</li>
    </ol></section><section><h4>Design Model</h4><p>A simulation design model
  is based on a conceptual model. Depending on the purposes/goals of a
  simulation study, it may abstract away from certain elements of the
  real-world domain described by the conceptual model, and it adds
  computational elements representing design decisions, such as <em>random
  variables</em> expressed int he form of random variate sampling functions
  based on specific probability distributions for modeling the random
  variation of certain system variables.</p><p>An information design model of
  the single workstation system described above is shown in <a
  href="#figWS2_IDM"></a>. This model defines the multi-valued
  <code>waitingParts</code> association end to be ordered, which means that it
  corresponds to a multi-valued reference property holding an ordered
  collection (such as an array list or a queue) as its value. </p><p>The
  information design model of <a href="#figWS2_IDM"></a> defines that a
  <i>PartArrival</i> event must reference both a <i>Part</i> and a
  <i>WorkStation</i>, representing situations where specific parts arrive at
  specific workstations. Notice that, computationally, this model requires
  creating new <i>Part</i> objects (or retrieving them from an object pool)
  before a new <i>PartArrival</i> event is created (or scheduled), while it is
  more common in simulation models to create a new <i>Part</i> object only
  when an arrival event has occurred, which can be modeled by defining a
  multiplicity of 0..1 for the <i>Part</i> end of the
  <i>PartArrival</i>-<i>Part</i> association (with the meaning that
  <i>PartArrival</i> has an optional, instead of a mandatory, reference
  property with name <i>part</i>).</p><figure id="figWS2_IDM"><figcaption>An
  information design model</figcaption><div><img alt="???"
  src="../img/WS2_IDM.svg" width="500" /></div></figure><p>Notice that the
  model defines two class level operations (designated with the stereotype
  «rv») implementing random variate sampling functions:
  <code>PartArrival::recurrence()</code> complies with a triangular
  probability distribution with minimum, mode and maximum parameter values 3,
  4 and 8, while <code>ProcessingStart::processingTime()</code> complies with
  an exponential distribution with a mean of 6.</p><p>A process design model
  based on the object and event types defined by the information design model
  of <a href="#figWS2_IDM"></a> and derived from the conceptual process model
  of <a href="#figWS_CPM"></a> is shown in <a
  href="#figWS2_PDM"></a>.</p><figure id="figWS2_PDM"><figcaption>A process
  design model in the form of a DPMN Process Diagram</figcaption><div><img
  alt="???" src="../img/WS2_PDM.svg" width="550" /></div></figure><p>Notice
  that, since all events happen at the same workstation, all three event
  scheduling arrows are annotated with the same event property assignment
  <code>workStation := ws</code>, which simply propagates the object reference
  to the given workstation along the event scheduling chain. Such property
  propagation assignments (in event property assignment annotations), where a
  property value of a follow-up event is set to the corresponding property
  value of the scheduling (or triggering) event, will be omitted (as implied
  by event types having the same property names) for avoiding to clutter the
  process model diagrams.</p><p>A DPMN Process Diagram, like the one shown in
  <a href="#figWS2_PDM"></a>, can be split up into a set of event rule
  diagrams, one for each of its Event circles, as shown in the following
  table. This reduction of a DPMN process design model to a set of event rule
  design models, together with the operational semantics of event rules
  presented in (Wagner 2017a), provides the semantics of DPMN Process
  Diagrams. </p><p>Notice that an event rule design model can also be
  expressed textually in the form of a pseudo-code block with four parts: part
  1 indicates the triggering event type and declares a rule variable
  representing the triggering event, part 2 declares further rule variables
  and initializes them, part 3 contains a state change script consisting of
  state change statements, and part 4 schedules follow-up events.</p><table
      border="1">
      <tbody>
        <tr>
          <th>Rule design model</th>

          <th>Pseudo-code</th>
        </tr>

        <tr>
          <td><img src="../img/WS2_RDM_PartArrival.svg" /></td>

          <td><table border="1">
              <thead>
                <tr>
                  <th>ON a:PartArrival</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>ws : WorkStation<br />ws := a.workStation</td>
                </tr>

                <tr>
                  <td>ws.waitingParts.enqueue( a.part)</td>
                </tr>

                <tr>
                  <td>IF ws.status = AVAILABLE <br />THEN SCHEDULE
                  ProcessingStart( workStation:=ws)</td>
                </tr>
              </tbody>
            </table></td>
        </tr>

        <tr>
          <td><img src="../img/WS2_RDM_ProcStart.svg" /></td>

          <td><table border="1">
              <thead>
                <tr>
                  <th>ON ps:ProcessingStart</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>ws : WorkStation<br />ws := ps.workStation</td>
                </tr>

                <tr>
                  <td>ws.status := BUSY</td>
                </tr>

                <tr>
                  <td>SCHEDULE ProcessingEnd( workStation:=ws) <br />DELAYED
                  BY ProcessingStart.processingTime()</td>
                </tr>
              </tbody>
            </table></td>
        </tr>

        <tr>
          <td><img src="../img/WS2_RDM_ProcEnd.svg" /></td>

          <td><table border="1">
              <thead>
                <tr>
                  <th>ON pe:ProcessingEnd</th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>ws : WorkStation<br />ws := pe.workStation</td>
                </tr>

                <tr>
                  <td>ws.waitingParts.dequeue()<br />IF ws.waitingParts.length
                  = 0 <br />THEN ws.status := AVAILABLE</td>
                </tr>

                <tr>
                  <td>IF ws.waitingParts.length &gt; 0 <br />THEN SCHEDULE
                  ProcessingStart( workStation:=ws)</td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table></section></body>
</html>
