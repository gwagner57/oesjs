<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Introduction</title>
  </head>

  <body><p>Object Event (OE) Modeling and Simulation (M&amp;S) is a new
  general Discrete Event Simulation (DES) paradigm based on the two most
  important ontological categories: <strong>objects</strong> and
  <strong>events</strong>. In philosophy, objects have also been called
  <em>endurants</em> or <em>continuants</em>, while events have also been
  called <em>perdurants</em> or <em>occurrents</em>.</p><p>OEM&amp;S combines
  <em>Object-Oriented</em> (OO) Modeling with the <em>event scheduling</em>
  paradigm of <em>Event Graphs</em> (<a
  href="https://dl.acm.org/citation.cfm?id=358460">Schruben 1983</a>). The
  relevant <i>object types</i> and <i>event types</i> are described in an
  information model, which is the basis for making a process model. A modeling
  approach that follows the OEM paradigm is called an <i>OEM approach</i>.
  Such an approach needs to choose, or define, an information modeling
  language (such as <em>Entity Relationship Diagrams</em> or <em>UML Class
  Diagrams</em>) and a process modeling language (such as <em>UML Activity
  Diagrams</em> or <em>BPMN Process Diagrams</em>).</p><p>We propose an OEM
  approach based on <em>UML Class Diagrams</em> for conceptual information
  modeling and information design modeling, as well as <em>DPMN Process
  Diagrams</em> for conceptual process modeling and for process design
  modeling.</p><p>In the proposed approach, object types and event types are
  modeled as special categories of classes in a UML Class Diagram. <i>Random
  variables</i> are modeled as a special category of class-level operations
  constrained to comply with a specific probability distribution such that
  they can be implemented as static methods of a class. <i>Queues</i> are not
  modeled as objects, but rather as ordered association ends, which can be
  implemented as collection-valued reference properties. Finally, <i>event
  rules</i>, which include <i>event routines</i>, are modeled in DPMN process
  diagrams (and possibly also in pseudo-code), such that they can be
  implemented in the form of special <i>onEvent</i> methods of event
  classes.</p><p>Like Petri Nets and DEVS, OEM&amp;S has a formal semantics.
  But while Petri Nets and DEVS are abstract computational formalisms without
  an ontological foundation, OEM&amp;S is based on the ontological categories
  of objects, events and causal regularities.</p><p>An OEM approach results in
  a simulation design model that has a well-defined operational semantics in
  terms of a transition system based on the event rules modeled in a DPMN
  process design diagram, as shown by Wagner (<a
  href="https://www.informs-sim.org/wsc17papers/includes/files/056.pdf">2017a</a>).
  Such a model can, in principle, be implemented with any object-oriented (OO)
  simulation technology. However, a straightforward implementation can only be
  expected from a technology that implements the OEM&amp;S paradigm, such as
  the <a href="https://gwagner57.github.io/oes/js/index.html">OES JavaScript
  (OESjs)</a> framework.</p><p>Both <em>conceptual models</em> for DES and
  <em>DES design models</em> consist of (1) an <em>information model</em> and
  (2) a <em>process model</em>. In the case of conceptual modeling, an
  information model describes the types of objects and events representing the
  main entities of the real-world system under investigation, while a process
  model describes its dynamics in the form of a set of <em>conceptual event
  rule models</em> that capture the <em>causal regularities</em> of the
  system. </p><p>In the case of simulation design modeling, an <em>information
  design model</em> defines the types of all objects and events that are
  relevant for the purpose of a simulation study, thus defining the state
  structure of a DES system, while a <em>process design model</em> defines the
  dynamics of a DES system by defining, for all event types of the underlying
  information model, an <em>event rule design model</em> that specifies the
  state changes and follow-up events implied by the occurrence of an event of
  that type. </p><p>In the first part of this article series, (<a
  href="../Bibliography.html#Wagner2018b">Wagner 2018b</a>), we have
  introduced a variant of the <em>Business Process Modeling Notation
  (BPMN)</em>, called <em>Discrete Event Process Modeling Notation
  (DPMN)</em>, and have shown how to use UML Class Diagrams and DPMN Process
  Diagrams for making basic OE models defining a set of object types
  <i>OT</i>, a set of event types <i>ET</i>, and a set of event rules
  <i>R</i>. In (Wagner 2017a), we have shown that (a) these three sets define
  a state transition system, where the state space is defined by <i>OT</i> and
  <i>ET</i>, and the transitions are defined by <i>R</i>, and (b) such a
  transition system represents an <em>Abstract State Machine</em> in the sense
  of Gurevich (1985). This fundamental characterization of an OE model
  provides a formal (operational) semantics for OE Simulation (OES) by
  defining an <em>OES formalism</em> that any OE simulator has to
  implement.</p><p>In this second part, we extend basic OEM/DPMN in two steps
  by adding support for (1) <em>resource-constrained activities</em> and (2)
  GPSS/SIMAN/Arena-style <em>processing activities</em> and <em>processing
  networks (PNs)</em>. </p><p>Modeling resource-constrained activities has
  been a major issue in DES since its inception in the nineteen-sixties, while
  it has been neglected and is still considered an advanced topic in the field
  of Business Process Modeling (BPM). BPMN only provides partial support for
  modeling resource-constrained activities. It allows assigning resources to
  activities, but it does not allow modeling <em>resource pools</em>, and it
  does neither allow specifying <em>resource multipliclity constraints</em> nor
  <em>parallel participation multiplicity
  constraints</em>.</p><p><em>Processing objects</em> enter PNs via
  <em>arrival events</em> at an <em>entry node</em> and then flow through one
  or more <em>processing nodes</em> where they are subject to <em>processing
  activities</em> before they leave the system at an <em>exit node</em> via a
  <em>departure event</em>. The first extension, OEM/DPMN-A, comprises five
  new information modeling categories ("stereotypes") and one new process
  modeling element, while the second extension, OEM/DPMN-PN, comprises a set
  of four pre-defined object types and three pre-defined event types, three
  new (node type) categories and one new process modeling element, as listed
  in tables in <a href="../Appendix.html"></a>.</p></body>
</html>
