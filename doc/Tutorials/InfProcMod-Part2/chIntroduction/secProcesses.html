<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Discrete Processes and Business Processes</title>
  </head>

  <body><p>A <strong>discrete process</strong> (DP), or <em>discrete event
  process</em>, consists of a partially ordered set of <strong>events</strong>
  such that each of them causes zero or more discrete state changes of
  affected <b>objects</b>. When two or more events within such a process have
  the same order rank, this means that they occur simultaneously. A discrete
  process may be an instance of a <em>discrete process type</em> defined by a
  <strong>discrete process model</strong>.</p><p>A <strong>business
  process</strong> (BP) is a discrete process that involves
  <strong>activities</strong> performed by <em>organizational agents</em> qua
  one of their <em>organizational roles</em> defined by their
  <em>organizational position</em>. Typically, a business process is an
  instance of a business process type defined by an organization or
  organizational unit (as the owner of the business process type) in the form
  of a <strong>business process model</strong>. </p><p>While there are DPs
  that do not have an organizational context (like, for instance, message
  exchange processes in digital communication networks or private
  conversations among human agents), a BP always happens in the context of an
  organization.</p><p>When a person, as an organizational agent, performs an
  activity within a BP, the person is a <strong>resource</strong> (called
  <em>performer</em> in BPMN) and the type of activity is
  <strong>resource-constrained</strong>. When a person performs an activity
  within a discrete process that is not a BP (e.g., laughing in a
  conversation), the person is the performer of the activity, but not a
  resource of it. Consequently, while <em>all business activity types are
  resource-constrained</em>, there are also activity types that are not
  resource-constrained.</p><p>The performance of a resource-constrained
  activity is constrained by the availability of the required resources, which
  may include <em>human resources</em> or other (passive) resource objects,
  such as rooms or devices. For providing the resources required for
  performing its business processes, an organization has <strong>resource
  pools</strong>. </p><p>The dependency of an activity on resources is modeled
  with the help of <strong>resource roles</strong>, which are special
  properties of the activity type. A resource role can be defined in a UML
  class model in the form of an association between the class representing the
  activity type and the class representing the resource object type. The
  multiplicity of the association end at the side of the resource object type
  defines <strong>resource cardinality constraints</strong>, while the
  multiplicity of the association end at the side of the activity type defines
  a <strong>multitasking constraint</strong>.</p><p>There are two kinds of
  business process models: </p><ol>
      <li><p>BPMN-style <strong>Activity Networks</strong> (ANs) consisting of
      <em>event nodes</em> and <em>activity nodes</em> (with task queues)
      connected by means of <strong>event scheduling</strong> arrows pointing
      to event nodes and <strong>resource-dependent activity scheduling
      (RDAS)</strong> arrows pointing to activity nodes, such that event and
      activity nodes may be associated with objects representing their
      participants. In the case of an activity node, these participating
      objects include the resources required for performing an activity.
      Typically, an activity node is associated with a particular resource
      object representing the activity <strong>performer</strong>.</p></li>

      <li><p>GPSS/Arena-style <strong>Processing Networks</strong> (PNs)
      consisting of <em>entry nodes</em>, <em>processing nodes</em> (with task
      queues and input buffers) and <em>exit nodes</em> connected by means of
      <strong>processing flow</strong> arrows, which combine an <abbr
      title="Resource-Dependent Activity Scheduling">RDAS</abbr> arrow with an
      <strong>object flow</strong> arrow. The PN concept is a conservative
      extension of the AN concept, that is, a PN is a special type of AN.
      </p></li>
    </ol><p>While in <abbr title="Processing Network">AN</abbr>s, there is
  only a flow of events (including activities), in <abbr
  title="Processing Network">PN</abbr>s, this flow of events is combined with
  a flow of <strong>processing objects</strong> (often called “entities").
  Correspondingly, while the activity nodes of an <abbr
  title="Processing Network">AN</abbr> only have a <em>task queue</em> (a
  queue of planned activities), the processing nodes of a <abbr
  title="Processing Network">PN</abbr> have both a task queue and a
  corresponding queue of processing objects (waiting to be
  processed).</p><p>In an <abbr title="Processing Network">AN</abbr>, all
  activity nodes have a task queue filled with tasks (or planned activities)
  waiting for the availability of the required resources. An <abbr
  title="Resource-Dependent Activity Scheduling">RDAS</abbr> arrow from an AN
  node to a successor activity node expresses the fact that a corresponding
  activity end event (or plain event) triggers the deferred scheduling of a
  successor activity start event, corresponding to the creation of a new task
  in the task queue of the successor activity node. </p>A <strong>workflow
  model</strong> is an AN model that only involves performer resources
  (typically human resources). Examples of industries with workflow processes
  are insurance, finance (including banks) and public administration. Most
  other industries, such as manufacturing and health care, have business
  processes that also involve non-performer resources or physical processing
  objects, such as manufacturing parts or patients. <p>A <strong>PN
  process</strong> is a business process that involves one or more
  <em>processing objects</em> and includes <em>arrival events</em>,
  <em>processing activities</em> and <em>departure events</em>. An arrival
  event for one or more <em>processing objects</em> happens at an <em>entry
  station</em>, from where they are routed to a <em>processing station</em>
  where <em>processing activities</em> are performed on them, before they are
  routed to another processing station or to an <em>exit station</em> where
  they leave the system via a departure event.</p><p>A <strong>PN process
  model</strong> defines a <abbr title="Processing Network">PN</abbr> where
  each node represents a combination of a spatial object and an event or
  activity variable:</p><ol>
      <li>Defining an <strong>entry node</strong> means defining both an
      <em>entry station</em> object (e.g., a reception area or a factory
      entrance) and a variable representing <em>arrival</em> events for
      arriving <em>processing objects</em> (such as people or manufacturing
      parts).</li>

      <li>Defining a <strong>processing node</strong> means defining both a
      <em>processing station</em> object (often used as a resource object,
      such as a workstation or a room) and a variable representing
      <em>processing activities</em>.</li>

      <li>Defining an <strong>exit node</strong> means defining both an
      <em>exit station</em> object and a variable representing
      <em>departure</em> events.</li>
    </ol><p>In a <abbr title="Processing Network">PN</abbr>, all processing
  nodes have a task queue and an input buffer filled with processing objects
  that wait to be processed. A <abbr title="Processing Network">PN</abbr>
  where all processing activities have exactly one abstract resource (often
  called a "server") is also known as a <strong>Queuing Network</strong> in
  <em>Operations Research</em> where processing nodes are called "servers" and
  processing objects are called "customers" or "jobs" (while they are called
  "entities" in Arena).</p><p>For accommodating resource-constrained
  activities and Processing Networks, basic OEM and DPMN are extended in two
  steps. The first extension, OEM/DPMN-AN, comprises four new information
  modeling elements (activity types, resource roles, resource cardinality
  constraints, multitasking constraint, and resource pools) and one new
  process modeling element (<abbr
  title="Resource-Dependent Activity Scheduling">RDAS</abbr> arrows), while
  the second extension, OEM/DPMN-PN, comprises a set of four pre-defined
  object type categories (processing objects, entry stations, processing
  stations, exit stations), two pre-defined event type categories (arrival
  events, departure events), one activity type category (processing
  activities), three node type categories (entry nodes, processing nodes, exit
  nodes) and one new process modeling element (object flow arrows).</p></body>
</html>
