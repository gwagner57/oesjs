<!DOCTYPE html>
<html class="role-ebook-page" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="UTF-8" />

    <title>Object Event Simulation</title>
  </head>

  <body><p>The Object Event Simulation (OES) paradigm is an extension of the
  <em>Event-Based Simulation (ES)</em> paradigm pioneered by SIMSCRIPT
  (Markowitz, Hausner &amp; Karr 1962) and later formalized by Event Graphs
  (Schruben 1983). Essentially, OES extends ES, or Event Graphs, by adding the
  modeling concepts of <em>objects</em> and <em>event rules</em>.
  </p><p>Starting with an initial simulation state, an OE model is executed by
  successively applying the event rules of the model to the evolving
  simulation states.</p><figure id="figActIsEvt"><figcaption>A model of the
  core classes of individuals an OE simulator has to deal with at
  runtime.</figcaption><div><img
  alt="core class model for object event simulators"
  src="../img/Activity-isA-Event.svg" width="450" /></div></figure><p>Notice
  that the <em>occurrence time</em> of an activity is the time when it
  completes, that is, it is equal to <i>startTime</i> + <i>duration</i>.
  Typically, the duration of an activity in a simulation run is known, and
  set, when it is started. An activity type is normally defined with a fixed
  duration or a random variable duration for all activities of that type. This
  allows a simulator to schedule the activity's end event when the activity is
  started. However, in certain cases, an activity type may not define a preset
  duration, but leave the duration of activities of that type open. When such
  an activity is still ongoing, it does only have a start time, but no
  duration and no occurrence time.</p><section><h4>The OES
  formalism</h4><p>The OEM&amp;S paradigm is based on the OES formalism
  presented in (Wagner 2017a), which is summarized below. </p>Both <em>object
  types</em> and <em>event types</em> are defined in the form of classes: with
  a name, a set of properties and a set of operations, which together define
  their signature. A <em>property</em> is essentially defined by a name and a
  <em>range</em>, which is either a datatype (like Integer or String) or
  another object type.<p>A set of object types <i>OT</i> defines a
  <em>predicate-logical signature</em> as the basis of a logical expression
  language <i>L<sub>OT</sub></i>: each object type defines a unary predicate,
  and its properties define binary predicates. A state change language
  <i>C<sub>OT</sub></i> based on <i>OT</i> defines state change statements
  expressed with the help of the object type names and property names defined
  by <i>OT</i>. In the simplest case, state change statements are property
  value assignments like <i>o</i>.p<sub>1</sub> := 4 or <i>o</i>.p<sub>1</sub>
  := <i>o</i>.p<sub>2</sub> where <i>o</i> is an object variable and
  p<sub>1</sub>, p<sub>2</sub> are property names.</p><p>A set of objects
  <i><u>O</u></i> = {o<sub>1</sub>, o<sub>2</sub>, ...o<sub>n</sub>} where
  each of them has a state in the form of a set of <em>slots</em>
  (property-value pairs) represents a <em>system state</em>, that is a state
  of the real-world system being modeled and simulated. A system state
  <i><u>O</u></i> can be updated by a set of state changes (or, more
  precisely, state change statements) <i>Δ</i> ⊆ <i>C<sub>OT</sub></i> with
  the help of an update operation Upd. For instance, for a system state
  <i><u>O</u></i><sub>1</sub> = {o<sub>1</sub>} with o<sub>1</sub> = {
  p<sub>1</sub>: 2, p<sub>2</sub>: 5} and a set of state changes
  <i>Δ</i><sub>1</sub> = { o<sub>1</sub>.p<sub>1</sub> :=
  o<sub>1</sub>.p<sub>2</sub> } we obtain</p><div
  style="text-align:center;">Upd<i>( <u>O</u></i><sub>1</sub>,
  <i>Δ</i><sub>1</sub>) = {{ p<sub>1</sub>: 5, p<sub>2</sub>: 5}}</div><p>An
  <i>event expression</i> is a term E<i>(<u>x</u>)@t</i> where</p><ol>
      <li>E is an event type,</li>

      <li><i><u>x</u></i> is a (possibly empty) list of event parameters
      <i>x<sub>1</sub></i>, <i>x<sub>2</sub></i>, …, <i>x<sub>n</sub></i>
      according to the signature of the event type E,</li>

      <li><i>t</i> is a parameter for the occurrence time of events.</li>
    </ol><p>For instance, PartArrival(<i>ws</i>)@<i>t</i> is an event
  expression for describing part arrival events where the event parameter
  <i>ws</i> is of type WorkStation, and <i>t</i> denotes the arrival time. An
  individual event of type E is a <i>ground</i> event expression, <i>e</i> =
  E(<i><u>v</u></i>)@<i>i</i>, where the event parameter list <i><u>x</u></i>
  and the occurrence time parameter <i>t</i> have been instantiated with a
  corresponding value list <i><u>v</u></i> and a specific time instant
  <i>i</i>. For instance, PartArrival(ws1)@1 is a ground event expression
  representing an individual PartArrival event occurring at workstation ws1 at
  time 1.</p>A <em>Future Events List (FEL)</em> is a set of ground event
  expressions partially ordered by their occurrence times, which represent
  future time instants either from a discrete or a continuous model of time.
  The partial order implies the possibility of simultaneous events, as in the
  example {ProcessingEnd(ws1)@4, PartArrival(ws1)@4}.<p>An <i>event
  routine</i> is a procedure that essentially computes state changes and
  follow-up events, possibly based on conditions on the current state. In
  practice, state changes are often directly performed by immediately updating
  the objects concerned, and follow-up events are immediately scheduled by
  adding them to the FEL. For the OES formalism, we assume that an event
  routine is a pure function that computes state changes and follow-up events,
  but does not apply them, as illustrated by the examples in the following
  table.</p><table border="1" id="tblEvtRules">
      <colgroup style="text-align:center;"></colgroup>

      <colgroup></colgroup>

      <colgroup></colgroup>

      <thead>
        <tr>
          <td style="vertical-align:top;"><p><b>Event rule name / rule
          variables</b></p></td>

          <td style="text-align:center;"><p><b>ON (event
          expression)</b></p></td>

          <td style="text-align:center;"><p><b>DO (event routine)</b></p></td>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td><p>r<i><sub>PA</sub> </i></p><p><i>a</i>:
          PartArrival<i><br />ws</i>: WorkStation<br /><i>ws</i> :=
          <i>a</i>.workStation</p></td>

          <td><p>PartArrival(<i>ws</i>) @ <i>t</i></p></td>

          <td><p><i>Δ</i> := { <i>ws</i>.waitingParts.push(
          <i>a</i>.part)}<br /></p><p>IF <i>ws</i>.status = AVAILABLE
          <br />THEN <i>FE</i> :=
          {ProcessingStart(<i>ws</i>)@<i>t</i>+1}<br />ELSE <i>FE</i> :=
          {}</p><p>RETURN ⟨ <i>Δ</i>, <i>FE</i> ⟩</p></td>
        </tr>

        <tr>
          <td><p>r<i><sub>PS</sub></i> </p><i>ps</i>:
          ProcessingStart<i><br />ws</i>: WorkStation<br /><i>ws</i> :=
          <i>ps</i>.workStation</td>

          <td><p>ProcessingStart(<i>ws</i>) @ <i>t</i></p></td>

          <td><p><i>Δ</i> := { ws.status := BUSY}</p><p><i>FE</i> :=
          {ProcessingEnd(<i>ws</i>)@<i>t +
          ProcessingStart.processingTime()</i>}</p><p>RETURN ⟨ <i>Δ</i>,
          <i>FE</i> ⟩ </p></td>
        </tr>

        <tr>
          <td><p>r<i><sub>PE</sub></i> </p><i>pe</i>:
          ProcessingEnd<i><br />ws</i>: WorkStation<br /><i>ws</i> :=
          <i>pe</i>.workStation</td>

          <td><p>ProcessingEnd(<i>ws</i>) @ <i>t</i></p></td>

          <td><p><i>Δ</i> := { ws.waitingParts.pop()}<br />IF
          ws.waitingParts.length = 0 <br />THEN <i>Δ</i> := <i>Δ</i> ∪
          {ws.status := AVAILABLE}</p><p>IF ws.waitingParts.length &gt; 0
          <br />THEN <i>FE</i> := {
          ProcessingStart(<i>ws</i>)@<i>t</i>+1}<br />ELSE <i>FE</i> :=
          {}</p><p>RETURN ⟨ <i>Δ</i>, <i>FE</i> ⟩ </p></td>
        </tr>
      </tbody>
    </table><p>An <b><i>event rule</i></b> associates an event expression with
  an <i>event routine F</i>:</p><div style="text-align:center;"><b>ON</b>
  E<i>(<u>x</u>)@t</i> <b>DO</b> <i>F( t, <u>x</u>)</i>,</div><p>where the
  event expression E<i>(<u>x</u>)@t</i> specifies the type E of events that
  trigger the rule, and <i>F( t, <u>x</u>)</i> is a function call expression
  for computing a set of <i>state changes</i> and a set of <i>follow-up
  events</i>, based on the event parameter values <i><u>x</u></i>, the event's
  occurrence time <i>t</i> and the current system state, which is accessed in
  the event routine <i>F</i> for testing conditions expressed in terms of
  state variables.</p><p>An <em>OE model</em> based on a state change language
  <i>C<sub>OT</sub></i> and a corresponding update operation Upd is a triple
  ⟨<i>OT, ET, R</i>⟩, consisting of a set of object types <i>OT</i>, event
  types <i>ET</i> and event rules <i>R</i>.</p>An <em>OE</em> <i>simulation
  (system) state</i> based on an OE model ⟨<i>OT, ET, R</i>⟩ is a triple
  <i>S</i> = ⟨<i>t, <u>O</u>, <u>E</u></i>⟩ with <i>t</i> being the current
  simulation time, <i><u>O</u></i> being a <em>system state</em> (a set of
  objects instantiating types from <i>OT</i>), and <i><u>E</u></i> being a set
  of <em>imminent</em> events to occur at times greater than <i>t</i> (and
  instantiating types from <i>ET</i>), also called <em>Future Event List
  (FEL)</em>.<p>An event rule <i>r</i> = <b>ON</b> E<i>(<u>x</u>)@t</i>
  <b>DO</b> <i>F( t, <u>x</u>)</i> can be considered as a 2-step function
  that, in the first step, maps an event <i>e</i> =
  E(<i><u>v</u></i>)@<i>i</i> to a parameter-free state change function
  <i>r<sub>e</sub></i> = <i>F( i, <u>v</u>)</i>, which maps a system state
  <i><u>O</u></i> to a pair ⟨ <i>Δ</i>, <i>FE</i> ⟩ of system state changes
  <i>Δ</i> ⊆ <i>C<sub>OT</sub></i> and follow-up events <i>FE</i>. When the
  parameters <i>t</i> and<i> <u>x</u></i> of <i>F( t, <u>x</u>)</i> are
  replaced by the values <i>i</i> and <i><u>v</u></i> provided by a ground
  event expression E(<i><u>v</u></i>)@<i>i</i>, we also simply write
  <i>F<sub>i,<u>v</u></sub></i> instead of <i>F( i, <u>v</u>)</i> for the
  resulting parameter-free state change function.</p><p>We say that an event
  rule <i>r</i> is <i>triggered</i> by an event <i>e</i> when the event's type
  is the same as the rule's event type. When <i>r</i> is triggered by
  <i>e</i>, we can form the state change function <i>r<sub>e</sub></i> =
  <i>F<sub>i,<u>v</u></sub></i> and apply it to a system state <i><u>O</u></i>
  by mapping it to a set of state changes and a set of follow-up
  events:</p><div style="text-align:center;"><i>r<sub>e</sub>(<u>O</u>)</i> =
  <i>F<sub>i,<u>v</u></sub>(<u>O</u>)</i> = ⟨ <i>Δ</i>, <i>FE</i> ⟩</div><p>We
  can illustrate this with the help of our workstation example. Consider the
  rule r<i><sub>PA</sub></i> defined in the table above triggered by the event
  PartArrival(ws1)@1 in state <i><u>O</u></i><sub>0</sub> = {ws1.status:
  AVAILABLE, ws1.waitingParts: []}. We obtain</p><div
  style="text-align:center;"><i>r<sub>PA</sub>( <u>O</u><sub>0</sub> )</i> =
  <i>F</i><sub>1,ws1</sub><i>( <u>O</u><sub>0</sub> )</i> = ⟨
  <i>Δ</i><sub>1</sub>, <i>FE</i><sub>1</sub> ⟩ </div><p>with
  <i>Δ</i><sub>1</sub> = { ws1.waitingParts.push( <i>a</i>.part)} and
  <i>FE</i><sub>1</sub> = {ProcessingStart@2}.</p><p>An OE model defines a
  <em>state transition system</em> where</p><ol>
      <li><p>A state is a simulation state <i>S</i> = ⟨<i>t, <u>O</u>,
      <u>E</u></i>⟩.</p></li>

      <li><p>A transition of a simulation state <i>S</i> consists of</p><ol>
          <li><p>advancing <i>t</i> to the occurrence time <i>t'</i> of the
          next events <i>NE</i> ⊆ <i><u>E</u></i>, which is the set of all
          imminent events with minimal occurrence time;</p></li>

          <li><p>processing all next events <i>e</i> ∈ <i>NE</i> by applying
          the event rules <i>r</i> ∈ <i>R</i> triggered by them to the current
          system state <i><u>O</u></i> according to</p><div
          style="text-align:center;"><i>r<sub>e</sub></i>( <i><u>O</u></i> ) =
          ⟨ <i>Δ<sub>e</sub> , FE<sub>e</sub></i> ⟩</div><p>resulting in a set
          of state changes <i>Δ</i> = ∪ {<i>Δ<sub>e</sub></i> | <i>e</i> ∈
          <i>NE</i> } and a set of follow-up events <i>FE</i> = ∪
          {<i>FE<sub>e</sub></i> | <i>e</i> ∈ <i>NE</i> }.</p></li>
        </ol><p>such that the resulting successor simulation state is
      <i>S'</i> = ⟨<i> t', <u>O</u>', <u>E</u>'</i> ⟩ with <i><u>O</u>'</i> =
      Upd( <i><u>O</u></i>, <i>Δ</i>) and <i><u>E</u>'</i> = <i><u>E</u></i> −
      <i>NE</i> ∪ <i>FE</i>.</p></li>
    </ol><p>Notice that the OES formalism first collects all state changes
  brought about by all the simultaneous next events (from the set <i>NE</i>)
  of a simulation step before applying them. This prevents the state changes
  brought about by one event from <i>NE</i> to affect the application of event
  rules for other events from <i>NE</i>, thus avoiding the problem of
  non-determinism through the potential non-confluence (or
  non-serializability) of parallel events.</p><p>OE simulators are computer
  programs that implement the OES formalism. Typically, for performance
  reasons, discrete event simulators do not first collect all state changes
  brought about by all the simultaneous next events (the set <i>NE</i>) of a
  simulation step before applying them, but rather apply them immediately in
  each triggered event routine. However, this approach takes the risk of an
  unreliable semantics of certain simulation models in favor of performance.
  </p></section><section><h4>OESjs – a JavaScript-based OE simulator
  </h4><p>The OESjs simulator presented in (Wagner 2017b) implements the OES
  formalism by implementing (1) object types as classes extending the
  pre-defined class <code>oBJECT</code>, (2) event types as classes extending
  the pre-defined class <code>eVENT</code>, and (3) event rules as
  <code>onEvent</code> methods of event classes. </p><p>The OESjs simulator is
  available from the educational simulation website <a
  href="https://sim4edu.com">sim4edu.com</a>.</p></section></body>
</html>
