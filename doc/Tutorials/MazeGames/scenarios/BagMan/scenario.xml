<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="prettyprint.xsl"?>
<SimulationScenario xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://aor-simulation.org ../../../../../../AOR-Complete/trunk/AORSL/AORSL_0-9.xsd"
 xmlns="http://aor-simulation.org"
 xmlns:dc="http://purl.org/dc/elements/1.1/"
 xmlns:aors="http://aor-simulation.org"
 createSimulationLog="true"
 scenarioName="BagMan"
 version="0.9">
    <documentation>
        <dc:creator>Simulario UG</dc:creator>
        <dc:created>3 December 2011</dc:created>
        <dc:title>BagMan</dc:title>
        <CcLicense license="CC BY-SA"/>
        <dc:subject>Maze Game - reconstruction of the classic PacMan game</dc:subject>
        <aors:shortDescription xmlns="http://www.w3.org/1999/xhtml">
          Reconstruction of the classical <a href="http://en.wikipedia.org/wiki/Pac-Man">PacMan</a> arcade game. 
        	You play BagMan who has to collect all the food pills in the maze while trying to avoid a "meeting" 
        	with any of the ghosts Inky and Blinky that will try to kill you. 
        </aors:shortDescription>
        <aors:shortDescription xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
          Reconstruction of the classical <a href="http://en.wikipedia.org/wiki/Pac-Man">PacMan</a> arcade game. 
          You play BagMan who has to collect all the food pills in the maze while trying to avoid a "meeting" 
    		with any of the ghosts Inky and Blinky that will try to kill you. 
        </aors:shortDescription>
    </documentation>
    <SimulationParameters simulationSteps="100000" stepTimeDelay="50" createDebuggingOutput="true"/>
    <SimulationModel modelName="BagMan" modelTitle="Pac Man Classic Arcade Game Clone">        
        <SpaceModel geometry="Euclidean">
            <TwoDimensionalGrid xMax="9" yMax="7">
                <!-- determine the cell role (0 = free cell containing food, 1 = cell has wall role, -1 = free cell witout food) -->
                <GridCellProperty name="cellRole" type="Integer" />
            </TwoDimensionalGrid>
        </SpaceModel>
        
        <DataTypes>
            <Enumeration name="Direction">
                <EnumerationLiteral>NONE</EnumerationLiteral>
                <EnumerationLiteral>SOUTH</EnumerationLiteral>
                <EnumerationLiteral>NORTH</EnumerationLiteral>
                <EnumerationLiteral>EAST</EnumerationLiteral>
                <EnumerationLiteral>WEST</EnumerationLiteral>
            </Enumeration>
        </DataTypes>
        <Globals>
            <GlobalVariable dataType="Integer" name="score" />
            <GlobalVariable dataType="Integer" name="passedTime" />
            <GlobalVariable dataType="Integer" name="startTime" />
            <GlobalVariable dataType="Integer" initialValue="3" maxValue="5" minValue="1"
                name="level" />
            <GlobalVariable name="remainingPillFoodNumber" dataType="Integer"/>
            <GlobalFunction name="getGameMap" resultType="Array">
                <Body language="JavaScript">
                    <![CDATA[
                        var gameMap = [ 
                            [1, 1, 1, 1, 1, 1, 1, 1, 1],
                            [1, 0, 0, 0, 0, 0, 0, 0, 1],
                            [1, 0, 1, 1,-1, 1, 1, 0, 1],
                            [1, 0, 1,-1,-1,-1, 1, 0, 1],
                            [1, 0, 1, 1, 1, 1, 1, 0, 1],
                            [1, 0, 0, 0, 0, 0, 0, 0, 1],
                            [1, 1, 1, 1, 1, 1, 1, 1, 1]
                        ];
                        
                        return gameMap;
                    ]]>
                </Body>
            </GlobalFunction>
            <GlobalFunction name="getNumberOfFoodCells" resultType="int">
                <Body language="JavaScript">
                    <![CDATA[
                        var result = 0;
                        var map = Global.getGameMap();
                        var n = map.length;
                        var m = (n > 0 ? map[0].length : 0);
                        for(var i = 0; i < n; i++) {
                            for(var j = 0; j < m; j++) {
                                if(map[i][j] === 0) {
                                    result++;
                                }
                            }
                        }
                        return result;
                    ]]>
                </Body>
            </GlobalFunction>
            <GlobalFunction name="isWallOnCoordinates" resultType="boolean">
                <Parameter name="x" type="int" />
                <Parameter name="y" type="int" />
                <Body language="JavaScript">
                    <![CDATA[
                        var map = Global.getGameMap();
                        var rows = map.length;
                        
                        return map[rows - y][x-1] === 1;
                    ]]>
                </Body>
            </GlobalFunction>
        </Globals>
        <EntityTypes>
            <CausedEventType name="Start"/>
            <!-- The Event that will let the environment knows that the BagMan must move -->
            <ActionEventType name="MoveBagMan">
                <!-- The moving X increment/decrement -->
                <Attribute name="x" type="Integer" />
                <!-- The moving Y increment/decrement -->
                <Attribute name="y" type="Integer" />
            </ActionEventType>
            <!-- The event is triggered when the BagMan eats a PillFood -->
            <ActionEventType name="EatPillFood" />
            <!-- The event is triggered when the BagMan game starts -->
            <ExogenousEventType name="StartGame" />
            <!-- The event is triggered when the BagMan dies -->
            <ActionEventType name="BagManDies" />
            <!-- The event is triggered when the BagMan wins the game - eated all PillFoods -->
            <ActionEventType name="WinGame" />
            <!-- The Lord of the simulation...BacMan -->
            <PhysicalAgentType name="BagMan">
                <!-- flag used to determine if the BagMan has the mouth open or closed -->
                <Attribute initialValue="false" name="mouthOpen" type="Boolean" />
                <!-- the current moving direction -->
                <EnumerationProperty name="movingDirection" type="Direction" />
                <!-- Action rule invoked when the user decides to move to east -->
                <ActionRule agentVariable="bagman" name="MoveEast_ActionRule">
                    <IF language="JavaScript">
                        <![CDATA[
                            !Global.isWallOnCoordinates(this.bagman.getX() + 1, this.bagman.getY())
                        ]]>
                    </IF>
                    <THEN>
                        <UPDATE-AGT>
                            <Slot property="movingDirection" value="Direction.EAST"/>
                        </UPDATE-AGT>
                        <SCHEDULE-EVT>
                            <ActionEventExpr actionEventType="MoveBagMan">
                                <Slot property="x" value="1" />
                                <Slot property="y" value="0" />
                            </ActionEventExpr>
                        </SCHEDULE-EVT>
                    </THEN>
                </ActionRule>
                <!-- Action rule invoked when the user decides to move to west -->
                <ActionRule agentVariable="bagman" name="MoveWest_ActionRule">
                    <IF language="JavaScript">
                        <![CDATA[
                           !Global.isWallOnCoordinates(this.bagman.getX() - 1, this.bagman.getY())
                        ]]>
                    </IF>
                    <THEN>
                        <UPDATE-AGT>
                            <Slot property="movingDirection" value="Direction.WEST" />
                        </UPDATE-AGT>
                        <SCHEDULE-EVT>
                            <ActionEventExpr actionEventType="MoveBagMan">
                                <Slot property="x" value="-1" />
                                <Slot property="y" value="0" />
                            </ActionEventExpr>
                        </SCHEDULE-EVT>
                    </THEN>
                </ActionRule>
                <!-- Action rule invoked when the user decides to move to North -->
                <ActionRule agentVariable="bagman" name="MoveNorth_ActionRule">
                    <IF language="JavaScript">
                    <![CDATA[
                        !Global.isWallOnCoordinates(this.bagman.getX(), this.bagman.getY() + 1)
                    ]]>
                    </IF>
                    <THEN>
                        <UPDATE-AGT>
                            <Slot property="movingDirection" value="Direction.NORTH" />
                        </UPDATE-AGT>
                        <SCHEDULE-EVT>
                            <ActionEventExpr actionEventType="MoveBagMan">
                                <Slot property="x" value="0" />
                                <Slot property="y" value="1" />
                            </ActionEventExpr>
                        </SCHEDULE-EVT>
                    </THEN>
                </ActionRule>
                <!-- Action rule invoked when the user decides to move to South -->
                <ActionRule agentVariable="bagman" name="MoveSouth_ActionRule">
                    <IF language="JavaScript">
                    <![CDATA[
                        !Global.isWallOnCoordinates(this.bagman.getX(), this.bagman.getY() - 1)
                    ]]>
                    </IF>
                    <THEN>
                        <UPDATE-AGT>
                            <Slot property="movingDirection" value="Direction.SOUTH" >
                                <ValueExpr language="JavaScript" logPeriodicity="1" ></ValueExpr>
                            </Slot>
                        </UPDATE-AGT>
                        <SCHEDULE-EVT>
                            <ActionEventExpr actionEventType="MoveBagMan">
                                <Slot property="x" value="0" ><ValueExpr language="JavaScript" logPeriodicity="1" ></ValueExpr></Slot>
                                <Slot property="y" value="-1" />
                            </ActionEventExpr>
                        </SCHEDULE-EVT>
                    </THEN>
                </ActionRule>
            </PhysicalAgentType>
            <!-- The type that represents the BagMan's enemy Ghost -->
            <PhysicalAgentType name="Ghost">
                <!-- the current moving direction -->
                <EnumerationProperty name="movingDirection" type="Direction" />
                <Function name="getXIncrement" resultType="int">
                    <Parameter name="bagManX" type="int" />
                    <Parameter name="bagManY" type="int" />
                    <Body language="JavaScript">
                        <![CDATA[
                            var astar = new js.algorithms.ASTAR(Global.getGameMap(), [0, -1]);
                            var currentX = this.getX() - 1;
                            var currentY = this.getY() - 1;
                            var start = {
                                x: currentX, 
                                y: currentY
                            };
							
							var end = {
								x: bagManX - 1, 
								y: bagManY - 1
							};
							var result = astar.search(start, end);
							
							if(result.length < 1) {
							     return 0;
							} else {
							     var firstNodeFromPath = result[0];
							     return (firstNodeFromPath.x - currentX)
							}
                        ]]>
                    </Body>
                </Function>
                <Function name="getYIncrement" resultType="int">
                    <Parameter name="bagManX" type="int" />
                    <Parameter name="bagManY" type="int" />
                    <Body language="JavaScript">
                        <![CDATA[
                            var astar = new js.algorithms.ASTAR(Global.getGameMap(), [0, -1]);
                            var currentX = this.getX() - 1;
                            var currentY = this.getY() - 1;
                            var start = {
                                x: currentX, 
                                y: currentY
                            };
							
							var end = {
								x: bagManX - 1, 
								y: bagManY - 1
							};
							var result = astar.search(start, end);
							
							if(result.length < 1) {
							     return 0;
							} else {
							     var firstNodeFromPath = result[0];
							     return (firstNodeFromPath.y - currentY)
							}
                        ]]>
                    </Body>
                </Function>
            </PhysicalAgentType>
        </EntityTypes>
        <!-- This rule is used to allow close/open of the BagMan mouth -->
        <EnvironmentRules>
            <!-- The rule executed when the game starts -->
            <EnvironmentRule name="StartGame_Rule">
                <WHEN eventType="StartGame" />
                <DO>
                    <UPDATE-ENV>
                        <UpdateGlobalVariable name="startTime">
                            <ValueExpr language="JavaScript">
                                <![CDATA[
                                        (new Date()).getTime()
                                    ]]>
                            </ValueExpr>
                        </UpdateGlobalVariable>
                    </UPDATE-ENV>
                </DO>
            </EnvironmentRule>
            <!-- The rule execute when BagMan eats all PillFood pieces available on the map -->
            <EnvironmentRule name="WinGame_Rule">
                <ON-EACH-SIMULATION-STEP />
                <IF language="JavaScript">
                    <![CDATA[
                        Global.getRemainingPillFoodNumber() === 0
                    ]]>
                </IF>
                <THEN>
                    <SCHEDULE-EVT>
                        <CausedEventExpr eventType="WinGame" />
                        <CausedEventExpr eventType="StopSimulation" />
                    </SCHEDULE-EVT>
                </THEN>
            </EnvironmentRule>
            <!-- Check if the BagMan is killed by the ghosts - that is, game lost -->
            <EnvironmentRule name="BagManDies_Rule">
                <ON-EACH-SIMULATION-STEP />
                <FOR-ObjectVariable objectIdRef="1" objectType="BagMan" variable="bagman" />
                <FOR-ObjectVariable objectIdRef="10000" objectType="Ghost"
                    variable="ghostBlinky" />
                <FOR-ObjectVariable objectIdRef="11000" objectType="Ghost" variable="ghostInky" />
                <IF language="JavaScript">
                    <![CDATA[
                        (this.bagman.getX() === this.ghostBlinky.getX() && this.bagman.getY() === this.ghostBlinky.getY())
                        || (this.bagman.getX() === this.ghostInky.getX() && this.bagman.getY() === this.ghostInky.getY())
                    ]]>
                </IF>
                <THEN>
                    <SCHEDULE-EVT>
                        <CausedEventExpr eventType="BagManDies" />
                        <CausedEventExpr eventType="StopSimulation" />
                    </SCHEDULE-EVT>
                </THEN>
            </EnvironmentRule>
            <!-- This rule will execute on each step and determine the BagMan mouth state -->
            <EnvironmentRule name="BagManMouthState_Rule">
                <ON-EACH-SIMULATION-STEP />
                <FOR-ObjectVariable objectIdRef="1" objectType="BagMan" variable="bagman" />
                <IF language="JavaScript">
                    <![CDATA[
                        Simulator.getCurrentSimulationStep() % 5 === 0
                    ]]>
                </IF>
                <THEN>
                    <UPDATE-ENV>
                        <UpdateGlobalVariable name="passedTime">
                            <ValueExpr language="JavaScript">
                                <![CDATA[
                                    parseInt(((new Date()).getTime() - Global.getStartTime()) / 1000)
                                ]]>
                            </ValueExpr>
                        </UpdateGlobalVariable>
                        <UpdateObject objectVariable="bagman">
                            <Slot property="mouthOpen">
                                <ValueExpr language="JavaScript">
                                    <![CDATA[
                                        !this.bagman.isMouthOpen()
                                    ]]>
                                </ValueExpr>
                            </Slot>
                        </UpdateObject>
                    </UPDATE-ENV>
                </THEN>
            </EnvironmentRule>
            <!-- The BagMan finds PillFood in the cell and will eat it -->
            <EnvironmentRule name="BagManEatPillFood">
                <ON-EACH-SIMULATION-STEP />
                <FOR-DataVariable dataType="Integer" variable="currentPeriod">
                    <ValueExpr language="JavaScript">
                        <![CDATA[
                            25 - parseInt(Simulator.getCurrentSimulationStep() / 100)
                        ]]>
                    </ValueExpr>
                </FOR-DataVariable>
                <FOR-DataVariable dataType="Integer" variable="gainedPoints">
                    <ValueExpr language="JavaScript">
                        <![CDATA[
                             this.currentPeriod > 1 ? this.currentPeriod : 1
                        ]]>
                    </ValueExpr>
                </FOR-DataVariable>
                <FOR-ObjectVariable objectIdRef="1" objectType="BagMan" variable="bagman" />
                <IF language="JavaScript">
                    <![CDATA[
                        Simulator.spaceModel.getGridCell(this.bagman.getX(), this.bagman.getY()).getCellRole() === 0
                    ]]>
                </IF>
                <THEN>
                    <UPDATE-ENV>
                        <IncrementGlobalVariable name="remainingPillFoodNumber" value="-1" />
                        <UpdateGlobalVariable name="score">
                            <ValueExpr language="JavaScript">
                                <![CDATA[
                                   Global.getScore() +  this.gainedPoints
                                ]]>
                            </ValueExpr>
                        </UpdateGlobalVariable>
                        <UpdateGridCell gridCellVariable="cell">
                            <XCoordinate language="JavaScript">
                                <![CDATA[
                                    this.bagman.getX()
                                ]]>
                            </XCoordinate>
                            <YCoordinate language="JavaScript">
                                <![CDATA[
                                    this.bagman.getY()
                                ]]>
                            </YCoordinate>
                            <Slot property="cellRole" value="-1" />
                        </UpdateGridCell>
                    </UPDATE-ENV>
                    <SCHEDULE-EVT>
                        <CausedEventExpr eventType="EatPillFood" />
                    </SCHEDULE-EVT>
                </THEN>
            </EnvironmentRule>
            <!-- Move one object/agent with the given increments/decrements -->
            <EnvironmentRule name="MoveBagMan_Rule">
                <WHEN eventType="MoveBagMan" eventVariable="moveEvent" />
                <FOR-ObjectVariable objectIdRef="1" objectType="BagMan" variable="bagman" />
                <DO>
                    <UPDATE-ENV>
                        <UpdateObject objectVariable="bagman">
                            <Slot property="x">
                                <ValueExpr language="JavaScript">
                                    <![CDATA[
                                        this.bagman.getX() + this.moveEvent.getX()
                                    ]]>
                                </ValueExpr>
                            </Slot>
                            <Slot property="y">
                                <ValueExpr language="JavaScript">
                                    <![CDATA[
                                        this.bagman.getY() + this.moveEvent.getY()
                                    ]]>
                                </ValueExpr>
                            </Slot>
                        </UpdateObject>
                    </UPDATE-ENV>
                </DO>
            </EnvironmentRule>
            <!-- Decide next movement of the Ghosts -->
            <EnvironmentRule name="MoveGhosts_Rule">
                <ON-EACH-SIMULATION-STEP />
                <FOR-ObjectVariable objectIdRef="1" objectType="BagMan" variable="bagman" />
                <FOR-ObjectVariable objectType="Ghost" variable="ghost" />
                <FOR-DataVariable variable="bagManXPos" dataType="Integer">
                    <ValueExpr language="JavaScript"><![CDATA[this.bagman.getX()]]></ValueExpr>
                </FOR-DataVariable>
                <FOR-DataVariable variable="bagManYPos" dataType="Integer">
                    <ValueExpr language="JavaScript"><![CDATA[this.bagman.getY()]]></ValueExpr>
                </FOR-DataVariable>
                <FOR-DataVariable variable="xIncrement" dataType="Integer">
                    <ValueExpr language="JavaScript"><![CDATA[0]]></ValueExpr>
                </FOR-DataVariable>
                <FOR-DataVariable variable="yIncrement" dataType="Integer">
                    <ValueExpr language="JavaScript"><![CDATA[0]]></ValueExpr>
                </FOR-DataVariable>
                <IF language="JavaScript">
                    <![CDATA[
                        Simulator.getCurrentSimulationStep() > 25
                        && Simulator.getCurrentSimulationStep() % (this.ghost.getId()/1000 - Global.getLevel()) === 0
                    ]]>
                </IF>
                <THEN>
                    <UPDATE-ENV>
                        <UpdateObject objectVariable="ghost">
                            <Slot property="x">
                                <ValueExpr language="JavaScript">
                                    <![CDATA[
                                        this.xIncrement = this.ghost.getX() +  this.ghost.getXIncrement(this.bagManXPos, this.bagManYPos)
                                    ]]>
                                </ValueExpr>
                            </Slot>
                            <Slot property="y">
                                <ValueExpr language="JavaScript">
                                    <![CDATA[
                                         this.yIncrement = this.ghost.getY() + this.ghost.getYIncrement(this.bagManXPos, this.bagManYPos)
                                    ]]>
                                </ValueExpr>
                            </Slot>
                            <Slot property="movingDirection">
                                <ValueExpr language="JavaScript">
                                    <![CDATA[
                                        (this.xIncrement === 1 ? Direction.EAST : 
                                            (this.xIncrement === -1 ? Direction.WEST : 
                                                (this.yIncrement === 1 ? Direction.NORTH : 
                                                    (this.yIncrement === -1 ? Direction.SOUTH : Direction.NONE))))
                                    ]]>
                                </ValueExpr>
                            </Slot>
                        </UpdateObject>
                    </UPDATE-ENV>
                </THEN>
            </EnvironmentRule>
        </EnvironmentRules>
    </SimulationModel>
    <InitialState>
        <!-- the initial score the player get -->
        <GlobalVariable name="score" value="0" />
        <!-- keep tracking of the time past from start of the simulation -->
        <GlobalVariable name="passedTime" value="0" />
        <!-- the initial number of cells that have contains Pill Food -->
        <GlobalVariable name="remainingPillFoodNumber">
            <ValueExpr language="JavaScript">
                <![CDATA[
                    Global.getNumberOfFoodCells()
                ]]>
            </ValueExpr>
        </GlobalVariable>
        
        <!-- the start game event - results in some initializations -->
        <ExogenousEvent type="StartGame" occurrenceTime="1"/>
        
        <!-- BagMan agent -->
        <PhysicalAgent id="1" name="BagMan" type="BagMan" x="5" y="2" />
        <!-- Ghost Blinky agent -->
        <PhysicalAgent id="10000" name="Blinky" type="Ghost" x="4" y="4" />
        <!-- Ghost Inky agent -->
        <PhysicalAgent id="11000" name="Inky" type="Ghost" x="6" y="4" />
        <InitializationRule name="InitializeMap">
            <ForEachGridCell endX="9" endY="7" gridCellVariable="cell" startX="1" startY="1">
                <Code language="JavaScript">
                    <![CDATA[
                        // get map matrix
                        var map = Global.getGameMap();
                        
                        // the map width (number of columns) 
                        var mapWidth = Global.getGameMap()[0].length;
                        
                        // the map height (number of rows)
                        var mapHeight = Global.getGameMap().length;
                        
                        // the left-down corner is (1,1) so need to adjust this
                        var yPos = mapHeight - cell.getPosY();
                        
                        // need to do POS - 1 while the space starts with pos (1,1) 
                        // but the Map Matrix starts with (0,0)
                        var xPos = cell.getPosX() - 1;
                        
                        // initialize the cell roles based on the map matrix
                        cell.setCellRole(map[yPos][xPos]);
                    ]]>
                </Code>
            </ForEachGridCell>
        </InitializationRule>
    </InitialState>
    <UserInterface supportedLanguages="en">
        <InitialStateUI>
            <GlobalVariableUI variable="level" widget="Slider" label="Difficulty level">
                <Hint>
                    <Text>The level of difficulty: controls the ghosts walking speed.</Text>
                </Hint>
            </GlobalVariableUI>
        </InitialStateUI>
        <AnimationUI showZoomControlPanel="false">
            <aors:StartPageText xmlns="http://www.w3.org/1999/xhtml">
                <aors:HtmlText>
                    <div style="float:left; width: 40%; padding: 7px 50px; text-align: center;">
                        <img src="media/BagMan_screenShot.png"
                            style="width: 90%; border: 1px solid black;" />
                    </div>
                    <div style="float:left; width: 40%; padding: 7px 0px">
                        <p>You play the role of <b>BagMan</b>, the main character of a classic
                            PacMan game clone.</p>
                        <p>These are the possible actions you can do:</p>
                        <ul>
                            <li>Move the <b>BagMan</b>: <ul>
                                    <li>left: '&#8592;' or '<i>A</i>' keys</li>
                                    <li>right: '&#8594;' or '<i>D</i>' keys</li>
                                    <li>up: '&#8593;' or '<i>W</i>' keys</li>
                                    <li>down: '&#8595;' or '<i>S</i>' keys</li>
                                </ul>
                            </li>
                            <li>Collect the <i>Food Pills</i> from all the cells you enter.</li>
                            <li>Avoid <i>Blinky</i> and <i>Inky</i>, the unfriendly ghosts, that
                                will enjoy eating you.</li>
                        </ul>
                        <p>The goals: <i>guide the BagMan towards the map and collect the food from
                                all cells</i>.</p>
                        <p>If <i>Inky</i> or <i>Bliky</i> will catch you, the game is over, but
                            don't give up, try again!</p>
                    </div>
                </aors:HtmlText>
            </aors:StartPageText>
			<aors:HelpText xmlns="http://www.w3.org/1999/xhtml">
                    <aors:HtmlText>
                        <p>You play the role of <b>BagMan</b>, the main character of the classic
                            PacMan game clone.</p>
                        <p>These are the possible actions you can do:</p>
                        <ul>
                            <li>Move the <b>BagMan</b>: <ul>
                                    <li>left: '&#8592;' or '<i>A</i>' keys</li>
                                    <li>right: '&#8594;' or '<i>D</i>' keys</li>
                                    <li>up: '&#8593;' or '<i>W</i>' keys</li>
                                    <li>down: '&#8595;' or '<i>S</i>' keys</li>
                                </ul>
                            </li>
                            <li>Collect the <i>Food Pills</i> from all the cells you enter.</li>
                            <li>Avoid <i>Blinky</i> and <i>Inky</i>, the unfriendly ghosts, that
                                will enjoy eating you.</li>
                        </ul>
                        <p>The goals: <i>guide the BagMan towards the map and collect the food from
                                all cells</i>.</p>
                        <p>If <i>Inky</i> or <i>Bliky</i> will catch you, the game is over, but
                            don't give up, try again!</p>
                    </aors:HtmlText>
            </aors:HelpText>
            <Views>
                <SpaceView canvasColor="grey">
                    <TwoDimensionalGridSpaceView2D backgroundColor="black">
                        <GridCellPropertyVisualizationMap a0="0" a1="1" a2="-1"
                            cellViewProperty="fill" mapType="equalityCaseWise" property="cellRole"
                            v0="black" v1="blue" v2="black" />
                        <GridCellPropertyVisualizationMap a0="0" cellViewProperty="texture"
                            mapType="equalityCaseWise" property="cellRole" v0="PillFood.png" />
                    </TwoDimensionalGridSpaceView2D>
                </SpaceView>
                <PhysicalObjectView physicalObjectIdRef="1">
                    <PhysicalShape2D>
                        <Square>
                            <ShapePropertyVisualizationMap a0="true" a1="false"
                                mapType="equalityCaseWise" property="mouthOpen"
                                shapeProperty="texture" v0="BagMan_open.png" v1="BagMan_close.png" />
                            <ShapePropertyVisualizationMap a0="SOUTH" a1="WEST" a2="NORTH" a3="EAST"
                                mapType="enumerationMap" property="movingDirection"
                                shapeProperty="horizontalFlip" v0="true" v1="true" v2="false"
                                v3="false" />
                            <ShapePropertyVisualizationMap a0="EAST" a1="NORTH" a2="WEST" a3="SOUTH"
                                mapType="enumerationMap" property="movingDirection"
                                shapeProperty="rot" v0="0" v1="90" v2="0" v3="90" />
                        </Square>
                    </PhysicalShape2D>
                </PhysicalObjectView>
                <PhysicalObjectView physicalObjectType="Ghost" physicalObjectIdRef="10000">
                    <PhysicalShape2D>
                        <Square>
                            <ShapePropertyVisualizationMap a0="EAST" a1="NORTH" a2="WEST" a3="SOUTH"
                                mapType="enumerationMap" property="movingDirection"
                                shapeProperty="texture" v0="red_ghost_look_east.png"
                                v1="red_ghost_look_north.png" v2="red_ghost_look_west.png"
                                v3="red_ghost_look_south.png" v4="red_ghost_look_none.png" />
                        </Square>
                    </PhysicalShape2D>
                </PhysicalObjectView>
                <PhysicalObjectView physicalObjectType="Ghost" physicalObjectIdRef="11000">
                    <PhysicalShape2D>
                        <Square>
                            <ShapePropertyVisualizationMap a0="EAST" a1="NORTH" a2="WEST" a3="SOUTH"
                                mapType="enumerationMap" property="movingDirection"
                                shapeProperty="texture" v0="turquoise_ghost_look_east.png"
                                v1="turquoise_ghost_look_north.png"
                                v2="turquoise_ghost_look_west.png"
                                v3="turquoise_ghost_look_south.png"
                                v4="turquoise_ghost_look_none.png" />
                        </Square>
                    </PhysicalShape2D>
                </PhysicalObjectView>
                <EventAppearance eventType="EatPillFood">
                    <Sound soundFile="eating" />
                </EventAppearance>
                <EventAppearance eventType="StartGame">
                    <Sound soundFile="start" />
                </EventAppearance>
                <EventAppearance eventType="BagManDies">
                    <Sound soundFile="die" />
                </EventAppearance>
                <EventAppearance eventType="WinGame">
                    <Sound soundFile="win" />
                </EventAppearance>
            </Views>
            <AgentControlUI initiallyPlayedAgent="BagMan">
                <AgentControlByAgentType type="BagMan">
                    <TopOutputPanel>
                        <OutputFieldGroup>
                            <OutputField label="Score">
                                <Hint>
                                    <Text>Your current score.</Text>
                                </Hint>
                                <Source>
                                    <GlobalVariable name="score" />
                                </Source>
                            </OutputField>
                            <OutputField label="Elapsed time">
                                <Hint>
                                    <Text>The time passed from simulation start, up to now.</Text>
                                </Hint>
                                <Format decimalPlaces="0">
                                    <Time>s</Time>
                                </Format>
                                <Source>
                                    <GlobalVariable name="passedTime" />
                                </Source>
                            </OutputField>
                        </OutputFieldGroup>
                    </TopOutputPanel>
                    <BottomOutputPanel>
                        <OutputFieldGroup>
                            <OutputField label="Level">
                                <Hint>
                                    <Text>Current game level</Text>
                                </Hint>
                                <Source>
                                    <GlobalVariable name="level" />
                                </Source>
                            </OutputField>
                        </OutputFieldGroup>
                    </BottomOutputPanel>
                    <UserActionEventListener>
                        <WHEN>
                            <KeyboardEventExpr keyIdentifier="Right" />
                            <KeyboardEventExpr keyIdentifier="D" />
                        </WHEN>
                        <EXECUTE actionRule="MoveEast_ActionRule" />
                    </UserActionEventListener>
                    <UserActionEventListener>
                        <WHEN>
                            <KeyboardEventExpr keyIdentifier="Left" />
                            <KeyboardEventExpr keyIdentifier="A" />
                        </WHEN>
                        <EXECUTE actionRule="MoveWest_ActionRule" />
                    </UserActionEventListener>
                    <UserActionEventListener>
                        <WHEN>
                            <KeyboardEventExpr keyIdentifier="Up" />
                            <KeyboardEventExpr keyIdentifier="W" />
                        </WHEN>
                        <EXECUTE actionRule="MoveNorth_ActionRule" />
                    </UserActionEventListener>
                    <UserActionEventListener>
                        <WHEN>
                            <KeyboardEventExpr keyIdentifier="Down" />
                            <KeyboardEventExpr keyIdentifier="S" />
                        </WHEN>
                        <EXECUTE actionRule="MoveSouth_ActionRule" />
                    </UserActionEventListener>
                </AgentControlByAgentType>
            </AgentControlUI>
        </AnimationUI>
    </UserInterface>
</SimulationScenario>
