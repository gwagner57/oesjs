/**
 * Create a table of contents as a nested list within the nav#toc element.
 * 
 * Supports book formats with and without parts.
 * 
 * TODO: Instead of using the section title as id/href, better use a unique number/code
 * 
 * @method 
 * @author Gerd Wagner
 * @author Uwe Herrmann
 */
function createTableOfContents() {
  var navEl = document.querySelector("body>header>nav"),
      tblOfContListEl=null, headingEl=null, chapterElems=[], chapterListEl=null, 
      partEl=null, partElems=[], partTocItemEl=null,
      chapterEl=null, chapterTocItemEl=null, 
	  //lineBreakNode = document.createTextNode("\n"),
      sectElems=[], sectListEl=null;
	
  function createOneLevelSectionList( sectEl, sectionHierarchySelector, listEl) {
    var headingEl=null, el=null, i=0,
        nextLevelListEl=null, listItemEl=null,
        subsectionEl=null, subsubSectionElems=[],
        subsectionElems = sectEl.querySelectorAll( sectionHierarchySelector);
    for (i=0; i < subsectionElems.length; i++) {
      subsectionEl = subsectionElems[i];	
      headingEl = subsectionEl.querySelector("h1");
      subsectionEl.id = headingEl.textContent.replace(/ /g, "_");
      listItemEl = document.createElement("li");
      el = document.createElement("a");
      el.innerHTML = headingEl.innerHTML;
      el.href = "#" + subsectionEl.id;
      listItemEl.appendChild( el);
      listEl.appendChild( listItemEl);
      //listEl.appendChild( lineBreakNode);
      subsubSectionElems = subsectionEl.querySelectorAll( sectionHierarchySelector + ">section");
      if (subsubSectionElems.length > 0) {
        nextLevelListEl = document.createElement("ul");
        listItemEl.appendChild( nextLevelListEl);
        createOneLevelSectionList( subsectionEl, sectionHierarchySelector + ">section", nextLevelListEl);
      }
    }
  }

  if (navEl) {
    tblOfContListEl = navEl.lastElementChild;
    tblOfContListEl.innerHTML = "";
  } else {
    navEl =  document.createElement("nav");
    headingEl = document.createElement("h1");
    headingEl.textContent = "Contents";
    navEl.appendChild( headingEl);
    tblOfContListEl = document.createElement("ul");
    navEl.appendChild( tblOfContListEl);
    document.querySelector("body>header").appendChild( navEl);
  }
  partElems = document.querySelectorAll("body>div.part");
  if (partElems.length > 0) {
    for (i=0; i < partElems.length; i++) {
      partEl = partElems[i];  
      partTocItemEl = document.createElement("li");
      partTocItemEl.innerHTML = "Part "+ (i+1) +" "+ 
          partEl.querySelector("h1").innerHTML;
      tblOfContListEl.appendChild( partTocItemEl);
      chapterElems = partEl.querySelectorAll("body>div.part>section.chapter");
      if (chapterElems.length > 0) {
        chapterListEl = document.createElement("ul");
        partTocItemEl.appendChild( chapterListEl);
        createOneLevelSectionList( partEl, "body>div.part>section.chapter", chapterListEl);
      }
    }    
  } else {  // if there are no parts
    chapterElems = document.querySelectorAll("body>main>section.chapter");
    if (chapterElems.length > 0) {
      for (i=0; i < chapterElems.length; i++) {
        chapterEl = chapterElems[i];  
        chapterTocItemEl = document.createElement("li");
        chapterTocItemEl.innerHTML = "Chapter "+ (i+1) +" "+ chapterEl.firstElementChild.innerHTML;
        tblOfContListEl.appendChild( chapterTocItemEl);
        sectElems = chapterEl.querySelectorAll("body>main>section>section");
        if (sectElems.length > 0) {
          sectListEl = document.createElement("ul");
          chapterTocItemEl.appendChild( sectListEl);
          createOneLevelSectionList( chapterEl, "body>main>section>section", sectListEl);
        }
      }
    } else {  // article
      createOneLevelSectionList( document.querySelector("#main"), 
          "#main>section", tblOfContListEl);
	}
  }
}