# OESjs #
## A JavaScript-Based Simulation Framework ##

OESjs implements *Object Event Simulation (OES)*, which is a Discrete Event Simulation paradigm combining *object-oriented* modeling with the simulation approach of *event scheduling*.

OESjs allows making web-based simulations, which are especially good for using simulations in education. Find out more about OESjs in the [introductory tutorial](https://sim4edu.com/reading/IntroTutorial.html) and on the [Simulation for Education](https://sim4edu.com) website.

OESjs supports 

1. both discrete and continuous simulation time;
2. *next-event time progression*, which is often used in discrete event simulation; 
3. *fixed incrememt time progression*, which is often used in social science simulation (e.g., in *NetLogo* models), and is needed for simulating *continuous state changes*;
4. combinations of fixed incrememt and next-event time progression in "hybrid" simulations combining continuous and discrete state changes;
5. spatial simulation with a choice of space models: grid space, 1D/2D/3D continuous space, graph space.

OESjs has a [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0) copyleft license. 