//self.importScripts("../../lib/cLASSjsWorkerLib.js");
self.importScripts("../../../cLASSjs/lib/errorTypes.js");
self.importScripts("../../../cLASSjs/lib/util.js");
self.importScripts("../../../cLASSjs/src/eNUMERATION.js");
self.importScripts("../../../cLASSjs/src/cLASS.js");

self.importScripts("../../lib/rand.js", "../../lib/BinaryHeap.js");
self.importScripts("../../src/v1/OES.js", "../../src/v1/EventList.js",
    "../../src/v1/statistics.js");
self.importScripts("../../src/space/space.js", "../../src/space/gridOfIntegers.js",
    "../../src/v1/simulator.js");

let path = "../../src/v1/sims/006-SchellingSegregationModel-1/";
self.importScripts(path + "simulation.js");
sim.config.stepDuration = 0;  // observation time per step in ms
sim.config.visualize = false;
sim.scenario.randomSeed = 12345;

if (sim.model.objectTypes) {
  sim.model.objectTypes.forEach( function (objT) {
    self.importScripts( path + objT + ".js");
  })
}
if (sim.model.eventTypes) {
  sim.model.eventTypes.forEach( function (evtT) {
    self.importScripts( path + evtT + ".js");
  });
}
if (sim.model.activityTypes) {
  sim.model.activityTypes.forEach( function (actT) {
    self.importScripts( path + actT + ".js");
  });
}

//=================================================================

onmessage = function (e) {
  // receive parameter/variable values changed via the UI
  if (e.data.endTime) sim.scenario.simulationEndTime = e.data.endTime;
  if (e.data.changedModelVarValues) {
    Object.keys( e.data.changedModelVarValues).forEach( function (varName) {
      sim.model.v[varName].value = e.data.changedModelVarValues[varName];
    });
  }
  if (e.data.runExperiment) {
    sim.initializeSimulator( e.data.dbName);
    if (e.data.expReplications) sim.experiment.replications = e.data.expReplications;
    sim.runExperiment();
  } else {
    sim.initializeSimulator();
    if (e.data.createLog !== undefined) sim.config.createLog = e.data.createLog;
    sim.runScenario( true);  // run in worker thread
  }
};