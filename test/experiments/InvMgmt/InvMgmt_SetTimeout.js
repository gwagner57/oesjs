/*
 Improvements/extensions
 + applyRule must return a collection of follow-up events
 + event priorities for developer-controlled rule execution ordering
 + generic handling of event re-scheduling for exogeneous event types
*/
/*******************************************************
 Utility Methods
 ********************************************************/
// Returns a random number between min (inclusive) and max (exclusive)
function getRandomNum( min, max) {
  return Math.random() * (max - min) + min;
}
// Returns a random integer between min (included) and max (included)
function getRandomInt( min, max) {
  return Math.floor( Math.random() * (max - min + 1)) + min;
}
// Simulation Log
var rowEl = null,
    tbodyEl = document.getElementById("simLog");
function logSimulationStep( obj, fE) {
  rowEl = tbodyEl.insertRow();  // create new table row
  rowEl.insertCell().textContent = String( simTime);
  rowEl.insertCell().textContent = obj.toString();
  rowEl.insertCell().textContent = fE.events.reduce( function (aggr, el) {
    return aggr +", "+ el.toString();
  });
}
/*******************************************************
 Simulation Model
********************************************************/
var SingleProductShop, DailyDemand, Delivery;

SingleProductShop = new cLASS({
  Name: "SingleProductShop",
  properties: {
    "name": {range: "NonEmptyString"},
    "quantityInStock": {range:"NonNegativeInteger", label:"Stock"},
    "reorderLevel": {range:"NonNegativeInteger"},
    "reorderUpToLevel": {range:"PositiveInteger"},
    // output statistics
    "lostSales": {range:"NonNegativeInteger", label:"Lost"}
  }
});
DailyDemand = new cLASS({
  Name: "DailyDemand",
  properties: {
    "occTime": {range: "NonNegativeNumber", label:"occTime"},
    "quantity": {range: "PositiveInteger", label:"quant"},
    "shop": {range: SingleProductShop}
  },
  methods: {
    "applyRule": function () {
      var q = this.quantity,
          prevStockLevel = this.shop.quantityInStock,
		  followUpEvents = [];
      // update lostSales if demand quantity greater than stock level
      if (q > prevStockLevel) {  
        this.shop.lostSales += q - prevStockLevel;
      }
      // update quantityInStock 
      this.shop.quantityInStock = Math.max( prevStockLevel-q, 0);
      // schedule Delivery if stock level falls below reorder level
      if (prevStockLevel > this.shop.reorderLevel && 
          prevStockLevel - q <= this.shop.reorderLevel) {
        followUpEvents.push( new Delivery({
          occTime: this.occTime + Delivery.sampleLeadTime(),
          quantity: this.shop.reorderUpToLevel - this.shop.quantityInStock,
          receiver: this.shop
        }));
      }
	  // since DailyDemand is exogeneous, schedule next one
      followUpEvents.push( new DailyDemand({
        occTime: this.occTime + DailyDemand.recurrence(),
        quantity: DailyDemand.sampleQuantity(),
        shop: this.shop
      }));
	  return followUpEvents;
    }
  }
});
DailyDemand.recurrence = function () {
  return 1;
};
DailyDemand.sampleQuantity = function () {
  return getRandomInt( 5, 30);
};

Delivery = new cLASS({
  Name: "Delivery",
  properties: {
    "occTime": {range: "NonNegativeNumber", label:"occTime"},
    "quantity": {range: "PositiveInteger", label:"quant"},
    "receiver": {range: SingleProductShop}
  },
  methods: {
    "applyRule": function () {
      this.receiver.quantityInStock += this.quantity;
    return [];  // no follow-up events
    }
  }
});
Delivery.sampleLeadTime = function () {
  var r = getRandomInt( 0, 99);
  if (r < 25) return 1;         // probability 0.25
  else if (r < 85) return 2;    // probability 0.60
  else return 3;                // probability 0.15
};

/*******************************************************
 Initial State
 ********************************************************/
var tvShop = new SingleProductShop({
  name:"TV",
  quantityInStock: 80,
  reorderLevel: 50,
  reorderUpToLevel: 100,
  lostSales: 0
});
var FEL = new oes.EventList([new DailyDemand({occTime:1, quantity:25, shop: tvShop})]);

/*******************************************************
 General Simulation Variables
 ********************************************************/
var simTime=0, nextTime=0, simEnd=1000, i=0, j=0;
var nextEvents=[], followUpEvents=[], e=null;
var stepStartTime = 0, totalStepTime = 0, simulationStartTime = 0;
var stepDuration = 0;
/*******************************************************
 Run a simulation step - recursive call using setTimout is used 
 ********************************************************/
function runStep() {
  var stepDiffTimeDelay = 0;
  if (simTime >=simEnd)  {  // terminate simulation
    console.log("InvMgmt SetTimeout total execution time for " +
      simEnd + " steps : ", ((new Date()).getTime() - simulationStartTime) + "ms");
    return;
  }   
  stepStartTime = (new Date()).getTime();
  logSimulationStep( tvShop, FEL);
  nextTime = FEL.getNextOccurrenceTime();
  // extract and process next events
  nextEvents = FEL.removeNextEvents();
  for (i=0; i < nextEvents.length; i++) {
    e = nextEvents[i];
    // apply event rule
    followUpEvents = e.applyRule();
    // schedule follow-up events
    for (j=0; j < followUpEvents.length; j++) {
      FEL.add( followUpEvents[j]);
    };
  };
  // advance simulation time
  simTime = nextTime;
  // time consumed by running the current simulation step
  totalStepTime = (new Date()).getTime() - stepStartTime;
   // check if we need some delay, because of the stepDuration parameter
  if (stepDuration > totalStepTime) {
    stepDiffTimeDelay = sim.stepDuration - totalStepTime
  } else {
    /*if (sim.stepDuration > 0) {
     console.log("Duration of step "+ String( sim.step-1) +": "+ totalStepTime);
     }*/
    stepDiffTimeDelay = 0;
  }
  // continue simulation loop
  // in the browser, use setTimeout to prevent script blocking
  setTimeout(runStep, stepDiffTimeDelay);
};
 
/*******************************************************
 Simulation Loop
 ********************************************************/
simulationStartTime = (new Date()).getTime();
runStep();
/*simEnd = 100;
while (simTime < simEnd) {
  stepStartTime = (new Date()).getTime();
  logSimulationStep( tvShop, FEL);
  nextTime = FEL.getNextOccurrenceTime();
  // extract and process next events
  nextEvents = FEL.removeNextEvents();
  for (i=0; i < nextEvents.length; i++) {
    e = nextEvents[i];
    // apply event rule
    followUpEvents = e.applyRule();
    // schedule follow-up events
    for (j=0; j < followUpEvents.length; j++) {
      FEL.add( followUpEvents[j]);
    };
  };
  // advance simulation time
  simTime = nextTime;
  // time consumed by running the current simulation step
  totalStepTime = (new Date()).getTime() - stepStartTime;
   // check if we need some delay, because of the stepDuration parameter
  if (sim.stepDuration > totalStepTime) {
    stepDiffTimeDelay = sim.stepDuration - totalStepTime
  } else {
    stepDiffTimeDelay = 0;
  }
  // continue simulation loop
  // in the browser, use setTimeout to prevent script blocking
  setTimeout( sim.runStep, stepDiffTimeDelay);
}*/

