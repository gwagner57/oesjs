/*******************************************************************************
 * EventList maintains an ordered list of events
 * @copyright Copyright 2015-2016 Gerd Wagner
 *   Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Gerd Wagner
 ******************************************************************************/
var oes = oes || {};
oes.EventList = function EventList(a) {
  this.events = Array.isArray(a) ? a : [];
 };
oes.EventList.prototype.add = function (e) {
  this.events.push( e);
  this.events.sort( function (e1, e2) {
    return e1.occTime - e2.occTime;
  });
};
oes.EventList.prototype.getNextOccurrenceTime = function () {
  if (this.events.length > 0) return this.events[0].occTime;
  else return 0;
};
oes.EventList.prototype.getNextEvent = function () {
  if (this.events.length > 0) return this.events[0];
  else return null;
};
oes.EventList.prototype.isEmpty = function () {
  return (this.events.length <= 0);
};
oes.EventList.prototype.removeNextEvents = function () {
  var nextTime=0, nextEvents=[];
  if (this.events.length === 0) return [];
  nextTime = this.events[0].occTime;
  while (this.events.length > 0 && this.events[0].occTime === nextTime) {
    nextEvents.push( this.events.shift());
  }
  return nextEvents;
};
oes.EventList.prototype.clear = function (e) {
  this.events = [];
};
oes.EventList.prototype.containsEventOfType = function (evtType) {
  return this.events.some( function (evt) {
    return evt.constructor.Name === evtType;
  });
};
oes.EventList.prototype.toString = function () {
  var str="";
  if (this.events.length > 0) {
    str = this.events.reduce( function (serialization, e) {
      return serialization +", "+ e.toDisplayString();
    }, "");
    str = str.slice(1);
  }
  return str;
};
