/**
 * 
 */
var eventList = new oes.EventList( function ( e ) {
  return e.occTime;
} );

var i, event, nextEvents;
var values = [9, 4, 8, 2, 8, 0, 6, 9];

for ( i = 0; i < 1000; i += 1 ) {
  values[i] = Math.floor( Math.random() * 10 );
}

console.log( "ADD" );

values.forEach( function ( v ) {
  event = {
    "occTime": v
  };
  // console.log( event.occTime );
  eventList.add( event );
} );

console.log( "REMOVE" );
i = 0;
while ( !eventList.isEmpty() ) {
  nextEvents = eventList.removeNextEvents();
  nextEvents.forEach( function ( el ) {
    if ( i > 800 ) {
      console.log( el.occTime );
    }
    i += 1;
  } );
}
