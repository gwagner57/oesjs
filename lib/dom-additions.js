 /**
 * @fileOverview  A library of DOM element creation methods and 
 * other DOM manipulation methods.
 * 
 * @author Gerd Wagner
 */
 /**
  * Create a progress bar
  * @param {string} title
  * @return {object}  an element object
  */
 dom.createProgressBar = function (title, initialProgress) {
   var progressContainer = document.createElement("div"),
       progress = document.createElement("progress"),
       progressTitle = document.createElement("h1"),
       progressInfo = document.createElement("p");
   progress.max = 100;  // for 100%
   progress.value = initialProgress || 10;  // initial value, 10% by default
   progressTitle.textContent = title;
   progressContainer.id = "progress-container";
   progressContainer.appendChild( progressTitle);
   progressContainer.appendChild( progress);
   progressContainer.appendChild( progressInfo);
   return progressContainer
 };
 /**
  * Create an expandable UI panel
  * @param {object} slots
  * @return {object} uiPanelEl  element object
  */
 dom.createExpandablePanel = function (slots) {
   var uiPanelEl = dom.createElement("div", slots),
       contentEl = dom.createElement("div", {classValues:"xpanel-content"}),
       expandButtonEl = dom.createElement("button", {content:"+"});
   uiPanelEl.classList.add("expandablePanel");
   uiPanelEl.appendChild( dom.createElement("h2", {
     content:"<span>"+ slots.heading +"</span>",
     title: slots.hint
   }));
   uiPanelEl.appendChild( contentEl);
   uiPanelEl.firstElementChild.insertBefore(
       expandButtonEl, uiPanelEl.firstElementChild.firstElementChild);
   expandButtonEl.addEventListener("click", function (e) {
     // toggle display of main content
     if (contentEl.style.display !== "none") {
       contentEl.style.display = "none";
       e.target.textContent = "+";  // or "►" = "&#9654;"
     }
     else {
       contentEl.style.display = "block";
       e.target.textContent = "−";  // = "&minus;" or "▼"
     }
     e.preventDefault();
   });
   uiPanelEl.style.overflowX = "auto";  // horizontal scrolling
   contentEl.style.display = "none";
   return uiPanelEl;
 };
 /**
  * Create a Modal Window/Panel
  *
  * @param {object} slots
  * @return {object} uiPanelEl  element object
  */
 dom.createModal = function (slots) {
   var modalEl = dom.createElement("div", {classValues:"modal"}),
       el = document.getElementById("overlay"),
       overlayEl = el ? el : dom.createElement("div", {id:"overlay"}),
       h1El=null;
   if (slots.id) modalEl.id = slots.id;
   if (slots.classValues) modalEl.className += " "+ slots.classValues;
   if (slots.width) modalEl.style.width = slots.width;
   if (!slots.title) slots.title = "No Title?";
   h1El = dom.createElement("h1", {content: "<span class='title'>"+ slots.title +"</span>"});
     //  "</span><span class='closeButton'>&times;</span>"});
   if (!slots.classValues || !slots.classValues.includes("action-required")) {
     el = dom.createElement("span", {classValues:"closeButton", content:"&times;"});
     el.addEventListener("click", function () {
       overlayEl.style.display = "none";
       modalEl.style.display = "none";
     });
     h1El.appendChild( el);
   }
   modalEl.appendChild( h1El);
   if (slots.fromElem) {
     modalEl.appendChild( slots.fromElem);
     slots.fromElem.classList.add("modal-body");
   } else {
     el = dom.createElement("div", {classValues:"modal-body"});
     if (slots.textContent) el.textContent = slots.textContent;
     modalEl.appendChild( el);
   }
   overlayEl.appendChild( modalEl);
   document.body.appendChild( overlayEl);
   return modalEl;
 };
 /**
  * Create a Draggable Modal Window/Panel
  *
  * @param {object} slots
  * @return {object} uiPanelEl  element object
  */
 dom.createDraggableModal = function (slots) {
   var modalEl = dom.createModal( slots),
       overlayEl = document.getElementById("overlay");
   // make the element draggable
   modalEl.draggable = true;
   if (!modalEl.id) modalEl.id = "dragMod";
   modalEl.addEventListener("dragstart", dom.handleDragStart);
   overlayEl.addEventListener("dragover", dom.handleDragOver);
   overlayEl.addEventListener("drop", dom.handleDrop);
   return modalEl;
 };
 dom.handleDragStart = function (evt) {
   evt.dataTransfer.dropEffect = 'move';
   evt.dataTransfer.setData("text/plain", evt.target.id);
 };
 dom.handleDragOver = function (evt) {
   // allow dropping by preventing the default behavior
   evt.preventDefault();
 };
 dom.handleDrop = function (evt) {
   var elId = evt.dataTransfer.getData("text/plain"),
       el = document.getElementById( elId),
       x = evt.clientX, y = evt.clientY;
   evt.preventDefault();
   el.style.position = "absolute";
   el.style.left = x +"px";
   el.style.top = y +"px";
 };
 /**
  * Convert HTML to Text
  * @param {string} htmlElemContent
  * @return {string}
  */
 dom.convertHtml2Text = function (htmlElemContent) {
   var blockElem = document.createElement("div");
   blockElem.innerHTML = htmlElemContent;
   return blockElem.textContent;
 };
