
/*******************************************************************************
 * Define a double linked list implementation. It is mostly supposed to replace
 * standard Array implementation in the case when a lot of push/pop operations 
 * are required because of the issues (memory, performance) caused by the JS 
 * Array.push, Array.pop and Array.slice implementation.
 * 
 * @constructor
 * @author Mircea Diaconescu
 * @copyright Copyright � 2014 Gerd Wagner, Mircea Diaconescu et al, 
 *            Chair of Internet Technology, Brandenburg University of Technology, Germany.
 * @date July 08, 2014, 11:04:23
 * @license [%LICENSE-TEXT%]
 ******************************************************************************/
oes.LinkedList = function LinkedList() {
  this.length = 0;
  this.head = null;
  this.tail = null;
}  
/**
 * Add a new node in the list.
 * @param data
 *          the data to be stored in the list node.
 */
oes.LinkedList.prototype.add = function (data) {
  var node = {
        data: data,
        next: null,
        prev: null
      };
  //special case: no items in the list yet
  if (this.length == 0) {
      this.head = node;
      this.tail = node;
  } else {
      this.tail.next = node;
      node.prev = this.tail;
      this.tail = node;
  }        
  this.length++;
};
/**
 * Remove a node from list.
 * @param index
 *          the index of the node to be removed from list.
 */
oes.LinkedList.prototype.remove = function (index) {
  var current = this.head, i = 0;
  //check for out-of-bounds values
  if (index > -1 && index < this.length) {
    //special case: removing first item
    if (index === 0) {
      this.head = current.next;
      if (!this.head) this.tail = null;
      else this.head.prev = null;
    } 
    //special case: removing last item
    else if (index === this.length-1){
      current = this.tail;
      this.tail = current.prev;
      this.tail.next = null;
    } else {  //find the right location
      while (i++ < index){
        current = current.next;
      }
      //skip over the item to remove
      current.prev.next = current.next;
    }
    //decrement the length
    this.length--;
    //return the value
    return current.data;            
  } else {
    return null;
  }
}
/**
 * Get the current size of the list.
 * @return the current list size
 */
oes.LinkedList.prototype.size = function () {
  return this.length;
};
/**
 * Retrieves the data in the given position in the list.
 * @param index 
 *          the zero-based index of the item
 * @return the object stored by the node at the given position
 */
oes.LinkedList.prototype.get = function (index){
  var current = this.head, i = 0;
  //check for out-of-bounds values
  if (index > -1 && index < this.length){
    while (i++ < index){
      current = current.next;            
    }
    return current.data;
  } else {
    return null;
  }
};