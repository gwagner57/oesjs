/*******************************************************************************
 * Binary Heap function based on the Appendix 2 Binary Heaps of M. Haverbeke
 * "Eloquent JavaScript", 3rd Edition
 *
 * @copyright Copyright 2018-2019 Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 * @author Luis Gustavo Nardin
 ******************************************************************************/
function BinaryHeap( scoreFunction ) {
  this.content = [];
  this.scoreFunction = scoreFunction;
}
BinaryHeap.prototype.push = function ( element ) {
  this.content.push( element );
  this.bubbleUp( this.content.length - 1 );
};
BinaryHeap.prototype.pop = function () {
  var result = this.content[ 0 ];
  var end = this.content.pop();

  if ( this.content.length > 0 ) {
    this.content[ 0 ] = end;
    this.sinkDown( 0 );
  }
  return result;
};
BinaryHeap.prototype.remove = function ( element ) {
  var len = this.content.length;
  var end, i;
  for ( i = 0; i < len; i += 1 ) {
    if ( this.content[ i ] !== element ) {
      continue;
    }

    end = this.content.pop();
    if ( i === len - 1 ) {
      break;
    }

    this.content[ i ] = end;
    this.bubbleUp( i );
    this.sinkDown( i );
    break;
  }
};
BinaryHeap.prototype.getContent = function () {
  return this.content;
};
BinaryHeap.prototype.getFirst = function () {
  if ( this.content.length > 0 ) {
    return this.content[ 0 ];
  }
  return [];
};
BinaryHeap.prototype.clear = function () {
  this.content = [];
};
BinaryHeap.prototype.isEmpty = function () {
  return this.content.length <= 0;
};
BinaryHeap.prototype.size = function () {
  return this.content.length;
};
BinaryHeap.prototype.bubbleUp = function ( n ) {
  var element = this.content[ n ];
  var score = this.scoreFunction( element );
  var parentN, parentEl;

  while ( n > 0 ) {
    parentN = Math.floor( ( n + 1 ) / 2 ) - 1;
    parentEl = this.content[ parentN ];
    if ( score >= this.scoreFunction( parentEl ) ) {
      break;
    }

    this.content[ parentN ] = element;
    this.content[ n ] = parentEl;
    n = parentN;
  }
};
BinaryHeap.prototype.sinkDown = function ( n ) {
  var len = this.content.length;
  var element = this.content[ n ];
  var elemScore = this.scoreFunction( element );
  var swap, child1, child2, child1N, child2N, child1Score, child2Score;

  while ( true ) {
    child2N = ( n + 1 ) * 2;
    child1N = child2N - 1;
    swap = null;
    if ( child1N < len ) {
      child1 = this.content[ child1N ];
      child1Score = this.scoreFunction( child1 );
      if ( child1Score < elemScore ) {
        swap = child1N;
      }
    }
    if ( child2N < len ) {
      child2 = this.content[ child2N ];
      child2Score = this.scoreFunction( child2 );
      if ( child2Score < ( swap === null ? elemScore : child1Score ) ) {
        swap = child2N;
      }
    }
    if ( swap === null ) {
      break;
    }

    this.content[ n ] = this.content[ swap ];
    this.content[ swap ] = element;
    n = swap;
  }
};
